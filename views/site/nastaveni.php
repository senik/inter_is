<?php
use app\models\NastaveniModel;
use app\components\Nastaveni;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */

/**
 * @var $model NastaveniModel
 * @var $form ActiveForm
 */

$this->title = Yii::$app->name . ' - Nastavení';

$this->params['breadcrumbs'] = array(
    'Nastavení'
);
?>
<div class="h2-buttons">
    <h2>Nastavení aplikace</h2>
    <div class="clearfix"></div>
</div>

<p>
    Tohle bude přístupné pouze pro superadmina, zákazník do toho nebude mít vůbec přístup...
</p>
<p>
    Jde o to, aby, když bude potřeba povolit nový modul, se nemuselo lízt do konfigu, ale prostě to udělám přes webový rozhraní...
</p>

<?php
$inputs = Yii::$app->params['polozky-nastaveni'];
$form = ActiveForm::begin(array(
    'id' => 'nastaveni-form',
    'layout' => 'horizontal',
    'options' => array(
        'class' => 'horizontal'
    )
));

// nejdriv vsechny inputy
echo Html::beginTag('div', array('class' => 'form-fields'));
foreach ($inputs as $input) {
    switch ($input) {
        case 'admini':
            echo $form->field($model, $input)->textarea()->hint('Vložte jedno nebo více uživatelských jmen, na každém řádku jedno.');
            break;
        case 'instalovaneModuly':
            $moduly = array_keys(Yii::$app->getModules());
            $modulyDropdown = array();
            foreach ($moduly as $modul) {
                $modulyDropdown[$modul] = $modul;
            }
            echo $form->field($model, $input)->dropDownList($modulyDropdown, ['multiple' => true, 'size' => count($modulyDropdown)])->hint('Vyberte, které moduly budou v aplikaci aktivni.');
            break;
        case Nastaveni::KLIC_KONFIGUROVANO:
            echo $form->field($model, $input, array('inputOptions' => array('readonly' => 'readonly')));
            break;
        default:
            echo $form->field($model, $input);
    }
}
echo Html::endTag('div');
// potom tlacitka
echo Html::beginTag('div', array('class' => 'form-actions well'));
{
    echo Html::submitButton('Uložit', array(
        'class' => 'btn btn-success'
    ));
    echo Html::resetButton('Původní hodnoty', array(
        'class' => 'btn btn-primary'
    ));
    echo Html::a('Zrušit', array('/dashboard'),array(
        'class' => 'btn btn-danger'
    ));
}
echo Html::endTag('div');

ActiveForm::end();
