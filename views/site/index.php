<?php
/**
 * @var $this yii\web\View
 * @var $mObjednavka Objednavka
 * @var $mFaktura Faktura
 * @var $mUdalost Udalost
 */
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

use app\components\Html;
use app\modules\faktury\models\Faktura;
use app\modules\kalendar\models\Udalost;
use app\modules\objednavky\models\Objednavka;
use kartik\grid\GridView;

$this->title = Yii::$app->name . ' - Dashboard';

$this->params['breadcrumbs'] = array(
    'Dashboard'
);
?>

<div class="row dashboard">
    <div class="col-sm-6">
        <div class="h3-buttons">
            <h3>Nejbližší události</h3>
            <div class="clearfix"></div>
        </div>
        <?php
        echo GridView::widget([
            'dataProvider' => $mUdalost->vratPrvniNadchazejici(),
            'columns' => [
                'nazev' => [
                    'label' => 'Typ : Název',
                    'format' => 'raw',
                    'value' => function($data) {
                            $typ = substr(Udalost::itemAlias('typy', $data['typ']), 0, 1);
                            $nazev = $data['nazev'];
                            return "<b>{$typ}</b> : {$nazev}";
                        },
                ],
                'datum_od' => [
                    'attribute' => 'datum_od',
                    'label' => 'Začátek',
                    'format' => 'dateTime'
                ],
                'akce' => [
                    'class' => 'app\components\columns\ActionColumn',
                    'template' => '{view}',
                    'urlCreator' => function ($action, $model, $key, $index) {
                            switch ($action) {
                                case 'view':
                                    $url = array('/kalendar/udalost/detail', 'id' => $model['meta']);
                                    break;
                                default:
                                    throw new Exception('Undefined action for model');
                            }
                            return $url;
                        },
                ]
            ],
            'resizableColumns' => false,
            'layout' => '{items}',
            'id' => 'dashboard-nejblizsi-udalosti',
            'export' => false
        ]);
        ?>
    </div>

    <div class="col-sm-6">
        <div class="h3-buttons">
            <h3>Proběhlé události</h3>
            <div class="clearfix"></div>
        </div>
        <?php
        echo GridView::widget([
            'dataProvider' => $mUdalost->vratPosledniProbehle(),
            'columns' => [
                'nazev' => [
                    'label' => 'Typ : Název',
                    'format' => 'raw',
                    'value' => function($data) {
                            $typ = substr(Udalost::itemAlias('typy', $data['typ']), 0, 1);
                            $nazev = $data['nazev'];
                            return "<b>{$typ}</b> : {$nazev}";
                        }
                ],
                'datum_od' => [
                    'attribute' => 'datum_od',
                    'label' => 'Začátek',
                    'format' => 'dateTime'
                ],
                'akce' => [
                    'class' => 'app\components\columns\ActionColumn',
                    'template' => '{view}',
                    'urlCreator' => function ($action, $model, $key, $index) {
                            switch ($action) {
                                case 'view':
                                    $url = array('/kalendar/udalost/detail', 'id' => $model['meta']);
                                    break;
                                default:
                                    throw new Exception('Undefined action for model');
                            }
                            return $url;
                        },
                ]
            ],
            'resizableColumns' => false,
            'layout' => '{items}',
            'id' => 'dashboard-probehle-udalosti',
            'export' => false
        ]);
        ?>
    </div>
</div>

<div class="row dashboard">
    <?php

    ?>
    <div class="col-sm-12">
        <?php
        $mObjednavka->stav = Objednavka::STAV_ZALOZENO;
        $dataProvider = $mObjednavka->search(false);
        $dataProvider->setPagination(['pageSize' => 10]);
        $dataProvider->setSort(['defaultOrder' => ['datum_zalozeni' => SORT_DESC], 'attributes' => ['datum_zalozeni']]);
        $dataProvider->refresh();
        ?>
        <div class="h3-buttons">
            <h3>Nové objednávky: <span class="label <?= Html::urovenDlePoctu($dataProvider->count, 'label') ?>"><?= $dataProvider->count ?></span></h3>
            <?php
            echo Html::a(
                'Přidat',
                array('/objednavky/default/pridat'),
                array(
                    'class' => 'btn btn-sm btn-success'
                )
            );
            ?>
            <div class="clearfix"></div>
        </div>
        <?php
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => $mObjednavka->vratSloupce(),
            'resizableColumns' => false,
            'layout' => '{items}',
            'export' => false
        ]);
        ?>
    </div>
    <div class="col-sm-12">
        <?php
        $dataProvider = $mObjednavka->vratPoTerminu();
        $count = $dataProvider->getCount();

        $dataProvider->setPagination(['pageSize' => 10]);
        $dataProvider->setSort(['defaultOrder' => ['datum_zalozeni' => SORT_ASC], 'attributes' => ['datum_zalozeni']]);
        $dataProvider->refresh();

        $wrapper = "dashboard-objednavky-termin-wrapper";
        ?>
        <div class="h3-buttons">
            <h3>Objednávky po termínu: <span class="xtooltip label <?= Html::urovenDlePoctu($count, 'label') ?>" title="Zobrazuji nejstarších 10"><?= $count ?></span></h3>
            <?= Html::collapseButton($wrapper) ?>
            <div class="clearfix"></div>
        </div>
        <div class="collapse" id="<?= $wrapper ?>">
        <?php
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => $mObjednavka->vratSloupce(),
            'layout' => '{items}',
        ]);
        ?>
        </div>
    </div>
    <div class="col-sm-12">
        <?php
        $dataProvider = $mFaktura->vratPoSplatnosti(false);
        $count = $dataProvider->getCount();

        $dataProvider->setPagination(['pageSize' => 10]);
        $dataProvider->setSort(['defaultOrder' => ['datum_splatnosti' => SORT_ASC], 'attributes' => ['datum_splatnosti']]);
        $dataProvider->refresh();

        $wrapper = "dashboard-faktury-splatnost-wrapper";
        ?>
        <div class="h3-buttons">
            <h3>Faktury po splatnosti: <span class="xtooltip label <?= Html::urovenDlePoctu($count, 'label') ?>" title="Zobrazuji nejstarších 10"><?= $count ?></span></h3>
            <?= Html::collapseButton($wrapper) ?>
            <div class="clearfix"></div>
        </div>
        <div class="collapse" id="<?= $wrapper ?>">
        <?php
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => $mFaktura->vratSloupce(),
            'layout' => '{items}',
        ]);
        ?>
        </div>
    </div>
</div>