<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
?>
<div class="site-error">

    <h2>
        Omlouváme se, na stránce se vyskytly potíže.
    </h2>

    <p>
        Administrátoři již byli informování a na nápravě se pracuje.
    </p>

</div>
