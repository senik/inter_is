<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 27.12.14
 * Time: 17:40
 *
 * @var $mModel Model3D
 */

use app\modules\modely\models\Model3D;
use kartik\grid\GridView;

echo GridView::widget(array(
    'dataProvider' => $mModel->search(),
    'columns' => $mModel->vratSloupce(),
    'resizableColumns' => false,
    'export' => false,
));