<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 18. 2. 2015
 * Time: 19:20
 */

namespace app\modules\sklad\controllers;


use app\components\Controller;
use app\modules\sklad\models\Material;
use app\modules\sklad\models\MaterialCenaForm;
use Yii;

/**
 * Class DefaultController
 * @package app\modules\sklad\controllers
 */
class DefaultController extends Controller
{
    /**
     * return string
     */
    public function actionIndex()
    {
        $mMaterial = new Material();
        $mMaterialCena = new MaterialCenaForm();

        $get = Yii::$app->request->get();
        if (!empty($get)) {
            $mMaterial->load($get);
        }

        return $this->render('index', [
            'mMaterial' => $mMaterial,
            'mMaterialCena' => $mMaterialCena,
        ]);
    }

}