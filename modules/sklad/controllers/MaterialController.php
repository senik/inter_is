<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 18. 2. 2015
 * Time: 23:38
 */

namespace app\modules\sklad\controllers;

use app\components\Application;
use app\components\Controller;
use app\modules\sklad\models\Material;
use app\modules\sklad\models\MaterialCenaForm;
use kartik\widgets\ActiveForm;
use yii\web\Response;

/**
 * Class MaterialController
 * @package app\modules\sklad\controllers
 */
class MaterialController extends Controller
{
    /**
     * @param $id
     * @return string
     */
    public function actionDetail($id)
    {
        /** @var Material $mMaterial */
        $mMaterial = (new Material())->nactiPodlePk($id);

        return $this->render('detail', [
            'mMaterial' => $mMaterial
        ]);
    }

    /**
     * akce pro doplnění materiálu na sklad
     */
    public function actionPridat()
    {
        $mMaterial = new Material(['scenario' => Material::SCENARIO_PRIDAT]);

        $post = \Yii::$app->request->post();

        if (!empty($post)) {
            if ($mMaterial->load($post) && $mMaterial->validate()) {

                if ($mMaterial->doplnit()) {
                    Application::setFlashSuccess("Materiál doplněn na sklad"); // todo něco jako "500g materiálu ABS - bílá doplněno na sklad"
                    return $this->redirect(['/sklad/default/index']);
                } else {
                    Application::setFlashError("Nepodařilo se doplnit materiál na sklad");
                }
            }
        }

        return $this->render('pridat', [
            'mMaterial' => $mMaterial
        ]);
    }

    /**
     * Změna ceny materiálu
     * @return array
     */
    public function actionZmenitCenu()
    {
        $request = \Yii::$app->request;
        $mMaterialCena = new MaterialCenaForm();
        $post = $request->post();

        if ($request->isAjax && $mMaterialCena->load($post)) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($mMaterialCena);
        }

        try {
            if ($mMaterialCena->load($post) && $mMaterialCena->validate()) {
                $mMaterialCena->uloz();
                Application::setFlashSuccess('Cena byla změněna');
            } else {
                throw new \Exception('[material] chyba pri validaci: ' . print_r($mMaterialCena->getErrors()) . ', data: ' . print_r($post, true));
            }
        } catch (\Exception $e) {
            \Yii::error($e->getMessage());
            Application::setFlashError("Vyskytla se chyba, opakujte prosím akci později.");
        }

        return $this->redirect(['/sklad/default/index']);
    }
}