<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 22. 2. 2015
 * Time: 8:31
 *
 * @var $mMaterial Material
 */

use app\modules\sklad\models\Material;
use miloschuman\highcharts\Highcharts;

$nazev_dlouhy = $mMaterial->nazev . ', ' . $mMaterial->barva;

$this->title = Yii::$app->name . ' - Sklad: ' . $nazev_dlouhy;

$this->params['breadcrumbs'] = array(
    ['url' => ['/sklad/default/index'], 'label' => 'Skladové zásoby'],
    $nazev_dlouhy
);
?>

<div class="h2-buttons">
    <h2><?= $nazev_dlouhy ?></h2>
    <div class="clearfix"></div>
</div>

<?php
// tohle je děsná oblož, ale formatter mi to skloňuje a to nechci...
$mesice = ['Leden', 'Únor', 'Březen', 'Duben', 'Květen', 'Červen', 'Červenec', 'Srpen', 'Září', 'Říjen', 'Listopad', 'Prosinec'];
$cena = $objednavek = $xAxis = array();
foreach ($mMaterial->nactiHistoriiCen() as $row) {
    $xAxis[] = sprintf('%s %s', $mesice[date('m', $row['datum_timestamp']) - 1], date('Y', $row['datum_timestamp']));
    $cena[] = (float) $row['cena'];
    $objednavek[] = (int) $row['objednavek'];
}

echo Highcharts::widget([
    'options' => [
        'title' => ['text' => 'Vývoj ceny a objednávek za poslední rok'],
        'xAxis' => [
            'type' => 'datetime',
            'categories' => $xAxis
        ],
        'yAxis' => [
            [
                'title' => ['text' => 'Průměrná cena v měsíci'],
                'min' => 0
            ],
            [
                'title' => ['text' => 'Objednávek'],
                'min' => 0,
                'tickInterval' => 1,
                'opposite' => true
            ],
        ],
        'series' => [
            [
                'name' => 'Počet objednávek',
                'type' => 'column',
                'data' => $objednavek,
                'yAxis' => 1
            ],
            [
                'name' => 'Cena materiálu',
                'type' => 'line',
                'data' => $cena,
                'yAxis' => 0,
                'tooltip' => [
                    'valueSuffix' => ' Kč'
                ]
            ],
        ],
        'chart' => [
            'events' => [
                'load' => new \yii\web\JsExpression('function() { footerMax(); }')
            ]
        ]
    ]
]);