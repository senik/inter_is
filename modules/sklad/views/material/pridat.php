<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 19. 2. 2015
 * Time: 20:54
 *
 * @var $mMaterial Material
 */

use app\components\Html;
use app\modules\sklad\models\Material;
use kartik\widgets\ActiveForm;
use kartik\widgets\DateTimePicker;

$this->title = Yii::$app->name . ' - Doplnit materiál';

$this->params['breadcrumbs'] = array(
    ['url' => ['/sklad/default/index'], 'label' => 'Skladové zásoby'],
    'Doplnění'
);
?>

<div class="h2-buttons">
    <h2>Doplnění materiálu na sklad</h2>
    <div class="clearfix"></div>
</div>

<?php
$form = ActiveForm::begin([
    'id' => 'material-pridat-form',
    'action' => array('/sklad/material/pridat'),
    'method' => 'posta',
    'type' => 'horizontal',
    'options' => array(
//        'class' => 'well'
    ),
    'formConfig' => ['labelSpan' => 3, 'spanSize' => ActiveForm::SIZE_SMALL],
    'enableClientValidation' => false
]);

$materialDisabled = $mMaterial->druh_id != null;
$barvaDisabled = $mMaterial->barva_id != null;
?>

<div class="form-fields row">
    <div class="col-sm-6">
        <?= $form->field($mMaterial, 'druh_id')->dropDownList($mMaterial->vratDruhyProDropdown('Nový druh - vyplňte název'), ['class' => 'selectpicker']) ?>
        <?= $form->field($mMaterial, 'nazev')->textInput(['disabled' => $materialDisabled])->label('') ?>
        <?= $form->field($mMaterial, 'barva_id')->dropDownList($mMaterial->vratBarvyProDropdown('Nová barva - vyplňte název'), ['class' => 'selectpicker']) ?>
        <?= $form->field($mMaterial, 'barva')->textInput(['disabled' => $barvaDisabled])->label('') ?>
        <?= $form->field($mMaterial, 'hustota', [
            'addon' => ['append' => ['content' => 'g/cm3']]
        ])->textInput(['disabled' => $materialDisabled && $barvaDisabled])->label('Hustota') ?>
    </div>

    <div class="col-sm-6">
        <?= $form->field($mMaterial, 'cena', [
            'addon' => ['append' => ['content' => 'Kč/g']]
        ])->textInput()->label('Cena') ?>
        <?= $form->field($mMaterial, 'platne_od')->widget(DateTimePicker::className(), [
            'convertFormat' => true,
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'php:j.n.Y H:i'
            ]
        ]) ?>
        <?= $form->field($mMaterial, 'skladem_celkem', [
            'addon' => ['append' => ['content' => 'g']]
        ])->textInput()->label('Množství') ?>
    </div>
</div>

<div class="form-actions well">
    <?php
    echo Html::submitButton('Uložit', array(
        'class' => 'btn btn-success'
    ));

    echo Html::a('Zrušit', array('/sklad/default/index'), array(
        'class' => 'btn btn-danger'
    ));
    ?>
</div>

<?php ActiveForm::end(); ?>