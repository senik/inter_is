<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 18. 2. 2015
 * Time: 19:31
 *
 * @var $mMaterial Material
 * @var $mMaterialCena MaterialCenaForm
 * @var $this View
 */

use app\components\Html;
use app\modules\sklad\models\Material;
use app\modules\sklad\models\MaterialCenaForm;
use kartik\widgets\ActiveForm;
use kartik\widgets\DateTimePicker;
use yii\web\View;
use yii\bootstrap\Modal;

$this->title = Yii::$app->name . ' - Skladové zásoby';

$this->params['breadcrumbs'] = array(
    'Skladové zásoby'
);
?>

<div class="h2-buttons">
    <h2>Skladové zásoby</h2>
    <?php
    echo Html::a(
        'Doplnit',
        array('/sklad/material/pridat'),
        array(
            'class' => 'btn btn-success'
        )
    );
    ?>
    <div class="clearfix"></div>
</div>

<?php
$form = ActiveForm::begin([
    'id' => 'sklad-search-form',
    'action' => array('/sklad/default/index'),
    'method' => 'get',
    'type' => 'horizontal',
    'options' => array(
        'class' => 'well'
    ),
    'formConfig' => ['labelSpan' => 3, 'spanSize' => ActiveForm::SIZE_SMALL]
]);
?>
<div class="form-fields">
    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($mMaterial, 'druh_id')->dropDownList($mMaterial->vratDruhyProDropdown('Všechny'), ['class' => 'selectpicker']) ?>
        </div>

        <div class="col-sm-4">
            <?= $form->field($mMaterial, 'barva_id')->dropDownList($mMaterial->vratBarvyProDropdown('Všechny'), ['class' => 'selectpicker']) ?>
        </div>

        <div class="col-sm-4">

        </div>
    </div>
</div>

<div class="form-actions">
    <?php
    echo Html::submitButton('Filtrovat', array('class' => 'btn btn-success'));
    ?>
</div>

<?php
ActiveForm::end();

echo \kartik\grid\GridView::widget(array(
    'dataProvider' => $mMaterial->search(),
    'columns' => $mMaterial->vratSloupce(),
    'resizableColumns' => false,
    'export' => false
));

Modal::begin([
    'header' => '<h3>Změna ceny materiálu</h3>',
    'id' => 'material-cena-modal'
]);
$mMaterial->scenario = Material::SCENARIO_CENA;
$form = ActiveForm::begin(array(
    'id' => 'material-cena-form',
    'type' => 'horizontal',
    'action' => array('/sklad/material/zmenit-cenu'),
    'method' => 'POST',
    'enableAjaxValidation' => true,
    'formConfig' => array(
        'labelSpan' => 3,
        'deviceSize' => ActiveForm::SIZE_SMALL
    )
));
?>
    <div class="form-fields">
        <?= Html::activeHiddenInput($mMaterialCena, 'material_pk', ['value' => '']) ?>

        <?= $form->field($mMaterialCena, 'cena', [
            'addon' => ['append' => ['content' => 'Kč/g']]
        ])->textInput()->label('Cena') ?>

        <?= $form->field($mMaterialCena, 'platne_od')->widget(DateTimePicker::className(), [
            'convertFormat' => true,
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'php:j.n.Y H:i',
                'calendarWeeks' => true
            ]
        ])->hint('Pokud zůstane prázdné, bude cena platit od chvíle uložení.')->label('Platí od') ?>

        <?= $form->field($mMaterialCena, 'platne_do')->widget(DateTimePicker::className(), [
            'convertFormat' => true,
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'php:j.n.Y H:i',
                'calendarWeeks' => true
            ]
        ])->hint('Pokud zůstane prázdné, cena bude mít neomezenou platnost.')->label('Platí do') ?>
    </div>

    <div class="form-actions">
        <?php
        echo Html::submitButton('Uložit', array(
            'class' => 'btn btn-success'
        ));
        echo Html::a('Zrušit', '#', array(
            'class' => 'btn btn-danger modal-close'
        ));
        ?>
    </div>
<?php
ActiveForm::end();

Modal::end();

$this->registerJs("
$(document).on('click', 'a[data-target=\"#material-cena-modal\"]', function() {
    var material = $(this).attr('data-material');
    $('input[id=\"materialcenaform-material_pk\"]').val(material);
});

$('#material-cena-modal').on('hide.bs.modal', function(ev) {
    // dateTimePicker odpali tenhle event taky, takze musim porovnat target...
    if ($(ev.target).attr('id') == 'material-cena-modal') {
        $('input[name^=\"Material\"]', '#material-cena-form').each(function() {
            $(this).val('');
        });
    }
});
");