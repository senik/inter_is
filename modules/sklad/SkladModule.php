<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 17.10.14
 * Time: 21:16
 */

namespace app\modules\sklad;


use app\components\ModuleInterface;
use yii\base\Module;

class SkladModule extends Module implements ModuleInterface
{
    /**
     * @var string
     */
    public $controllerNamespace = 'app\modules\sklad\controllers';

    /**
     * @var
     */
    public $name;

    /**
     * @var array
     */
    public $baseUrl = array('/sklad/default/index');

    public function vratMenuModulu()
    {
        return array();
    }
} 