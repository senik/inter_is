<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 15. 3. 2015
 * Time: 10:35
 */

namespace app\modules\sklad\models;


use app\components\Model;

/**
 * Class MaterialCenaForm
 *
 * Pomocná třída pro změnu ceny materiálu
 *
 * @package app\modules\sklad\models
 */
class MaterialCenaForm extends Model
{
    /**
     * @var
     */
    public $material_pk;

    /**
     * @var
     */
    public $cena;

    /**
     * @var
     */
    public $platne_od;

    /**
     * @var
     */
    public $platne_do;

    /**
     * @var
     */
    private $_mMaterial;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['material_pk', 'cena'], 'required'],
            ['cena', 'match', 'pattern' => '/^[0-9]+([,][0-9]{2})?$/', 'message' => 'Neplatná hodnota - cena musí mít formát 5[,50]'],
            [['platne_od', 'platne_do'], 'safe'],
            [['platne_od', 'platne_do'], 'match', 'pattern' => '/^([0-9]{1,2}[.]){2}[0-9]{4}[ ][0-9]{1,2}[:][0-9]{1,2}$/'],
            [['platne_od', 'platne_do'], 'platnostValidator']
        ];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function uloz() {
        $mMaterial = new Material(['scenario' => Material::SCENARIO_CENA]);

        $data['Material'] = [
            'material_pk' => $this->material_pk,
            'cena' => $this->cena,
            'cena_od' => $this->platne_od,
            'cena_do' => $this->platne_do
        ];

        if ($mMaterial->load($data) && $mMaterial->validate()) {
            return $mMaterial->ulozCenu();
        } else {
            throw new \Exception("[material] nepodarilo se zmenit cenu materialu, data: " . print_r($data, true));
        }
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function platnostValidator($attribute, $params)
    {
        if ($attribute == 'platne_od' && $this->platne_do != null) {
            if (strtotime($this->$attribute) >= strtotime($this->platne_do)) {
                $this->addError($attribute, 'Platnost od musí být menší než platnost do');
            }
        }

        if ($attribute == 'platne_do' && $this->platne_od != null) {
            if (strtotime($this->$attribute) <= strtotime($this->platne_od)) {
                $this->addError($attribute, 'Platnost od musí být větší než platnost od');
            }
        }

        if (strtotime($this->$attribute) < time()) {
            $this->addError($attribute, 'Není možné měnit cenu zpětně');
        }
    }
}