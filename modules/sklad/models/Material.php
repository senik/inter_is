<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 18. 2. 2015
 * Time: 21:20
 */

namespace app\modules\sklad\models;


use app\components\Html;
use app\components\ItemAliasTrait;
use app\components\SqlDataProvider;
use Exception;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Class Material
 * @package app\modules\sklad\models
 */
class Material extends Model
{
    use ItemAliasTrait;

    const SCENARIO_DRUH = 'sc_druh';
    const SCENARIO_CENA = 'sc_cena';
    const SCENARIO_BARVA = 'sc_barva';
    const SCENARIO_PRIDAT = 'pridat';

    /**
     * @var
     */
    public $material_pk;

    /**
     * @var
     */
    public $nazev;

    /**
     * @var
     */
    public $hustota;

    /**
     * @var
     */
    public $barva;

    /**
     * @var
     */
    public $skladem;

    /**
     * @var
     */
    public $skladem_celkem;

    /**
     * @var
     */
    public $skladem_blokace;

    /**
     * @var
     */
    public $cena;

    /**
     * @var
     */
    public $platne_od;

    public $cena_od, $cena_do, $druh_id, $barva_id;

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return [
            self::SCENARIO_DEFAULT  => array_keys($this->attributes),
            self::SCENARIO_PRIDAT   => ['druh_id', 'barva_id', 'nazev', 'barva', 'cena', 'platne_od', 'hustota', 'skladem_celkem'],
            self::SCENARIO_CENA     => ['cena', 'cena_od', 'cena_do', 'material_pk']
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = array();
        if ($this->scenario == self::SCENARIO_DEFAULT) {
            $rules = [
                [['nazev', 'barva', 'skladem', 'cena_od', 'cena_do', 'druh_id', 'barva_id', 'material_pk'], 'safe']
            ];
        } else if ($this->scenario == self::SCENARIO_CENA) {
            $rules = [
                [['material_pk', 'cena'], 'required'],
                [['cena_od', 'cena_do'], 'safe'],
            ];
        } else if ($this->scenario == self::SCENARIO_PRIDAT) {
            $rules = [
                [['druh_id'], 'required', 'when' => function($model) {
                    /** @var Material $model */
                    return $model->druh_id != '';
                }, 'message' => 'Vyberte materiál'],
                [['barva_id'], 'required', 'when' => function($model) {
                    /** @var Material $model */
                    return $model->barva_id != '';
                }, 'message' => 'Vyberte barvu'],
                [['nazev'], 'required', 'when' => function($model) {
                    /** @var Material $model */
                    return $model->druh_id == '';
                }, 'message' => 'Vyplňte název nového materiálu'],
                [['barva'], 'required', 'when' => function($model) {
                    /** @var Material $model */
                    return $model->barva_id == '';
                }, 'message' => 'Vyplňte název nové barvy'],
                [['hustota'], 'required', 'when' => function($model) {
                    /** @var Material $model */
                    return $model->barva_id == '' || $model->druh_id == '';
                }],
                [['skladem_celkem', 'cena'], 'required'],
                [['nazev', 'barva', 'skladem', 'cena_od', 'cena_do'], 'safe']
            ];
        }

        return $rules;
    }

    /**
     * @return SqlDataProvider
     */
    public function search()
    {
        return $this->vratDataProvider();
    }

    /**
     * todo
     * @param $id
     * @return Material
     */
    public function nactiPodlePk($id)
    {
        return $this->_nacti($id);
    }

    /**
     * @param $id
     * @return $this
     */
    protected function _nacti($id)
    {
        $db = Yii::$app->db;
        $data['Material'] = $db->createCommand("
            select
                m.material_pk
              , m.skladem_celkem
              , m.skladem_blokace
              , m.material_barva_id as barva_id
              , m.material_druh_id as druh_id
              , md.nazev as nazev
              , mb.barva as barva
            from material m
              join material_barva mb on m.material_barva_id = mb.material_barva_id
              join material_druh md on m.material_druh_id = md.material_druh_id
            where material_pk = :pk
        ")->bindValue('pk', $id)->queryOne();

        if ($this->load($data)) {
            return $this;
        } else {
            return null;
        }
    }

    /**
     * todo pocitat prumernou cenu za ten mesic
     * @return array
     */
    public function nactiHistoriiCen() {
        $sql = "
            select
                extract(epoch from s.datum) as datum_timestamp
              , count(o.*) as objednavek
              , vrat_prumernou_cenu_materialu(:pk, s.datum) as cena
            from (SELECT to_char(generate_series::date, '01.MM.YYYY')::date as datum FROM generate_series((now()::date - interval '12 months')::date, now()::date, '1 month')) as s
              left join objednavka o on o.datum_zalozeni between s.datum and (s.datum + interval '1 month' - interval '1 second')
                and o.objednavka_pk in (select objednavka_pk from objednavka_polozka where objednavka_polozka.material_pk = :pk)
            group by s.datum
            order by s.datum asc
        ";

        $data = Yii::$app->db->createCommand($sql)->bindValue('pk', $this->material_pk)->queryAll();
        return $data;
    }

    /**
     * Doplní materiál na sklad<br>
     * Pokud neexistuje druh nebo barva materiálu, nejprve je založí a poté přidá materiál na sklad
     * todo hlavne doresit, ze kdyz zadam novy material (jako ze neexistuje)
     *
     * @throws \Exception
     * @return bool
     */
    public function doplnit()
    {
        $db = Yii::$app->db;
        $transakce = $db->beginTransaction();

        try {
            if ($this->druh_id == null) {
                $this->druh_id = $db
                    ->createCommand("select material_druh_zmena(null, :nazev, :hustota)")
                    ->bindValues([
                        'nazev' => $this->nazev,
                        'hustota' => str_replace(',', '.', $this->hustota)
                    ])
                    ->queryScalar();
            }

            if ($this->barva_id == null) {
                $this->barva_id = $db
                    ->createCommand("select material_barva_zmena(null, :barva)")
                    ->bindValue('barva', $this->barva)
                    ->queryScalar();
            }

            if ($this->barva_id == null || $this->druh_id == null) {
                throw new Exception("neznam barvu ({$this->barva_id}) nebo druh ({$this->druh_id})");
            }

            $pk = $db->createCommand("select material_dopln_na_sklad(:druh, :barva, :mnozstvi)")
                ->bindValues([
                    'druh' => $this->druh_id,
                    'barva' => $this->barva_id,
                    'mnozstvi' => $this->skladem_celkem
                ])
                ->queryScalar();

            if ($pk > 0) {
                if ($this->cena != null) {
                    $cena = $db->createCommand("select material_zmen_cenu(:pk, :cena, :pod, :pdo)")
                        ->bindValues([
                            ':pk' => $pk,
                            ':cena' => str_replace(',', '.', $this->cena),
                            ':pod' => ($this->platne_od != null) ? date('Y-m-d H:i:s', strtotime($this->platne_od)) : null,
                            ':pdo' => null
                        ])
                        ->queryScalar();

                    if ($cena < 0) {
                        throw new \Exception("zmena ceny vraci chybu: {$pk}");
                    }
                }

                $transakce->commit();
                return true;
            } else {
                throw new \Exception("doplneni na sklad vraci chybu: {$pk}");
            }
        } catch (\Exception $e) {
            $transakce->rollBack();
            Yii::error("chyba pri doplneni materialu na sklad: {$e->getMessage()}");

            return false;
        }
    }

    /**
     *
     */
    public function ulozCenu() {
        $db = Yii::$app->db;

        $command = $db->createCommand('select material_zmen_cenu(:material, :cena, :od, :do)');
        $command->bindValues([
            'material' => $this->material_pk,
            'cena' => str_replace(',', '.', $this->cena),
            'od' => ($this->cena_od != null) ? date('Y-m-d H:i:s', strtotime($this->cena_od)) : null,
            'do' => ($this->cena_do != null) ? date('Y-m-d H:i:s', strtotime($this->cena_do)) : null
        ]);

        $result = $command->queryScalar();

        if ($result > 0) {
            return true;
        } else {
            throw new Exception("[material] zaporny navrat fce material_zmen_cenu() = {$result}, data: " . print_r($this->attributes, true));
        }
    }

    /**
     * @param int $strankovani
     * @return SqlDataProvider
     */
    protected function vratDataProvider($strankovani = 10)
    {
        $sql = 'SELECT * FROM material_view';
        $aParams = $aWhere = array();

        if ($this->druh_id != null) {
            $aWhere[] = "druh_id = :druh";
            $aParams['druh'] = $this->druh_id;
        }

        if ($this->barva_id != null) {
            $aWhere[] = "barva_id = :barva";
            $aParams['barva'] = $this->barva_id;
        }

        if (!empty($aWhere)) {
            $sql .= " where " . implode(' and ', $aWhere);
        }

        return new SqlDataProvider([
            'sql' => $sql,
            'params' => $aParams,
            'pagination' => $strankovani == false ? false : [
                'pageSize' => $strankovani
            ],
            'sort' => [
                'defaultOrder' => ['nazev' => SORT_ASC],
                'attributes' => ['nazev', 'barva', 'hustota', 'skladem_celkem', 'skladem_blokace', 'cena']
            ]
        ]);
    }

    /**
     * @param bool $prompt
     * @return array
     */
    public function vratDruhyProDropdown($prompt = false)
    {
        $data = Yii::$app->db->createCommand('SELECT * FROM material_druh order by nazev asc')->queryAll();
        $materialy = ArrayHelper::map($data, 'material_druh_id', 'nazev');

        if ($prompt) {
            $materialy = ['' => $prompt] + $materialy;
        }

        return $materialy;
    }

    /**
     * @param bool $prompt
     * @return array
     */
    public function vratBarvyProDropdown($prompt = false)
    {
        $data = Yii::$app->db->createCommand('SELECT * FROM material_barva order by barva asc')->queryAll();
        $barvy = ArrayHelper::map($data, 'material_barva_id', 'barva');

        if ($prompt) {
            $barvy = ['' => $prompt] + $barvy;
        }

        return $barvy;
    }

    /**
     * @param bool $prompt
     * @return array
     */
    public function vratMaterialySklademProDropdown($prompt = false)
    {
        $data = Yii::$app->db->createCommand('SELECT * FROM material_skladem_view ORDER BY druh_id ASC, barva ASC')->queryAll();
        $materialy = ArrayHelper::map($data, 'material_pk', 'nazev_dlouhy');

        if ($prompt) {
            $materialy = ['' => $prompt] + $materialy;
        }

        return $materialy;
    }

    /**
     * @return array
     */
    public function vratSloupce()
    {
        return [
            'nazev' => [
                'attribute' => 'nazev',
                'label' => $this->getAttributeLabel('nazev')
            ],
            'barva' => [
                'attribute' => 'barva',
                'label' => $this->getAttributeLabel('barva_id')
            ],
            'hustota' => [
                'attribute' => 'hustota',
                'label' => $this->getAttributeLabel('hustota')
            ],
            'skladem_celkem' => [
                'attribute' => 'skladem_celkem',
                'label' => $this->getAttributeLabel('skladem_celkem')
            ],
            'skladem_blokace' => [
                'attribute' => 'skladem_blokace',
                'label' => $this->getAttributeLabel('skladem_blokace')
            ],
            'cena' => [
                'class' => 'app\components\columns\PriceColumn',
                'attribute' => 'cena',
                'label' => $this->getAttributeLabel('cena')
            ],
            'akce' => [
                'header' => 'Akce',
                'class' => 'app\components\columns\ActionColumn',
                'template' => '{view} {update} {cena} {delete}',
                'urlCreator' => function ($action, $model, $key, $index) {
                    switch ($action) {
                        case 'view':
                            $url = array('/sklad/material/detail', 'id' => $model['material_pk']);
                            break;
                        case 'update':
                            $url = array('/sklad/material/upravit', 'id' => $model['material_pk']);
                            break;
                        case 'delete':
                            $url = array('/sklad/material/smazat', 'id' => $model['material_pk']);
                            break;
                        case 'cena':
                            $url = '';
                            break;
                        default:
                            throw new Exception('Undefined action for model');
                    }
                    return $url;
                },
                'buttons' => [
                    'view' => function($url, $model, $key) {
                        // <a href="/inter_is/web/sklad/material/detail?id=1" title="Náhled" data-pjax="0"><span class="glyphicon glyphicon-eye-open"></span></a>
                        return Html::a(
                            Html::tag('span', '', [
                                'class' => 'glyphicon glyphicon-eye-open'
                            ]),
                            $url,
                            [
                                'data-pjax' => 0,
                                'title' => 'Náhled'
                            ]
                        );
                    },
                    'update' => function($url, $model, $key) {
                        // <a href="/inter_is/web/sklad/material/upravit?id=1" title="Upravit" data-pjax="0"><span class="glyphicon glyphicon-pencil"></span></a>
                        return Html::a(
                            Html::tag('span', '', [
                                'class' => 'glyphicon glyphicon-pencil'
                            ]),
                            $url,
                            [
                                'data-pjax' => 0,
                                'title' => 'Upravit'
                            ]
                        );
                    },
                    'delete' => function($url, $model, $key) {
                        // <a href="/inter_is/web/sklad/material/smazat?id=1" title="Smazat" data-confirm="Opravdu chcete smazat tuto položku?" data-method="post" data-pjax="0"><span class="glyphicon glyphicon-trash"></span></a>
                        return Html::a(
                            Html::tag('span', '', [
                                'class' => 'glyphicon glyphicon-trash'
                            ]),
                            $url,
                            [
                                'data-pjax' => 0,
                                'data-confirm' => "Opravdu chcete smazat tuto položku?",
                                'data-method' => "post",
                                'title' => 'Smazat'
                            ]
                        );
                    },
                    'cena' => function($url, $model, $key) {
                        return Html::a(
                            Html::tag('span', '', [
                                'class' => 'glyphicon glyphicon-usd'
                            ]),
                            $url,
                            [
                                'data-pjax' => 0,
                                'data-toggle' => 'modal',
                                'data-target' => '#material-cena-modal',
                                'data-material' => $model['material_pk'],
                                'title' => 'Změnit cenu'
                            ]
                        );
                    },
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nazev' => 'Materiál',
            'druh_id' => 'Materiál',
            'cena' => 'Cena (Kč/g)',
            'skladem_celkem' => 'Skladem (g)',
            'skladem_blokace' => 'Blokace (g)',
            'hustota' => 'Hustota (g/cm3)',
            'barva_id' => 'Barva',
            'platne_od' => 'Cena platí od',
            'platne_do' => 'Cena platí do',
        ];
    }

    public static function itemAliasData()
    {
        return array();
    }
}