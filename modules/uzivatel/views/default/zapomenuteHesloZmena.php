<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 5.10.14
 * Time: 13:59
 *
 * @var $model \app\modules\uzivatel\models\ZapomenuteHesloZmenaForm
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::$app->name . ' - Změna hesla';
?>
<div id="zapomenute-heslo">
    <div class="h2-buttons">
        <h2>Změna hesla</h2>
        <div class="clearfix"></div>
    </div>

    <?php
    $form = ActiveForm::begin(
        array(
            'id' => 'zapomenute-heslo-zmena-form',
            'layout' => 'horizontal'
        )
    ); ?>
    <div class="form-fields">
        <?= $form->field($model, 'heslo')->passwordInput() ?>
        <?= $form->field($model, 'kontrola')->passwordInput() ?>
    </div>

    <div class="form-actions">
        <?= Html::submitButton('Uložit', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Na přihlášení', Yii::$app->getUrlManager()->createAbsoluteUrl('/uzivatel/default/prihlaseni'), ['class' => 'btn']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>