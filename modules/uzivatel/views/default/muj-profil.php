<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 6.4.2015
 * Time: 13:50
 *
 * @var $mUzivatel Uzivatel
 */

use app\modules\uzivatel\models\Uzivatel;
use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = Yii::$app->name . ' - Můj profil';

$this->params['breadcrumbs'] = array(
    'Můj profil'
);
?>
    <div class="h2-buttons">
        <h2>Můj profil</h2>
        <?php
        echo Html::a(
            'Změnit heslo',
            array('/uzivatel/default/zmena-hesla'),
            array(
                'class' => 'btn btn-info'
            )
        );
        echo Html::a(
            'Upravit',
            array('/uzivatel/admin/upravit', 'id' => $mUzivatel->uzivatel_pk),
            array(
                'class' => 'btn btn-warning'
            )
        );
        ?>
        <div class="clearfix"></div>
    </div>

<?php
echo DetailView::widget(array(
    'model' => $mUzivatel,
    'attributes' => array(
        'uzivatel_pk',
        'email' => array(
            'label' => 'E-mail',
            'value' => Html::a($mUzivatel->email, 'mailto:'.$mUzivatel->email),
            'format' => 'raw'
        ),
        'jmeno',
        'prijmeni',
        'stav' => array(
            'attribute' => 'stav',
            'value' => Uzivatel::itemAlias('stavy', $mUzivatel->stav)
        ),
        'cas_registrace',
        'cas_prihlaseni'
    )
));