<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 6.4.2015
 * Time: 14:05
 *
 * @var $mZmenaHeslaForm ZmenaHeslaForm
 */

use app\components\Html;
use app\modules\uzivatel\models\ZmenaHeslaForm;
use kartik\form\ActiveForm;

$this->title = Yii::$app->name . ' - Změna hesla';

$this->params['breadcrumbs'] = array(
    ['url' => ['/uzivatel/default/muj-profil'], 'label' => 'Můj profil'],
    'Změna hesla'
);
?>
<div class="row">
<div class=" col-sm-6 col-sm-offset-3">

    <?php
$form = ActiveForm::begin([
    'method' => 'POST',
    'id' => 'zmena-hesla-form',
    'type' => 'horizontal',
    'formConfig' => array(
        'labelSpan' => 4,
        'deviceSize' => ActiveForm::SIZE_SMALL
    )
]);
?>

<div class="form-fields row">
    <?= $form->field($mZmenaHeslaForm, 'stare')->passwordInput() ?>
    <?= $form->field($mZmenaHeslaForm, 'nove')->passwordInput() ?>
    <?= $form->field($mZmenaHeslaForm, 'kontrola')->passwordInput() ?>
</div>

<div class="form-actions well row">
    <?= Html::submitButton('Uložit', ['class' => 'btn btn-success']) ?>
    <?= Html::a('Zrušit', Yii::$app->getUrlManager()->createAbsoluteUrl('uzivatel/default/muj-profil'), ['class' => 'btn btn-danger']) ?>
</div>

<?php
ActiveForm::end();
?>

</div></div>
