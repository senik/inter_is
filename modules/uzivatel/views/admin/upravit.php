<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 17.10.14
 * Time: 17:14
 * @var $mUzivatel app\modules\uzivatel\models\Uzivatel
 * @var $this \yii\web\View
 */

$this->title = Yii::$app->name . ' - Upravit uživatele';
?>

<div class="h2-buttons">
    <h2>Upravit uživatele <?= $mUzivatel->prijmeni . ' ' . $mUzivatel->jmeno ?></h2>
    <div class="clearfix"></div>
</div>

<?= $this->render('_form', array('mUzivatel' => $mUzivatel));