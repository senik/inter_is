<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 5.10.14
 * Time: 10:47
 */

namespace app\modules\uzivatel\models;

use app\components\ActiveRecord;
use app\components\helpers\DbUtils;
use app\components\SqlDataProvider;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use Yii;

/**
 * Class Uzivatel
 * @package app\modules\uzivatel\models
 *
 * @property integer uzivatel_pk
 * @property string email
 * @property string heslo
 * @property string stav
 * @property string validacni_klic
 * @property string cas_registrace
 * @property string cas_prihlaseni
 * @property string jmeno
 * @property string prijmeni
 * @property string role
 */
class Uzivatel extends ActiveRecord
{

    const STAV_AKTIVNI = 'AKTIVNI';
    const STAV_NEAKTIVNI = 'NEAKTIVNI';
    const STAV_REGISTRACE = 'REGISTRACE';
    const STAV_SMAZANO = 'SMAZANO';

    const ROLE_USER = 'user';
    const ROLE_ADMIN = 'admin';
    const ROLE_SUPERADMIN = 'superadmin';

    /**
     * @var string
     */
    public $heslo_kontrola;

    /**
     * @var
     */
    public $zapamatovat;

    public function scenarios()
    {
        $aScenarios = parent::scenarios();
        $aScenarios['upravit'] = array(
            'email', 'jmeno', 'prijmeni', 'role'
        );
        $aScenarios['login'] = array(
            'email', 'heslo', 'zapamatovat'
        );
        $aScenarios['search'] = array(
            'email', 'jmeno', 'role', 'stav'
        );

        return $aScenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        switch ($this->scenario) {
            case 'login':
                return array(
                    array(array('email', 'heslo'), 'required'),
                    array(array('zapamatovat'), 'safe')
                );
                break;
            case 'upravit':
                return array(
                    array(array('email', 'jmeno', 'prijmeni'), 'required'),
                    array(array('role'), 'required', 'when' => function($model) {
                        /** @var $model Uzivatel */
                        return ($model->uzivatel_pk != Yii::$app->user->id) && ($model->smiEditovatRoleRoli($model->role, Yii::$app->user->identity->getRole()));
                    }),
                    array('email', 'email'),
                    array('email', 'unikatniUcetValidator'),
                );
                break;
            case 'search':
                return array(
                    array(array('email', 'jmeno', 'stav', 'role'), 'safe'),
                );
                break;
            default:
                return array(
                    array(array('email', 'heslo', 'heslo_kontrola', 'jmeno', 'prijmeni', 'role'), 'required'),
                    array('heslo_kontrola', 'compare', 'compareAttribute' => 'heslo'),
                    array('email', 'email'),
                    array('email', 'unikatniUcetValidator'),
                );
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array(
            'uzivatel_pk'       => 'ID',
            'email'             => 'E-mail',
            'heslo'             => 'Heslo',
            'heslo_kontrola'    => 'Kontrola hesla',
            'jmeno'             => 'Jméno',
            'prijmeni'          => 'Příjmení',
            'cas_prihlaseni'    => 'Poslední přihlášení',
            'cas_registrace'    => 'Registrován',
            'role'              => 'Role'
        );
    }

    /**
     * @return SqlDataProvider
     */
    public function search()
    {
        return $this->vratDataprovider();
    }

    /**
     *
     */
    public function delete()
    {
        $sql = "WITH t AS (
            UPDATE uzivatel SET stav = :stav1 WHERE uzivatel_pk = :pk RETURNING 1
        ) SELECT count(*) FROM t";
        $params = [
            ':stav1' => Uzivatel::STAV_SMAZANO,
            ':pk' => $this->uzivatel_pk
        ];

        $trans = Yii::$app->db->beginTransaction();
        $res = Yii::$app->db->createCommand($sql, $params)->queryScalar();

        if ($res === 1) {
            $trans->commit();
            return true;
        } else {
            $trans->rollBack();
            return false;
        }
    }

    /**
     * @param int $pagination
     * @return SqlDataProvider
     */
    public function vratDataprovider($pagination = 10)
    {
        $sql = $this->_sql();

        $aParams = $aWhere = [];

        if ($this->email != null && !$this->hasErrors('email')) {
            $aParams['email'] = $this->email;
            $aWhere[] = " trim(both ' ' from lower(uzivatel.email)) = trim(both ' ' from lower(:email))";
        }

        if ($this->jmeno != null && !$this->hasErrors('jmeno')) {
            $aParams['jmeno'] = $this->jmeno;
            $aWhere[] = " ((uzivatel.prijmeni || ' ' || uzivatel.jmeno ~* :jmeno) OR (uzivatel.jmeno || ' ' || uzivatel.prijmeni ~* :jmeno))";
        }

        if (is_array($this->stav) && !empty($this->stav) && !$this->hasErrors('stav')) {
            $aParams['stav'] = DbUtils::implodePgArray($this->stav);
            $aWhere[] = " uzivatel.stav = ANY(:stav) ";
        }

        if (is_array($this->role) && !empty($this->role) && !$this->hasErrors('role')) {
            $aParams['role'] = DbUtils::implodePgArray($this->role);
            $aWhere[] = " uzivatel.role = ANY(:role) ";
        }

        if (!empty($aWhere)) {
            $sql .= " where " . implode(' and ', $aWhere);
        }

        $dataProvider = new SqlDataProvider(array(
            'sql' => $sql,
            'sort' => array(
                'defaultOrder' => array('uzivatel_pk' => SORT_ASC),
                'attributes' => array(
                    'uzivatel_pk',
                    'email',
                    'stav',
                    'prijmeni',
                    'jmeno',
                    'cas_prihlaseni',
                    'role'
                )
            ),
            'params' => $aParams,
            'pagination' => $pagination !== false ? array(
                'pageSize' => $pagination
            ) : $pagination
        ));

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function vratSloupce()
    {
        return array(
            'email' => array('attribute' => 'email', 'label' => 'E-mail'),
            'prijmeni' => array('attribute' => 'prijmeni', 'label' => 'Příjmení'),
            'jmeno' => array('attribute' => 'jmeno', 'label' => 'Jméno'),
            'role' => array(
                'attribute' => 'role',
                'label' => 'Role',
                'value' => function ($data) {
                    return self::itemAlias('role', $data['role']);
                }
            ),
            'stav' => array(
                'attribute' => 'stav',
                'label' => 'Stav',
                'value' => function ($data) {
                    return self::itemAlias('stavy', $data['stav']);
                },
            ),
            'cas_prihlaseni' => array(
                'attribute' => 'cas_prihlaseni',
                'label' => 'Čas posledního přihlášení',
                'format' => array('dateTime', 'php:d.m.Y H:i:s')
            ),
            'akce' => array(
                'class' => 'app\components\columns\ActionColumn',
                'template' => '{view} {update} {delete}',
                'urlCreator' => function ($action, $model, $key, $index) {
                    switch ($action) {
                        case 'view':
                            $url = array('/uzivatel/admin/detail', 'id' => $model['uzivatel_pk']);
                            break;
                        case 'update':
                            $url = array('/uzivatel/admin/upravit', 'id' => $model['uzivatel_pk']);
                            break;
                        case 'delete':
                            $url = array('/uzivatel/admin/smazat', 'id' => $model['uzivatel_pk']);
                            break;
                        default:
                            throw new Exception('Undefined action for model');
                    }
                    return $url;
                },
                'buttons' => array(
                    'view' => function($url, $model, $key) {
                        return Html::a(
                            Html::tag('span', '', [
                                'class' => 'glyphicon glyphicon-eye-open',
                            ]),
                            $url,
                            [
                                'title' => 'Náhled',
                                'data-pjax' => $key
                            ]
                        );
                    },
                    'update' => function($url, $model, $key) {
                        $link = '';
                        if (
                            $this->smiEditovatUzivatelUzivatele($model['uzivatel_pk'])
                            && $model['stav'] != Uzivatel::STAV_SMAZANO
                        ) {
                            $link = Html::a(
                                Html::tag('span', '', [
                                    'class' => 'glyphicon glyphicon-pencil',
                                ]),
                                $url,
                                [
                                    'title' => 'Upravit',
                                    'data-pjax' => $key
                                ]
                            );
                        }

                        return $link;
                    },
                    'delete' => function($url, $model, $key) {
                        $link = '';
                        if (
                            $this->smiEditovatUzivatelUzivatele($model['uzivatel_pk'])
                            && $model['uzivatel_pk'] != Yii::$app->user->id
                            && $model['stav'] != Uzivatel::STAV_SMAZANO
                        ) {
                            $link = Html::a(
                                Html::tag('span', '', [
                                    'class' => 'glyphicon glyphicon-trash',
                                ]),
                                $url,
                                [
                                    'title' => 'Smazat',
                                    'data-pjax' => $key,
                                    'data-confirm' => "Opravdu chcete smazat uživatele {$model['prijmeni']} {$model['jmeno']}?",
                                    'data-method' => "post",
                                ]
                            );
                        }

                        return $link;
                    }
                ),
                'header' => 'Akce',
            )
        );
    }

    /**
     * @return string
     */
    protected function _sql()
    {
        $sql = 'SELECT uzivatel_pk, email, stav, cas_prihlaseni, prijmeni, jmeno, role FROM uzivatel';

        return $sql;
    }

    /**
     *
     * @param $attribute
     * @param $params
     */
    public function unikatniUcetValidator($attribute, $params)
    {
        $sql = "
            SELECT 1 FROM uzivatel
            WHERE
                lower(trim(BOTH ' ' FROM email)) = lower(trim(BOTH ' ' FROM :email))
                AND stav = :stav
        ";

        $dbParams = array(
            'email' => $this->$attribute,
            'stav' => self::STAV_AKTIVNI
        );

        // pri editaci musim z dat vyhodit sam sebe, jinak nezvaliduju
        if (!$this->isNewRecord) {
            $sql .= " and uzivatel_pk != :upk";
            $dbParams['upk'] = $this->uzivatel_pk;
        }

        $result = \Yii::$app->db->createCommand($sql, $dbParams)->queryScalar();

        if ($result == 1) {
            $this->addError($attribute, 'Tento email je už použitý!');
        }

        return;
    }

    public function vratCeleJmeno($id = null)
    {
        if (null == $id) {
            $mUzivatel = $this;
        } else {
            $mUzivatel = self::findOne($id);
        }

        return $mUzivatel->prijmeni . ' ' . $mUzivatel->jmeno;
    }

    /**
     * @return string
     */
    public function generujValidacniKlic()
    {
        return md5(uniqid() . '|' . time() . '|' . $this->email);
    }

    /**
     * @param array $vynechat pole ID uzivatelu, ktere chci vynechat
     * @return array
     */
    public static function vratProDropdown($vynechat = array())
    {
        $sql = "SELECT uzivatel_pk, prijmeni || ' ' || jmeno AS jmeno FROM uzivatel";
        if (!empty($vynechat)) {
            $sql .= " where uzivatel_pk not in (" . implode(',', $vynechat) . ")";
        }
        $sql .= " order by prijmeni desc, jmeno desc";
        $db = Yii::$app->db;

        $result = $db->createCommand($sql)->queryAll();

        $uzivatele = array();
        foreach ($result as $row) {
            $uzivatele[$row['uzivatel_pk']] = $row['jmeno'];
        }

        return $uzivatele;
    }

    /**
     * @return array
     */
    public static function vratVystaviteleProDropdown()
    {
        $sql = "
            select
              f.vystavil as pk, (u.prijmeni || ' ' || u.jmeno) as jmeno2
            from faktura f
              join uzivatel u on f.vystavil = u.uzivatel_pk
            GROUP BY pk, jmeno2
        ";
        $command = Yii::$app->db->createCommand($sql);
        $result = $command->queryAll();

        return ArrayHelper::map($result, 'pk', 'jmeno2');
    }

    public static function itemAliasData()
    {
        return array(
            'stavy' => array(
                self::STAV_AKTIVNI => 'aktivní',
                self::STAV_NEAKTIVNI => 'neaktivní',
                self::STAV_REGISTRACE => 'registrace',
                self::STAV_SMAZANO => 'smazaný'
            ),
            'role' => array(
                self::ROLE_SUPERADMIN => 'Superadmin',
                self::ROLE_ADMIN => 'Administrátor',
                self::ROLE_USER => 'Uživatel'
            )
        );
    }

    /* *** tohle by mělo být někde v Auth, dělat SQL dotazy a hlavně být statické *** */
    /**
     * @param $editovany_pk
     * @param null $editujici_pk
     * @return bool
     */
    public function smiEditovatUzivatelUzivatele($editovany_pk, $editujici_pk = null)
    {
        if ($editujici_pk == null) {
            $editujici_pk = Yii::$app->user->id;
        }

        if ($editujici_pk == $editovany_pk) {
            return true;
        }

        $mUzivatel1 = self::findOne($editovany_pk);
        $mUzivatel2 = self::findOne($editujici_pk);

        return $this->smiEditovatRoleRoli($mUzivatel1->role, $mUzivatel2->role);
    }

    /**
     * @param $role1
     * @param $role2
     * @return bool
     */
    public function smiEditovatRoleRoli($role1, $role2)
    {
        $rolePravidla = $this->_pravidlaRoli();

        if (!isset($rolePravidla[$role2])) {
            Yii::warning("[uzivatel] dotaz na editaci neexistujici roli ({$role2})");
            return false;
        }

        $editovatelne = $rolePravidla[$role2];
        return in_array($role1, $editovatelne);
    }

    /**
     * @param $role
     * @return array
     */
    public function smiZalozitRoleRoli($role)
    {
        $pravidla = $this->_pravidlaRoli();

        $zalozit = array();
        if (!isset($pravidla[$role])) {
            Yii::warning("[uzivatel] dotaz na zalozeni neexistujici roli ({$role})");
            return $zalozit;
        }

        foreach (array_merge([$role], $pravidla[$role]) as $jedna) {
            $zalozit[$jedna] = Uzivatel::itemAlias('role', $jedna);
        }

        return $zalozit;
    }

    /**
     * Vraci pravidla pro zakladni, editaci a mazani roli, v podstate vraci pole s rolemi pod dotazovanou
     * @return array
     */
    protected function _pravidlaRoli()
    {
        return [
            self::ROLE_SUPERADMIN => [
                self::ROLE_ADMIN, self::ROLE_USER
            ],
            self::ROLE_ADMIN => [
                self::ROLE_USER
            ],
            self::ROLE_USER => [

            ],
        ];
    }
    /* *** ********************************************************************** *** */
}