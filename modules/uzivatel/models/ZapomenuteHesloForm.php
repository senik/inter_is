<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 25.10.14
 * Time: 14:18
 */

namespace app\modules\uzivatel\models;

use app\mail\models\ZapomenuteHesloMail;
use yii\base\Model;

/**
 * Class ZapomenuteHesloForm
 * @package app\modules\uzivatel\models
 */
class ZapomenuteHesloForm extends Model
{
    /**
     * @var
     */
    public $email;

    /**
     * @var
     */
    public $validacniKlic;

    /**
     * @var Uzivatel
     */
    protected $_uzivatel;

    /**
     * @return array
     */
    public function rules()
    {
        return array(
            array('email', 'required'),
            array('email', 'email'),
            array('email', 'existujiciUcetValidator'),
        );
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return array(
            'email' => 'E-mail'
        );
    }

    /**
     * @return bool
     */
    public function existujiciUcetValidator()
    {
        /** @var Uzivatel $mUzivatel */
        $mUzivatel = Uzivatel::findOne(array('email' => trim(strtolower($this->email))));
        if (is_null($mUzivatel)) {
            $this->addError('email', 'Neplatné uživatelské jméno!');
            return false;
        }

        $this->_uzivatel = $mUzivatel;
        return true;
    }

    /**
     * @return bool
     */
    public function resetujHeslo()
    {
        if ($this->_uzivatel == NULL || !($this->_uzivatel instanceof Uzivatel)) {
            \Yii::error("Pro reset hesla musi byt uzivatel tridy Uzivatel");
            return false;
        }

        $this->_uzivatel->validacni_klic = $this->_uzivatel->generujValidacniKlic();
        if ($this->_uzivatel->save(false)) {
            $this->validacniKlic = $this->_uzivatel->validacni_klic;

            try {
                $mail = new ZapomenuteHesloMail();
                $mail->adresat = $this->_uzivatel->email;
                $mail->params = [
                    'klic' => $this->validacniKlic
                ];

                return $mail->send();
            } catch (\Exception $e) {
                \Yii::error("Chyba pri odesilani emailu se zapomenutym heslem: {$e->getMessage()}");

                return false;
            }
        } else {
            $this->addError('email', 'Něco se pokazilo, zkuste znovu později.');
            return false;
        }
    }
}