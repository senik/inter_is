<?php

namespace app\modules\uzivatel\controllers;

use app\modules\uzivatel\models\ZapomenuteHesloZmenaForm;
use app\modules\uzivatel\models\ZmenaHeslaForm;
use Yii;
use app\components\Application;
use app\components\Controller;
use app\components\Nastaveni;
use app\modules\uzivatel\components\UzivatelIdentity;
use app\modules\uzivatel\models\Uzivatel;
use app\modules\uzivatel\models\ZapomenuteHesloForm;
use yii\base\Action;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\web\HttpException;

/**
 * Class DefaultController
 * @package app\modules\uzivatel\controllers
 */
class DefaultController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['prihlaseni', 'zapomenute-heslo', 'zapomenute-heslo-zmena'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionPrihlaseni()
    {
        $app = Yii::$app;

        if (!$app->user->isGuest) {
            $this->goHome();
        }

        $mUzivatel = new Uzivatel(['scenario' => 'login']);

        $post = $app->request->post();
        if ($mUzivatel->load($post)) {
            $zapamatovat = ($post['Uzivatel']['zapamatovat'] == '1');
            $identity = new UzivatelIdentity($mUzivatel->email, $mUzivatel->heslo);

            $vysledek = $identity->authenticate();
            // pokud je to v pořádku, přihlásím
            if ($vysledek == UzivatelIdentity::ERROR_NONE) {
                Yii::$app->user->login($identity, $zapamatovat ? 3600 * 24 * 2 : 0);
                if (Nastaveni::instance()->konfigurovat()) {
                    if (Yii::$app->user->identity->getIsAdmin()) {
                        Application::setFlashInfo('První spuštění aplikace, nastavte prosím konfiguraci!');
                        $this->redirect(array('/site/nastaveni'));
                    } else {
                        $mUzivatel->addError('email', '');
                        $mUzivatel->addError('heslo', 'Nebyla provedena první konfigurace, kontaktujte support!');
                    }
                } else {
                    $this->redirect(array('/site/index'));
                }
            }
            // pokud chyba, na jejím základě rozhodnu, co se zobrazí
            else {
                $mUzivatel->addError('email', '');
                switch ($vysledek) {
                    case UzivatelIdentity::ERROR_LOGIN;
                        $mUzivatel->addError('heslo', 'Neplatné uživatelské jméno.');
                        break;
                    case UzivatelIdentity::ERROR_PASS;
                        $mUzivatel->addError('heslo', 'Neplatné heslo. Pokud jste jej zapomněl(a), použijte odkaz pro vytvoření nového.');
                        break;
                    default:
                        // todo tady zalogovat a vyhodit nejakou univerzalni hlasku
                        $message = $identity->getLastError();
                        $mUzivatel->addError('heslo', "[{$message['code']}]: {$message['message']}");
                }
            }
        }

        return $this->render('prihlaseni', array(
            'mUzivatel' => $mUzivatel
        ));
    }

    /**
     * @return \yii\web\Response
     */
    public function actionOdhlaseni()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(Yii::$app->getModule('uzivatel')->urlPrihlaseni);
        }

        Yii::$app->user->logout();

        return $this->redirect(Yii::$app->getModule('uzivatel')->urlPrihlaseni);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionZapomenuteHeslo()
    {
        $model = new ZapomenuteHesloForm();

        $post = Yii::$app->request->post();

        if ($model->load($post) && $model->validate()) {
            if ($model->resetujHeslo()) {
                Application::setFlashInfo("Na Váš email byl zaslán odkaz na změnu hesla.");
                return $this->redirect(array('/uzivatel/default/prihlaseni'));
            }
        }

        return $this->render('zapomenuteHeslo', array(
            'model' => $model
        ));
    }

    /**
     * @param bool $klic
     * @return string|\yii\web\Response
     * @throws HttpException
     */
    public function actionZapomenuteHesloZmena($klic = false)
    {
        if ($klic == false) {
            throw new HttpException(400, 'Neplatný požadavek!');
        }

        try {
            $model = new ZapomenuteHesloZmenaForm($klic);
        } catch (Exception $e) {
            Application::setFlashError('Neplatný validační klíč!');
            return $this->redirect(array('/uzivatel/default/prihlaseni'));
        }

        $post = Yii::$app->request->post();

        if ($model->load($post) && $model->validate()) {
            if ($model->ulozNoveHeslo()) {
                Application::setFlashSuccess('Vaše heslo bylo změněno, můžete se přihlásit.');
            } else {
                Application::setFlashError('Nepodařilo se změnit heslo.');
            }

            return $this->redirect(Yii::$app->getModule('uzivatel')->urlPrihlaseni);
        }

        return $this->render('zapomenuteHesloZmena', array(
            'model' => $model
        ));
    }

    /**
     * Můj profil
     * @return string
     */
    public function actionMujProfil()
    {
        $mUzivatel = Uzivatel::findOne(Yii::$app->user->id);

        return $this->render('muj-profil', [
            'mUzivatel' => $mUzivatel
        ]);
    }

    /**
     * Změna hesla uživatele
     * @return string
     */
    public function actionZmenaHesla()
    {
        $mZmenaHeslaForm = new ZmenaHeslaForm();

        $post = Yii::$app->request->post();
        if ($mZmenaHeslaForm->load($post) && $mZmenaHeslaForm->validate()) {
            if ($mZmenaHeslaForm->uloz()) {
                Application::setFlashSuccess("Heslo bylo změněno.");
            } else {
                Application::setFlashError("Při změně hesla došlo k chybě, opakujte akci později");
            }

            return $this->redirect(['/uzivatel/default/muj-profil']);
        }

        return $this->render('zmena-hesla', [
            'mZmenaHeslaForm' => $mZmenaHeslaForm
        ]);
    }
}
