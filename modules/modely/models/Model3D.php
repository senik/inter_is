<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 10.11.14
 * Time: 22:10
 */

namespace app\modules\modely\models;


use app\components\ActiveRecord;
use app\components\Application;
use app\components\helpers\STLException;
use app\components\helpers\STLUtils;
use app\components\Html;
use app\components\SqlDataProvider;
use yii\base\Exception;
use yii\db\Exception as DbException;
use Yii;
use yii\web\UploadedFile;

/**
 * Class Model3D
 * @package app\modules\modely\models
 *
 * @property integer model_pk
 * @property string nazev
 * @property string model_id
 * @property string soubor
 * @property float objem
 * @property string poznamka
 * @property string typ
 */
class Model3D extends ActiveRecord
{

    const TYP_OBJ = 'obj';
    const TYP_STL = 'stl';

    const SCENARIO_HLEADAT = 'search';
    const SCENARIO_PRIDAT = 'pridat';
    const SCENARIO_OBJEDN = 'objednavka';

    /**
     * @var string
     */
    public $vytisteno_cas;

    /**
     * @var
     */
    public $vytisteno_pocet;

    public static function tableName()
    {
        return 'model3d';
    }

    public function rules()
    {
        switch ($this->scenario) {
            case self::SCENARIO_PRIDAT:
                return array(
                    array(array('nazev', 'soubor', 'typ'), 'required'),
                    array(array('model_pk', 'model_id', 'poznamka', 'objem'), 'safe')
                );
                break;
            case self::SCENARIO_HLEADAT:
                return array(
                    array(array('nazev', 'vytisteno_cas', 'vytisteno_pocet'), 'safe')
                );
                break;
            default:
                return array(
                    array($this->attributes(), 'safe')
                );
        }
    }

    public function scenarios()
    {
        $aScenarios = parent::scenarios();
        $aScenarios[self::SCENARIO_PRIDAT] = array(
            'nazev', 'soubor', 'objem', 'typ', 'model_pk', 'model_id', 'poznamka'
        );
        $aScenarios[self::SCENARIO_HLEADAT] = array(
            'nazev', 'vytisteno_cas', 'vytisteno_pocet'
        );
        $aScenarios[self::SCENARIO_OBJEDN] = array(
            'nazev', 'vytisteno_cas', 'vytisteno_pocet'
        );

        return $aScenarios;
    }

    public function attributeLabels()
    {
        return array(
            'soubor' => 'Soubor',
            'objem' => 'Objem',
            'nazev' => 'Název',
            'model_id' => 'ID modelu',
            'poznamka' => 'Poznámka',
            'vytisteno_cas' => 'Tištěno',
            'vytisteno_pocet' => 'Kolikrát',
            'typ' => 'Typ'
        );
    }

    /**
     * @return SqlDataProvider
     */
    public function search() {
        return $this->_vratDataProvider();
    }

    /**
     * @return SqlDataProvider
     */
    public function export() {
        return $this->_vratDataProvider(false);
    }

    /**
     * @param int $pagination
     * @return SqlDataProvider
     */
    protected function _vratDataProvider($pagination = 10)
    {
        $sql = "select * from model3d";

        $aParams = $aWhere = array();
        if ($this->nazev != null) {
            $aParams[':nazev'] = $this->nazev;
            $aWhere[] = ' nazev ~* :nazev';
        }

        if (!empty($aWhere)) {
            $sql .= " where " . implode('and', $aWhere);
        }

        $dataProvider = new SqlDataProvider(array(
            'sql' => $sql,
            'params' => $aParams,
            'pagination' => $pagination ? [
                'pageSize' => $pagination
            ] : false,
            'sort' => [
                'attributes' => ['nazev', 'objem', 'cena'],
                'defaultOrder' => ['nazev' => SORT_ASC]
            ]
        ));

        return $dataProvider;
    }

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool|void
     * @throws DbException
     * @throws \Exception
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        $db = \Yii::$app->db;
        $transakce = $db->beginTransaction();

        try {
            $this->soubor = $this->_ulozSouborModelu();
            $this->model_id = $db->createCommand("select model_generuj_meta(:nazev::text)")->bindValue('nazev', $this->nazev)->queryScalar();

            if ($this->model_id === false || $this->model_id == null) {
                throw new Exception("nepodarilo se vygenerovat meta_id z nazvu ({$this->nazev})");
            }

            if ($this->soubor != null) {
                $soubor = $this->vratCestuModely($this->soubor);

                // objem jsem schopne pocitat (zatim) jen pro STL
                if ($this->typ == self::TYP_STL) {
                    try {
                        $this->objem = STLUtils::getInstance($soubor)->volume;
                    } catch (STLException $e) {
                        $this->objem = -1;
                    } catch (\Exception $e) {
                        throw $e;
                    }
                } else {
                    $this->objem = -1;
                }

                parent::save($runValidation, $attributeNames);
                $transakce->commit();

                return true;
            } else {
                throw new Exception('nekde to spadlo :)');
            }
        }
        // todo a opet logovani :)
        catch (DbException $e) {
            Application::setFlashError($e->getMessage());
            $transakce->rollBack();

            return false;
        } catch (Exception $e) {
            Application::setFlashError($e->getMessage());
            $transakce->rollBack();

            return false;
        }
    }

    /**
     * @return string
     * @throws \yii\base\Exception
     */
    protected function _ulozSouborModelu() {
//        'type' => string 'application/vnd.ms-pki.stl' (length=26)
//        'size' => int 1046884
        if ($this->soubor instanceof UploadedFile) {
            $filename = $this->soubor->baseName . '.' . $this->soubor->extension;
            $cesta = $this->vratCestuModely() . DIRECTORY_SEPARATOR . $filename;
            $i = 0;

            while (file_exists($cesta)) {
                $i++;
                $filename = $this->soubor->baseName . '-' . $i . '.' . $this->soubor->extension;
                $cesta = $this->vratCestuModely() . DIRECTORY_SEPARATOR . $filename;
            }

            if ($this->soubor->saveAs($cesta)) {
                return $filename;
            } else {
                throw new Exception("nepodarilo se ulozit soubor ({$cesta})");
            }
        } else {
            throw new Exception('atribut soubor neni instance UploadedFile!');
        }
    }

    /**
     * todo doresit, lip udelat, proc se to neuklada pres parent::save()
     */
    public function zmenObjem()
    {
        // objem si musim ulozit vedle, protoze ho v zapeti premlasknu
        $objem = $this->objem;
        $sql = "select * from model3d where model_pk = :pk";

        $data[$this->formName()] = Yii::$app->db->createCommand($sql)->bindValue('pk', $this->model_pk)->queryOne();

        if ($this->load($data)) {
            $this->objem = $objem;

            $result = Yii::$app->db->createCommand("update model3d set objem = :objem where model_pk = :pk RETURNING objem")
                ->bindValues([
                    'objem' => $this->objem,
                    'pk' => $this->model_pk
                ])
                ->queryScalar();

            return $result == $this->objem;
        } else {
            throw new Exception("[model3d] nepodarilo se nacist data do modelu: " . print_r($data, true));
        }
    }

    /**
     * @param null $model
     * @return string
     */
    public function vratCestuModely($model = null)
    {
        $runtime = Yii::getAlias('@runtime');
        $cestaSoubory = $runtime . DIRECTORY_SEPARATOR . 'soubory';
        $cestaModely = $cestaSoubory . DIRECTORY_SEPARATOR . 'modely';

        if (!is_dir($cestaSoubory)) {
            mkdir($cestaSoubory);
        }

        if (!is_dir($cestaModely)) {
            mkdir($cestaModely);
        }

        if ($model == null) {
            return $cestaModely;
        } else {
            return $cestaModely . DIRECTORY_SEPARATOR . $model;
        }
    }

    /**
     * @param null $model_pk
     * @return CenikModelu
     */
    public function vratCenik($model_pk = null)
    {
        if ($model_pk == null) $model_pk = $this->model_pk;

        return new CenikModelu(['model_pk' => $model_pk]);
    }

    /**
     * todo až budu mít materiály, tak bude potřeba počítat váhu a cenu nad db
     * @return array
     */
    public function vratSloupce()
    {
        $sloupce = array(
//            'id' => array(
//                'label' => $this->getAttributeLabel('model_id'),
//                'attribute' => 'model_id'
//            ),
            'nazev' => array(
                'label' => $this->getAttributeLabel('nazev'),
                'attribute' => 'nazev'
            ),
            'objem' => array(
                'label' => $this->getAttributeLabel('objem') . ' (cm3)',
                'value' => function($data) { return round($data['objem'], 3); }
            ),
            'vaha' => array(
                'label' => 'Váha (g)',
                'value' => function($data) { return round($data['objem'] * 1.04, 3); }
            ),
//            'cena' => array(
//                'label' => 'Cena (Kč)',
//                'value' => function($data) { return round($data['objem'] * 1.04 * 0.65, 3); }
//            ),
            'poznamka' => array(
                'label' => $this->getattributelabel('poznamka'),
                'attribute' => 'poznamka',
                'value' => function ($data) {
                        return mb_substr($data['poznamka'], 0, 30, 'UTF-8') . '...';
                    }
            ),
            'akce' => array(
                'header' => 'Akce',
                'class' => 'app\components\columns\ActionColumn',
                'template' => '{view} {update} {download} {delete}',
                'buttons' => [
                    'download' => function($url, $model, $key) {
                            return Html::a(
                                Html::tag('span', '', ['class' => 'glyphicon glyphicon-download-alt']),
                                $url,
                                ['title' => 'Stáhnout model']
                            );
                        },
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                        switch ($action) {
                            case 'view':
                                $url = array('/modely/default/detail', 'id' => $model['model_pk']);
                                break;
                            case 'update':
                                $url = array('/modely/default/upravit', 'id' => $model['model_pk']);
                                break;
                            case 'delete':
                                $url = array('/modely/default/smazat', 'id' => $model['model_pk']);
                                break;
                            case 'download':
                                $url = array('/modely/default/stahni', 'model' => $model['soubor']);
                                break;
                            default:
                                throw new Exception("Undefined action ({$action}) for model (" . get_class($model) . ")");
                        }
                        return $url;
                    },
            ),
        );

        if ($this->scenario == self::SCENARIO_OBJEDN) {
            // unsetnu nepotrebne sloupce
            unset($sloupce['vaha']); unset($sloupce['akce']);
            // a pridam naopak ty, ktere potrebuju
            $sloupce['material'] = array(
                'header' => 'Materiál',
                'class' => 'app\components\columns\MaterialColumn'
            );
            $sloupce['cena'] = array(
                'label' => 'Cena (Kč)',
                'value' => function($data) { return round($data['objem'] * 1.04 * 0.65, 2); }
            );
            $sloupce['akce'] = array(
                //'header' => 'Akce',
                'class' => 'app\components\columns\ActionColumn',
                'template' => '{vybrat}',
                'buttons' => array(
                    'vybrat' => function ($url, $model, $key) {
                            // udelat z nej ikonku
                            return Html::a('<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>', '', array(
                                'class' => 'btn btn-primary btn-xs',
                                'onclick' => "objednavkaPridejPolozku({$model['model_pk']}); return false;",
                                'data-toggle' => 'tooltip',
                                'title' => 'Vybrat'
                            ));
                    },
                )
            );
        }

        return $sloupce;
    }

    /**
     * Vrátí absolutní cestu k modelu
     *
     * @param $soubor
     * @return string cesta
     */
    public static function vratAboslutniCestu($soubor)
    {
        return Yii::getAlias('@runtime') . DIRECTORY_SEPARATOR . 'soubory' . DIRECTORY_SEPARATOR . 'modely' . DIRECTORY_SEPARATOR . $soubor;
    }

    public static function itemAliasData() {
        return array(
            'typ' => array(
                self::TYP_STL => 'STL',
                self::TYP_OBJ => 'OBJ'
            )
        );
    }

}