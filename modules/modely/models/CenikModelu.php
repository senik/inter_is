<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 18. 2. 2015
 * Time: 22:21
 */

namespace app\modules\modely\models;


use yii\base\Model;
use yii\helpers\HtmlPurifier;

/**
 * Class CenikModelu
 *
 * Pokud predam v konfiguraci atribut "model_pk", naplni cenik daty a da se pak vyuzit v DetailView.<br>
 * Kvuli tomu byl tento cenik vlastne taky napsan...
 *
 * @package app\modules\modely\models
 */
class CenikModelu extends Model
{
    /**
     * @var array
     */
    private $_data = [];

    /**
     * @var
     */
    public $model_pk;

    /**
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);

        if (isset($config['model_pk'])) {
            $sql = "
              SELECT
                  replace(lower(unaccent(mv.nazev || '_' || mv.barva)), ' ', '_') AS id
                , mv.nazev || ' - ' || mv.barva AS \"label\"
                , round(coalesce((SELECT objem FROM model3d WHERE model_pk = :mpk) * mv.hustota * mv.cena, 0.00), 2) AS cena
              FROM material_view mv
            ";

            $data = \Yii::$app->db->createCommand($sql)->bindValue('mpk', $config['model_pk'])->queryAll();

            foreach ($data as $radek) {
                $this->_data[$radek['id']] = ['label' => $radek['label'], 'cena' => $radek['cena']];
            }
        }
    }

    /**
     * @param string $attr
     * @return mixed|string
     * @throws \yii\base\UnknownPropertyException
     */
    public function __get($attr)
    {
        if (in_array($attr, $this->attributes())) {
            return HtmlPurifier::process(
                number_format(
                    $this->_data[$attr]['cena'],
                    2,
                    ',',
                    '&nbsp;'
                ) . '&nbsp;' . \Yii::$app->formatter->currencyCode
            );
        } else {
            return parent::__get($attr);
        }
    }

    public function attributes()
    {
        return array_keys($this->_data);
    }

    public function getAttributes()
    {
        $attrs = array();

        foreach ($this->attributes() as $attr) {
            $attrs[$attr] = $this->_data[$attr]['cena'];
        }

        return $attrs;
    }

    public function attributeLabels()
    {
        $labels = array();

        foreach ($this->attributes() as $attr) {
            $labels[$attr] = $this->_data[$attr]['label'];
        }

        return $labels;
    }

    public function vratData()
    {
        return $this->_data;
    }
}