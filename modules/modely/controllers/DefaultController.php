<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 10.11.14
 * Time: 22:09
 */

namespace app\modules\modely\controllers;


use app\components\Application;
use app\components\Controller;
use app\components\Model;
use app\modules\modely\models\Model3D;
use Exception;
use Yii;
use yii\web\HttpException;
use yii\web\UploadedFile;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        $mModel = new Model3D();

        return $this->render('index', array(
            'mModel' => $mModel
        ));
    }

    public function actionDetail($id)
    {
        $mModel = Model3D::findOne($id);
        if (null == $mModel) {
            throw new HttpException(404, "Model s pk ({$id}) nenalezen!");
        }

        return $this->render('detail', array(
            'mModel' => $mModel
        ));
    }

    /**
     * todo dodelat s logovanim a vsemoznym presmerovanim
     * pokud $mModel->objem <= 0, tak doplnit rucne
     * @return string|\yii\web\Response
     */
    public function actionPridat()
    {
        $mModel = new Model3D(array('scenario' => Model3D::SCENARIO_PRIDAT));

        $post = Yii::$app->request->post();

        try {
            if (!empty($post)) {
                $mModel->load($post);
                $mModel->soubor = UploadedFile::getInstance($mModel, 'soubor');

                if ($mModel->validate()) {
                    if ($mModel->save()) {
                        Application::setFlashSuccess("Nahrán nový model - {$mModel->nazev}");

                        return $this->redirect(array('/modely/default/index'));
                    }
                } else {
                    Application::setFlashError(implode("; ", $mModel->getErrors()));
                }
            }
        } catch (Exception $e) {
            Application::setFlashError($e->getMessage());
        }

        return $this->render('pridat', array(
            'mModel' => $mModel
        ));
    }

    /**
     * Action pro umenu objemu modelu<br>
     * @return \yii\web\Response
     */
    public function actionZmenObjem()
    {
        $mModel = new Model3D();
        $post = Yii::$app->request->post();

        if (empty($post) || !$mModel->load($post)) {
            Application::setFlashError("Neplatný požadavek!");
            return $this->redirect(['/modely/default/index']);
        }

        try {
            if ($mModel->zmenObjem()) {
                Application::setFlashSuccess("Objem byl úspěšně změněn.");
            } else {
                throw new Exception("[model3d] chyba pri zmene objemu");
            }
        } catch (Exception $e) {
            Application::setFlashSuccess("Objem byl úspěšně změněn.");
        }

        return $this->redirect(['/modely/default/detail', 'id' => $mModel->model_pk]);
    }

    /**
     * Akce pro stazeni modelu
     * Slouzi predevsim pro zobrazeni - tim, ze je v runtimu, tak je potreba ho pres PHP vytahnout a pak poslat do JS
     *
     * @param $model
     */
    public function actionStahni($model)
    {
        $file = Model3D::vratAboslutniCestu($model);

        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            readfile($file);
            exit;
        }
    }
}