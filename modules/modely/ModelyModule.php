<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 17.10.14
 * Time: 21:16
 */

namespace app\modules\modely;


use app\components\ModuleInterface;
use yii\base\Module;

class ModelyModule extends Module implements ModuleInterface
{
    /**
     * @var string
     */
    public $controllerNamespace = 'app\modules\modely\controllers';

    /**
     * @var
     */
    public $name = "3D modely";

    /**
     * @var array
     */
    public $baseUrl = array('/modely/default/index');

    public function vratMenuModulu()
    {
        return array();
    }
} 