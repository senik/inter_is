<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 10.11.14
 * Time: 22:21
 *
 * @var $mModel Model3D
 * @var $this View
 */

use app\components\Html;
use app\modules\modely\models\Model3D;
use kartik\widgets\ActiveForm;
use kartik\widgets\FileInput;
use yii\web\View;

$action = Yii::$app->controller->action->id;

$form = ActiveForm::begin(array(
    'id' => 'model-form',
    'type' => 'horizontal',
    'options' => array(
        'class' => 'horizontal',
        'enctype' => 'multipart/form-data'
    ),
));
?>
    <div class="form-fields">
        <div class="row">
            <div class="col-sm-12">
                <?= $form->field($mModel, 'nazev') ?>
                <?= $form->field($mModel, 'typ')->dropDownList(Model3D::itemAlias('typ'), ['class' => 'selectpicker']) ?>
                <?= $form->field($mModel, 'soubor')->widget(FileInput::className(),
                    array(
                        'pluginOptions' => array(
                            'showUpload' => false,
                            'showPreview' => false,
                            'browseLabel' => 'Vyber soubor',
                            'removeLabel' => 'Odebrat'
                        )
                    )
                ) ?>
                <?= $form->field($mModel, 'poznamka')->textarea(array('style' => 'width: 100%;', 'rows' => 3)) ?>
            </div>
        </div>
    </div>
    <div class="form-actions well">
        <?php
        echo Html::submitButton('Uložit', array(
            'class' => 'btn btn-success'
        ));
//        echo Html::resetButton(($action == 'upravit' ? 'Původní hodnoty' : 'Vymazat formulář'), array(
//            'class' => 'btn btn-primary'
//        ));
        echo Html::a('Zrušit', array('/modely/default/index'), array(
            'class' => 'btn btn-danger'
        ));
        ?>
    </div>
<?php
ActiveForm::end();