<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 10.11.14
 * Time: 22:17
 *
 * @var $mModel Model3D
 */

use app\components\Html;
use app\modules\modely\models\Model3D;

$this->title = Yii::$app->name . ' - 3D modely';

$this->params['breadcrumbs'] = array(
    '3D Modely'
);
?>

<div class="h2-buttons">
    <h2>3D Modely</h2>
    <?php
    echo Html::a(
        'Přidat',
        array('/modely/default/pridat'),
        array(
            'class' => 'btn btn-success'
        )
    );
    ?>
    <div class="clearfix"></div>
</div>

<?= $this->render('_search', array('mModel' => $mModel)) ?>

<?= \kartik\grid\GridView::widget(array(
    'dataProvider' => $mModel->search(),
    'columns' => $mModel->vratSloupce(),
    'resizableColumns' => false,
    'export' => false
)) ?>