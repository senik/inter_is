<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 17.1.15
 * Time: 13:09
 *
 * @var $mModel Model3D
 */
use app\components\Html;
use app\modules\modely\models\Model3D;

?>

<div class="model-galerie">
    <?php
    $path = Yii::$app->basePath .
        DIRECTORY_SEPARATOR . 'web' .
        DIRECTORY_SEPARATOR . 'images' .
        DIRECTORY_SEPARATOR . 'modely' .
        DIRECTORY_SEPARATOR . $mModel->model_pk
    ;

    $cesta = Yii::getAlias('@web') .
        DIRECTORY_SEPARATOR . 'images' .
        DIRECTORY_SEPARATOR . 'modely' .
        DIRECTORY_SEPARATOR . $mModel->model_pk
    ;

    if (file_exists($path)) {
        $galerie = scandir($path);
        unset($galerie[0], $galerie[1]);
    } else {
        $galerie = array();
    }

    if (empty($galerie)) {
        echo '<h3>V galerii zatím nejsou žádné snímky</h3>';
    } else {
        foreach ($galerie as $obrazek) {
            echo Html::a(
                Html::img($cesta . DIRECTORY_SEPARATOR . $obrazek, [
                    'class' => 'img-thumbnail'
                ]),
                $cesta . DIRECTORY_SEPARATOR . $obrazek,
                [
                    'class' => 'fancybox',
                    'rel' => 'group'
                ]
            );
        }
    }

    ?>
</div>