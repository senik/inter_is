<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 17.1.15
 * Time: 13:09
 *
 * @var $mModel Model3D
 * @var $modelExistuje boolean
 * @var $this View
 */
use app\components\Html;
use app\modules\modely\models\Model3D;
use yii\web\View;

?>

<div class="thingie">
    <div id="model3d-viewer">
        <?php
        if (!$modelExistuje) {
            echo '<div class="file-not-found"><h3>Soubor s modelem nenalezen :/</h3></div>';
        }
        ?>
    </div>

    <div class="viewer-ovladani well">
        <span>Pohyb:</span>
        <?php
        echo Html::radioList('thingie_run', null, array(
            'start' => Html::tag('span' ,'Spustit', array('class' => 'btn btn-sm btn-info active')),
            'stop' => Html::tag('span' ,'Zastavit', array('class' => 'btn btn-sm btn-info'))
        ), array(
            'encode' => false,
            'class' => 'thingie-radio'
        ));
        ?>
        <span>Materiál:</span>
        <?php
        echo Html::radioList('thingie_model', null, array(
            'solid' => Html::tag('span' ,'Těleso', array('class' => 'btn btn-sm btn-info active')),
            'wireframe' => Html::tag('span' ,'Wireframe', array('class' => 'btn btn-sm btn-info'))
        ), array(
            'encode' => false,
            'class' => 'thingie-radio'
        ));
        ?>
        <span>Pozice kamery:</span>
        <?php
        echo Html::radioList('thingie_camera', null, array(
            'diagonal' => Html::tag('span' ,'Diagonálně', array('class' => 'btn btn-sm btn-info active')),
            'top' => Html::tag('span' ,'Nahoře', array('class' => 'btn btn-sm btn-info')),
            'side' => Html::tag('span' ,'Strana', array('class' => 'btn btn-sm btn-info')),
            'bottom' => Html::tag('span' ,'Dole', array('class' => 'btn btn-sm btn-info'))
        ), array(
            'encode' => false,
            'class' => 'thingie-radio'
        ));
        echo Html::a(
            'screen',
            '',
            [
                'id' => 'screenshot',
                'class' => 'btn btn-sm btn-info'
            ]
        );
        ?>
    </div>
</div>