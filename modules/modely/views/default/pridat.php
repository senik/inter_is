<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 10.11.14
 * Time: 22:21
 *
 * @var $mModel Model3D
 */

use app\modules\modely\models\Model3D;

$this->title = Yii::$app->name . ' - Přidat 3D model';

$this->params['breadcrumbs'] = array(
    array('label' => '3D Modely', 'url' => array('/modely/default/index')),
    'Přidat'
);
?>

<div class="h2-buttons">
    <h2>Přidat nový model</h2>
    <div class="clearfix"></div>
</div>

<?= $this->render('_form', array('mModel' => $mModel)); ?>