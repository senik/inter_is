<?php
use app\components\Html;
use app\modules\modely\models\Model3D;
use kartik\daterange\DateRangePicker;
use kartik\widgets\ActiveForm;
use yii\web\View;
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 15.11.14
 * Time: 11:18
 *
 * @var $this View
 * @var $mModel Model3D
 */

$form = ActiveForm::begin(array(
    'method' => 'get',
    'action' => array('/modely/default/index'),
    'type' => 'horizontal',
    'options' => array(
        'class' => 'well'
    ),
    'formConfig' => ['labelSpan' => 3, 'spanSize' => ActiveForm::SIZE_SMALL]
));
?>
    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($mModel, 'nazev', array()) ?>
        </div>

        <div class="col-sm-4">
            <?= $form->field($mModel, 'vytisteno_cas', array(
                'addon' => ['append' => ['content' => '<i class="glyphicon glyphicon-calendar"></i>']],
            ))->widget(DateRangePicker::className(), array(
                    'useWithAddon' => true,
                    'convertFormat' => true,
                    'pluginOptions' => array(
                        'format' => 'j.n.Y',
                        'separator' => ' až ',
                        'ranges' => array(
                            "Dnes" => ["moment().startOf('day')", "moment().endOf('day')"],
                            "Včera" => ["moment().subtract('days', 1)", "moment().subtract('days', 1)"],
                            "Posledních 7 dní" => ["moment().subtract('days', 6)", "moment()"],
                            "Posledních 30 dní" => ["moment().subtract('days', 29)", "moment()"],
                            "Tento měsíc" => ["moment().startOf('month')", "moment().endOf('month')"],
                            "Předchozí měsíc" => ["moment().subtract('month', 1).startOf('month')", "moment().subtract('month', 1).endOf('month')"],
                        )
                    )
                )) ?>
        </div>

        <div class="col-sm-4">
            <?= $form->field($mModel, 'vytisteno_pocet', array()) ?>
        </div>
    </div>

    <div class="form-actions">
        <?php
        echo Html::submitButton('Filtrovat', array('class' => 'btn btn-success'));
        echo Html::submitButton('Vymazat filtr', array('class' => 'btn btn-warning filter-reset'));
        ?>
    </div>

<?php
ActiveForm::end();