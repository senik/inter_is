<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 11.11.14
 * Time: 19:59
 *
 * @var $this View
 * @var $mModel Model3D
 *
 * https://github.com/rckt/3dmodelviewer.jquery
 * https://github.com/tbuser/thingiview.js - tohle je pouzito...
 */

use app\components\helpers\STLUtils;
use app\components\Html;
use app\modules\modely\models\Model3D;
use kartik\detail\DetailView;
use kartik\widgets\ActiveForm;
use kartik\widgets\Alert;
use yii\bootstrap\Modal;
use yii\web\View;

$this->title = Yii::$app->name . ' - Detail 3D modelu';

$this->params['breadcrumbs'] = array(
    array('label' => '3D Modely', 'url' => array('/modely/default/index')),
    $mModel->nazev
);

$modelExistuje = file_exists(Yii::getAlias('@runtime') . DIRECTORY_SEPARATOR . 'soubory' . DIRECTORY_SEPARATOR . 'modely' . DIRECTORY_SEPARATOR . $mModel->soubor);

if ($modelExistuje) {
    $jsPath = Yii::getAlias('@web') . '/js/thingiview/javascripts';
    $objetsPath = Yii::getAlias('@web') . '/js/thingiview/examples/objects';
    $modelsPath = Yii::$app->urlManager->createUrl(array('/modely/default/stahni', 'model' => $mModel->soubor));
    $thingiFunc = strtoupper($mModel->typ);

    $this->registerJsFile($jsPath . '/Three.js', array('position' => View::POS_HEAD));
    $this->registerJsFile($jsPath . '/plane.js', array('position' => View::POS_HEAD));
    $this->registerJsFile($jsPath . '/thingiview.js', array('position' => View::POS_HEAD));

    $this->registerJs("
    var thingiurlbase = '{$jsPath}';
    var thingiview;
    window.onload = function() {
        thingiview = new Thingiview('model3d-viewer');
        thingiview.setObjectColor('C0D8F0'); // tohle bych taky udelal nastavitelny
        thingiview.initScene();
        thingiview.setBackgroundColor('#ffffff');
        thingiview.setObjectMaterial('solid'); // 'wireframe'
        thingiview.load{$thingiFunc}('{$modelsPath}');
        thingiview.centerCamera();
    }
    ", View::POS_HEAD);
}
?>

<div class="h2-buttons">
    <h2><?= $mModel->nazev ?></h2>
    <?php
    echo Html::a(
        'Upravit',
        array('/modely/default/upravit', 'id' => $mModel->model_pk),
        array(
            'class' => 'btn btn-warning'
        )
    );
    echo Html::a(
        'Smazat',
        array('/modely/default/smazat', 'id' => $mModel->model_pk),
        array(
            'class' => 'btn btn-danger'
        )
    );
    ?>
    <div class="clearfix"></div>
</div>

<?php

echo \yii\bootstrap\Tabs::widget([
    'items' => [
        [
            'options' => [
                'id' => 'model',
            ],
            'label' => 'Model',
            'content' => $this->render('_tab_model', [
                    'mModel' => $mModel,
                    'modelExistuje' => $modelExistuje
                ])
        ],
        [
            'options' => [
                'id' => 'detail',
            ],
            'label' => 'Detaily',
            'content' => $this->render('_tab_detail', [
                    'mModel' => $mModel
                ])
        ],
        [
            'options' => [
                'id' => 'galerie',
            ],
            'label' => 'Galerie',
            'content' => $this->render('_tab_galerie', [
                    'mModel' => $mModel
                ])
        ],
    ]
])

?>

<?php
// --- modalni okno s formularem pro zmenu objemu modelu
if ($mModel->objem <= 0) {
    Modal::begin([
        'header' => '<h3>Opravte chybný objem</h3>',
        'id' => 'model-objem-modal'
    ]);

    echo Alert::widget([
        'type' => Alert::TYPE_DANGER,
        'body' => 'Během zpracování modelu došlo k chybě a nebyl správně vypočten objem, zde ho můžete opravit.',
        'delay' => false
    ]);

    $form = ActiveForm::begin([
        'id' => 'model-objem-form',
        'type' => 'horizontal',
        'action' => ['/modely/default/zmen-objem'],
        'method' => 'POST',
        'formConfig' => [
            'labelSpan' => 3,
            'deviceSize' => ActiveForm::SIZE_SMALL
        ]
    ]);
    ?>
        <div class="form-fields">
            <?= Html::activeHiddenInput($mModel, 'model_pk', ['value' => $mModel->model_pk]) ?>
            <?= $form->field($mModel, 'objem')->textInput() ?>
        </div>

        <div class="form-actions">
            <?php
            echo Html::submitButton('Uložit', array(
                'class' => 'btn btn-success'
            ));
            echo Html::a('Zrušit', '#', array(
                'class' => 'btn btn-danger modal-close'
            ));
            ?>
        </div>
    <?php
    ActiveForm::end();

    Modal::end();
}
// -----------------------------------------------------

// javascripty s magii okolo ovladani 3D modelu a tak
$screenshotUrl = Yii::$app->urlManager->createUrl(['/ajax/uloz-screen']);
$imageUrl = Yii::getAlias('@web') . '/images/modely/{pk}/{soubor}';
$this->registerJs("
$(window).on('load', function() {
    var objemModal = $('#model-objem-modal');
    if (objemModal.length != 0) {
        objemModal.modal('show');
    }
});

$('input[name*=\"thingie_\"]').on('change', function() {
    var name = $(this).attr('name');
    var value = $(this).val();
    var vsechnyLabely = $(this).closest('div.thingie-radio').find('label span');

    vsechnyLabely.each(function() {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        }
    });

    $(this).next('span').addClass('active');

    switch (name) {
        case 'thingie_run':
            thingiview.setRotation((value == 'start'));
            break;
        case 'thingie_model':
            thingiview.setObjectMaterial(value);
            break;
        case 'thingie_camera':
            thingiview.setCameraView(value);
            break;
    }
});

$('#screenshot').on('click', function(ev) {
    ev.preventDefault();

    var r_data = thingiview.getScreenshot();
    var request = $.ajax({
        url: '{$screenshotUrl}',
        type: \"POST\",
        data: {img: r_data, model: {$mModel->model_pk}}
    });

    request.done(function(response) {
        var parsed = $.parseJSON(response);
        var pk = parsed.data.pk;
        var soubor = parsed.data.soubor;

        var imageUrl = '{$imageUrl}';
        imageUrl = imageUrl.replace('{pk}', pk).replace('{soubor}', soubor);

        var obr_novy = '<a class=\"fancybox\" href=\"'+imageUrl+'\" rel=\"group\">'+'<img src=\"'+imageUrl+'\" alt=\"\" class=\"img-thumbnail\">'+'</a>';
        var gal_nadpis = $('h3', '.model-galerie');
        if (gal_nadpis.length) {
            gal_nadpis.remove();
        }

        $('.model-galerie').append(obr_novy);
    });
});

$('a[data-toggle=\"tab\"]').on('shown.bs.tab', function (e) {
    var target = $(e.target).attr('href');
    if (target == '#model') {
        var run = $('input[name=\"thingie_run\"]:checked').val();
        if (run == 'start') {
            thingiview.setRotation(true);
        } else {
            thingiview.setRotation(false);
        }
    } else {
        thingiview.setRotation(false);
    }
//    window.console.log(e.target, e.relatedTarget);
})
", View::POS_END);
