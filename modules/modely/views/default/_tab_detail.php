<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 17.1.15
 * Time: 13:09
 *
 * @var $mModel Model3D
 */
use app\modules\modely\models\Model3D;
use yii\widgets\DetailView;

?>


<div class="row">
    <div class="col-sm-6">
        <?php
        echo DetailView::widget(array(
            'model' => $mModel,
            'attributes' => array(
                'model_id',
                'nazev',
                'soubor',
                'objem' => array(
                    'attribute' => 'objem',
                    'label' => 'Objem (cm3)',
                    'value' => round($mModel->objem, 3)
                ),
                'vaha' => array(
                    'attribute' => 'objem',
                    'label' => 'Váha (g)',
                    'value' => round($mModel->objem * 1.04, 3)
                ),
                'cena' => array(
                    'attribute' => 'objem',
                    'label' => 'Cena (Kč)',
                    'value' => round($mModel->objem * 1.04 * 0.65, 3)
                ),
                'poznamka',
            )
        ));
        ?>
    </div>
    <div class="col-sm-6">
        <?php
        echo DetailView::widget([
            'model' => $mModel->vratCenik(),
        ]);
        ?>
    </div>
</div>