<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 3.11.14
 * Time: 23:19
 */

namespace app\modules\objednavky\models;


use app\components\Model;
use yii\base\Exception;
use Yii;

/**
 * Class DodaciUdaje
 * @package app\modules\objednavky\models
 *
 */
class DodaciUdaje extends Model
{
    /**
     * @var
     */
    public $dodaci_udaje_pk = null;

    /**
     * @var
     */
    public $adresa_pk;

    /**
     * @var
     */
    public $firma;

    /**
     * @var
     */
    public $jmeno;

    /**
     * @var
     */
    public $prijmeni;

    /**
     * @var
     */
    public $ulice;

    /**
     * @var
     */
    public $mesto;

    /**
     * @var
     */
    public $psc;

    /**
     * @var
     */
    public $zeme_id;

    /**
     * @var
     */
    public $zakaznik_pk;

    /**
     * @var
     */
    public $dodavatel_pk;

    /**
     * @var
     */
    public $stejne;

    /**
     * @return $this
     */
    public static function model()
    {
        return parent::model(__CLASS__);
    }

    public function rules()
    {
        return [
            [
                ['jmeno', 'prijmeni', 'ulice', 'mesto', 'psc', 'zeme_id'],
                'required',
                'when' => function($model) {
                    return $model->dodaci_udaje_pk == null;
                },
                'whenClient' => "function (attribute, value) {
                    return $('#dodaciudaje-dodaci_udaje_pk').val() == '';
                }"
            ],
            [['dodaci_udaje_pk', 'adresa_pk', 'zakaznik_pk', 'stejne', 'firma'], 'safe'],
            ['psc', 'match', 'pattern' => '/^\d{5}$/i'],
            [['firma', 'jmeno', 'prijmeni', 'ulice', 'mesto', 'psc', 'zeme_id'], 'filter', 'filter' => function($value) {
                return $value == '' ? null : $value;
            }]
        ];
    }

    public function attributeLabels()
    {
        return array(
            'dodaci_udaje_pk' => '',
            'adresa_pk' => '',
            'firma' => 'Firma',
            'jmeno' => 'Jméno',
            'prijmeni' => 'Příjmení',
            'ulice' => 'Ulice',
            'mesto' => 'Město',
            'psc' => 'PSČ',
            'zeme_id' => 'Stát'
        );
    }

    /**
     * @return mixed
     * @throws \yii\base\Exception
     */
    public function uloz()
    {
        if (
            ($this->zakaznik_pk == null && $this->dodavatel_pk == null)
            || ($this->zakaznik_pk != null && $this->dodavatel_pk != null)
        ) {
            throw new Exception("[dodaci udaje] nevim komu mam ulozit dodaci udaje! zakaznik = ({$this->zakaznik_pk}) dodavatel = ({$this->dodavatel_pk})");
        }

        if ($this->dodaci_udaje_pk == '') {
            $this->dodaci_udaje_pk = null;
        }

        if ($this->zakaznik_pk != null) {
            $this->dodaci_udaje_pk = $this->_ulozDodaciUdaje($this->zakaznik_pk, 'zakaznik');
        }

        if ($this->dodavatel_pk != null) {
            $this->dodaci_udaje_pk = $this->_ulozDodaciUdaje($this->dodavatel_pk, 'dodavatel');
        }

        return $this->dodaci_udaje_pk;
    }

    /**
     * @param $pk
     * @return $this|null
     */
    public function nactiPodlePk($pk)
    {
        return $this->_nacti($pk);
    }

    /**
     * @param $pk
     * @return $this|null
     */
    public function nactiPodleZakaznika($pk)
    {
        $db = Yii::$app->db;
        $command = $db->createCommand("select dodaci_udaje_pk from zakaznik_dodaci_udaje where zakaznik_pk = :zakaznik limit 1");
        $command->bindValue('zakaznik', $pk);

        if (($udaje_pk = $command->queryScalar()) === false) {
            return null;
        } else {
            $this->zakaznik_pk = $pk;
            return $this->_nacti($udaje_pk);
        }
    }

    /**
     * @param $entita
     * @param $entita_pk
     * @param array $dodaci_udaje
     * @throws \yii\base\Exception
     * @return array
     */
    public function nactiProDropdown($entita, $entita_pk, $dodaci_udaje = array())
    {
        if ($entita_pk == null) {
            return [];
        }

        switch ($entita) {
            case 'zakaznik':
                $sql = "
                    select
                          du.*
                        , coalesce(du.firma || ', ', '') || du.jmeno || ' ' || du.prijmeni || ', '
                            || du.ulice || ', ' || du.psc || ' ' || du.mesto || ', ' || z.zeme_id as adresa
                    from dodaci_udaje du
                        join zakaznik_dodaci_udaje zdu using(dodaci_udaje_pk)
                        join zeme z using(zeme_id)
                    where zakaznik_pk = :zakaznik
                ";
                $params = array(
                    'zakaznik' => $entita_pk
                );
                break;
            case 'dodavatel':
                throw new Exception("nemohu nacist dodaci udaje pro dodavatele - dodavatele nejsou implementovani");
                break;
            default:
                throw new Exception("neznama entita pro nacteni dodacich udaju, entita = ({$entita}) pk = ({$entita_pk})");
        }

        $command = Yii::$app->db->createCommand($sql);
        $command->bindValues($params);

        $dodaci_udaje = array();
        foreach ($command->queryAll() as $row) {
            $dodaci_udaje[$row['dodaci_udaje_pk']] = $row;
        }

        return $dodaci_udaje;
    }

    /**
     * @param $udaje_pk
     * @return $this|null
     */
    protected function _nacti($udaje_pk)
    {
        $db = Yii::$app->db;
        $command = $db->createCommand("
            select
                du.*
              --, a.*
            from dodaci_udaje du
              --join adresa a using (adresa_pk)
            where du.dodaci_udaje_pk = :udaje
        ");
        $command->bindValue('udaje', $udaje_pk);

        $udaje = $command->queryOne();
        if (false == $udaje) {
            return null;
        } else {
            foreach ($udaje as $atribut => $hodnota) {
                $this->$atribut = $hodnota;
            }

            return $this;
        }
    }

    /**
     * @param $entita_pk
     * @param $entita
     * @return bool|null|string
     * @throws \yii\base\Exception
     */
    protected function _ulozDodaciUdaje($entita_pk, $entita)
    {
        switch ($entita) {
            case 'zakaznik':
                $sql = "select zakaznik_uloz_dodaci_udaje(:udaje, :firma, :jmeno, :prijmeni, :ulice, :mesto, :psc, :zeme, :zakaznik)";
                $params = array(
                    'udaje' => [$this->dodaci_udaje_pk, ($this->dodaci_udaje_pk == null ? \PDO::PARAM_NULL : \PDO::PARAM_INT)],
                    'firma' => $this->firma,
                    'jmeno' => $this->jmeno,
                    'prijmeni' => $this->prijmeni,
                    'ulice' => $this->ulice,
                    'mesto' => $this->mesto,
                    'psc' => $this->psc,
                    'zeme' => $this->zeme_id,
                    'zakaznik' => $entita_pk
                );
                break;
            case 'dodavatel':
                throw new Exception('dodaci udaje dodavatele nejsou zatim implementovany!');
//                $sql = "";
//                $params = array(
//                    'dodavatel' => $entita_pk,
//                    'udaje' => $this->dodaci_udaje_pk
//                );
                break;
            default:
                throw new Exception("neznama entita pro ulozeni prechodove tabulky k dodacim udajum, entita = ({$entita}) pk = ({$entita_pk})");
        }

        $command = \Yii::$app->db->createCommand($sql);
        $command->bindValues($params);

        if (($vysledek = $command->queryScalar()) < 0) {
            throw new Exception("nepodarilo se ulozit dodaci udaje, {$entita}_uloz_dodaci_udaje() = $vysledek");
        } else {
            return $vysledek;
        }
    }
}