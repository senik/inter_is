<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 18.10.14
 * Time: 13:09
 */

namespace app\modules\objednavky\models;


use app\components\helpers\DbUtils;
use app\components\Html;
use app\components\Model;
use app\components\SqlDataProvider;
use Exception;
use Yii;
use yii\db\Connection;
use yii\helpers\Json;

/**
 * Class Objednavka
 * @package app\modules\objednavky\models
 */
class Objednavka extends Model
{
    const SCENARIO_HLEDAT = 'search';
    const SCENARIO_UPDATE = 'update';
    const SCENARIO_HLEDAT_AJAX = 'search-ajax';

    /**
     * @var
     */
    public $objednavka_pk;

    /**
     * @var
     */
    public $zakaznik_pk;

    /**
     *
     */
    public $f_obchodni_jmeno, $f_ic, $f_dic, $f_ulice, $f_mesto, $f_psc;

    /**
     * @var FakturacniUdaje
     */
    public $mFakturacniUdaje;

    /**
     *
     */
    public $d_ulice, $d_mesto, $d_psc;

    /**
     * @var DodaciUdaje
     */
    public $mDodaciUdaje;

    /**
     * @var
     */
    public $fa_udaje_pk;

    /**
     * @var
     */
    public $do_udaje_pk;

    /**
     * @var
     */
    public $zpusob_platby;

    /**
     * @var
     */
    public $datum_zalozeni;

    /**
     * @var
     */
    public $datum_zmeny;

    /**
     * @var string termin splneni objednavky
     */
    public $termin;

    /**
     * @var
     */
    public $stav;

    /**
     * @var integer generovane cislo objednavky ve tvaru YYYYXXXXX
     */
    public $cislo;

    /**
     * @var array pole modelu objednavky
     */
    public $polozky = array();

    /**
     * @var string ID uzivatele, ktery objednavku zalozil
     */
    public $vlozil;

    /**
     * @var
     */
    public $popis;

    /**
     * @var array
     */
    public static $sparovatelne_stavy = [
        Objednavka::STAV_POTVRZENO, Objednavka::STAV_ZPRACOVAVA, Objednavka::STAV_HOTOVO,
        Objednavka::STAV_VYFAKTUROVANO, Objednavka::STAV_ODESLANO, Objednavka::STAV_ZAPLACENO
    ];


    // todo pokud budu nekdy posouvat stavy, tak pozor, zkontrolovat vsechny db procedury!!!
    // kladne stavy - v poradku
    const STAV_KOSIK = 0;
    const STAV_ZALOZENO = 1;
    const STAV_POTVRZENO = 2;
    const STAV_ZPRACOVAVA = 3;
    const STAV_HOTOVO = 4;
    const STAV_VYFAKTUROVANO = 5;
    const STAV_ODESLANO = 9;
    const STAV_ZAPLACENO = 10;

    // zaporne stavy - chyba
    const STAV_STORNO_UZIVATEL = -1;
    const STAV_STORNO_SYSTEM = -2;
    const STAV_SMAZANO = -10;

    // druhy platby
    const PLATBA_DOBIRKA = 'DOBRIKA';
    const PLATBA_HOTOVE = 'HOTOVE';
    const PLATBA_KARTA = 'KARTA';
    const PLATBA_PREVODEM = 'PREVODEM';

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $aScenarios = parent::scenarios();
        $aScenarios[self::SCENARIO_HLEDAT] = array(
            'cislo', 'datum_zalozeni', 'stav', 'zakaznik_pk', 'termin', 'zpusob_platby'
        );
        $aScenarios[self::SCENARIO_HLEDAT_AJAX] = array(
            'datum_zalozeni', 'stav', 'zakaznik_pk'
        );
        $aScenarios[self::SCENARIO_UPDATE] = array(
            'termin', 'zpuson_platby', 'objednavka_pk', 'fa_udaje_pk', 'do_udaje_pk', 'cislo'
        );

        return $aScenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        if ($this->scenario == self::SCENARIO_HLEDAT) {
            $rules = array(
                array(array('zakaznik_pk', 'cislo', 'stav', 'datum_zalozeni', 'objednavka_pk', 'termin'), 'safe')
            );
        } else if ($this->scenario == self::SCENARIO_UPDATE) {
            $rules = array(
                array(array('cislo', 'objednavka_pk', 'termin', 'do_udaje_pk', 'fa_udaje_pk', 'vlozil'), 'safe')
            );
        } else if ($this->scenario == self::SCENARIO_HLEDAT_AJAX) {
            $rules = array(
                array(array('datum_zalozeni', 'stav', 'zakaznik_pk'), 'safe')
            );
        } else {
            $rules = array(
                array(array('zakaznik_pk', 'cislo', 'zpusob_platby', 'termin', 'polozky'), 'required'),
                array(array('datum_zalozeni', 'datum_zmeny', 'fa_udaje_pk', 'do_udaje_pk', 'stav', 'objednavka_pk', 'vlozil', 'popis'), 'safe'),
                array(array('f_obchodni_jmeno', 'f_ic', 'f_dic', 'f_ulice', 'f_mesto', 'f_psc', 'd_ulice', 'd_mesto', 'd_psc'), 'safe')
            );
        }

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array(
            'zakaznik_pk' => 'Zákazník',
            'datum_zalozeni' => 'Datum',
            'datum_zmeny' => 'Datum změny',
            'stav' => 'Stav',
            'cislo' => 'Číslo',
            'fa_udaje_pk' => 'Fakturační údaje',
            'do_udaje_pk' => 'Dodací údaje',
            'zpusob_platby' => 'Způsob platby',
            'termin' => 'Termín dodání'
        );
    }

    /**
     * @param int $pagination
     * @return SqlDataProvider
     */
    public function search($pagination = 10)
    {
        return $this->_vratDataProvider($pagination);
    }

    /**
     * @return SqlDataProvider
     */
    public function export()
    {
        return $this->_vratDataProvider(false);
    }

    /**
     * Obsahuje SQL s where podminkou na cislo objednavky.<br>
     * Vola univerzalni metodu, ktere preda where podminku a parametry.<br>
     *
     * @param $cislo
     * @return $this bud instance Objednavka nebo null
     */
    public function nactiPodleCisla($cislo)
    {
        $where = 'cislo = :cislo';
        $params = ['cislo' => $cislo];

        return $this->_nacti($where, $params);
    }

    /**
     * Obsahuje SQL s where podminkou na pk objednavky.<br>
     * Vola univerzalni metodu, ktere preda where podminku a parametry.<br>
     *
     *
     * @param $pk null|int muze byt null - pro pripad, kdy system nacita pk do objednavky z getu/postu pomoci load
     * @return $this bud instance Objednavka nebo null
     */
    public function nactiPodlePk($pk = null)
    {
        if ($pk == null) {
            $pk = $this->objednavka_pk;
        }

        $where = 'o.objednavka_pk = :objednavka';
        $params = ['objednavka' => $pk];

        return $this->_nacti($where, $params);
    }

    /**
     * @param null $where
     * @param array $params
     * @return $this
     */
    protected function _nacti($where = null, $params = array())
    {
        if ($where != null) {
            $where = ' and ' . $where;
        }

        $sql = "
            select
                o.*
              , array_agg(ARRAY[op.model_pk, op.material_pk, op.pocet, op.cena_kus]::text) as polozky
              , sum(op.pocet) as polozek
              , cislo || '; ' || z.prijmeni || ' ' || z.jmeno || '; ' || sum(op.pocet) || ' položek za ' || replace(sum(op.cena_kus * op.pocet)::text, '.', ',') || ' Kč' as popis
              , odu.dodaci_udaje_pk AS do_udaje_pk
              , ofu.fakturacni_udaje_pk AS fa_udaje_pk
            from objednavka o
              join objednavka_polozka op on op.objednavka_pk = o.objednavka_pk
              join zakaznik z on o.zakaznik_pk = z.zakaznik_pk
              left join objednavka_dodaci_udaje odu on o.objednavka_pk = odu.objednavka_pk
              left join objednavka_fakturacni_udaje ofu on o.objednavka_pk = ofu.objednavka_pk
            where (1=1) {$where}
            group by o.objednavka_pk, z.prijmeni, z.jmeno, do_udaje_pk, fa_udaje_pk
        ";

        $data[$this->formName()] = Yii::$app->db->createCommand($sql)->bindValues($params)->queryOne();
        if ($this->load($data)) {
            if ($data[$this->formName()]['do_udaje_pk'] != null) {
                $this->mDodaciUdaje = (new DodaciUdaje())->nactiPodlePk($data[$this->formName()]['do_udaje_pk']);
            }

            if ($data[$this->formName()]['fa_udaje_pk'] != null) {
                $this->mFakturacniUdaje = (new FakturacniUdaje())->nactiPodlePk($data[$this->formName()]['fa_udaje_pk']);
            }

            return $this;
        } else {
            return null;
        }
    }

    /**
     * todo proceduru upravit, aby ukladala do objednavky komplet udaje - jestli je teda nebudu verzovat, jako ze nebudu
     */
    public function uloz()
    {
        /** @var Connection $db */
        $db = Yii::$app->db;
        $sql = "select objednavka_zmena(:pk::int, :cislo::int, :zakaznik::int, :vlozil::int, :termin::date, :fa_udaje::int, :do_udaje::int, :polozky)";

        $polozky = array();

        // pk => array(nazev, kusu)
        foreach ($this->polozky as $model => $polozka) {
            foreach ($polozka as $material => $data) {
                $polozky[] = [$model, $material, $data['kusu']];
            }
        }

        if ($this->fa_udaje_pk == null) $this->fa_udaje_pk = 0;
        if ($this->do_udaje_pk == null) $this->do_udaje_pk = 0;

        $params = array(
            'pk' => $this->objednavka_pk,
            'cislo' => $this->cislo,
            'zakaznik' => $this->zakaznik_pk,
            'vlozil' => Yii::$app->user->id,
            'termin' => $this->termin,
            'fa_udaje' => $this->fa_udaje_pk,
            'do_udaje' => $this->do_udaje_pk,
            'polozky' => DbUtils::implodePgArray($polozky)
        );

        $command = $db->createCommand($sql);
        $command->bindValues($params);

        return $command->queryScalar();
    }

    /**
     * Vraci celkovou cenu objednavky (bud predane pk nebo primo z modelu)
     *
     * @param null|integer $pk
     * @return null|string
     */
    public function vratCelkovouCenu($pk = null)
    {
        if ($pk == null) $pk = $this->objednavka_pk;

        $sql = "
            select
                coalesce(sum(op.pocet * op.cena_kus), sum(op.pocet * (p.objem * mv.hustota * mv.cena))) as suma_celkem
            from objednavka o
                join objednavka_polozka op on op.objednavka_pk = o.objednavka_pk
                join model3d p on p.model_pk = op.model_pk
                left join material_view mv on op.material_pk = mv.material_pk
                  and mv.platne_od <= o.datum_zalozeni
                  and (mv.platne_do > o.datum_zalozeni or mv.platne_do is null)
            where o.objednavka_pk = :objednavka
        ";

        return Yii::$app->db->createCommand($sql)->bindValue('objednavka', $pk)->queryScalar();
    }

    /**
     * @param int $pagination
     * @return SqlDataProvider
     */
    protected function _vratDataProvider($pagination = 10)
    {
        $sql = "
            select
                  o.*
                , z.prijmeni || ' ' || z.jmeno as zakaznik_jmeno
                , sum(op.pocet) as polozek
                , array_agg(op.objednavka_polozka_pk) as polozky
                , coalesce(sum(op.pocet * op.cena_kus), sum(round(op.pocet * (p.objem * mv.hustota * mv.cena), 2))) as cena -- tady pak cena materialu
            from objednavka o
                join zakaznik z on z.zakaznik_pk = o.zakaznik_pk
                join objednavka_polozka op on op.objednavka_pk = o .objednavka_pk
                join model3d p on op.model_pk = p.model_pk -- tady to prijde prekopat na polozku
                left join material_view mv on op.material_pk = mv.material_pk
                  and mv.platne_od <= o.datum_zalozeni
                  and (mv.platne_do > o.datum_zalozeni or mv.platne_do is null)
        ";

        $aParams = array();
        $aWhere = array();

        if ($this->zakaznik_pk != null) {
            $aParams[':zakaznik'] = $this->zakaznik_pk;
            $aWhere[] = ' o.zakaznik_pk = :zakaznik ';
        }

        if ($this->cislo != null) {
            $aParams[':cislo'] = $this->cislo;
            $aWhere[] = ' o.cislo = :cislo ';
        }

        if ($this->stav != null) {
            if (!is_array($this->stav)) $this->stav = [$this->stav];

            $aParams[':stav'] = DbUtils::implodePgArray($this->stav);
            $aWhere[] = ' o.stav = ANY (:stav) ';
        }

        if ($this->termin != null) {
            $aParams[':termin'] = $this->termin;
            $aWhere[] = ' o.termin <= :termin ';
        }

        if (!empty($aWhere)) {
            $sql .= ' where ' . implode('and', $aWhere);
        }

        $sql .= ' group by o.objednavka_pk, zakaznik_jmeno';

        return new SqlDataProvider(array(
            'sql' => $sql,
            'params' => $aParams,
            'sort' => array(
                'defaultOrder' => array('cislo' => SORT_DESC),
                'attributes' => array('cislo', 'zakaznik_jmeno', 'datum_zalozeni', 'stav')
            ),
            'pagination' => $pagination == false ? $pagination : array(
                    'pageSize' => $pagination
                )
        ));
    }

    /**
     * Vygeneruje cislo objednavky pomoci db funkce objednavka_generuj_cislo
     * @return bool|null|string
     */
    public static function generujCislo()
    {
        $sql = "select objednavka_generuj_cislo()";
        return \Yii::$app->db->createCommand($sql)->queryScalar();
    }

    /**
     * Zmena stavu objednavky, vola proceduru a vraci jeji vysledek
     * todo update, neni to idealni
     *
     * @param $novy
     * @param null $stary
     * @param $objednavka
     * @return bool|null|string
     * @throws \Exception
     */
    public function zmenStav($novy, $stary = null, $objednavka = null)
    {
        if ($stary == null) {
            $stary = $this->stav;
        }

        if ($objednavka == null) {
            $objednavka = $this->objednavka_pk;
        }

        try {
            /** @var Objednavka $mObjednavka */
            $mObjednavka = $this->nactiPodlePk($objednavka);

            if ($novy == self::STAV_ODESLANO && !$mObjednavka->jeKompletni()) {
                return -3;
            }

            $stavy = $this->pravidlaStavObjednavky($stary);

            if (in_array($novy, array_keys($stavy))) {
                $sql = "select objednavka_zmen_stav(:objednavka, :stav)";

                return \Yii::$app->db->createCommand($sql)->bindValues([
                    'objednavka' => $objednavka,
                    'stav' => $novy
                ])->queryScalar();
            } else {
                throw new Exception(sprintf("[objednavka] nelze zmenit stav z %s na %s", $stary, $novy));
            }
        } catch (Exception $e) {
            Yii::error($e->getMessage(), __METHOD__);
            return false;
        }
    }

    /**
     * @param $pk
     * @param DodaciUdaje $udaje
     * @return bool
     * @throws Exception
     */
    public function zmenDodaciUdaje($pk, DodaciUdaje $udaje)
    {
        if ($pk != null) {
            $this->nactiPodlePk($pk);
        }

        if ($this->objednavka_pk == null) {
            throw new Exception("[objednavka] nemuzu zmenit dodaci udaje, neznam pk objednavky");
        }

        if ($this->stav >= self::STAV_ODESLANO) {
            throw new Exception("[objednavka] nekdo (" . Yii::$app->user->id . ") se snazi zmenit dodaci udaje vyfakturovane objednavky ({$this->objednavka_pk})");
        }

        $db = Yii::$app->db;
        $transakce = $db->beginTransaction();
        try {
            if ($udaje->dodaci_udaje_pk == null) {
                $udaje->dodaci_udaje_pk = $udaje->uloz();
            }

            $command = $db->createCommand("select objednavka_zmen_dodaci(:obj, :dodaci)")
                ->bindValues([
                    'obj'       => $pk,
                    'dodaci'    => $udaje->dodaci_udaje_pk
                ]);

            $result = $command->queryScalar();

            // doladit navratovou hodnotu fce
            if ($result > 0) {
                $transakce->commit();
                return true;
            } else {
                throw new Exception("[objednavka] nemuzu zmenit dodaci udaje, db chyba: {$result}");
            }
        } catch (Exception $e) {
            $transakce->rollBack();
            Yii::error($e->getMessage(), __METHOD__);
            return false;
        }
    }

    /**
     * @param $pk
     * @param FakturacniUdaje $mFakturacniUdaje
     * @return bool
     * @throws Exception
     */
    public function zmenFakturacniUdaje($pk, FakturacniUdaje $mFakturacniUdaje)
    {
        if ($pk != null) {
            $this->nactiPodlePk($pk);
        }

        if ($this->objednavka_pk == null) {
            throw new Exception("[objednavka] nemuzu zmenit fakturacni udaje, neznam pk objednavky");
        }

        if ($this->stav >= self::STAV_ODESLANO) {
            throw new Exception("[objednavka] nekdo (" . Yii::$app->user->id . ") se snazi zmenit fakturacni udaje vyfakturovane objednavky ({$this->objednavka_pk})");
        }

        $db = Yii::$app->db;
        $transakce = $db->beginTransaction();
        try {
            if ($mFakturacniUdaje->fakturacni_udaje_pk == null) {
                $mFakturacniUdaje->fakturacni_udaje_pk = $mFakturacniUdaje->uloz();
            }

            $command = $db->createCommand("select objednavka_zmen_fakturacni(:obj, :fakturacni)")
                ->bindValues([
                    'obj'           => $this->objednavka_pk,
                    'fakturacni'    => $mFakturacniUdaje->fakturacni_udaje_pk
                ]);
            $result = $command->queryScalar();

            if ($result > 0) {
                $transakce->commit();
                return true;
            } else {
                throw new Exception("[objednavka] nemuzu zmenit fakturacni udaje, db chyba: {$result}");
            }
        } catch (Exception $e) {
            $transakce->rollBack();
            Yii::error($e->getMessage(), __METHOD__);
            return false;
        }
    }

    /**
     * Objednavku, ktera byla zpracovana (tj. vyfakturovana, odeslana nebo zaplacena) nebo stornovana, uz nemuzu editovat<br>
     * Casem asi pribude i overeni na uzivatele
     * @return bool
     */
    public function muzuEditovat()
    {
        $zpracovana = $this->stav >= self::STAV_ODESLANO;
        $stornovana = $this->stav <= self::STAV_STORNO_UZIVATEL;

        return !$zpracovana && !$stornovana;
    }

    /**
     * Zkontroluje, jestli je objednavka kompletni, tj ma vyplnene fakturacni i dodaci udaje
     * @return bool
     */
    public function jeKompletni()
    {
        return $this->maVyplneneFakturacni() && $this->maVyplneneDodaci();
    }

    /**
     * Zkontroluje, jestli ma objednavka vyplnene fakturacni udaje
     * @return bool
     */
    public function maVyplneneFakturacni()
    {
        return $this->fa_udaje_pk != NULL;
    }

    /**
     * Zkontroluje, jestli ma objednavka vyplnene dodaci udaje
     * @return bool
     */
    public function maVyplneneDodaci()
    {
        return $this->do_udaje_pk != NULL;
    }

    /**
     * @return bool
     */
    public function jePoTerminu()
    {
        $poTerminu = strtotime($this->termin) < time();
        $nesplnena = in_array($this->stav, [
            self::STAV_KOSIK, self::STAV_ZALOZENO, self::STAV_POTVRZENO, self::STAV_ZPRACOVAVA, self::STAV_HOTOVO, self::STAV_VYFAKTUROVANO
        ]);

        return $poTerminu && $nesplnena;
    }

    /**
     *
     */
    public function maFakturu()
    {
        $sql = "select count(*) = 1 from faktura where objednavka_pk = :pk and stav <> -2";
        $params = [
            ':pk' => $this->objednavka_pk
        ];

        return Yii::$app->db->createCommand($sql, $params)->queryScalar();
    }

    /**
     * @inheritdoc
     */
    public static function itemAliasData()
    {
        return array(
            'stav' => array(
                // kladne stavy
                self::STAV_KOSIK => 'Zboží v košíku',
                self::STAV_ZALOZENO => 'Založená',
                self::STAV_POTVRZENO => 'Potvrzená',
                self::STAV_ZPRACOVAVA => 'Zpracovává se',
                self::STAV_HOTOVO => 'Hotová',
                self::STAV_VYFAKTUROVANO => 'Vyfakturovaná',
                self::STAV_ODESLANO => 'Odeslaná',
                self::STAV_ZAPLACENO => 'Zaplacená',
                // zaporne stavy
                self::STAV_STORNO_UZIVATEL => 'Stornováno uživatelem',
                self::STAV_STORNO_SYSTEM => 'Stornováno systémem',
                self::STAV_SMAZANO => 'Smazáno'
            ),
            'typ_platby' => array(
                self::PLATBA_DOBIRKA => 'Dobírka',
                self::PLATBA_HOTOVE => 'Hotově',
                self::PLATBA_KARTA => 'Kartou',
                self::PLATBA_PREVODEM => 'Převodem',
            ),
        );
    }

    /**
     * @return SqlDataProvider
     * @throws \Exception
     */
    public function vratPolozkyObjednavky()
    {
        if ($this->objednavka_pk == null) {
            throw new \Exception('[objednavka] nemuzu nacist polozky, neznam objednavka_pk');
        }

        $sql = "
            select
                  op.*
                , m.nazev as model_nazev
                , mv.nazev_dlouhy as material_nazev
                , coalesce(op.cena_kus, round((m.objem * mv.hustota * mv.cena), 2)) as cena_ks
                , coalesce(op.pocet * op.cena_kus, round((m.objem * mv.hustota * mv.cena * op.pocet), 2)) as cena_celkem
                , pocet
                , pocet_hotovo
                , o.stav as stav_objednavky
            from objednavka_polozka op
                join model3d m on m.model_pk = op.model_pk
                join material_view mv on mv.material_pk = op.material_pk
                join objednavka o on o.objednavka_pk = op.objednavka_pk
            where op.objednavka_pk = :objednavka
            order by (op.pocet_hotovo / op.pocet) desc, model_nazev asc, material_nazev asc
        ";

        $params = array(
            'objednavka' => $this->objednavka_pk
        );

        return new SqlDataProvider([
            'sql' => $sql,
            'params' => $params,
            'pagination' => false
        ]);
    }

    /**
     * @return SqlDataProvider
     */
    public function vratPoTerminu()
    {
        $this->stav = [
            self::STAV_ZALOZENO, self::STAV_POTVRZENO, self::STAV_ZPRACOVAVA,
            self::STAV_HOTOVO, self::STAV_VYFAKTUROVANO
        ];

        return $this->_vratDataProvider(false);
    }

    /**
     * @return array
     */
    public function vratSloupce()
    {
        $sloupce = array(
            'cislo' => array(
                'attribute' => 'cislo',
                'label' => $this->getAttributeLabel('cislo'),
                'value' => function($data) {
                    return Html::a($data['cislo'], array('/objednavky/default/detail', 'id' => $data['cislo']));
                },
                'format' => 'raw',
                'contentOptions' => [
                    'class' => 'show-detail-column'
                ]
            ),
            'zakaznik' => array(
                'attribute' => 'zakaznik_jmeno',
                'label' => 'Zákazník',
                'value' => function($data) {
                        return Html::a($data['zakaznik_jmeno'], array('/objednavky/zakaznici/detail', 'id' => $data['zakaznik_pk']));
                    },
                'format' => 'raw',
                'contentOptions' => [
                    'class' => 'show-detail-column'
                ]
            ),
            'datum_zalozeni' => array(
                'attribute' => 'datum_zalozeni',
                'label' => $this->getAttributeLabel('datum_zalozeni'),
                'format' => 'dateTime'
            ),
            'stav' => array(
                'label' => $this->getAttributeLabel('stav'),
                'value' => function ($data) {
                        return self::itemAlias('stav', $data['stav']);
                    }
            ),
            'polozek' => array(
                'label' => 'Celkem položek',
                'attribute' => 'polozek'
            ),
            'cena' => array(
                'class' => '\app\components\columns\PriceColumn',
                'attribute' => 'cena',
                'label' => 'Celková cena',
            ),
        );

        if ($this->scenario == self::SCENARIO_HLEDAT_AJAX) {
            $sloupce['akce'] = array(
                'class' => 'app\components\columns\ActionColumn',
                'contentOptions' => ['class' => 'action-column'],
                'template' => '{select}',
                'buttons' => [
                    'select'   => function($url, $model, $key) {
                        $polozky = Json::encode(DbUtils::explodePgArray($model['polozky']));
                        $text = sprintf(
                            "%s; %s; %d položek za %s Kč",
                            $model['cislo'],
                            $model['zakaznik_jmeno'],
                            $model['polozek'],
                            number_format($model['cena'], 2, ',', ' ')
                        );

                        return Html::a(
                            Html::tag('span', '', [
                                'class' => 'glyphicon glyphicon-ok'
                            ]),
                            '#',
                            [
                                'class' => 'btn btn-primary btn-xs',
                                'data-pjax' => 0,
                                'title' => 'Vybrat',
                                'onclick' => "Senovo.fakturaPridejObjednavku({$model['objednavka_pk']}, {$polozky}, '{$text}');"
                            ]
                        );
                    },
                ]
            );
        } else if ($this->scenario == self::SCENARIO_HLEDAT) {
            $sloupce['akce'] = array(
                'class' => 'app\components\columns\ActionColumn',
                'header' => 'Akce',
                'contentOptions' => ['class' => 'action-column'],
                'template' => '{view} {update} {delete}',
                'urlCreator' => function ($action, $model, $key, $index) {
                    switch ($action) {
                        case 'view':
                            $url = array('/objednavky/default/detail', 'id' => $model['cislo']);
                            break;
                        case 'update':
                            $url = array('/objednavky/default/upravit', 'id' => $model['cislo']);
                            break;
                        case 'delete':
                            $url = array('/objednavky/default/smazat', 'id' => $model['objednavka_pk']);
                            break;
                        default:
                            throw new Exception('Undefined action for model');
                    }
                    return $url;
                },
                'buttons' => [
                    'view'   => function($url, $model, $key) {
                        return Html::a(
                            Html::tag('span', '', [
                                'class' => 'glyphicon glyphicon-eye-open'
                            ]),
                            $url,
                            [
                                'data-pjax' => 0,
                                'title' => 'Náhled'
                            ]
                        );
                    },
                    'update' => function($url, $model, $key) {
                        if ($model['stav'] == Objednavka::STAV_ZALOZENO && $model['vlozil'] == Yii::$app->user->id) {
                            return Html::a(
                                Html::tag('span', '', [
                                    'class' => 'glyphicon glyphicon-pencil'
                                ]),
                                $url,
                                [
                                    'data-pjax' => 0,
                                    'title' => 'Upravit'
                                ]
                            );
                        } else {
                            return null;
                        }
                    },
                    'delete' => function($url, $model, $key) {
                        if ($model['stav'] > Objednavka::STAV_KOSIK) {
                            return Html::a(
                                Html::tag('span', '', [
                                    'class' => 'glyphicon glyphicon-trash'
                                ]),
                                $url,
                                [
                                    'data-pjax' => 0,
                                    'data-confirm' => "Opravdu chcete smazat objednávku č. {$model['cislo']}?",
                                    'data-method' => "post",
                                    'title' => 'Smazat'
                                ]
                            );
                        } else {
                            return null;
                        }
                    },
                ]
            );
        }

        return $sloupce;
    }

    /**
     * @return array
     */
    public function vratSloupcePolozky()
    {
        return [
            'nazev' => [
                //'attribute' => 'nazev',
                'label' => 'Položka',
                'value' => function($data) {
                    return Html::a(
                        sprintf('%s, materiál: %s', $data['model_nazev'], $data['material_nazev']),
                        array('/modely/default/detail', 'id' => $data['model_pk'], 'material' => $data['material_pk'])
                    );
                },
                'format' => 'raw',
                'contentOptions' => [
                    'class' => 'show-detail-column'
                ]
            ],
            'pocet' => [
                'attribute' => 'pocet',
                'label' => 'Počet'
            ],
            'hotovo' => [
                'attribute' => 'pocet_hotovo',
                'label' => 'Hotovo'
            ],
            'cena_ks' => [
                'class' => '\app\components\columns\PriceColumn',
                'attribute' => 'cena_ks',
                'label' => 'Cena za kus',
            ],
            'cena_celkem' => [
                'class' => '\app\components\columns\PriceColumn',
                'attribute' => 'cena_celkem',
                'label' => 'Cena celkem',
            ],
            'akce' => [
                'class' => 'app\components\columns\ActionColumn',
                'header' => 'Akce',
                'contentOptions' => ['class' => 'action-column'],
                'template' => '{hotovo}',
                'buttons' => [
                    'hotovo' => function($url, $model, $key) {
                        if ($model['pocet_hotovo'] < $model['pocet'] && in_array($model['stav_objednavky'], [Objednavka::STAV_POTVRZENO, Objednavka::STAV_ZPRACOVAVA])) {
                            return Html::a(
                                Html::tag('span', '', [
                                    'class' => 'glyphicon glyphicon-print',
                                ]),
                                '',
                                [
                                    'title' => 'Přidat vytištěné',
                                    'data-toggle' => 'modal',
                                    'data-target' => '#polozky-vytisteno-modal',
                                    'data-objednavka' => $model['objednavka_pk'],
                                    'data-material' => $model['material_pk'],
                                    'data-model' => $model['model_pk'],
                                ]
                            );
                        } else {
                            return null;
                        }
                    },
                ]
            ]
        ];
    }

    /**
     * Vraci mozne stavy pro zmenu objednavky na zaklade stareho stavu
     * todo uzivatel muze stornovat svym, admin muze stornovat svym
     * @param $stav
     * @return array
     * @throws \Exception
     */
    public function pravidlaStavObjednavky($stav = null)
    {
        if ($stav == null) {
            $stav = $this->stav;
        }

        $stavy = [
            self::STAV_KOSIK => [
                self::STAV_ZALOZENO,
                self::STAV_ZPRACOVAVA,
                self::STAV_STORNO_UZIVATEL,
                self::STAV_STORNO_SYSTEM
            ],
            self::STAV_ZALOZENO => [
                self::STAV_POTVRZENO,
                self::STAV_STORNO_UZIVATEL,
                self::STAV_STORNO_SYSTEM
            ],
            self::STAV_POTVRZENO => [
                self::STAV_ZPRACOVAVA,
                self::STAV_STORNO_SYSTEM
            ],
            self::STAV_ZPRACOVAVA => [
                self::STAV_HOTOVO,
                self::STAV_STORNO_SYSTEM
            ],
            self::STAV_HOTOVO => [
                self::STAV_ODESLANO,
                self::STAV_STORNO_SYSTEM
            ],
            self::STAV_VYFAKTUROVANO => [
                self::STAV_ZAPLACENO
            ],
            self::STAV_ODESLANO => [

            ],
            self::STAV_ZAPLACENO => [

            ],
            self::STAV_STORNO_UZIVATEL => [

            ],
            self::STAV_STORNO_SYSTEM => [

            ],
            self::STAV_SMAZANO => [

            ]
        ];

        if (in_array($stav, array_keys($stavy))) {
            $return = array();
            foreach ($stavy[$stav] as $jeden) {
                if ($jeden == self::STAV_STORNO_UZIVATEL) continue;
                $return[$jeden] = $this->itemAlias('stav', $jeden);
            }

            return $return;
        } else {
            throw new \Exception("[objednavka] neznamy stav ({$stav})");
        }
    }
}