<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 3.11.14
 * Time: 19:19
 */

namespace app\modules\objednavky\models;


use app\components\Model;
use yii\base\Exception;
use Yii;

/**
 * Class FakturacniUdaje
 * @package app\modules\objednavky\models
 *
 */
class FakturacniUdaje extends Model
{
    /**
     * @var
     */
    public $fakturacni_udaje_pk = null;

    /**
     * @var
     */
    public $adresa_pk;

    /**
     * @var
     */
    public $typ;

    /**
     * @var
     */
    public $ic;

    /**
     * @var
     */
    public $dic;

    /**
     * @var
     */
    public $obchodni_jmeno;

    /**
     * @var
     */
    public $jmeno;

    /**
     * @var
     */
    public $prijmeni;

    /**
     * @var
     */
    public $ulice;

    /**
     * @var
     */
    public $mesto;

    /**
     * @var
     */
    public $psc;

    /**
     * @var
     */
    public $zeme_id;

    /**
     * @var
     */
    public $zakaznik_pk;

    /**
     * @var
     */
    public $dodavatel_pk;

    /**
     * @var
     */
    public $faktura_pk;

    const TYP_FYZICKA_OSOBA = 'FO';
    const TYP_PRAVNICKA_OSOBA = 'PO';

    /**
     * @return $this
     */
    public static function model()
    {
        return parent::model(__CLASS__);
    }

    public function init()
    {
        // defaultni hodnota
        if (null == $this->typ) $this->typ = 'FO';
    }

    public function rules()
    {
        return array(
            array(array('fakturacni_udaje_pk', 'typ', 'ic', 'dic', 'obchodni_jmeno', 'zakaznik_pk'), 'safe'),
            array(
                array('jmeno', 'prijmeni', 'ulice', 'mesto', 'psc', 'zeme_id'),
                'required',
                'when' => function($model) {
                    return $model->fakturacni_udaje_pk == null;
                },
                'whenClient' => "function(attribute, value) {
                    return $('#fakturacniudaje-fakturacni_udaje_pk').val() == null;
                }"
            ),
            array(array('ic', 'obchodni_jmeno'), 'required', 'when' => function($model) {
                    return $model->typ == self::TYP_PRAVNICKA_OSOBA;
                }, 'whenClient' => "function (attribute, value) {
                    return $('#fakturacniudaje-typ').val() == '" . self::TYP_PRAVNICKA_OSOBA . "';
                }"
            ),
            array('psc', 'match', 'pattern' => '/^\d{5}$/i'),
            [['typ', 'ic', 'dic', 'obchodni_jmeno', 'jmeno', 'prijmeni', 'ulice', 'mesto', 'psc', 'zeme_id'], 'filter', 'filter' => function($value) {
                return $value == '' ? null : $value;
            }]
        );
    }

    public function attributeLabels()
    {
        return array(
            'fakturacni_udaje_pk' => '',
            'typ' => '',
            'ic' => 'IČ',
            'dic' => 'DIČ',
            'obchodni_jmeno' => 'Obchodní jméno',
            'jmeno' => 'Jméno',
            'prijmeni' => 'Příjmení',
            'ulice' => 'Ulice',
            'mesto' => 'Město',
            'psc' => 'PSČ',
            'zeme_id' => 'Stát'
        );
    }

    /**
     * @return bool|null|string
     * @throws Exception
     */
    public function uloz()
    {
        $checkNotNull = array('zakaznik_pk', 'dodavatel_pk', 'faktura_pk');
        $allNull = false; $allNotNull = false;
        foreach ($checkNotNull as $attr) {

        }

        if ($this->zakaznik_pk == null && $this->dodavatel_pk == null && $this->faktura_pk == null) {
            throw new Exception("nevim komu mam ulozit fakturacni udaje! zakaznik = ({$this->zakaznik_pk}) dodavatel = ({$this->dodavatel_pk})");
        }

        if ($this->fakturacni_udaje_pk == '') {
            $this->fakturacni_udaje_pk = null;
        }

        if ($this->ic == '---') {
            $this->ic = null;
        }

        if ($this->dic == '---') {
            $this->dic = null;
        }

        if ($this->zakaznik_pk != null) {
            $entita = 'zakaznik';
        } else if ($this->dodavatel_pk != null) {
            $entita = 'dodavatel';
        } else if ($this->faktura_pk != null) {
            $entita = 'faktura';
        } else {
            throw new Exception("nevim, komu ulozit fakturacni udaje, neni vyplnene zadne PK!");
        }

        $this->fakturacni_udaje_pk = $this->_ulozFakturacniUdaje($this->{$entita.'_pk'}, $entita);
        return $this->fakturacni_udaje_pk;
    }

    /**
     * @param $pk
     * @return FakturacniUdaje|null
     */
    public function nactiPodlePk($pk)
    {
        return $this->_nacti($pk);
    }

    /**
     * @param $pk
     * @return $this|null
     */
    public function nactiPodleZakaznika($pk) {
        $db = Yii::$app->db;
        $command = $db->createCommand("select fakturacni_udaje_pk from zakaznik_fakturacni_udaje where zakaznik_pk = :zakaznik");
        $command->bindValue('zakaznik', $pk);

        if (($udaje_pk = $command->queryScalar()) === false) {
            return null;
        } else {
            $this->zakaznik_pk = $pk;
            return $this->_nacti($udaje_pk);
        }
    }

    public function nactiProDropdown($entita, $entita_pk)
    {
        switch ($entita) {
            case 'zakaznik':
                $sql = "
                    select
                          fu.*
                        , coalesce(fu.obchodni_jmeno, fu.jmeno || ' '  || fu.prijmeni) || ', '
                            || fu.ulice || ', ' || fu.psc || ' ' || fu.mesto || ', ' || z.zeme_id as adresa
                    from fakturacni_udaje fu
                        join zakaznik_fakturacni_udaje zfu using(fakturacni_udaje_pk)
                        join zeme z using(zeme_id)
                    where zakaznik_pk = :zakaznik
                ";
                $params = array(
                    'zakaznik' => $entita_pk
                );
                break;
            case 'dodavatel':
                throw new Exception("nemohu nacist dodaci udaje pro dodavatele - dodavatele nejsou implementovani");
                break;
            default:
                throw new Exception("neznama entita pro nacteni dodacich udaju, entita = ({$entita}) pk = ({$entita_pk})");
        }

        $command = Yii::$app->db->createCommand($sql);
        $command->bindValues($params);

        $fakturacni_udaje = array();
        foreach ($command->queryAll() as $row) {
            $fakturacni_udaje[$row['fakturacni_udaje_pk']] = $row;
        }

        return $fakturacni_udaje;
    }

    /**
     * @param $datum
     * @return $this|FakturacniUdaje|null
     */
    public function nactiSystemoveProDatum($datum)
    {
        $sql = "
            SELECT fakturacni_udaje_pk FROM system_fakturacni_udaje
            WHERE platne_od <= :datum::DATE
                AND (platne_do IS NULL OR platne_do > :datum::DATE)
        ";

        $pk = Yii::$app->db->createCommand($sql, [
            ':datum' => $datum
        ])->queryScalar();

        if ($pk === false) {
            return null;
        }

        return $this->_nacti($pk);
    }

    /**
     * @param $entita_pk
     * @param $entita
     * @return bool|null|string
     * @throws \yii\base\Exception
     */
    protected function _ulozFakturacniUdaje($entita_pk, $entita)
    {
        if ($this->typ == FakturacniUdaje::TYP_FYZICKA_OSOBA) {
            $this->ic = null;
            $this->dic = null;
            $this->obchodni_jmeno = null;
        }

        $params = array(
            //'udaje' => [$this->fakturacni_udaje_pk, ($this->fakturacni_udaje_pk == null ? \PDO::PARAM_NULL : \PDO::PARAM_INT)],
            'typ' => $this->typ,
            'ic' => $this->ic,
            'dic' => $this->dic,
            'obchodni_jmeno' => $this->obchodni_jmeno,
            'jmeno' => $this->jmeno,
            'prijmeni' => $this->prijmeni,
            'ulice' => $this->ulice,
            'mesto' => $this->mesto,
            'psc' => $this->psc,
            'zeme' => $this->zeme_id,
            $entita => $entita_pk
        );

        switch ($entita) {
            case 'zakaznik':
                // todo upravit volani
                $sql = "select zakaznik_uloz_fakturacni_udaje(:udaje, :typ, :ic, :dic, :obchodni_jmeno, :jmeno, :prijmeni, :ulice, :mesto, :psc, :zeme, :zakaznik)";
                $params['udaje'] = [$this->fakturacni_udaje_pk, ($this->fakturacni_udaje_pk == null ? \PDO::PARAM_NULL : \PDO::PARAM_INT)];
                break;
            case 'dodavatel':
                $sql = "";
                throw new Exception('fakturacni udaje dodavatele nejsou zatim implementovany!');
                break;
            case 'faktura':
                // todo upravit volani
                $sql = 'select faktura_uloz_fakturacni_udaje(:faktura, :typ, :ic, :dic, :obchodni_jmeno, :jmeno, :prijmeni, :ulice, :mesto, :psc, :zeme)';
                break;
            default:
                throw new Exception("neznama entita pro ulozeni prechodove tabulky k fakturacnim udajum, entita = ({$entita}) pk = ({$entita_pk})");
        }

        $command = \Yii::$app->db->createCommand($sql);
        $command->bindValues($params);

        if (($vysledek = $command->queryScalar()) < 0) {
            throw new Exception("nepodarilo se ulozit fakturacni udaje, {$entita}_uloz_fakturacni_udaje() = $vysledek");
        } else {
            return $vysledek;
        }
    }

    /**
     * @param $udaje_pk
     * @return $this|null
     */
    protected function _nacti($udaje_pk) {
        $db = Yii::$app->db;
        $command = $db->createCommand("
            select
                fu.*
              --, a.*
            from fakturacni_udaje fu
              --join adresa a using (adresa_pk)
            where fu.fakturacni_udaje_pk = :udaje
        ");
        $command->bindValue('udaje', $udaje_pk);

        $udaje = $command->queryOne();
        if (false == $udaje) {
            return null;
        } else {
            foreach ($udaje as $atribut => $hodnota) {
                $this->$atribut = $hodnota;
            }

            return $this;
        }
    }

    /**
     * @return array
     */
    protected static function itemAliasData()
    {
        return array(
            'typ' => array(
                self::TYP_FYZICKA_OSOBA => 'Fyzická osoba',
                self::TYP_PRAVNICKA_OSOBA => 'Firma'
            ),
        );
    }

} 