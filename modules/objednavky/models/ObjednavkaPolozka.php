<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 10. 3. 2015
 * Time: 22:40
 */

namespace app\modules\objednavky\models;

use app\components\Model;

/**
 * Class Polozka
 * @package app\modules\objednavky\models
 */
class ObjednavkaPolozka extends Model
{
    /**
     * @var
     */
    public $objednavka_polozka_pk;

    /**
     * @var
     */
    public $objednavka_pk;

    /**
     * @var
     */
    public $material_pk;

    /**
     * @var
     */
    public $model_pk;

    /**
     * @var
     */
    public $pocet;

    /**
     * @var
     */
    public $pocet_hotovo;

    /**
     * @var
     */
    public $rozdil;

    /**
     * @var
     */
    public $doplnit;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['objednavka_pk', 'material_pk', 'model_pk', 'doplnit'], 'safe'],
            [['doplnit'], 'integer'],
            [['doplnit'], 'pocetValidator'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'doplnit' => 'Hotových ks'
        ];
    }

    /**
     * Vola databazovou proceduru pro doplneni hotovych vyrobku do objednavky.<br>
     * Procedura ma mnozstvi kontrolnich mechanizmu, vraci 0 v pripade uspechu nebo zaporne hodnoty, pokud doslo k nejake chybe.
     *
     * @throws \Exception
     * @return void
     */
    public function doplnVytisteneObjednavce()
    {
        $sql = "select polozka_vytisteno(:objednavka, :model, :material, :pocet)";
        $params = [
            'objednavka' => $this->objednavka_pk,
            'model'      => $this->model_pk,
            'material'   => $this->material_pk,
            'pocet'      => $this->doplnit
        ];

        $result = \Yii::$app->db->createCommand($sql)->bindValues($params)->queryScalar();

        if ($result < 0) {
            throw new \Exception('nenulovy navrat procedury polozka_vytistena(), parametry: ' . print_r($params, true));
        } else {
            return;
        }
    }

    /**
     * Nevim, jestli to budu nejak vyrazne pouzivat, ale je to takova obezlicka...
     * @param array $attributes
     * @return bool
     */
    public function maVyplneneAtributy($attributes = [])
    {
        if (empty($attributes)) {
            $attributes = $this->attributes();
        }

        $return = true;
        if (empty($attributes)) {
            return false;
        }

        foreach ($attributes as $attribute) {
            $return = $return && !is_null($this->$attribute);
        }

        return $return;
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function pocetValidator($attribute, $params)
    {
        $sql = "select pocet, pocet_hotovo, (pocet - pocet_hotovo) as rozdil from objednavka_polozka where objednavka_pk = :obj and material_pk = :mat and model_pk = :mod";
        $params = [
            'obj' => $this->objednavka_pk,
            'mat' => $this->material_pk,
            'mod' => $this->model_pk
        ];

        $result = \Yii::$app->db->createCommand($sql)->bindValues($params)->queryOne();

        if ($this->$attribute > $result['rozdil']) {
            $this->addError($attribute, "Můžete doplnit maximálně {$result['rozdil']} ks položky");
        }
    }
}