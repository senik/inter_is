<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 2.11.14
 * Time: 18:41
 */

namespace app\modules\objednavky\models;


use app\components\Log;
use app\components\Model;
use app\components\SqlDataProvider;
use yii\base\Exception;
use Yii;
use yii\db\Connection;

/**
 * Class Zakaznik
 * @package app\modules\objednavky\models
 */
class Zakaznik extends Model
{
    const SCENARIO_RYCHLE   = 'rychle-pridani';
    const SCENARIO_HLEDAT   = 'search';
    const SCENARIO_PRIDAT   = 'pridani';
    const SCENARIO_UPRAVIT  = 'upravit';

    /**
     * @var
     */
    public $zakaznik_pk;

    /**
     * @var
     */
    public $jmeno;

    /**
     * @var
     */
    public $prijmeni;

    /**
     * @var
     */
    public $telefon;

    /**
     * @var
     */
    public $email;

    /**
     * @var string slouzi pro filtr v prehledu zakazniku
     */
    public $ic;

    /**
     * @var DodaciUdaje
     */
    public $dodaci_udaje = null;

    /**
     * @var FakturacniUdaje
     */
    public $fakturacni_udaje = null;

    /**
     * @var array
     */
    protected $_errors = array();

    /**
     * @var Log
     */
    protected $_log;

    /**
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->_log = new Log(__CLASS__);
    }

    /**
     * @return Zakaznik
     */
    public static function model()
    {
        return new self;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        switch ($this->scenario) {
            case self::SCENARIO_HLEDAT:
                return array(
                    array(array('jmeno', 'email', 'ic'), 'safe')
                );
                break;
            case self::SCENARIO_RYCHLE:
                return array(
                    array(array('jmeno', 'prijmeni', 'telefon', 'email'), 'required')
                );
                break;
            default:
                return array(
                    array(array('jmeno', 'prijmeni', 'telefon', 'email'), 'required'),
                    array('email', 'email'),
                    //array('email', 'app\modules\objednavky\components\ZakaznikValidator'),
                    array(array('ic', 'zakaznik_pk'), 'safe')
                );
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array(
            'jmeno' => 'Jméno',
            'prijmeni' => 'Příjmení',
            'telefon' => 'Telefon',
            'email' => 'Email',
            'dodaci_udaje' => '',
            'fakturacni_udaje' => '',
            'ic' => 'IČ'
        );
    }

    public function scenarios()
    {
        $aScenarios = parent::scenarios();
        $aScenarios[self::SCENARIO_RYCHLE] = array('jmeno', 'prijmeni', 'telefon', 'email');
        $aScenarios[self::SCENARIO_HLEDAT] = array('jmeno', 'email', 'ic');

        return $aScenarios;
    }

    /**
     * @param int $pagination
     * @return SqlDataProvider
     */
    public function search($pagination = 10)
    {
        return $this->_vratDataProvider($pagination);
    }

    /**
     * @param null $pk
     * @return $this
     */
    public function nactiPodlePk($pk = null)
    {
        $zakaznik_pk = $pk == null ? $this->zakaznik_pk : $pk;

        return $this->nacti($zakaznik_pk);
    }

    /**
     * @param $zakaznik_pk
     * @throws \yii\base\Exception
     * @return $this
     */
    public function nacti($zakaznik_pk)
    {
        $db = Yii::$app->db;

        $command = $db->createCommand("select * from zakaznik where zakaznik_pk = :zakaznik");
        $command->bindValue('zakaznik', $zakaznik_pk);
        $zakaznik = $command->queryOne();
        if (false == $zakaznik) {
            return null;
        }

        foreach ($zakaznik as $atribut => $hodnota) {
            $this->$atribut = $hodnota;
        }

        if (null != $this->zakaznik_pk) {
            $this->dodaci_udaje = DodaciUdaje::model()->nactiPodleZakaznika($this->zakaznik_pk);
            $this->fakturacni_udaje = FakturacniUdaje::model()->nactiPodleZakaznika($this->zakaznik_pk);
        }

        return $this;
    }

    /**
     * @return bool
     * @throws \yii\base\Exception
     */
    public function uloz()
    {
        if ($this->dodaci_udaje != null && !($this->dodaci_udaje instanceof DodaciUdaje)) {
            throw new Exception('[zakaznik] dodaci udaje museji byt instanci modelu DodaciUdaje');
        }

        if ($this->fakturacni_udaje != null && !($this->fakturacni_udaje instanceof FakturacniUdaje)) {
            throw new Exception('[zakaznik] dodaci udaje museji byt instanci modelu FakturacniUdaje');
        }

        /** @var Connection $db */
        $db = Yii::$app->db;
        $transakce = $db->beginTransaction();

        try {
            $command = $db->createCommand("select zakaznik_uloz(:zakaznik, :jmeno, :prijmeni, :email, :telefon)");
            $command->bindParam('zakaznik', $this->zakaznik_pk, ($this->zakaznik_pk == null ? \PDO::PARAM_NULL : \PDO::PARAM_INT));
            $command->bindParam('jmeno', $this->jmeno);
            $command->bindParam('prijmeni', $this->prijmeni);
            $command->bindParam('email', $this->email);
            $command->bindParam('telefon', $this->telefon);

            $zakaznik_pk = $command->queryScalar();

            if ($zakaznik_pk > 0) {
                $this->zakaznik_pk = $zakaznik_pk;
                /**
                 * pokud je scenario jine nez rychle pridani, ukladam i dodaci a fakturacni udaje
                 * rychle pridani slouzi pro pridani zakaznika v ramci nove objednavky, tj bez vyplneni dalsich udaju
                 */
                if ($this->scenario != self::SCENARIO_RYCHLE) {
                    $this->dodaci_udaje->zakaznik_pk = $zakaznik_pk;
                    if (($du = $this->dodaci_udaje->uloz()) < 0) {
                        throw new Exception("[zakaznik] nepodarilo se ulozit dodaci udaje, vysledek ($du)");
                    }

                    $this->fakturacni_udaje->zakaznik_pk = $zakaznik_pk;
                    if (($fu = $this->fakturacni_udaje->uloz()) < 0) {
                        throw new Exception("[zakaznik] nepodarilo se ulozit fakturacni udaje, vysledek ($fu)");
                    }
                }

                $transakce->commit();

                return true;
            } else {
                switch ($zakaznik_pk) {
                    case -1:
                        throw new Exception("[zakaznik] zakaznik s emailem \"{$this->email}\" uz v systemu existuje.");
                        break;
                    default:
                        throw new Exception("[zakaznik] nepodarilo se zalozit zakaznika, zakaznik_uloz() = $zakaznik_pk");
                }
            }
        } catch (Exception $e) {
            $transakce->rollBack();
            $this->_errors[] = $e->getMessage();

            return false;
        }
    }

    /**
     * @param int $pagination
     * @return SqlDataProvider
     */
    protected function _vratDataProvider($pagination = 10)
    {
        $sql = "
            select
                  z.*
                , z.prijmeni || ' ' || z.jmeno as cele_jmeno
                , nullif(string_agg(fu.ic, ', '), '') as ic
            from zakaznik z
                left join zakaznik_fakturacni_udaje zfu on z.zakaznik_pk = zfu.zakaznik_pk
                left join fakturacni_udaje fu on zfu.fakturacni_udaje_pk = fu.fakturacni_udaje_pk
        ";

        $aParams = $aWhere = array();
        if ($this->jmeno != null) {
            $aParams[':jmeno'] = $this->jmeno;
            $aWhere[] = "(lower(z.jmeno || ' ' || z.prijmeni) ~ lower(:jmeno) or lower(z.prijmeni || ' ' || z.jmeno) ~ lower(:jmeno))";
        }

        if ($this->ic != null) {
            $aParams[':ic'] = $this->ic;
            $aWhere[] = "ic ~ :ic";
        }

        if (!empty($aWhere)) {
            $sql .= " where " . implode(" and ", $aWhere);
        }

        $sql .= " group by z.zakaznik_pk";

        $dataprovider = new SqlDataProvider(array(
            'sql' => $sql,
            'params' => $aParams,
            'sort' => array(
                'defaultOrder' => array('prijmeni' => SORT_ASC),
                'attributes' => array(
                    'zakaznik_pk', 'prijmeni', 'jmeno', 'email', 'telefon', 'ic'
                )
            ),
            'pagination' => $pagination == false ? $pagination : array(
                    'pageSize' => $pagination
                )
        ));

        return $dataprovider;
    }

    /**
     * @param bool $prompt
     * @return array
     */
    public function vratProDropdown($prompt = false)
    {
        $data = $this->search(false)->getModels();

        $zakaznici = $prompt ? array('' => 'Vyberte') : array();
        foreach ($data as $zakaznik) {
            $zakaznici[$zakaznik['zakaznik_pk']] = $zakaznik['cele_jmeno'];
        }

        return $zakaznici;
    }

    /**
     * @return array
     */
    public function vratSloupce()
    {
        return array(
            'jmeno' => array(
                'label' => $this->getAttributeLabel('jmeno'),
                'attribute' => 'jmeno'
            ),
            'prijmeni' => array(
                'label' => $this->getAttributeLabel('prijmeni'),
                'attribute' => 'prijmeni'
            ),
            'email' => array(
                'label' => $this->getAttributeLabel('email'),
                'attribute' => 'email'
            ),
            'telefon' => array(
                'label' => $this->getAttributeLabel('telefon'),
                'attribute' => 'telefon'
            ),
            'ic' => array(
                'label' => $this->getAttributeLabel('ic'),
                'attribute' => 'ic'
            ),
            'akce' => array(
                'class' => 'app\components\columns\ActionColumn',
                'template' => '{view} {update} {delete}',
                'urlCreator' => function ($action, $model, $key, $index) {
                        switch ($action) {
                            case 'view':
                                $url = array('/objednavky/zakaznici/detail', 'id' => $model['zakaznik_pk']);
                                break;
                            case 'update':
                                $url = array('/objednavky/zakaznici/upravit', 'id' => $model['zakaznik_pk']);
                                break;
                            case 'delete':
                                $url = array('/objednavky/zakaznici/smazat', 'id' => $model['zakaznik_pk']);
                                break;
                            default:
                                throw new Exception('Undefined action for model');
                        }
                        return $url;
                    },
                'header' => 'Akce',
            )
        );
    }

    /**
     * Metoda vraci posledni error zapsany do modelu
     * @return mixed
     */
    public function getLastError()
    {
        $keys = array_keys($this->_errors);
        $lastKey = end($keys);

        return isset($this->_errors[$lastKey]) ? $this->_errors[$lastKey] : null;
    }

}