<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 4.11.14
 * Time: 16:34
 */

namespace app\modules\objednavky\models;


use app\components\ActiveRecord;

/**
 * Class Adresa
 * @package app\modules\objednavky\models
 *
 * @property $adresa_pk;
 * @property $ulice;
 * @property $mesto;
 * @property $psc;
 */
class Adresa extends ActiveRecord
{

    public function rules() {
        return array(
            array(array('adresa_pk', 'ulice', 'mesto', 'psc'), 'safe')
        );
    }

    /**
     * @param $adresa_pk
     * @return Adresa
     */
    public static function nactiPodlePk($adresa_pk) {
        if ($adresa_pk != null) {
            $mAdresa = Adresa::findOne($adresa_pk);
            if ($mAdresa == null) $mAdresa = new self;
        } else {
            $mAdresa = new self;
        }

        return $mAdresa;
    }

} 