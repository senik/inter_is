<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 2.11.14
 * Time: 18:09
 */

namespace app\modules\objednavky\controllers;


use app\components\Application;
use app\components\Controller;
use app\modules\objednavky\models\DodaciUdaje;
use app\modules\objednavky\models\FakturacniUdaje;
use app\modules\objednavky\models\Objednavka;
use app\modules\objednavky\models\Zakaznik;
use yii\base\Exception;

/**
 * Class ZakazniciController
 * @package app\modules\objednavky\controllers
 */
class ZakazniciController extends Controller
{

    /**
     * @return string
     */
    public function actionIndex()
    {
        $mZakaznik = new Zakaznik(array('scenario' => 'search'));

        $get = \Yii::$app->request->get();

        if (!empty($get)) {
            $mZakaznik->load($get);
        }

        return $this->render('index', array(
            'mZakaznik' => $mZakaznik
        ));
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionDetail($id)
    {
        if (($mZakaznik = (new Zakaznik)->nacti($id)) == null) {
            Application::setFlashError("Neplatný požadavek");
            \Yii::warning("[zakaznik] detail zakaznika, id = ($id) neexistuje", __METHOD__);
            return $this->redirect(['/objednavky/zakaznici/index']);
        }

        $mObjednavka = new Objednavka();
        $mObjednavka->zakaznik_pk = $id;

        return $this->render('detail', array(
            'mZakaznik' => $mZakaznik,
            'mObjednavka' => $mObjednavka,
            'mFaUdaje' => new FakturacniUdaje(),
            'mDoUdaje' => new DodaciUdaje()
        ));
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function actionPridat()
    {
        $mZakaznik = new Zakaznik();
        $mFaUdaje = new FakturacniUdaje();
        $mDoUdaje = new DodaciUdaje();

        try {
            $post = \Yii::$app->request->post();
            // pokud mam data a podari se mi je nacist, tak pokracuji
            if (!empty($post)) {
                if ($mZakaznik->load($post) && $mFaUdaje->load($post) && $mDoUdaje->load($post)) {
                    $valid = $mDoUdaje->validate();
                    if ($valid) {
                        $mZakaznik->dodaci_udaje = $mDoUdaje;
                    }

                    $valid = $valid && $mFaUdaje->validate();
                    if ($valid) {
                        $mZakaznik->fakturacni_udaje = $mFaUdaje;
                    }

                    if ($valid && $mZakaznik->validate()) {
                        if ($mZakaznik->uloz()) {
                            Application::setFlashSuccess("Zákazník {$mZakaznik->jmeno} {$mZakaznik->prijmeni} byl uložen");
                            return $this->redirect(array('/objednavky/zakaznici/index'));
                        } else {
                            throw new \Exception("nepodarilo se ulozit zakaznika");
                        }
                    }
                } else {
                    Application::setFlashError("Neplatný požadavek");
                    \Yii::error("[zakaznik] nepodarilo se nacist data pri vytvareni noveho, post: " . var_export($post, true));
                }
            }
        } catch (Exception $e) {
            $error = $mZakaznik->getLastError();
            Application::setFlashError("Omlouváme se, při ukládání zákazníka došlo k chybě. Zkuste akci opakovat později.");
            \Yii::error("[zakaznik] {$e->getMessage()}: {$error}");
        }

        return $this->render('pridat', array(
            'mZakaznik' => $mZakaznik,
            'mFaUdaje' => $mFaUdaje,
            'mDoUdaje' => $mDoUdaje,
        ));
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws \Exception
     */
    public function actionUpravit($id)
    {
        // pokud zakaznik neexistuje, tak nema smysl cokoliv delat...
        if (($mZakaznik = (new Zakaznik)->nacti($id)) == null) {
            Application::setFlashError("Neplatný požadavek");
            \Yii::warning("[zakaznik] editace zakaznika, id = ($id) neexistuje");
            return $this->redirect(['/objednavky/zakaznici/index']);
        }

        $mFaUdaje = $mZakaznik->fakturacni_udaje != null ? $mZakaznik->fakturacni_udaje : new FakturacniUdaje();
        $mDoUdaje = $mZakaznik->dodaci_udaje != null ? $mZakaznik->dodaci_udaje : new DodaciUdaje();

        try {
            $post = \Yii::$app->request->post();
            if (!empty($post)) {
                if ($mZakaznik->load($post) && $mFaUdaje->load($post) && $mDoUdaje->load($post)) {
                    // slaba ochrana, ale alespon neco
                    if ($id != $mZakaznik->zakaznik_pk) {
                        throw new \Exception("[zakaznik] uzivatel id = " . (\Yii::$app->user->id) . " se snazi editovat jineho zakaznika, puvodni = ({$id}), data = (". var_export($mZakaznik->zakaznik_pk, true) . ")");
                    }

                    if ($mDoUdaje->validate()) $mZakaznik->dodaci_udaje = $mDoUdaje;
                    if ($mFaUdaje->validate()) $mZakaznik->fakturacni_udaje = $mFaUdaje;

                    if ($mZakaznik->validate()) {
                        if ($mZakaznik->uloz()) {
                            Application::setFlashSuccess('Zákazník byl uložen');
                            return $this->redirect(array('/objednavky/zakaznici/index'));
                        } else {
                            Application::setFlashError("Omlouváme se, při ukládání zákazníka došlo k chybě, opakujte akci později.");
                        }
                    }
                } else {
                    Application::setFlashError("Neplatný požadavek");
                    \Yii::error("[zakaznik] nepodarilo se nacist data pri editaci, post = (" . var_export($post, true) . ")");
                }
            }
        } catch (Exception $e) {
            $error = $mZakaznik->getLastError();
            Application::setFlashError("Omlouváme se, při ukládání zákazníka došlo k chybě. Zkuste akci opakovat později.");
            \Yii::error("[zakaznik] {$e->getMessage()}: {$error}");
        }

        return $this->render('upravit', array(
            'mZakaznik' => $mZakaznik,
            'mFaUdaje' => $mFaUdaje,
            'mDoUdaje' => $mDoUdaje,
        ));
    }

}