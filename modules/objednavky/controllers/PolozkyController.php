<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 10. 3. 2015
 * Time: 22:45
 */

namespace app\modules\objednavky\controllers;


use app\components\Application;
use app\components\Controller;
use app\modules\objednavky\models\Objednavka;
use app\modules\objednavky\models\ObjednavkaPolozka;
use kartik\widgets\ActiveForm;
use Yii;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Class PolozkyController
 * @package app\modules\objednavky\controllers
 */
class PolozkyController extends Controller
{
    /**
     * Mno mozna by to chtelo refactor, ale to az jestli to budu resit AJAXem
     *
     * @return \yii\web\Response
     * @throws HttpException
     */
    public function actionVytisteno()
    {
        $mPolozka = new ObjednavkaPolozka();
        $post = \Yii::$app->request->post();

        if (Yii::$app->request->isAjax && $mPolozka->load($post)) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($mPolozka);
        }

        if (!\Yii::$app->request->isPost) {
            throw new HttpException(400, 'Bad request');
        }

        try {
            if ($mPolozka->load($post)) {
                $mObjednavka = (new Objednavka())->nactiPodlePk($mPolozka->objednavka_pk);

                if ($mObjednavka == null) {
                    throw new HttpException(400, 'Bad request');
                }

                $attrs = ['objednavka_pk', 'model_pk', 'material_pk', 'doplnit'];
                if (!$mPolozka->maVyplneneAtributy($attrs)) {
                    throw new HttpException(400, 'Bad request');
                }

                $mPolozka->doplnVytisteneObjednavce();

                Application::setFlashSuccess("Hotové položky doplněny k objednávce.");
                return $this->redirect(['/objednavky/default/detail', 'id' => $mObjednavka->cislo]);
            } else {
                throw new \Exception('nepodarilo se naplnit polozku daty: ' . print_r($post, true));
            }
        } catch (HttpException $e) {
            throw $e;
        } catch (\Exception $e) {
            Application::setFlashError("Vyskytla se chyba. Opakujte prosím akci později.");
            Yii::error("[polozka] action vytisteno, chyba: {$e->getMessage()}");

            if (isset($mObjednavka)) {
                $url = ['/objednavky/default/detail', 'id' => $mObjednavka->cislo];
            } else {
                $url = ['/objednavky/default/index'];
            }

            return $this->redirect($url);
        }
    }
}