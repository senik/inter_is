<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 2.11.14
 * Time: 18:05
 */

namespace app\modules\objednavky\controllers;


use app\components\Application as App;
//use app\components\Application;
use app\components\Application;
use app\components\Controller;
use app\modules\faktury\models\Faktura;
use app\modules\modely\models\Model3D;
use app\modules\objednavky\models\DodaciUdaje;
use app\modules\objednavky\models\FakturacniUdaje;
use app\modules\objednavky\models\Objednavka;
use app\modules\objednavky\models\Zakaznik;
use Yii;
use Exception;
use yii\helpers\Json;

/**
 * Class DefaultController
 * @package app\modules\objednavky\controllers
 */
class DefaultController extends Controller
{

    /**
     * Výpis objednávek
     * @return string
     */
    public function actionIndex()
    {
        $mObjednavka = new Objednavka(array('scenario' => 'search'));

        $get = Yii::$app->request->get();

        if (!empty($get) && $mObjednavka->load($get)) {
            $mObjednavka->validate();
        }

        return $this->render('index', array(
            'mObjednavka' => $mObjednavka
        ));
    }

    /**
     * @param $id
     * @return string
     */
    public function actionDetail($id)
    {
        $mObjednavka = (new Objednavka())->nactiPodleCisla($id);
        $mZakaznik = (new Zakaznik())->nactiPodlePk($mObjednavka->zakaznik_pk);

        $mDoUdaje = new DodaciUdaje();
        $mFaUdaje = new FakturacniUdaje();

        if (!$mObjednavka->jeKompletni() && $mObjednavka->muzuEditovat()) {
            App::setFlashWarning('Objednávka nemá vyplněné některé údaje, nebude možné ji vyfakturovat!');
        }

        return $this->render('detail', [
            'mObjednavka' => $mObjednavka,
            'mZakaznik' => $mZakaznik,
            'mDoUdaje' => $mDoUdaje,
            'mFaUdaje' => $mFaUdaje
        ]);
    }

    /**
     * zakladani nove objednavky
     */
    public function actionPridat()
    {
        $mObjednavka = new Objednavka();
        $mZakaznik = new Zakaznik();
        $mModel = new Model3D(array('scenario' => Model3D::SCENARIO_OBJEDN));

        $post = Yii::$app->request->post();

        if (!empty($post)) {
            $mObjednavka->load($post);

            if (empty($mObjednavka->polozky)) {
                App::setFlashError("Nelze odeslat objednávku bez položek.");
            } else {
                if ($mObjednavka->validate()) {
                    if (($pk = $mObjednavka->uloz()) > 0) {
                        App::setFlashSuccess("Objednávka úspěšně založena.");
                    } else {
                        switch ($pk) {
                            case -1:
                                $chyba = "fakturacni udaje nenalezeny";
                                break;
                            case -2:
                                $chyba = "dodaci udaje nenalezeny";
                                break;
                            default:
                                $chyba = "neznama chyba";
                        }

                        App::setFlashError("Objednávku se nepodařilo založit: {$chyba}!");
                    }
                    $this->redirect(array('/objednavky/default/index'));
                }
            }
        } else {
            $mObjednavka->cislo = Objednavka::generujCislo();
        }

        return $this->render('pridat', array(
            'mObjednavka' => $mObjednavka,
            'mZakaznik' => $mZakaznik,
            'mModel' => $mModel
        ));
    }

    /**
     * Ajax fce pro vykresleni nove polozky objednavky
     */
    public function actionPridatPolozku()
    {
        // dostanu material_pk a model_pk
        $post = Yii::$app->request->post();
        $db = Yii::$app->db;

        $response = [];
        try {
            // data o modelu
            $dataModel = $db->createCommand("select * from model3d where model_pk = :pk")->bindValue('pk', $post['model_pk'])->queryOne();
            if ($dataModel === false) {
                throw new Exception("model neexistuje (nejspis)");
            }
            // data o materialu
            $dataMater = $db->createCommand("select * from material_skladem_view where material_pk = :pk")->bindValue('pk', $post['material_pk'])->queryOne();
            if ($dataMater === false) {
                throw new Exception("material neexistuje (nejspis)");
            }

            // todo cely nejak zpracovat sql dotazem
            $cena = round(($dataModel['objem'] * $dataMater['hustota'] * $dataMater['cena']), 2);
            $input = $this->renderPartial('_form_polozka_objednavky', [
                'model' => $post['model_pk'],
                'material' => $post['material_pk'],
                'kusu' => 1,
                'nazev' => "{$dataModel['nazev']}, {$dataMater['nazev_dlouhy']}",
                'cena' => $cena
            ]);

            $response['success'] = 1;
            $response['polozka'] = $input;
            $response['cena'] = $cena;
        } catch (Exception $e) {
            $response['error'] = $e->getMessage();
        }

        return Json::encode($response);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws Exception
     */
    public function actionUpravit($id)
    {
        try {
            /** @var Objednavka $mObjednavka */
            $mObjednavka = (new Objednavka())->nactiPodleCisla($id);
            $mModel = new Model3D(array('scenario' => Model3D::SCENARIO_OBJEDN));

            if ($mObjednavka == null) {
                throw new Exception("[objednavka] editace, neexistujici objednavka s cislem: {$id}");
            }

            if ($mObjednavka->stav != Objednavka::STAV_ZALOZENO) {
                App::setFlashWarning("Objednávku nelze upravit, je ve stavu " . Objednavka::itemAlias('stav', $mObjednavka->stav));
                return $this->redirect(['/objednavky/default/detail', 'id' => $mObjednavka->cislo]);
            }

            if ($mObjednavka->vlozil != Yii::$app->user->id) {
                App::setFlashError("Na tuto akci nemáte oprávnění.");
                return $this->redirect(['/objednavky/default/index']);
            }

            $post = Yii::$app->request->post();

            if ($mObjednavka->load($post) && $mObjednavka->validate()) {
                if ($mObjednavka->uloz()) {
                    App::setFlashSuccess('Objednávka byla upravena.');
                    return $this->redirect(['/objednavky/default/detail', 'id' => $mObjednavka->cislo]);
                }
            }

            return $this->render('upravit', [
                'mObjednavka' => $mObjednavka,
                'mModel' => $mModel
            ]);
        } catch (Exception $e) {
//            Application::setFlashError($e->getMessage());
//            return $this->redirect(['/objednavky/default/index']);
            throw $e;
        }
    }

    /**
     * Smazani objednavky, fakticky tu objednavku nemazu, pouze ji stornuju systemem (nebo ji dat stav SMAZANO?)
     *
     * @param $id
     * @return \yii\web\Response
     */
    public function actionSmazat($id)
    {
        $mObjednavka = (new Objednavka())->nactiPodlePk($id);

        switch ($mObjednavka->zmenStav(Objednavka::STAV_STORNO_SYSTEM)) {
            case $id:
                App::setFlashSuccess('Stav objednávky byl změněn.');
                break;
            case -3:
                App::setFlashError('Nelze změnit stav objednávky, nemá vyplněné některé údaje.');
                break;
            case -1:
            case -2:
            default:
                App::setFlashError('Nepodařilo se změnit stav objednávky.');
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Zmena stavu objednavky, zatim neresim AJAX, proste prectu POST a redirectim zpet na detail objednavky
     * @return \yii\web\Response
     * @throws \Exception
     */
    public function actionZmenStav()
    {
        $post = Yii::$app->request->post();
        $error = null;

        if (!isset($post['Objednavka']['objednavka_pk'])) {
            $error = "Neplatný požadavek!";
        } else {
            $pk = $post['Objednavka']['objednavka_pk'];
        }

        if (!isset($post['Objednavka']['stav'])) {
            $error = "Neplatný požadavek!";
        } else {
            $stav = $post['Objednavka']['stav'];
        }

        if ($error != null) {
            App::setFlashError($error);
            return $this->redirect(array('/objednavky/default/index'));
        } else {
            $mObjednavka = (new Objednavka())->nactiPodlePk($pk);

            if ($mObjednavka == null) {
                throw new \Exception("[objednavka] objednavka s pk = {$pk} neexistuje");
            }

            switch ($mObjednavka->zmenStav($stav)) {
                case $pk:
                    App::setFlashSuccess('Stav objednávky byl změněn.');
                    break;
                case -3:
                    App::setFlashError('Nelze změnit stav objednávky, nemá vyplněné některé údaje.');
                    break;
                case -1:
                case -2:
                default:
                    App::setFlashError('Nepodařilo se změnit stav objednávky.');
            }

            return $this->redirect(array('/objednavky/default/detail', 'id' => $mObjednavka->cislo));
        }
    }

    /**
     * Zmena dodacich udaju objednavky.<br>
     * Bud mi prijdou v postu dodaci_udaje_pk nebo data pro nove dodaci udaje. Pokud jsou dodaci_udaje_pk = null, pokusim se ulozit nove.<br>
     * Ve vysledku bych mel mit vzdy naplnene pk, udaje pak v jedne procedure nactu a ulozim k objednavce.
     *
     * @return \yii\web\Response
     * @throws \Exception
     */
    public function actionZmenDodaciUdaje()
    {
        $post = Yii::$app->request->post();
        if (empty($post)) {
            App::setFlashError("Neplatný požadavek!");
            return $this->redirect(['/objednavky/default/index']);
        }

        $mObjednavka = new Objednavka();
        $mDodaciUdaje = new DodaciUdaje();

        try {
            if ($mDodaciUdaje->load($post) && $mObjednavka->load($post)) {
                if ($mObjednavka->nactiPodlePk() == null) {
                    throw new Exception("[objednavka] chyba pri zmene dodacich udaju, nepodarilo se nacist objednavku, post: " . print_r($post, true));
                }

                if ($mObjednavka->zmenDodaciUdaje($mObjednavka->objednavka_pk, $mDodaciUdaje)) {
                    App::setFlashSuccess("Dodací údaje byly úspěšně změněny.");
                    return $this->redirect(['/objednavky/default/detail', 'id' => $mObjednavka->cislo]);
                }
            }

            throw new Exception("[objednavka] chyba pri zmene dodacich udaju, dosel jsem az na konec, post: " . print_r($post, true));
        } catch (Exception $e) {
            Yii::error($e->getMessage(), __METHOD__);
            App::setFlashError("Omlouváme se, při změně dodacích údajů došlo k chybě. Zkuste akci opakovat později.");
            return $this->redirect(
                $mObjednavka->cislo != null ? ['/objednavky/default/detail', 'id' => $mObjednavka->cislo] : ['/objednavky/default/index']
            );
        }
    }

    /**
     * Zmena fakturacnich udaju objednavky.<br>
     * Bud mi prijdou v postu fakturacni_udaje_pk nebo data pro nove fakturacni udaje. Pokud jsou fakturacni_udaje_pk = null, pokusim se ulozit nove.<br>
     * Ve vysledku bych mel mit vzdy naplnene pk, udaje pak v jedne procedure nactu a ulozim k objednavce.
     *
     * @return \yii\web\Response
     * @throws \Exception
     */
    public function actionZmenFakturacniUdaje()
    {
        $post = Yii::$app->request->post();
        if (empty($post)) {
            App::setFlashError("Neplatný požadavek!");
            return $this->redirect(['/objednavky/default/index']);
        }

        $mObjednavka = new Objednavka();
        $mFakturacniUdaje = new FakturacniUdaje();

        try {
            if ($mObjednavka->load($post) && $mFakturacniUdaje->load($post)) {
                if ($mObjednavka->nactiPodlePk() == null) {
                    throw new Exception("[objednavka] chyba pri zmene fakturacnich udaju, nepodarilo se nacist objednavku, post: " . print_r($post, true));
                }

                if ($mObjednavka->zmenFakturacniUdaje(null, $mFakturacniUdaje)) {
                    App::setFlashSuccess("Fakturační údaje byly úspěšně změněny.");
                    return $this->redirect(['/objednavky/default/detail', 'id' => $mObjednavka->cislo]);
                }
            }

            throw new Exception("[objednavka] chyba pri zmene fakturacnich udaju, dosel jsem az na konec, post: " . print_r($post, true));
        } catch (Exception $e) {
            Yii::error($e->getMessage(), __METHOD__);
            App::setFlashError("Vyskytla se chyba, nepodařilo se změnit fakturační údaje.");
            return $this->redirect(
                $mObjednavka->cislo != null ? ['/objednavky/default/detail', 'id' => $mObjednavka->cislo] : ['/objednavky/default/index']
            );
        }
    }

    /**
     * Vygeneruje fakturu k objednávce. Přidáno kvůli mazání faktur, aby bylo možné z objednávky vygenerovat novou.
     *
     * @param $id
     * @return \yii\web\Response
     */
    public function actionVygenerujFakturu($id)
    {
        /** @var Objednavka $mObjednavka */
        $mObjednavka = (new Objednavka())->nactiPodlePk($id);

        if ($mObjednavka == null) {
            Application::setFlashError("Neexistující objednávka!");
            return $this->redirect(['/objednavky/default/index']);
        }

        if ($mObjednavka->maFakturu()) {
            Application::setFlashInfo("K objednávce již existuje faktura.");
            return $this->redirect(['/objednavky/default/detail', 'id' => $mObjednavka->cislo]);
        } else {
            $mFaktura = new Faktura();

            $pk = $mFaktura->ulozZobjednavky($mObjednavka->objednavka_pk);

            if ($pk) {
                Application::setFlashSuccess("Faktura k objednávce č. {$mObjednavka->cislo} byla vytvořena.");
                return $this->redirect(['/faktury/default/detail', 'id' => $pk]);
            } else {
                Application::setFlashError("Chyba při generování faktury, opakujte akci později.");
                return $this->redirect(['/objednavky/default/detail', 'id' => $mObjednavka->cislo]);
            }
        }
    }

    /**
     * @return string
     */
    public function actionAjaxDetailModal()
    {
        $post = Yii::$app->request->post();
        $response = [];

        try {
            if (!isset($post['id'])) {
                Yii::error('[objednavkyController] ajax volani detailu objednavky bez ID v requestu!');
                throw new Exception('Chybný požadavek!');
            } else {
                $id = $post['id'];
            }

            $mObjednavka = (new Objednavka())->nactiPodlePk($id);
            $mZakaznik = (new Zakaznik())->nactiPodlePk($mObjednavka->zakaznik_pk);

            if ($mObjednavka == null) {
                Yii::error("[objednavkyController] ajax volani detailu objednavky, neexistujici objednavka ($id)");
                throw new Exception('Neexistující objednávka');
            }

            $response['header'] = "<h3>Objednávka č. {$mObjednavka->cislo}</h3>";
            $response['body'] = $this->renderPartial('_modal_detail', [
                'mObjednavka' => $mObjednavka,
                'mZakaznik' => $mZakaznik
            ]);
        } catch (Exception $e) {
            $response['error'] = $e->getMessage();
        }

        return Json::encode($response);
    }
}