<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 14.11.14
 * Time: 21:43
 */

namespace app\modules\objednavky\components;


use app\modules\objednavky\models\Zakaznik;
use yii\validators\Validator;
use Yii;


/**
 * Class ZakaznikValidator
 * @package app\modules\objednavky\models
 */
class ZakaznikValidator extends Validator
{
    public function init()
    {
        parent::init();
        $this->message = 'Zákazník s tímto emailem už v systému existuje.';
    }

    /**
     * @param Zakaznik $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        if ($this->_existujeZakaznik($model, $attribute)) {
            $model->addError($attribute, $this->message);
        }
    }

    /**
     * @param Zakaznik $model
     * @param string $attribute
     * @param \yii\web\View $view
     * @return string
     */
    public function clientValidateAttribute($model, $attribute, $view)
    {
        //if ($this->_existujeZakaznik($model, $attribute)) {
            $message = json_encode($this->message, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
            return <<<JS
            window.console.log("{$model->$attribute}", "{$view}");
    messages.push($message);
JS;
        //}
    }

    /**
     * @param Zakaznik $model
     * @param string $attribute
     * @return bool
     */
    protected function _existujeZakaznik($model, $attribute) {
        $sql = "select 1 from zakaznik where trim(both ' ' from lower(email)) = trim(both ' ' from lower(:attr))";
        $params = array(
            'attr' => $model->$attribute
        );

        if ($model->zakaznik_pk == '') $model->zakaznik_pk = null;

        if ($model->zakaznik_pk != null) {
            $sql .= " and zakaznik_pk <> :zakaznik";
            $params['zakaznik'] = $model->zakaznik_pk;
        }

        $command = Yii::$app->db->createCommand($sql);
        $command->bindValues($params);
        return ($command->queryScalar() == 1);
    }
} 