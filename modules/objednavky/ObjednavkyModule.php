<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 17.10.14
 * Time: 19:43
 */

namespace app\modules\objednavky;


use app\components\ModuleInterface;
use yii\base\Module;

class ObjednavkyModule extends Module implements ModuleInterface
{
    /**
     * @var string
     */
    public $controllerNamespace = 'app\modules\objednavky\controllers';

    /**
     * @var
     */
    public $name = "Objednávky";

    /**
     * @var array
     */
    public $baseUrl = array('/objednavky/default/index');

    public function vratMenuModulu()
    {
        return array(
            array('label' => 'Zákazníci', 'url' => array('/objednavky/zakaznici/index')),
            array('label' => 'Objednávky', 'url' => array('/objednavky/default/index')),
        );
    }
}