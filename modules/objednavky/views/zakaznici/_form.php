<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 2.11.14
 * Time: 20:12
 *
 * @var $mZakaznik Zakaznik
 * @var $mFaUdaje FakturacniUdaje
 * @var $mDoUdaje DodaciUdaje
 * @var $this View
 */

use app\components\ciselniky\Zeme;
use app\modules\objednavky\models\DodaciUdaje;
use app\modules\objednavky\models\FakturacniUdaje;
use app\modules\objednavky\models\Zakaznik;
use kartik\widgets\ActiveForm;
//use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

$form = ActiveForm::begin(array(
    'id' => 'zakaznik-pridat-form',
    'type' => 'horizontal',
    'formConfig' => array(
        'labelSpan' => 3,
        'deviceSize' => ActiveForm::SIZE_SMALL
    )
));

$adresy = DodaciUdaje::model()->nactiProDropdown('zakaznik', $mZakaznik->zakaznik_pk);
$adresyDropdown = array('' => 'Přidat údaje', '-1' => 'Stejné jako fakturační');

foreach ($adresy as $key => $row) {
    $adresyDropdown[$key] = $row['adresa'];
}

$action = Yii::$app->controller->action->id;
$this->registerJs("
    var dorucovaci_adresy = " . json_encode($adresy) . ";
", View::POS_HEAD);
?>
    <div class="form-fields osobni-udaje">
        <div class="row">
            <div class="col-sm-6">
                <?= Html::activeHiddenInput($mZakaznik, 'zakaznik_pk', ['value' => $mZakaznik->zakaznik_pk]) ?>
                <?= $form->field($mZakaznik, 'jmeno') ?>
                <?= $form->field($mZakaznik, 'prijmeni') ?>
            </div>

            <div class="col-sm-6">
                <?= $form->field($mZakaznik, 'telefon') ?>
                <?= $form->field($mZakaznik, 'email') ?>
            </div>
        </div>
    </div>

    <div class="form-fields">
        <div class="row">
            <div class="col-sm-6 fakturacni-udaje">
                <h3>Fakturační údaje</h3>
                <?= $form->field($mFaUdaje, 'typ')->dropDownList(FakturacniUdaje::itemAlias('typ'), ['class' => 'selectpicker']) ?>
                <?php $display = $mFaUdaje->typ == 'FO' ? 'display: none;' : '' ?>
                <div class="udaje-firma" style="<?= $display ?>">
                    <?= $form->field($mFaUdaje, 'ic', array(
                        'addon' => array(
                            'append' => array(
                                'content' => Html::button(
                                        '<i class="glyphicon glyphicon-search"></i> Vyhledat v ARES',
                                        array('class' => 'btn btn-info', 'id' => 'ares-search')
                                    ),
                                'asButton' => true
                            )
                        )
                    )) ?>
                    <?= $form->field($mFaUdaje, 'dic') ?>
                    <?= $form->field($mFaUdaje, 'obchodni_jmeno') ?>
                </div>
                <div class="spolecne">
                    <?= $form->field($mFaUdaje, 'jmeno', array(
                        'addon' => array(
                            'append' => array(
                                'content' => Html::button(
                                    '<i class="glyphicon glyphicon-copy"></i>',
                                    array('class' => 'btn btn-info jmeno-copy', 'title' => 'Zkopírovat sem jméno')
                                ),
                                'asButton' => true
                            )
                        )
                    )) ?>
                    <?= $form->field($mFaUdaje, 'prijmeni', array(
                        'addon' => array(
                            'append' => array(
                                'content' => Html::button(
                                    '<i class="glyphicon glyphicon-copy"></i>',
                                    array('class' => 'btn btn-info prijmeni-copy', 'title' => 'Zkopírovat sem příjmení')
                                ),
                                'asButton' => true
                            )
                        )
                    )) ?>
                    <?= $form->field($mFaUdaje, 'ulice') ?>
                    <?= $form->field($mFaUdaje, 'mesto') ?>
                    <?= $form->field($mFaUdaje, 'psc') ?>
                    <?= $form->field($mFaUdaje, 'zeme_id')->dropDownList(Zeme::itemAlias('zeme'), ['class' => 'selectpicker']) ?>
                </div>
            </div>

            <div class="col-sm-6 dodaci-udaje">
                <h3>Doručovací údaje</h3>
                <?= $form->field($mDoUdaje, 'dodaci_udaje_pk')->dropDownList($adresyDropdown, ['class' => 'selectpicker']) ?>
                <div class="adresa" style="">
                    <?= Html::activeHiddenInput($mDoUdaje, 'adresa_pk') ?>
                    <?= $form->field($mDoUdaje, 'firma') ?>
                    <?= $form->field($mDoUdaje, 'jmeno', array(
                        'addon' => array(
                            'append' => array(
                                'content' => Html::button(
                                    '<i class="glyphicon glyphicon-copy"></i>',
                                    array('class' => 'btn btn-info jmeno-copy', 'title' => 'Zkopírovat sem jméno')
                                ),
                                'asButton' => true
                            )
                        )
                    )) ?>
                    <?= $form->field($mDoUdaje, 'prijmeni', array(
                        'addon' => array(
                            'append' => array(
                                'content' => Html::button(
                                    '<i class="glyphicon glyphicon-copy"></i>',
                                    array('class' => 'btn btn-info prijmeni-copy', 'title' => 'Zkopírovat sem příjmení')
                                ),
                                'asButton' => true
                            )
                        )
                    )) ?>
                    <?= $form->field($mDoUdaje, 'ulice') ?>
                    <?= $form->field($mDoUdaje, 'mesto') ?>
                    <?= $form->field($mDoUdaje, 'psc') ?>
                    <?= $form->field($mDoUdaje, 'zeme_id')->dropDownList(Zeme::itemAlias('zeme'), ['class' => 'selectpicker']) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="form-actions well">
        <?php
        echo Html::submitButton('Uložit', array(
            'class' => 'btn btn-success'
        ));
        echo Html::resetButton(($action == 'upravit' ? 'Původní hodnoty' : 'Vymazat formulář'), array(
            'class' => 'btn btn-primary'
        ));
        echo Html::a('Zrušit', array('/objednavky/zakaznici/index'), array(
            'class' => 'btn btn-danger'
        ));
        ?>
    </div>
<?php
ActiveForm::end();
$urlManager = Yii::$app->urlManager;
$url = $urlManager->createAbsoluteUrl(['/ajax/ares']);

$this->registerJs("
$('#dodaciudaje-dodaci_udaje_pk').on('change', function() {
    var val = $(this).val();
    if (val == '') {
        $('#dodaciudaje-adresa_pk').val('');
        $('#dodaciudaje-ulice').val('').removeAttr('readonly');
        $('#dodaciudaje-mesto').val('').removeAttr('readonly');
        $('#dodaciudaje-psc').val('').removeAttr('readonly');
        $('#dodaciudaje-zeme_id').val('CZ').removeAttr('disabled').selectpicker('refresh');
    } else if (val == '-1') {
        $('#dodaciudaje-adresa_pk').val('');
        $('#dodaciudaje-ulice').val($('#fakturacniudaje-ulice').val()).attr('readonly', 'readonly');
        $('#dodaciudaje-mesto').val($('#fakturacniudaje-mesto').val()).attr('readonly', 'readonly');
        $('#dodaciudaje-psc').val($('#fakturacniudaje-psc').val()).attr('readonly', 'readonly');
        $('#dodaciudaje-zeme_id').val($('#fakturacniudaje-zeme_id').val()).attr('disabled', 'disabled').selectpicker('refresh');
    } else {
        $('#dodaciudaje-adresa_pk').val(dorucovaci_adresy[val]['adresa_pk']);
        $('#dodaciudaje-ulice').val(dorucovaci_adresy[val]['ulice']).removeAttr('readonly');
        $('#dodaciudaje-mesto').val(dorucovaci_adresy[val]['mesto']).removeAttr('readonly');
        $('#dodaciudaje-psc').val(dorucovaci_adresy[val]['psc']).removeAttr('readonly');
        $('#dodaciudaje-zeme_id').val(dorucovaci_adresy[val]['zeme_id']).attr('disabled', 'disabled').selectpicker('refresh');
    }
});

// --- je otazka, jestli to necham takhle nebo to budu resit jinak ---
$('#fakturacniudaje-ulice').on('keyup', function() {
    var ulice = $('#dodaciudaje-ulice');
    var dodaci_pk = $('#dodaciudaje-dodaci_udaje_pk').val();
    if (dodaci_pk == '-1' && ulice.length > 0) {
        ulice.val($(this).val());
    }
});

$('#fakturacniudaje-mesto').on('keyup', function() {
    var mesto = $('#dodaciudaje-mesto');
    var dodaci_pk = $('#dodaciudaje-dodaci_udaje_pk').val();
    if (dodaci_pk == '-1' && mesto.length > 0) {
        mesto.val($(this).val());
    }
});

$('#fakturacniudaje-psc').on('keyup', function() {
    var psc = $('#dodaciudaje-psc');
    var dodaci_pk = $('#dodaciudaje-dodaci_udaje_pk').val();
    if (dodaci_pk == '-1' && psc.length > 0) {
        psc.val($(this).val());
    }
});

$('#fakturacniudaje-zeme_id').on('change', function() {
    var zeme = $('#dodaciudaje-zeme_id');
    var dodaci_pk = $('#dodaciudaje-dodaci_udaje_pk').val();
    if (dodaci_pk == '-1') {
        zeme.val($(this).val()).selectpicker('refresh');
    }
});
/// --- ---------------------------------------------------------- ---
$('#ares-search').on('click', function(ev) {
    ev.preventDefault();
    var input_id = $(this).closest('div.input-group').find('input')[0].id;

    var ic = $('#' + input_id).val();
    if (ic != '') {
        var form = $(this).closest('form');

        form.addClass('loading');

        var request = $.ajax({
            url: \"{$url}\",
            type: \"POST\",
            data: {ico: ic}
        });

        request.done(function(response) {
            var data = $.parseJSON(response);
            window.console.log(data);
            if (data.success == 1) {
                var firma = data.firma;

                $('#fakturacniudaje-obchodni_jmeno').val(firma.obchodni_jmeno);
                $('#fakturacniudaje-ulice').val(firma.ulice);
                $('#fakturacniudaje-mesto').val(firma.mesto);
                $('#fakturacniudaje-psc').val(firma.psc);
            } else {
                alert(data.error);
            }

            form.removeClass('loading');
        });

        request.fail(function(jqXHR, textStatus) {
            resetujUdaje();

            alert('Request failed: ' + textStatus);
        });
    }
});
");