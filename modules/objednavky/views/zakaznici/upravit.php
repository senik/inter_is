<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 4.11.14
 * Time: 22:37
 *
 * @var $this View
 * @var $mZakaznik Zakaznik
 * @var $mFaUdaje FakturacniUdaje
 * @var $mDoUdaje DodaciUdaje
 */

use app\modules\objednavky\models\DodaciUdaje;
use app\modules\objednavky\models\FakturacniUdaje;
use app\modules\objednavky\models\Zakaznik;
use yii\web\View;

$this->title = Yii::$app->name . ' - Upravit zákazníka';

$this->params['breadcrumbs'] = array(
    array('label' => 'Zákaznící', 'url' => array('/objednavky/zakaznici/index')),
    'Upravit'
);
?>

<div class="h2-buttons">
    <h2>Upravit zákazníka <?= $mZakaznik->jmeno ?> <?= $mZakaznik->prijmeni ?></h2>
    <div class="clearfix"></div>
</div>

<?= $this->render('_form', array(
    'mZakaznik' => $mZakaznik,
    'mFaUdaje' => $mFaUdaje,
    'mDoUdaje' => $mDoUdaje
)) ?>