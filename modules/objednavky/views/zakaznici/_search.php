<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 2.11.14
 * Time: 20:12
 *
 * @var $mZakaznik Zakaznik
 */

use app\modules\objednavky\models\Zakaznik;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin(array(
    'method' => 'get',
    'action' => array('/objednavky/zakaznici/index'),
    'layout' => 'horizontal',
    'options' => array(
        'class' => 'well'
    )
));
?>
<div class="row">
    <div class="col-sm-4">
        <?= $form->field($mZakaznik, 'jmeno', array('horizontalCssClasses' => array(
            'wrapper' => 'col-sm-9',
        ))) ?>
    </div>

    <div class="col-sm-4">
        <?= $form->field($mZakaznik, 'ic', array('horizontalCssClasses' => array(
            'wrapper' => 'col-sm-9',
        ))) ?>
    </div>

    <div class="col-sm-4">
        <?= $form->field($mZakaznik, 'email', array('horizontalCssClasses' => array(
            'wrapper' => 'col-sm-9',
        ))) ?>
    </div>
</div>

<div class="form-actions">
    <?php
    echo Html::submitButton('Filtrovat', array('class' => 'btn btn-success'));
    echo Html::submitButton('Vymazat filtr', array('class' => 'btn btn-warning filter-reset'));
    ?>
</div>

<?php
ActiveForm::end();