<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 29.12.14
 * Time: 9:48
 *
 * todo rozhodit doručovací a fakturovací údaje do radio-listů a dát pod sebe
 *
 * @var $mZakaznik Zakaznik
 * @var $mObjednavka Objednavka
 * @var $mDoUdaje DodaciUdaje
 * @var $mFaUdaje FakturacniUdaje
 * @var $this View
 */

use app\components\ciselniky\Zeme;
use app\components\Html;
use app\modules\objednavky\models\DodaciUdaje;
use app\modules\objednavky\models\FakturacniUdaje;
use app\modules\objednavky\models\Objednavka;
use app\modules\objednavky\models\Zakaznik;
use kartik\widgets\ActiveForm;
use yii\bootstrap\Modal;
use yii\web\View;

$celeJmeno = $mZakaznik->prijmeni . ' ' . $mZakaznik->jmeno;

$this->title = Yii::$app->name . ' - ' . $celeJmeno;

$this->params['breadcrumbs'] = array(
    array('label' => 'Zákaznící', 'url' => array('/objednavky/zakaznici/index')),
    $celeJmeno
);
?>

<div class="h2-buttons">
    <h2>Detail zákazníka <?= $celeJmeno ?></h2>
    <div class="clearfix"></div>
</div>

<div id="zakaznik-detail">
    <div class="row">
        <div class="col-sm-4">
            <div class="h3-buttons">
                <h3>Kontaktní údaje</h3>
                <div class="clearfix"></div>
            </div>
            <?= Html::activeHiddenInput($mZakaznik, 'jmeno') ?>
            <?= Html::activeHiddenInput($mZakaznik, 'prijmeni') ?>
            <address>
                <ul>
                    <li><abbr title="Jméno">J:</abbr> <?= $mZakaznik->jmeno ?></li>
                    <li><abbr title="Příjmení">P:</abbr> <?= $mZakaznik->prijmeni ?></li>
                    <li><abbr title="Telefon">T:</abbr> <?= $mZakaznik->telefon ?></li>
                    <li><abbr title="E-mail">@:</abbr> <a href="mailto:<?= $mZakaznik->email ?>"><?= $mZakaznik->email ?></a></li>
                </ul>
            </address>
        </div>
    </div>

    <div class="row">
    <div class="col-sm-6">
        <div class="h3-buttons">
            <h3>Doručovací údaje</h3>
            <?php
            echo Html::a(
                'Přidat',
                '#',
                array(
                    'class' => 'btn btn-sm btn-success',
                    'data-toggle' => "modal",
                    'data-target' => "#zakaznik-dodaci-modal"
                )
            );
            ?>
            <div class="clearfix"></div>
        </div>

        <?php
        $dodaci = $mDoUdaje->nactiProDropdown('zakaznik', $mZakaznik->zakaznik_pk);

        if (!empty($dodaci)) {
            $dodaciDropdown = \yii\helpers\ArrayHelper::map($dodaci, 'dodaci_udaje_pk', 'adresa');
            $disabled = false;
        } else {
            $dodaciDropdown = array('' => 'Žádné údaje');
            $disabled = true;
        }

        $this->registerJs("var do_udaje = " . \yii\helpers\Json::encode($dodaci) . ";", View::POS_HEAD);

        echo Html::beginTag('div', array('class' => 'udaje-wrapper'));
        {
            echo Html::dropDownList('zakaznik_dodaci_udaje', null, $dodaciDropdown, array(
                'disabled' => $disabled,
                'class' => 'selectpicker'
            ));
            echo Html::beginTag('div', array('class' => 'btn-wrapper'));
            {
                echo Html::a('<i class="glyphicon glyphicon-new-window"></i>', null, array(
                    'id' => 'dodaci-show-button',
                    'class' => 'btn btn-primary xtooltip',
                    'title' => 'Zobrazit v novém okně',
                    'disabled' => $disabled
                ));
            }
            echo Html::endTag('div');
        }
        echo Html::endTag('div');

        /* --- modální okno s formulářem - nový zákazník --- */
        Modal::begin([
            'header' => '<h3>Dodací údaje</h3>',
            'id' => 'zakaznik-dodaci-modal'
        ]);

        $form = ActiveForm::begin(array(
            'id' => 'zakaznik-dodaci-form',
            'type' => 'horizontal',
            'action' => array('/ajax/zakaznik-uloz-dodaci'),
            'method' => 'POST',
            'formConfig' => array(
                'labelSpan' => 3,
                'deviceSize' => ActiveForm::SIZE_SMALL
            )
        ));
        ?>
        <div class="form-fields">
            <?= $form->field($mDoUdaje, 'zakaznik_pk')->hiddenInput(['value' => $mZakaznik->zakaznik_pk]) ?>
            <?= $form->field($mDoUdaje, 'dodaci_udaje_pk')->hiddenInput() ?>
            <?= $form->field($mDoUdaje, 'firma') ?>
            <?= $form->field($mDoUdaje, 'jmeno', array(
                'addon' => array(
                    'append' => array(
                        'content' => Html::button(
                            '<i class="glyphicon glyphicon-copy"></i>',
                            array('class' => 'btn btn-info jmeno-copy', 'title' => 'Zkopírovat sem jméno')
                        ),
                        'asButton' => true
                    )
                )
            )) ?>
            <?= $form->field($mDoUdaje, 'prijmeni', array(
                'addon' => array(
                    'append' => array(
                        'content' => Html::button(
                            '<i class="glyphicon glyphicon-copy"></i>',
                            array('class' => 'btn btn-info prijmeni-copy', 'title' => 'Zkopírovat sem příjmení')
                        ),
                        'asButton' => true
                    )
                )
            )) ?>
            <?= $form->field($mDoUdaje, 'ulice') ?>
            <?= $form->field($mDoUdaje, 'mesto') ?>
            <?= $form->field($mDoUdaje, 'psc') ?>
            <?= $form->field($mDoUdaje, 'zeme_id')->dropDownList(Zeme::itemAlias('zeme'), ['class' => 'selectpicker']) ?>
        </div>

        <div class="form-actions">
            <?php
            echo Html::submitButton('Uložit', array(
                'class' => 'btn btn-success'
            ));
            echo Html::a('Zrušit', '#', array(
                'class' => 'btn btn-danger modal-close'
            ));
            ?>
        </div>
        <?php
        ActiveForm::end();

        Modal::end();
        $this->registerJs("
        $('#dodaci-show-button').on('click', function(ev) {
            ev.preventDefault();
            var do_sel = $('select[name=\"zakaznik_dodaci_udaje\"]').val();

            $('#dodaciudaje-dodaci_udaje_pk').val(do_udaje[do_sel]['dodaci_udaje_pk']);
            $('#dodaciudaje-firma').val(do_udaje[do_sel]['firma']);
            $('#dodaciudaje-jmeno').val(do_udaje[do_sel]['jmeno']);
            $('#dodaciudaje-prijmeni').val(do_udaje[do_sel]['prijmeni']);
            $('#dodaciudaje-ulice').val(do_udaje[do_sel]['ulice']);
            $('#dodaciudaje-mesto').val(do_udaje[do_sel]['mesto']);
            $('#dodaciudaje-psc').val(do_udaje[do_sel]['psc']);
            $('#dodaciudaje-zeme_id').val(do_udaje[do_sel]['zeme_id']);

            $('#zakaznik-dodaci-modal').modal('show');
        });

        $('#zakaznik-dodaci-modal').on('hide.bs.modal', function() {
            $('input[name^=\"DodaciUdaje\"]', '#zakaznik-dodaci-form').each(function() {
                // todo chtelo by to elegantnejsi reseni
                if ($(this).attr('name') != 'DodaciUdaje[zakaznik_pk]') {
                    $(this).val('');
                }
            });
        });

        $('#zakaznik-dodaci-form').on('beforeSubmit', function() {
            $(this).addClass('loading');

            var request = $.ajax({
                url: $(this).attr('action'),
                type: $(this).attr('method'),
                data: $(this).serialize()
            });

            request.done(function(response) {
                var data = $.parseJSON(response);

                if (data.error != undefined) {
                    alert(data.error);
                } else {
                    if (data.success > 0) {
                        $('option', 'select[name=\"zakaznik_dodaci_udaje\"]').each(function() {
                            $(this).remove();
                        });

                        $.each(data.do_udaje, function(i, v) {
                            var selected = (data.success == v.pk) ? 'selected' : '';
                            $('select[name=\"zakaznik_dodaci_udaje\"]').append('<option value=\"'+v.pk+'\" '+selected+'>'+v.udaje+'</option>');
                        });
                        $('select[name=\"zakaznik_dodaci_udaje\"]').attr('disabled', false).selectpicker('refresh');
                        $('#dodaci-show-button').attr('disabled', false);
                    }
                }
            });

            request.fail(function(jqXHR, textStatus) {
                alert('Request failed: ' + textStatus);
            });

            $(this).removeClass('loading');
            $('#zakaznik-dodaci-modal').modal('hide');

//            window.location.reload();
            return false;
        }).on('submit', function(ev) {
            ev.preventDefault();
        });
        ");
        /* --- ----------------------------------------- --- */
        ?>
    </div>

    <div class="col-sm-6">
        <div class="h3-buttons">
            <h3>Fakturační údaje</h3>
            <?php
            echo Html::a(
                'Přidat',
                '#',
                array(
                    'class' => 'btn btn-sm btn-success',
                    'data-toggle' => "modal",
                    'data-target' => "#zakaznik-fakturacni-modal"
                )
            );
            ?>
            <div class="clearfix"></div>
        </div>

        <?php
        $fakturacni = $mFaUdaje->nactiProDropdown('zakaznik', $mZakaznik->zakaznik_pk);

        if (!empty($fakturacni)) {
            $fakturacniDropdown = \yii\helpers\ArrayHelper::map($fakturacni, 'fakturacni_udaje_pk', 'adresa');
            $disabled = false;
        } else {
            $fakturacniDropdown = array('' => 'Žádné údaje');
            $disabled = true;
        }

        $this->registerJs("var fa_udaje = " . \yii\helpers\Json::encode($fakturacni) . ";", View::POS_HEAD);

        echo Html::beginTag('div', array('class' => 'udaje-wrapper'));
        {
            echo Html::dropDownList('zakaznik_fakturacni_udaje', null, $fakturacniDropdown, array(
                'disabled' => $disabled,
                'class' => 'selectpicker'
            ));
            echo Html::beginTag('div', array('class' => 'btn-wrapper'));
            {
                echo Html::a('<i class="glyphicon glyphicon-new-window"></i>', '', array(
                    'id' => 'fakturacni-show-button',
                    'class' => 'btn btn-primary xtooltip',
                    'title' => 'Zobrazit v novém okně',
                    'disabled' => $disabled
                ));
            }
            echo Html::endTag('div');
        }
        echo Html::endTag('div');

        /* --- modální okno s formulářem - nový zákazník --- */
        Modal::begin([
            'header' => '<h3>Fakturační údaje</h3>',
            'id' => 'zakaznik-fakturacni-modal'
        ]);

        $form = ActiveForm::begin(array(
            'id' => 'zakaznik-fakturacni-form',
            'type' => 'horizontal',
            'action' => array('/ajax/zakaznik-uloz-fakturacni'),
            'method' => 'POST',
            'formConfig' => array(
                'labelSpan' => 3,
                'deviceSize' => ActiveForm::SIZE_SMALL
            )
        ));
        ?>
        <div class="form-fields">
            <?= $form->field($mFaUdaje, 'zakaznik_pk')->hiddenInput(['value' => $mZakaznik->zakaznik_pk]) ?>
            <?= $form->field($mFaUdaje, 'fakturacni_udaje_pk')->hiddenInput() ?>
            <?= $form->field($mFaUdaje, 'typ')->dropDownList(FakturacniUdaje::itemAlias('typ'), ['class' => 'selectpicker']) ?>
            <?php $display = $mFaUdaje->typ == 'FO' ? 'display: none;' : '' ?>
            <div class="udaje-firma" style="<?= $display ?>">
                <?= $form->field($mFaUdaje, 'ic', array(
                    'addon' => array(
                        'append' => array(
                            'content' => Html::button(
                                    '<i class="glyphicon glyphicon-search"></i> Vyhledat v ARES',
                                    array('class' => 'btn btn-info', 'id' => 'ares-search')
                                ),
                            'asButton' => true
                        )
                    )
                )) ?>
                <?= $form->field($mFaUdaje, 'dic') ?>
                <?= $form->field($mFaUdaje, 'obchodni_jmeno') ?>
            </div>
            <div class="spolecne">
                <?= $form->field($mFaUdaje, 'jmeno', array(
                    'addon' => array(
                        'append' => array(
                            'content' => Html::button(
                                '<i class="glyphicon glyphicon-copy"></i>',
                                array('class' => 'btn btn-info jmeno-copy', 'title' => 'Zkopírovat sem jméno')
                            ),
                            'asButton' => true
                        )
                    )
                )) ?>
                <?= $form->field($mFaUdaje, 'prijmeni', array(
                    'addon' => array(
                        'append' => array(
                            'content' => Html::button(
                                '<i class="glyphicon glyphicon-copy"></i>',
                                array('class' => 'btn btn-info prijmeni-copy', 'title' => 'Zkopírovat sem příjmení')
                            ),
                            'asButton' => true
                        )
                    )
                )) ?>
                <?= $form->field($mFaUdaje, 'ulice') ?>
                <?= $form->field($mFaUdaje, 'mesto') ?>
                <?= $form->field($mFaUdaje, 'psc') ?>
                <?= $form->field($mFaUdaje, 'zeme_id')->dropDownList(Zeme::itemAlias('zeme'), ['class' => 'selectpicker']) ?>
            </div>
        </div>

        <div class="form-actions">
            <?php
            echo Html::submitButton('Uložit', array(
                'class' => 'btn btn-success'
            ));
            echo Html::a('Zrušit', '#', array(
                'class' => 'btn btn-danger modal-close'
            ));
            ?>
        </div>
        <?php
        ActiveForm::end();

        Modal::end();
        $this->registerJs("
        $('#fakturacni-show-button').on('click', function(ev) {
            ev.preventDefault();
            var fa_sel = $('select[name=\"zakaznik_fakturacni_udaje\"]').val();
            var fa_typ = fa_udaje[fa_sel]['typ'];

            if (fa_typ == 'PO') {
                $('div.udaje-firma').show();
            } else {
                $('div.udaje-firma').hide();
            }

            $('#fakturacniudaje-fakturacni_udaje_pk').val(fa_udaje[fa_sel]['fakturacni_udaje_pk']);

            $('#fakturacniudaje-typ').val(fa_typ).selectpicker('refresh');

            $('#fakturacniudaje-ic').val(fa_udaje[fa_sel]['ic']);
            $('#fakturacniudaje-dic').val(fa_udaje[fa_sel]['dic']);
            $('#fakturacniudaje-obchodni_jmeno').val(fa_udaje[fa_sel]['obchodni_jmeno']);

            $('#fakturacniudaje-jmeno').val(fa_udaje[fa_sel]['jmeno']);
            $('#fakturacniudaje-prijmeni').val(fa_udaje[fa_sel]['prijmeni']);
            $('#fakturacniudaje-ulice').val(fa_udaje[fa_sel]['ulice']);
            $('#fakturacniudaje-mesto').val(fa_udaje[fa_sel]['mesto']);
            $('#fakturacniudaje-psc').val(fa_udaje[fa_sel]['psc']);
            $('#fakturacniudaje-zeme_id').val(fa_udaje[fa_sel]['zeme_id']);

            $('#zakaznik-fakturacni-modal').modal('show');
        });

        $('#zakaznik-fakturacni-modal').on('hide.bs.modal', function() {
            $('input[name^=\"FakturacniUdaje\"]', '#zakaznik-fakturacni-form').each(function() {
                // todo chtelo by to elegantnejsi reseni
                if ($(this).attr('name') != 'FakturacniUdaje[zakaznik_pk]') {
                    $(this).val('');
                }
            });
        });

        $('#zakaznik-fakturacni-form').on('beforeSubmit', function() {
            $(this).addClass('loading');

            var request = $.ajax({
                url: $(this).attr('action'),
                type: $(this).attr('method'),
                data: $(this).serialize()
            });

            request.done(function(response) {
                var data = $.parseJSON(response);

                if (data.error != undefined) {
                    alert(data.error);
                } else {
                    if (data.success > 0) {
                        $('option', 'select[name=\"zakaznik_fakturacni_udaje\"]').each(function() {
                            $(this).remove();
                        });

                        $.each(data.fa_udaje, function(i, v) {
                            var selected = (data.success == v.pk) ? 'selected' : '';
                            $('select[name=\"zakaznik_fakturacni_udaje\"]').append('<option value=\"'+v.pk+'\" '+selected+'>'+v.udaje+'</option>');
                        });
                        $('select[name=\"zakaznik_fakturacni_udaje\"]').attr('disabled', false).selectpicker('refresh');
                        $('#fakturacni-show-button').attr('disabled', false);
                    }
                }
            });

            request.fail(function(jqXHR, textStatus) {
                alert('Request failed: ' + textStatus);
            });

            $(this).removeClass('loading');
            $('#zakaznik-fakturacni-modal').modal('hide');

            //window.location.reload();
            return false;
        }).on('submit', function(ev) {
            ev.preventDefault();
        });

        ");
        /* --- ----------------------------------------- --- */
        ?>
    </div>

    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="h3-buttons">
            <h3>Objednávky zákazníka</h3>
            <div class="clearfix"></div>
        </div>
        <?php
        $columns = $mObjednavka->vratSloupce();
        unset($columns['zakaznik']);

        echo \kartik\grid\GridView::widget([
            'dataProvider' => $mObjednavka->search(),
            'columns' => $columns,
            'id' => 'objednavky-zakaznika',
            'resizableColumns' => false,
            'export' => false
        ]);
        ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="h3-buttons">
            <h3>Vystavené faktury</h3>
            <div class="clearfix"></div>
        </div>
        <?php
//        $columns = $mObjednavka->vratSloupce();
//        unset($columns['zakaznik']);
//
//        echo \kartik\grid\GridView::widget([
//            'dataProvider' => $mObjednavka->search(),
//            'columns' => $columns,
//            'resizableColumns' => false,
//            'export' => false
//        ]);
        ?>
    </div>
</div>

<?php
//$this->registerJs("
//$('.modal-close').on('click', function() {
//    var modal = $(this).closest('div[id$=\"-modal\"]');
//    $(modal).modal('hide');
//});
//");