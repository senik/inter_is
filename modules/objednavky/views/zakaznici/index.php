<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 2.11.14
 * Time: 18:09
 *
 * @var $mZakaznik Zakaznik
 * @var $this View
 */

use app\modules\objednavky\models\Zakaznik;
use yii\helpers\Html;
use yii\web\View;

$this->title = Yii::$app->name . ' - Zákazníci';

$this->params['breadcrumbs'] = array(
    'Zákazníci'
);

$provider = $mZakaznik->search();
?>

<div class="h2-buttons">
    <h2>Zákazníci</h2>
    <?php
    echo Html::a(
        'Přidat',
        array('/objednavky/zakaznici/pridat'),
        array(
            'class' => 'btn btn-success'
        )
    );
    ?>
    <div class="clearfix"></div>
</div>

<div class="search">
    <?= $this->render('_search', array('mZakaznik' => $mZakaznik)) ?>
</div>

<?= \kartik\grid\GridView::widget(array(
    'dataProvider' => $provider,
    'columns' => $mZakaznik->vratSloupce(),
    'resizableColumns' => false,
    'export' => false
)) ?>