<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 1.1.15
 * Time: 17:18
 *
 * @var $this View
 * @var $mObjednavka Objednavka
 * @var $mZakaznik Zakaznik
 * @var $mDoUdaje DodaciUdaje
 * @var $mFaUdaje FakturacniUdaje
 */

use app\components\Html;
use app\modules\objednavky\models\DodaciUdaje;
use app\modules\objednavky\models\FakturacniUdaje;
use app\modules\objednavky\models\Objednavka;
use app\modules\objednavky\models\ObjednavkaPolozka;
use app\modules\objednavky\models\Zakaznik;
use kartik\widgets\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\web\View;

$this->title = Yii::$app->name . ' - Objednávka ' . $mObjednavka->cislo;

$this->params['breadcrumbs'] = array(
    array('label' => 'Objednávky', 'url' => array('/objednavky/default/index')),
    $mObjednavka->cislo
);
?>

<div class="h2-buttons">
    <h2>Objednávka <?= $mObjednavka->cislo ?></h2>
    <?php
    if ($mObjednavka->muzuEditovat()) {
        echo Html::a(
            'Změnit stav',
            '#',
            array(
                'class' => 'btn btn-success',
                'data-toggle' => 'modal',
                'data-target' => '#objednavka-stav-modal'
            )
        );
    }

    if (!$mObjednavka->maFakturu() && $mObjednavka->stav >= Objednavka::STAV_HOTOVO) {
        echo Html::a(
            'Vygenerovat fakturu',
            ['/objednavky/default/vygeneruj-fakturu', 'id' => $mObjednavka->objednavka_pk],
            array(
                'class' => 'btn btn-warning',
            )
        );
    }
    ?>
    <div class="clearfix"></div>
</div>

<div class="row objednavka-detail-udaje">
    <div class="col-sm-4 well">
<!--        <div class="well">-->
            <h4>Základní info</h4>
            <ul class="zakladni-udaje">
                <li>Zákazník: <?= Html::a(sprintf("%s %s", $mZakaznik->prijmeni, $mZakaznik->jmeno), ['/objednavky/zakaznici/detail', 'id' => $mZakaznik->zakaznik_pk]) ?></li>
                <li>Stav objednávky: <b><?= Objednavka::itemAlias('stav', $mObjednavka->stav) ?></b></li>
                <li class="<?php if ($mObjednavka->jePoTerminu()) { echo 'warn'; } ?>">Termín: <b><?= Yii::$app->formatter->asDate($mObjednavka->termin) ?></b></li>
                <li>Dodání: </li><!-- < ?= Objednavka::itemAlias('typ_platby', $mObjednavka->zpusob_platby) ?> -->
                <li>Platba: </li>
            </ul>
<!--        </div>-->
    </div>

    <div class="col-sm-4 well">
<!--        <div class="well">-->
            <div class="h4-buttons">
                <h4>Dodací údaje</h4>
                <?php
                if ($mObjednavka->muzuEditovat()) {
                    echo Html::a(
                        Html::tag('span', '', ['class' => 'glyphicon glyphicon-pencil']),
                        '#',
                        array(
                            'title' => 'Změnit',
                            'data-toggle' => 'modal',
                            'data-target' => '#objednavka-dodaci-modal',
                        )
                    );
                }
                ?>
                <div class="clearfix"></div>
            </div>
            <?php
            if ($mObjednavka->maVyplneneDodaci()) {
                echo '<ul class="dodaci-udaje">';
                {
                    echo "<li>{$mObjednavka->mDodaciUdaje->prijmeni} {$mObjednavka->mDodaciUdaje->jmeno}</li>";
                    $do_udaje = array('ulice', 'mesto', 'psc');
                    foreach ($do_udaje as $attr) {
                        if ($mObjednavka->mDodaciUdaje->$attr != null) echo "<li>{$mObjednavka->mDodaciUdaje->$attr}</li>";
                    }
                }
                echo '</ul>';
            }
            ?>
<!--        </div>-->
    </div>

    <div class="col-sm-4 well">
<!--        <div class="well">-->
            <div class="h4-buttons">
                <h4>Fakturační údaje</h4>
                <?php
                if ($mObjednavka->muzuEditovat()) {
                    echo Html::a(
                        Html::tag('span', '', ['class' => 'glyphicon glyphicon-pencil']),
                        '#',
                        array(
                            'title' => 'Změnit',
                            'data-toggle' => 'modal',
                            'data-target' => '#objednavka-fakturacni-modal'
                        )
                    );
                }
                ?>
                <div class="clearfix"></div>
            </div>
            <?php
            if ($mObjednavka->maVyplneneFakturacni()) {
                echo '<ul class="fakturacni-udaje">';
                {
                    echo "<li>{$mObjednavka->mFakturacniUdaje->prijmeni} {$mObjednavka->mFakturacniUdaje->jmeno}</li>";
                    $fa_udaje = array('obchodni_jmeno', 'ic', 'dic', 'ulice', 'mesto', 'psc');
                    foreach ($fa_udaje as $attr) {
                        if ($mObjednavka->mFakturacniUdaje->$attr != null) echo "<li>{$mObjednavka->mFakturacniUdaje->$attr}</li>";
                    }
                }
                echo '</ul>';
            }
            ?>
<!--        </div>-->
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="h3-buttons">
            <h3>Položky objednávky</h3>
            <div class="clearfix"></div>
        </div>
        <?php
        echo \kartik\grid\GridView::widget([
            'dataProvider' => $mObjednavka->vratPolozkyObjednavky(),
            'columns' => $mObjednavka->vratSloupcePolozky(),
            'resizableColumns' => false,
            'layout' => '{items}',
            'export' => false
        ]);
        ?>
        <div class="objednavka-cena well">
            Celková cena objednávky: <span class="cena"><?= number_format($mObjednavka->vratCelkovouCenu(), 2, ',', ' ') ?> Kč</span>
        </div>
    </div>
</div>

<?php
$udaje = array('' => 'Přidat údaje');
$mPolozka = new ObjednavkaPolozka();
/* --- modální okno s formulářem - změna počty vytištěných modelů --- */
Modal::begin([
    'header' => '<h3>Přidat vytištěné</h3>',
    'id' => 'polozky-vytisteno-modal'
]);

$form = ActiveForm::begin(array(
    'id' => 'objednavka-vytisteno-form',
    'type' => 'horizontal',
    'action' => array('/objednavky/polozky/vytisteno'),
    'method' => 'POST',
    'enableAjaxValidation' => true,
    'formConfig' => array(
        'labelSpan' => 3,
        'deviceSize' => ActiveForm::SIZE_SMALL
    )
));
?>
    <div class="form-fields">
        <?= Html::activeHiddenInput($mPolozka, 'objednavka_pk', ['value' => $mPolozka->objednavka_pk]) ?>
        <?= Html::activeHiddenInput($mPolozka, 'material_pk', ['value' => $mPolozka->material_pk]) ?>
        <?= Html::activeHiddenInput($mPolozka, 'model_pk', ['value' => $mPolozka->model_pk]) ?>
        <?= $form->field($mPolozka, 'doplnit')->textInput(['value' => 1]) ?>
    </div>

    <div class="form-actions">
        <?php
        echo Html::submitButton('Uložit', array(
            'class' => 'btn btn-success'
        ));
        echo Html::a('Zrušit', '#', array(
            'class' => 'btn btn-danger modal-close'
        ));
        ?>
    </div>
<?php
ActiveForm::end();

Modal::end();
$this->registerJs("
$(document).on('click', 'a[data-target=\"#polozky-vytisteno-modal\"]', function(ev) {
    ev.preventDefault();

    var link = $(this);
    $('input[id=\"objednavkapolozka-objednavka_pk\"]').val(link.attr('data-objednavka'));
    $('input[id=\"objednavkapolozka-material_pk\"]').val(link.attr('data-material'));
    $('input[id=\"objednavkapolozka-model_pk\"]').val(link.attr('data-model'));

    $('#polozky-vytisteno-modal').modal('show');
});

$('#polozky-vytisteno-modal').on('hide.bs.modal', function() {
    $('input[name^=\"ObjednavkaPolozka\"]', '#objednavka-vytisteno-form').each(function() {
        if ($(this).attr('type') == 'text') {
            $(this).val(1);
        } else {
            $(this).val('');
        }
    });
});
");
/* --- ----------------------------------------- --- */

/* --- modální okno s formulářem - změna stavu objednávky --- */
Modal::begin([
    'header' => '<h3>Změna stavu objednávky</h3>',
    'id' => 'objednavka-stav-modal'
]);

$form = ActiveForm::begin(array(
    'id' => 'objednavka-stav-form',
    'type' => 'horizontal',
    'action' => array('/objednavky/default/zmen-stav'),
    'method' => 'POST',
    'formConfig' => array(
        'labelSpan' => 3,
        'deviceSize' => ActiveForm::SIZE_SMALL
    )
));
?>
    <div class="form-fields">
        <?= Html::activeHiddenInput($mObjednavka, 'objednavka_pk', ['value' => $mObjednavka->objednavka_pk]) ?>
        <?= $form->field($mObjednavka, 'stav')->dropDownList($mObjednavka->pravidlaStavObjednavky(), ['class' => 'selectpicker']) ?>
    </div>

    <div class="form-actions">
        <?php
        echo Html::submitButton('Uložit', array(
            'class' => 'btn btn-success'
        ));
        echo Html::a('Zrušit', '#', array(
            'class' => 'btn btn-danger modal-close'
        ));
        ?>
    </div>
<?php
ActiveForm::end();

Modal::end();
$this->registerJs("");
/* --- ----------------------------------------- --- */

/* --- modální okno s formulářem - změna dodacích údajů --- */
Modal::begin([
    'header' => '<h3>Změna dodacích údajů</h3>',
    'id' => 'objednavka-dodaci-modal'
]);

$form = ActiveForm::begin(array(
    'id' => 'objednavka-dodaci-form',
    'type' => 'horizontal',
    'action' => array('/objednavky/default/zmen-dodaci-udaje'),
    'method' => 'POST',
    'formConfig' => array(
        'labelSpan' => 3,
        'deviceSize' => ActiveForm::SIZE_SMALL
    )
));

$do_udaje = ArrayHelper::map($mDoUdaje->nactiProDropdown('zakaznik', $mObjednavka->zakaznik_pk), 'dodaci_udaje_pk', 'adresa') + $udaje;
?>
    <div class="form-fields">
        <?= Html::activeHiddenInput($mObjednavka, 'objednavka_pk', ['value' => $mObjednavka->objednavka_pk]) ?>
        <?= Html::activeHiddenInput($mObjednavka, 'zakaznik_pk', ['value' => $mObjednavka->zakaznik_pk]) ?>
        <?= $form->field($mDoUdaje, 'dodaci_udaje_pk')->dropDownList($do_udaje, ['class' => 'selectpicker'])->label('Vyberte') ?>
        <div id="zakaznik-dodaci"> <!-- todo jinak nazvat -->
            <?= $form->field($mDoUdaje, 'firma') ?>
            <?= $form->field($mDoUdaje, 'jmeno') ?>
            <?= $form->field($mDoUdaje, 'prijmeni') ?>
            <?= $form->field($mDoUdaje, 'ulice') ?>
            <?= $form->field($mDoUdaje, 'mesto') ?>
            <?= $form->field($mDoUdaje, 'psc') ?>
        </div>
    </div>

    <div class="form-actions">
        <?php
        echo Html::submitButton('Uložit', array(
            'class' => 'btn btn-success'
        ));
        echo Html::a('Zrušit', '#', array(
            'class' => 'btn btn-danger modal-close'
        ));
        ?>
    </div>
<?php
ActiveForm::end();

Modal::end();
$this->registerJs("
$('#objednavka-dodaci-modal').on('show.bs.modal', function () {
    var do_udaje_pk = $('#dodaciudaje-dodaci_udaje_pk').val();
    if (do_udaje_pk == '') {
        $('#zakaznik-dodaci').show();
    } else {
        $('#zakaznik-dodaci').hide();
    }
})

$('#objednavka-dodaci-modal').on('hide.bs.modal', function() {
    $('input[name^=\"DodaciUdaje\"]', '#objednavka-dodaci-form').each(function() {
        $(this).val('');
    });
});

$('#dodaciudaje-dodaci_udaje_pk').on('change', function() {
    if ($(this).val() == '') {
        $('#zakaznik-dodaci').show();
    } else {
        $('#zakaznik-dodaci').hide();
    }
});
");
/* --- ------------------------------------------------ --- */

/* --- modální okno s formulářem - změna fakturačních údajů --- */
Modal::begin([
    'header' => '<h3>Změna fakturačních údajů</h3>',
    'id' => 'objednavka-fakturacni-modal'
]);

$form = ActiveForm::begin(array(
    'id' => 'objednavka-fakturacni-form',
    'type' => 'horizontal',
    'action' => array('/objednavky/default/zmen-fakturacni-udaje'),
    'method' => 'POST',
    'formConfig' => array(
        'labelSpan' => 3,
        'deviceSize' => ActiveForm::SIZE_SMALL
    )
));

$fa_udaje = ArrayHelper::map($mFaUdaje->nactiProDropdown('zakaznik', $mObjednavka->zakaznik_pk), 'fakturacni_udaje_pk', 'adresa') + $udaje;
?>
    <div class="form-fields">
        <?= Html::activeHiddenInput($mObjednavka, 'objednavka_pk', ['value' => $mObjednavka->objednavka_pk]) ?>
        <?= Html::activeHiddenInput($mDoUdaje, 'zakaznik_pk', ['value' => $mObjednavka->zakaznik_pk]) ?>
        <?= $form->field($mFaUdaje, 'fakturacni_udaje_pk')->dropDownList($fa_udaje, ['class' => 'selectpicker'])->label('Vyberte') ?>
        <div id="zakaznik-fakturacni"> <!-- todo jinak nazvat -->
            <?= $form->field($mFaUdaje, 'obchodni_jmeno') ?>
            <?= $form->field($mFaUdaje, 'ic') ?>
            <?= $form->field($mFaUdaje, 'dic') ?>
            <?= $form->field($mFaUdaje, 'jmeno') ?>
            <?= $form->field($mFaUdaje, 'prijmeni') ?>
            <?= $form->field($mFaUdaje, 'ulice') ?>
            <?= $form->field($mFaUdaje, 'mesto') ?>
            <?= $form->field($mFaUdaje, 'psc') ?>
        </div>
    </div>

    <div class="form-actions">
        <?php
        echo Html::submitButton('Uložit', array(
            'class' => 'btn btn-success'
        ));
        echo Html::a('Zrušit', '#', array(
            'class' => 'btn btn-danger modal-close'
        ));
        ?>
    </div>
<?php
ActiveForm::end();

Modal::end();
$this->registerJs("
$('#objednavka-fakturacni-modal').on('show.bs.modal', function () {
    var fa_udaje_pk = $('#fakturacniudaje-fakturacni_udaje_pk').val();
    if (fa_udaje_pk == '') {
        $('#zakaznik-fakturacni').show();
    } else {
        $('#zakaznik-fakturacni').hide();
    }
})

$('#objednavka-fakturacni-modal').on('hide.bs.modal', function() {
    $('input[name^=\"FakturacniUdaje\"]', '#objednavka-fakturacni-form').each(function() {
        $(this).val('');
    });
});

$('#fakturacniudaje-fakturacni_udaje_pk').on('change', function() {
    if ($(this).val() == '') {
        $('#zakaznik-fakturacni').show();
    } else {
        $('#zakaznik-fakturacni').hide();
    }
});
");
/* --- ---------------------------------------------------- --- */