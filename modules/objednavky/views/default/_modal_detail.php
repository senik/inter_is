<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 06.09.2015
 * Time: 14:50
 *
 * @var $mObjednavka Objednavka
 * @var $mZakaznik Zakaznik
 */
use app\modules\objednavky\models\Objednavka;
use app\modules\objednavky\models\Zakaznik;
use kartik\helpers\Html;

?>

<div class="row objednavka-detail-udaje">
    <div class="col-sm-4 well">
        <h4>Základní info</h4>
        <ul class="zakladni-udaje">
            <li>Zákazník: <?= Html::a(sprintf("%s %s", $mZakaznik->prijmeni, $mZakaznik->jmeno), ['/objednavky/zakaznici/detail', 'id' => $mZakaznik->zakaznik_pk]) ?></li>
            <li>Stav objednávky: <b><?= Objednavka::itemAlias('stav', $mObjednavka->stav) ?></b></li>
            <li class="<?php if ($mObjednavka->jePoTerminu()) { echo 'warn'; } ?>">Termín: <b><?= Yii::$app->formatter->asDate($mObjednavka->termin) ?></b></li>
            <li>Dodání: </li><!-- < ?= Objednavka::itemAlias('typ_platby', $mObjednavka->zpusob_platby) ?> -->
            <li>Platba: </li>
        </ul>
    </div>

    <div class="col-sm-4 well">
        <div class="h4-buttons">
            <h4>Dodací údaje</h4>
            <div class="clearfix"></div>
        </div>
        <?php
        if ($mObjednavka->maVyplneneDodaci()) {
            echo '<ul class="dodaci-udaje">';
            {
                echo "<li>{$mZakaznik->prijmeni} {$mZakaznik->jmeno}</li>";
                $do_udaje = array('d_ulice', 'd_mesto', 'd_psc');
                foreach ($do_udaje as $attr) {
                    if ($mObjednavka->$attr != null) echo "<li>{$mObjednavka->$attr}</li>";
                }
            }
            echo '</ul>';
        }
        ?>
    </div>

    <div class="col-sm-4 well">
        <div class="h4-buttons">
            <h4>Fakturační údaje</h4>
            <div class="clearfix"></div>
        </div>
        <?php
        if ($mObjednavka->maVyplneneFakturacni()) {
            echo '<ul class="fakturacni-udaje">';
            {
                echo "<li>{$mZakaznik->prijmeni} {$mZakaznik->jmeno}</li>";
                $fa_udaje = array('f_obchodni_jmeno', 'f_ic', 'f_dic', 'f_ulice', 'f_mesto', 'f_psc');
                foreach ($fa_udaje as $attr) {
                    if ($mObjednavka->$attr != null) echo "<li>{$mObjednavka->$attr}</li>";
                }
            }
            echo '</ul>';
        }
        ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="h3-buttons">
            <h3>Položky objednávky</h3>
            <div class="clearfix"></div>
        </div>
        <?php
        echo \kartik\grid\GridView::widget([
            'dataProvider' => $mObjednavka->vratPolozkyObjednavky(),
            'columns' => $mObjednavka->vratSloupcePolozky(),
            'resizableColumns' => false,
            'layout' => '{items}',
            'export' => false
        ]);
        ?>
        <div class="objednavka-cena well">
            Celková cena objednávky: <span class="cena"><?= number_format($mObjednavka->vratCelkovouCenu(), 2, ',', ' ') ?> Kč</span>
        </div>
    </div>
</div>