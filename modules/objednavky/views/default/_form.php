<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 16. 3. 2015
 * Time: 18:19
 *
 * @var $mObjednavka Objednavka
 * @var $mModel Model3D
 * @var $this View
 */


use app\components\Html;
use app\modules\modely\models\Model3D;
use app\modules\objednavky\models\DodaciUdaje;
use app\modules\objednavky\models\FakturacniUdaje;
use app\modules\objednavky\models\Objednavka;
use app\modules\objednavky\models\Zakaznik;
use kartik\grid\GridView;
use kartik\widgets\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\web\View;

// pri editaci disabluju nektery pole
$editace = (Yii::$app->controller->action->id == 'upravit');

$zakaznici = (new Zakaznik())->vratProDropdown(true);
$dropdownOptions = array('class' => 'selectpicker');

if ($mObjednavka->zakaznik_pk != null) {
    $do_udaje = (new DodaciUdaje())->nactiProDropdown('zakaznik', $mObjednavka->zakaznik_pk);
    if (!empty($do_udaje)) {
        $do_udaje = ArrayHelper::map($do_udaje, 'dodaci_udaje_pk', 'adresa');
    } else {
        $do_udaje = ['' => 'Žádné údaje'];
    }

    $fa_udaje = (new FakturacniUdaje())->nactiProDropdown('zakaznik', $mObjednavka->zakaznik_pk);
    if (!empty($fa_udaje)) {
        $fa_udaje = ArrayHelper::map($fa_udaje, 'fakturacni_udaje_pk', 'adresa');
    } else {
        $fa_udaje = ['' => 'Žádné údaje'];
    }


} else {
    $do_udaje = array('' => 'Vyberte zákazníka');
    $fa_udaje = array('' => 'Vyberte zákazníka');

    $dropdownOptions['disabled'] = true;
}

$form = ActiveForm::begin(array(
    'id' => 'objednavka-pridat-form',
    'type' => 'horizontal',
    'formConfig' => array(
        'labelSpan' => 3,
        'deviceSize' => ActiveForm::SIZE_SMALL
    )
));

// todo pocitat cenu objednavky, JS to uz vice mene dela
$cena = 0;
$mObjednavka->termin = date('j.n.Y', strtotime($mObjednavka->termin));
?>

    <div class="form-fields">
        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($mObjednavka, 'cislo')->textInput(array('readonly' => 'readonly'))->hint('Pouze informativní, po založení může být jiné.') ?>
                <?= $form->field($mObjednavka, 'zpusob_platby')->dropDownList(Objednavka::itemAlias('typ_platby'), ['class' => 'selectpicker']) ?>
                <?= $form->field($mObjednavka, 'termin')->widget(\kartik\widgets\DatePicker::className(), [
                        'pluginOptions' => [
                            'autoclose'=> true,
                            'format' => 'd.m.yyyy',
                            'calendarWeeks' => true
                        ]
                    ]
                ) ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($mObjednavka, 'zakaznik_pk')->dropDownList($zakaznici, array(
                    'class' => 'selectpicker',
                    'title' => 'Vyberte',
                    'data-live-search' => "true",
                    'disabled' => $editace
                ))->hint($editace ? 'Při editaci objednávky není možné měnit zákazníka' : '<a href="#" data-toggle="modal" data-target="#objednavka-zakaznik-modal">Nebo přidejte nového.</a>') ?>
                <?= $form->field($mObjednavka, 'fa_udaje_pk')->dropDownList($fa_udaje, $dropdownOptions) ?>
                <?= $form->field($mObjednavka, 'do_udaje_pk')->dropDownList($do_udaje, $dropdownOptions) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="h3-buttons">
                    <h3>Položky</h3>
                    <?php
                    echo Html::a(
                        'Přidat',
                        '#',
                        array(
                            'class' => 'btn btn-sm btn-success',
                            'data-toggle' => "modal",
                            'data-target' => "#objednavka-polozka-modal"
                        )
                    );
                    ?>
                    <div class="clearfix"></div>
                </div>
                <div id="objednavka-polozky">
                    <?php
                    // vykreslim vsechny polozky objednavky
                    foreach ($mObjednavka->polozky as $model => $polozka) {
                        foreach ($polozka as $material => $data) {
                            $cena += ($data['cena'] * $data['kusu']);
                            echo $this->render('_form_polozka_objednavky', [
                                'model' => $model,
                                'material' => $material,
                                'nazev' => $data['nazev'],
                                'kusu' => $data['kusu'],
                                'cena' => $data['cena']
                            ]);
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>

    <div class="form-actions well">
        <div id="objednavka-cena">
            <!-- cena se bude pocitat v PHP pri loadu, viz vyse, a pak pomoci JS -->
            Cena: <span id="cena"><?= number_format($cena, 2, ',', '') ?></span> Kč
        </div>
        <?php
        echo Html::submitButton('Uložit', array(
            'class' => 'btn btn-success'
        ));
        echo Html::a('Zrušit', array('/objednavky/default/index'), array(
            'class' => 'btn btn-danger'
        ));
        ?>
    </div>

<?php
ActiveForm::end();


/* --- modální okno s gridem - položky --- */
Modal::begin([
    'header' => '<h3>Vyberte položku objednávky</h3>',
    'id' => 'objednavka-polozka-modal'
]);

$form = ActiveForm::begin(array(
    'id' => 'model3d-search-form',
    'method' => 'POST',
    'action' => array('/ajax/objednavka-filtruj-modely'),
    'type' => 'horizontal',
    'formConfig' => array(
        'labelSpan' => 3,
        'deviceSize' => ActiveForm::SIZE_SMALL
    ),
    'options' => array(
        'class' => 'well'
    )
));
?>
    <div class="form-fields">
        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($mModel, 'nazev') ?>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <?php
        echo Html::submitButton('Vyhledat', array(
            'class' => 'btn btn-success'
        ));
        ?>
    </div>
<?php
ActiveForm::end();

echo '<div id="objednavka-modely-grid">';
{
    echo GridView::widget(array(
        'dataProvider' => $mModel->search(),
        'columns' => $mModel->vratSloupce(),
        'resizableColumns' => false,
        'export' => false
    ));
}
echo '</div>';

Modal::end();
/* --- ------------------------------- --- */