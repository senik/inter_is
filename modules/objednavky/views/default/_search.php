<?php
use app\modules\objednavky\models\Objednavka;
use app\modules\objednavky\models\Zakaznik;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 2.11.14
 * Time: 18:25
 *
 * @var $this View
 * @var $mObjednavka Objednavka
 */

$form = ActiveForm::begin(array(
    'id' => 'objednavka-search-form',
    'action' => array('/objednavky/default/index'),
    'method' => 'get',
    'type' => 'horizontal',
    'options' => array(
        'class' => 'well'
    ),
    'formConfig' => ['labelSpan' => 3, 'spanSize' => ActiveForm::SIZE_SMALL]
));

$zakaznici = array('' => 'Všichni') + (new Zakaznik())->vratProDropdown();
?>

    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($mObjednavka, 'zakaznik_pk', array(
            ))->dropDownList($zakaznici, array(
                'title' => 'Všichni / vyberte',
                'data-live-search' => "true",
                'class' => 'selectpicker'
            )) ?>
        </div>

        <div class="col-sm-4">
            <?= $form->field($mObjednavka, 'cislo', array()) ?>
        </div>

        <div class="col-sm-4">
            <?= $form->field($mObjednavka, 'stav', array())->dropDownList(
                Objednavka::itemAlias('stav')
                , array(
                    'title' => 'Všechny / vyberte',
                    'multiple' => 'multiple',
                    'class' => 'selectpicker'
                )
            ) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($mObjednavka, 'datum_zalozeni', array(
                'addon'=>['append'=>['content'=>'<i class="glyphicon glyphicon-calendar"></i>']],
            ))->widget(\kartik\daterange\DateRangePicker::className(), array(
                'useWithAddon' => true,
                    'convertFormat' => true,
                    'pluginOptions' => array(
                        'format' => 'j.n.Y',
                        'separator' => ' až ',
                        'ranges' => array(
                            "Dnes" => ["moment()", "moment()"],
                            "Včera" => ["moment().subtract('days', 1)", "moment().subtract('days', 1)"],
                            "Posledních 7 dní" => ["moment().subtract('days', 6)", "moment()"],
                            "Posledních 30 dní" => ["moment().subtract('days', 29)", "moment()"],
                            "Tento měsíc" => ["moment().startOf('month')", "moment().endOf('month')"],
                            "Předchozí měsíc" => ["moment().subtract('month', 1).startOf('month')", "moment().subtract('month', 1).endOf('month')"],
                        )
                    )
            )) ?>
        </div>
    </div>

    <div class="form-actions">
        <?php
        echo Html::submitButton('Filtrovat', array('class' => 'btn btn-success'));
        echo Html::submitButton('Vymazat filtr', array('class' => 'btn btn-warning filter-reset'));
        ?>
    </div>


<?php
ActiveForm::end();
