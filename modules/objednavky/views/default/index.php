<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 2.11.14
 * Time: 18:06
 *
 * @var $this View
 * @var $mObjednavka Objednavka
 */

use app\modules\objednavky\models\Objednavka;
use yii\helpers\Html;
use yii\web\View;

$this->title = Yii::$app->name . ' - Seznam objednávek';

$this->params['breadcrumbs'] = array(
    'Objednávky'
);
?>

<div class="h2-buttons">
    <h2>Objednávky</h2>
    <?php
    echo Html::a(
        'Přidat',
        array('/objednavky/default/pridat'),
        array(
            'class' => 'btn btn-success'
        )
    );
    ?>
    <div class="clearfix"></div>
</div>

<?= $this->render('_search', array('mObjednavka' => $mObjednavka)) ?>

<?php
echo \kartik\grid\GridView::widget(array(
    'dataProvider' => $mObjednavka->search(),
    'columns' => $mObjednavka->vratSloupce(),
    'resizableColumns' => false,
    'export' => false
));