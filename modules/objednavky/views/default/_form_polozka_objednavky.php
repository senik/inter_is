<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 21. 2. 2015
 * Time: 15:54
 *
 * @var $model string
 * @var $material string
 * @var $kusu string
 * @var $nazev string
 * @var $cena string
 */
?>

<div class="objednavka-polozka" id="Objednavka_polozky_<?= $model ?>_<?= $material ?>">
    <input type="hidden" id="Objednavka_polozky_<?= $model ?>_<?= $material ?>_nazev" name="Objednavka[polozky][<?= $model ?>][<?= $material ?>][nazev]" value="<?= $nazev ?>">
    <div class="polozka-nazev col-sm-7"><?= $nazev ?></div>
    <div class="polozka-inputy col-sm-5">
        <div class="input-group">
            <input type="text" size="2" class="form-control" id="Objednavka_polozky_<?= $model ?>_<?= $material ?>_cena" name="Objednavka[polozky][<?= $model ?>][<?= $material ?>][cena]" value="<?= number_format($cena, 2, ',', '') ?>" disabled="disabled">
            <span class="input-group-addon kv-date-remove" title="Clear field">Kč</span>
            <input type="text" size="2" class="form-control" id="Objednavka_polozky_<?= $model ?>_<?= $material ?>_kusu" name="Objednavka[polozky][<?= $model ?>][<?= $material ?>][kusu]" value="<?= $kusu ?>">
        </div>

        <div class="link-group">
            <a href="#" title="" class="polozka-plus xtooltip" data-polozka="<?= $model ?>" data-material="<?= $material ?>" data-original-title="Přidat 1ks položky"><span class="glyphicon glyphicon-plus-sign"></span></a>
            <a href="#" title="" class="polozka-minus xtooltip" data-polozka="<?= $model ?>" data-material="<?= $material ?>" data-original-title="Odebrat 1ks položky"><span class="glyphicon glyphicon-minus-sign"></span></a>
            <a href="#" title="" class="polozka-odebrat xtooltip" data-polozka="<?= $model ?>" data-material="<?= $material ?>" data-original-title="Odebrat položku"><span class="glyphicon glyphicon-remove-sign"></span></a></div>
        </div>
    <div class="clearfix"></div>
</div>