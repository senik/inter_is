<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 16. 3. 2015
 * Time: 18:05
 *
 * @var $mObjednavka Objednavka
 * @var $mModel Model3D
 */

use app\modules\modely\models\Model3D;
use app\modules\objednavky\models\Objednavka;


$this->title = Yii::$app->name . ' - Editace objednávky ' . $mObjednavka->cislo;

$this->params['breadcrumbs'] = array(
    array('label' => 'Objednávky', 'url' => array('/objednavky/default/index')),
    'Upravit'
);

$polozky = $mObjednavka->vratPolozkyObjednavky()->getModels();
$sorted = [];
foreach ($polozky as $polozka) {
    $sorted[$polozka['model_pk']][$polozka['material_pk']] = [
        'nazev' => "{$polozka['model_nazev']}, {$polozka['material_nazev']}",
        'kusu' => $polozka['pocet'],
        'cena' => $polozka['cena_ks']
    ];
}
$mObjednavka->polozky = $sorted;
?>

<div class="h2-buttons">
    <h2>Úprava objednávky</h2>
    <div class="clearfix"></div>
</div>

<?= $this->render('_form', [
    'mObjednavka' => $mObjednavka,
    'mModel' => $mModel
]) ?>