<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 26.12.14
 * Time: 11:41
 *
 * @var $this View
 * @var $mObjednavka Objednavka
 * @var $mZakaznik Zakaznik
 * @var $mModel Model3D
 */

use app\components\Html;
use app\modules\modely\models\Model3D;
use app\modules\objednavky\models\DodaciUdaje;
use app\modules\objednavky\models\FakturacniUdaje;
use app\modules\objednavky\models\Objednavka;
use app\modules\objednavky\models\Zakaznik;
use kartik\grid\GridView;
use kartik\widgets\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\web\View;
use yii\widgets\Pjax;

$this->title = Yii::$app->name . ' - Nová objednávka';

$this->params['breadcrumbs'] = array(
    array('label' => 'Objednávky', 'url' => array('/objednavky/default/index')),
    'Nová'
);
?>

<div class="h2-buttons">
    <h2>Nová objednávka</h2>
    <div class="clearfix"></div>
</div>

<?php
$zakaznici = (new Zakaznik())->vratProDropdown(true);
$dropdownOptions = array('class' => 'selectpicker');

if ($mObjednavka->zakaznik_pk != null) {
    $do_udaje = (new DodaciUdaje())->nactiProDropdown('zakaznik', $mObjednavka->zakaznik_pk);
    $do_udaje = ArrayHelper::map($do_udaje, 'dodaci_udaje_pk', 'adresa');

    $fa_udaje = (new FakturacniUdaje())->nactiProDropdown('zakaznik', $mObjednavka->zakaznik_pk);
    $fa_udaje = ArrayHelper::map($fa_udaje, 'fakturacni_udaje_pk', 'adresa');
} else {
    $do_udaje = array('' => 'Vyberte zákazníka');
    $fa_udaje = array('' => 'Vyberte zákazníka');

    $dropdownOptions['disabled'] = true;
}

$form = ActiveForm::begin(array(
    'id' => 'objednavka-pridat-form',
    'type' => 'horizontal',
    'formConfig' => array(
        'labelSpan' => 3,
        'deviceSize' => ActiveForm::SIZE_SMALL
    )
));

// todo pocitat cenu objednavky, JS to uz vice mene dela
$cena = 0;
?>

    <div class="form-fields">
        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($mObjednavka, 'cislo')->textInput(array('readonly' => 'readonly'))->hint('Pouze informativní, po založení může být jiné.') ?>
                <?= $form->field($mObjednavka, 'zpusob_platby')->dropDownList(Objednavka::itemAlias('typ_platby'), ['class' => 'selectpicker']) ?>
                <?= $form->field($mObjednavka, 'termin')->widget(\kartik\widgets\DatePicker::className(), [
                        'pluginOptions' => [
                            'autoclose'=> true,
                            'format' => 'd.m.yyyy',
                            'startView' => 1, // pohled na rok
                            'calendarWeeks' => true
                        ]
                    ]
                ) ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($mObjednavka, 'zakaznik_pk')->dropDownList($zakaznici, array(
                    'title' => 'Vyberte',
                    'data-live-search' => "true",
                    'class' => 'selectpicker'
                ))->hint('<a href="#" data-toggle="modal" data-target="#objednavka-zakaznik-modal">Nebo přidejte nového.</a>') ?>
                <?= $form->field($mObjednavka, 'fa_udaje_pk')->dropDownList($fa_udaje, $dropdownOptions) ?>
                <?= $form->field($mObjednavka, 'do_udaje_pk')->dropDownList($do_udaje, $dropdownOptions) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="h3-buttons">
                    <h3>Položky</h3>
                    <?php
                    echo Html::a(
                        'Přidat',
                        '#',
                        array(
                            'class' => 'btn btn-sm btn-success',
                            'data-toggle' => "modal",
                            'data-target' => "#objednavka-polozka-modal"
                        )
                    );
                    ?>
                    <div class="clearfix"></div>
                </div>
                <div id="objednavka-polozky">
                    <?php
                    // vykreslim vsechny polozky objednavky
                    foreach ($mObjednavka->polozky as $model => $polozka) {
                        foreach ($polozka as $material => $data) {
//                            $cena += $data['cena'];
                            echo $this->render('_form_polozka_objednavky', [
                                'model' => $model,
                                'material' => $material,
                                'nazev' => $data['nazev'],
                                'kusu' => $data['kusu'],
                                'cena' => $data['cena']
                            ]);
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>

    <div class="form-actions well">
        <div id="objednavka-cena">
            <!-- cena se bude pocitat v PHP pri loadu, viz vyse, a pak pomoci JS -->
            Cena: <span id="cena"><?= $cena ?></span> Kč
        </div>
        <?php
        echo Html::submitButton('Uložit', array(
            'class' => 'btn btn-success'
        ));
        echo Html::a('Zrušit', array('/objednavky/default/index'), array(
            'class' => 'btn btn-danger'
        ));
        ?>
    </div>

<?php
ActiveForm::end();


/* --- modální okno s gridem - položky --- */
Modal::begin([
    'header' => '<h3>Vyberte položku objednávky</h3>',
    'id' => 'objednavka-polozka-modal'
]);

$form = ActiveForm::begin(array(
    'id' => 'model3d-search-form',
    'method' => 'POST',
    'action' => array('/ajax/objednavka-filtruj-modely'),
    'type' => 'horizontal',
    'formConfig' => array(
        'labelSpan' => 3,
        'deviceSize' => ActiveForm::SIZE_SMALL
    ),
    'options' => array(
        'class' => 'well'
    )
));
?>
    <div class="form-fields">
        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($mModel, 'nazev') ?>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <?php
        echo Html::submitButton('Vyhledat', array(
            'class' => 'btn btn-success'
        ));
        ?>
    </div>
<?php
ActiveForm::end();

Pjax::begin(['timeout' => false, 'enablePushState' => false]);
echo '<div id="objednavka-modely-grid">';
{
    echo GridView::widget(array(
        'dataProvider' => $mModel->search(),
        'columns' => $mModel->vratSloupce(),
        'resizableColumns' => false,
        'export' => false,
    ));
}
echo '</div>';
Pjax::end();

Modal::end();
/* --- ------------------------------- --- */


/* --- modální okno s formulářem - nový zákazník --- */
Modal::begin([
    'header' => '<h3>Rychlé založení zákazníka</h3>',
    'id' => 'objednavka-zakaznik-modal'
]);

$form = ActiveForm::begin(array(
    'id' => 'zakaznik-pridat-form',
    'type' => 'horizontal',
    'action' => array('/ajax/zaloz-zakaznika-rychle'),
    'method' => 'POST',
    'formConfig' => array(
        'labelSpan' => 3,
        'deviceSize' => ActiveForm::SIZE_SMALL
    )
));
?>
    <div class="form-fields">
        <div class="row">
            <div class="col-sm-12">
                <?= $form->field($mZakaznik, 'jmeno') ?>
                <?= $form->field($mZakaznik, 'prijmeni') ?>
                <?= $form->field($mZakaznik, 'telefon') ?>
                <?= $form->field($mZakaznik, 'email') ?>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <?php
        echo Html::submitButton('Uložit', array(
            'class' => 'btn btn-success'
        ));
        echo Html::a('Zrušit', '#', array(
            'class' => 'btn btn-danger form-cancel'
        ));
        ?>
    </div>
<?php
ActiveForm::end();

Modal::end();
/* --- ----------------------------------------- --- */


// nakonec potrebne javascripty
$adresyUrl = Yii::$app->urlManager->createAbsoluteUrl('/ajax/nacti-adresy-zakaznika');
//$zakaznikUrl = Yii::$app->urlManager->createAbsoluteUrl();
//$modelyUrl = Yii::$app->urlManager->createAbsoluteUrl();
$this->registerJs("
/**
 * vymaze vsechny optiony ze selecu
 */
var vymazUdaje = function() {
    $('option', '#objednavka-fa_udaje_pk').each(function() {
        $(this).remove();
    });
    $('option', '#objednavka-do_udaje_pk').each(function() {
        $(this).remove();
    });
};

/**
 * nastavi prompty do obou selectu
 */
var resetujUdaje = function() {
    var fa_udaje = $('#objednavka-fa_udaje_pk');
    var do_udaje = $('#objednavka-do_udaje_pk');

    fa_udaje.append('<option value=\"\">Vyberte zákazníka</option>').attr('disabled', true).selectpicker('refresh');
    do_udaje.append('<option value=\"\">Vyberte zákazníka</option>').attr('disabled', true).selectpicker('refresh');
};

/**
 * ajax na onchange zakazniku - dotahne dodaci a fakturacni udaje
 */
$('#objednavka-zakaznik_pk').on('change', function() {
    var fa_udaje = $('#objednavka-fa_udaje_pk');
    var do_udaje = $('#objednavka-do_udaje_pk');
    var selectVal = $(this).val();

    vymazUdaje();

    if (selectVal == '') {
        resetujUdaje();
    } else {
        var form = $(this).closest('form');

        form.addClass('loading');

        var request = $.ajax({
            url: \"$adresyUrl\",
            type: \"POST\",
            data: {zakaznik: selectVal}
        });

        request.done(function(response) {
            var data = $.parseJSON(response);
            if (data.success == 1) {
                $.each(data.fa_udaje, function(i, v) {
                    fa_udaje.append('<option value=\"'+v.pk+'\">'+v.udaje+'</option>');
                });

                $.each(data.do_udaje, function(i, v) {
                    do_udaje.append('<option value=\"'+v.pk+'\">'+v.udaje+'</option>');
                });
            }

            fa_udaje.attr('disabled', false).selectpicker('refresh');
            do_udaje.attr('disabled', false).selectpicker('refresh');

            form.removeClass('loading');
        });

        request.fail(function(jqXHR, textStatus) {
            resetujUdaje();
            form.removeClass('loading');
            alert('Request failed: ' + textStatus);
        });
    }
});

/**
 * resetovani formulare a schovani modalu
 */
$('a.form-cancel', '#zakaznik-pridat-form .form-actions').on('click', function() {
    var form = $(this).closest('form');
    var modal = $('#objednavka-zakaznik-modal');

    form[0].reset();
    modal.modal('hide');
});

/**
 * todo bude to chtit jeste doladit kvuli validaci
 * submit na formulari pro pridani zakaznika
 */
$('#zakaznik-pridat-form').on('beforeSubmit', function() {
    var currentForm = this;
    var jCurrentForm = $(this);
    var modal = $('#objednavka-zakaznik-modal');

    var request = $.ajax({
        url: jCurrentForm.attr('action'),
        type: jCurrentForm.attr('method'),
        data: jCurrentForm.serialize()
    });

    request.done(function(response) {
        var data = $.parseJSON(response);
        var fa_udaje = $('#objednavka-fa_udaje_pk');
        var do_udaje = $('#objednavka-do_udaje_pk');
        var zakaznik = $('#objednavka-zakaznik_pk');

        if (data.success == 1) {
            $('option', '#objednavka-zakaznik_pk').each(function() {
                $(this).remove();
            });

            zakaznik.append('<option value=\"\">Vyberte</option>');
            $.each(data.zakaznici, function(i, v) {
                var selected = (v.zakaznik_pk == data.pk) ? 'selected' : '';
                $('#objednavka-zakaznik_pk').append('<option value=\"'+v.zakaznik_pk+'\" '+selected+'>'+v.cele_jmeno+'</option>');
            });
            zakaznik.selectpicker('refresh');

            vymazUdaje();
            fa_udaje.append('<option value=\"\">Žádné údaje</option>').attr('disabled', false).selectpicker('refresh');
            do_udaje.append('<option value=\"\">Žádné údaje</option>').attr('disabled', false).selectpicker('refresh');
        }

        currentForm.reset();
        modal.modal('hide');
    });

    request.fail(function(jqXHR, textStatus) {
        resetujUdaje();
        jCurrentForm.removeClass('loading');
        alert('Request failed: ' + textStatus);
    });

    return false;
}).on('submit', function(ev) {
    ev.preventDefault();
});

/**
 * zahodim focus z tlacitka
 */
$('#objednavka-zakaznik-modal').on('hidden.bs.modal', function(e) {
    $('a[data-target=\"#objednavka-zakaznik-modal\"]').blur();
});

/**
 * submit objednavkoveho formulare
 */
$(document).on('submit', '#objednavka-pridat-form', function(ev) {
    var do_udaje = $('#objednavka-do_udaje_pk').val();
    var fa_udaje = $('#objednavka-fa_udaje_pk').val();
    var message = '';
    var currentForm = this;

    if (do_udaje == '' && fa_udaje == '') {
        message = 'Opravdu chcete založit novou objednávku bez dodacích ani fakturačních údajů?';
    } else if (do_udaje == '') {
        message = 'Opravdu chcete založit novou objednávku bez dodacích údajů?';
    } else if (fa_udaje == '') {
        message = 'Opravdu chcete založit novou objednávku bez fakturačních údajů?';
    }

    if (message != '') {
        message += '<br>Takovou objednávku nebude možné vyfakturovat!';
        return yii.confirm(message, function() {
            currentForm.submit();
        }, function() {
            return false;
        });
    }
});

/**
 * filtrovani modelu v modalnim okne
 */
$('#model3d-search-form').on('beforeSubmit', function() {
    var currentForm = this;
    var jCurrentForm = $(currentForm);
    var grid = $('div.grid-view', '#objednavka-modely-grid');
    var wrapper = $('#objednavka-modely-grid');

    var request = $.ajax({
        url:  jCurrentForm.attr('action'),
        type: jCurrentForm.attr('method'),
        data: jCurrentForm.serialize()
    });

    request.done(function(data) {
        grid.remove();
        wrapper.append(data);
    });

    request.fail(function(jqXHR, textStatus) {
        alert('Request failed: ' + textStatus);
    });

    return false;
}).on('submit', function(ev) {
    ev.preventDefault();
});

/**
 * jen tak pro srandu disabluju platbu kartou :)
 */
$('#objednavka-zpusob_platby').find('option[value=\"KARTA\"]').attr('disabled', true);
$('#objednavka-zpusob_platby').selectpicker('refresh');
");

