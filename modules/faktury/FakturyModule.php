<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 17.10.14
 * Time: 19:43
 */

namespace app\modules\faktury;


use app\components\ModuleInterface;
use yii\base\Module;

class FakturyModule extends Module implements ModuleInterface
{
    /**
     * @var string
     */
    public $controllerNamespace = 'app\modules\faktury\controllers';

    /**
     * @var
     */
    public $name = "Faktury";

    /**
     * @var array
     */
    public $baseUrl = array('/faktury/default/index');

    public function vratMenuModulu()
    {
        return array();
    }
}