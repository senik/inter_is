<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 7. 4. 2015
 * Time: 22:48
 */

namespace app\modules\faktury\models;


use app\components\Model;

/**
 * Class FakturaPolozka
 * @package app\modules\faktury\models
 */
class FakturaPolozka extends Model
{
    /**
     * @var
     */
    public $faktura_pk;

    /**
     * @var
     */
    public $faktura_polozka_pk;

    /**
     * @var
     */
    public $kod;

    /**
     * @var
     */
    public $nazev;

    /**
     * @var
     */
    public $kusu;

    /**
     * @var
     */
    public $cena_ks;

    /**
     * @var
     */
    public $cena_celkem;

    /**
     * @var
     */
    public $cena_ks_dph;

    /**
     * @var
     */
    public $cena_celkem_dph;

    /**
     * @var
     */
    public $dph;

    /**
     * @var
     */
    public $spojit;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [$this->attributes(), 'safe'],
            ['cena_ks', 'number']
        ];
    }

    /**
     * @returm integer
     */
    public function uloz()
    {
        $sql = "select faktura_polozka_zmena(
              :pk
            , :faktura
            , :nazev
            , :kusu
            , :cena_ks
            , :cena_celkem
            , :cena_ks_dph
            , :cena_celkem_dph
            , :dph
        )";
        $params = [
            ':pk' => $this->faktura_polozka_pk,
            ':faktura' => $this->faktura_pk,
            ':nazev' => $this->nazev,
            ':kusu' => $this->kusu,
            ':cena_ks' => $this->cena_ks,
            ':cena_celkem' => $this->cena_celkem,
            ':cena_ks_dph' => $this->cena_ks_dph,
            ':cena_celkem_dph' => $this->cena_celkem_dph,
            ':dph' => $this->dph
        ];

        return \Yii::$app->db->createCommand($sql)->bindValues($params)->queryScalar();
    }

    /**
     * @param $pk
     * @return $this|null
     */
    public function nactiPolozkuObjednavky($pk)
    {
        $sql = "
          select
              m3.nazev || ', materiál: ' || mv.nazev_dlouhy as nazev
            , op.pocet as kusu
            , dph_datum_vrat(o.datum_zalozeni::date) as dph
            , replace(op.cena_kus::text, '.', ',') as cena_ks_dph
            , op.cena_kus * op.pocet as cena_celkem_dph
            , replace((op.cena_kus / (1 + dph_datum_vrat(o.datum_zalozeni::date)))::text, '.', ',') as cena_ks
            , (op.cena_kus / (1 + dph_datum_vrat(o.datum_zalozeni::date))) * op.pocet as cena_celkem
            , m3.model_id AS kod
          from objednavka_polozka op
            join objednavka o on op.objednavka_pk = o.objednavka_pk
            left join model3d m3 on m3.model_pk = op.model_pk
            left join material_view mv on mv.material_pk = op.material_pk
          where objednavka_polozka_pk = :pk
        ";
        $params = [
            ':pk' => $pk
        ];

        $data = \Yii::$app->db->createCommand($sql)->bindValues($params)->queryOne();

        if ($this->load($data, '')) {
            return $this;
        } else {
            return null;
        }
    }

}