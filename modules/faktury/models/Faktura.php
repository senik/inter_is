<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 7. 4. 2015
 * Time: 22:33
 */

namespace app\modules\faktury\models;


use app\components\Application;
use app\components\helpers\DbUtils;
use app\components\Html;
use app\components\ItemAliasTrait;
use app\components\SqlDataProvider;
use app\modules\objednavky\models\FakturacniUdaje;
use app\modules\objednavky\models\Objednavka;
use app\modules\objednavky\models\Zakaznik;
use yii\base\Exception;
use yii\base\Model;

/**
 * Class Faktura
 * @package app\modules\faktury\models
 */
class Faktura extends Model
{
    use ItemAliasTrait;

    const STAV_ZALOZENA = 1;

    const STAV_VYSTAVENA = 2;
    const STAV_ZAPLACENA = 10;
    const STAV_STORNO = -1;
    const STAV_SMAZANO = -2;

    const TYP_OBJEDNAVKA = 'objednavka';
    const TYP_PRAZDNA = 'prazdna';

    const SCENARIO_HLEDAT = 'hledat';

    /**
     * @var
     */
    public $faktura_pk;

    /**
     * @var FakturaPolozka[]
     */
    public $polozky = [];

    /**
     * @var string
     */
    public $cislo;

    /**
     * @var string
     */
    public $datum_zalozeni;

    /**
     * @var string
     */
    public $datum_vystaveni;

    /**
     * @var string
     */
    public $datum_splatnosti;

    /**
     * @var string
     */
    public $datum_zaplaceni;

    /**
     * @var integer
     */
    public $stav;

    /**
     * @var integer
     */
    public $objednavka_pk;

    /**
     * @var Objednavka
     */
    public $objednavka;

    /**
     * @var integer
     */
    public $vystavil;

    /**
     * @var string
     */
    public $typ = self::TYP_OBJEDNAVKA;

    /**
     * @var Zakaznik
     */
    public $zakaznik;

    /**
     * @var FakturacniUdaje
     */
    public $fakturacni_udaje;

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_HLEDAT] = [
            'cislo', 'vystavil', 'stav', 'datum_vystaveni', 'datum_splatnosti'
        ];

        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        if ($this->scenario == self::SCENARIO_HLEDAT) {
            $rules = [
                [$this->attributes(), 'safe'],
                [['datum_vystaveni', 'datum_splatnosti'], 'match', 'pattern' => '/^(\d{1,2}[.]\d{1,2}[.]\d{4}) až (\d{1,2}[.]\d{1,2}[.]\d{4})$/', 'message' => 'Datum nemá platný tvar']
            ];
        } else {
            $rules = [
                [$this->attributes(), 'safe'],
                ['objednavka_pk', 'required', 'when' => function($model) {
                    return $model->typ == Faktura::TYP_OBJEDNAVKA;
                }, 'message' => 'Je zapotřebí vybrat objednávku.']
            ];
        }
        return $rules;
    }

    public function attributeLabels()
    {
        return [
            'cislo' => 'Číslo',
            'datum_zalozeni' => 'Datum založení',
            'datum_vystaveni' => 'Datum vystavení',
            'datum_splatnosti' => 'Datum splatnosti',
            'stav' => 'Stav',
            'vystavil' => 'Vystavil'
        ];
    }

    /**
     * @return SqlDataProvider
     */
    public function search()
    {
        return $this->_vratDataProvider();
    }

    /**
     * proslo to validaci, takze muzu ukladat<br>
     * zacatek transakce<br>
     * smazu vsechny polozky faktury<br>
     * updatuju data ve fakture - procedura faktura_zmena()<br>
     * postupne prochazim polozky faktury a ukladam je...<br>
     * jak budu resit spojovani polozek - ulozim do samostatneho pole, ktere projdu nasledovne
     */
    public function uloz()
    {
        $db = \Yii::$app->db;
        $trans = $db->beginTransaction();

        try {
            if ($this->faktura_pk != null) {
                $smazano = $db->createCommand("select faktura_smaz_polozky(:pk)")->bindValue(':pk', $this->faktura_pk)->queryScalar();
                if ($smazano !== 0) {
                    throw new \Exception("chybny vysledek procedury faktura_smaz_polozky() = {$smazano}!");
                }
            }

            if ($this->datum_splatnosti == '') {
                $this->datum_splatnosti = null;
            }

            $sql = "select faktura_zmena(:faktura_pk, :cislo, :datum_splatnosti::date, :objednavka)";
            $params = [
                ':faktura_pk' => $this->faktura_pk,
                ':cislo' => $this->cislo,
                ':datum_splatnosti' => $this->datum_splatnosti,
                ':objednavka' => $this->objednavka_pk
            ];

            $this->faktura_pk = $db->createCommand($sql)->bindValues($params)->queryScalar();
            if ($this->faktura_pk < 0) {
                throw new \Exception("chybny vysledek procedury faktura_zmena() = {$this->faktura_pk}");
            }

            $spojit = [];
            $pocet = null;
            $stejny_pocet = true;
            foreach ($this->polozky as $polozka) {
                $pk = $this->pridejPolozku($polozka);

                if ($polozka->spojit) {
                    $spojit[] = $pk;
                    $pocty[$pk] = $polozka->kusu;

                    if ($pocet == null) {
                        $pocet = $polozka->kusu;
                    } else {
                        $stejny_pocet = $stejny_pocet && $pocet == $polozka->kusu;
                        $pocet = $polozka->kusu;
                    }
                }
            }

            if (!empty($spojit)) {
                $this->spojPolozky($this->faktura_pk, $spojit, $stejny_pocet);
            }

            // jeste ulozim extra fakturacni udaje
            $this->fakturacni_udaje->faktura_pk = $this->faktura_pk;
            $this->fakturacni_udaje->uloz();

            $trans->commit();
        } catch (\Exception $e) {
            $trans->rollBack();
            throw $e;
        }
    }

    /**
     *
     * @param int $pagination
     * @return SqlDataProvider
     */
    public function vratPoSplatnosti($pagination = 10)
    {
        $where = [
            "t.datum_splatnosti < now()",
            "stav = :sta"
        ];

        $params = [
            ':sta' => self::STAV_VYSTAVENA
        ];

        return $this->_vratDataProvider($where, $params, $pagination);
    }

    /**
     * @param $pk
     * @return $this
     */
    public function nactiPodlePk($pk)
    {
        $sql = "
            select
                f.faktura_pk
              , f.cislo
              , f.datum_zalozeni
              , f.datum_splatnosti
              , f.datum_vystaveni
              , f.objednavka_pk
              , f.stav
              , f.vystavil
              , case when f.objednavka_pk is not null then 'objednavka' else 'prazdna' end as typ
              , fu.fakturacni_udaje_pk
            from faktura f
              left join faktura_fakturacni_udaje ffu on ffu.faktura_pk = f.faktura_pk
              left join fakturacni_udaje fu on fu.fakturacni_udaje_pk = ffu.fakturacni_udaje_pk
            where f.faktura_pk = :faktura
              --join objednavka o on f.objednavka_pk = o.objednavka_pk
              --left join uzivatel u on
        ";

        $command = \Yii::$app->db->createCommand($sql);
        $command->bindValues([
            'faktura' => $pk
        ]);

        $data = $command->queryOne();
        if ($this->load($data, '')) {
            $sql_polozky = "
              select
                  *
                , coalesce(fp.dph, dph_datum_vrat(f.datum_zalozeni::date)) as dph
              from faktura_polozka fp
                join faktura f on f.faktura_pk = fp.faktura_pk
              WHERE fp.faktura_pk = :faktura
            ";
            $command = \Yii::$app->db->createCommand($sql_polozky);
            $command->bindValue('faktura', $this->faktura_pk);

            foreach ($command->queryAll() as $row) {
                $mPolozka = new FakturaPolozka();

                if ($mPolozka->load($row, '')) {
                    $this->polozky[$mPolozka->faktura_polozka_pk] = $mPolozka;
                }
            }

            if ($this->typ == self::TYP_OBJEDNAVKA) {
                $this->objednavka = (new Objednavka())->nactiPodlePk($this->objednavka_pk);
                $this->zakaznik = (new Zakaznik())->nactiPodlePk($this->objednavka->zakaznik_pk);
            } else {
                $this->objednavka = new Objednavka();
                $this->zakaznik = new Zakaznik();
            }

            if ($data['fakturacni_udaje_pk'] != null) {
                $this->fakturacni_udaje = (new FakturacniUdaje())->nactiPodlePk($data['fakturacni_udaje_pk']);
            } else if ($this->objednavka != null) {
                $mFakturacniUdaje = new FakturacniUdaje();
                $mFakturacniUdaje->obchodni_jmeno   = $this->objednavka->f_obchodni_jmeno;
                $mFakturacniUdaje->ulice            = $this->objednavka->f_ulice;
                $mFakturacniUdaje->mesto            = $this->objednavka->f_mesto;
                $mFakturacniUdaje->psc              = $this->objednavka->f_psc;
                $mFakturacniUdaje->ic               = $this->objednavka->f_ic ?: '---';
                $mFakturacniUdaje->dic              = $this->objednavka->f_dic ?: '---';

                $this->fakturacni_udaje = $mFakturacniUdaje;
            } else {
                $this->fakturacni_udaje = new FakturacniUdaje();
            }

            return $this;
        } else {
            return null;
        }
    }

    /**
     * @param $objednavka_pk
     * @return bool|null|string
     */
    public function ulozZobjednavky($objednavka_pk)
    {
        $sql = "SELECT faktura_zaloz_z_objednavky(:pk)";
        $params = [
            'pk' => $objednavka_pk
        ];

        $result = \Yii::$app->db->createCommand($sql, $params)->queryScalar();

        if ($result > 0) {
            return $result;
        } else {
            \Yii::error("Chyba pri generovani faktury z objednavky, vysledek = {$result}");
            return false;
        }
    }

    /**
     * @return bool
     */
    public function vystavit()
    {
        $sql = "select faktura_vystav(:pk, :uzivatel, :splatnost)";

        $command = \Yii::$app->db->createCommand($sql);
        $command->bindValues([
            'pk' => $this->faktura_pk,
            'uzivatel' => \Yii::$app->user->id,
            'splatnost' => $this->datum_splatnosti
        ]);

        $result = $command->queryScalar();
        if ($result < 0) {
            \Yii::error("[faktura] nepodarilo se vystavit fakturu, vysledek = {$result}");
            return false;
        } else {
            return true;
        }
    }

    /**
     * @return array
     */
    public function vratSloupce()
    {
        return [
            'cislo' => [
                'attribute' => 'cislo',
                'label' => $this->getAttributeLabel('cislo'),
                'value' => function($data) {
                    return Html::a($data['cislo'], array('/faktury/default/detail', 'id' => $data['faktura_pk']));
                },
                'format' => 'raw',
                'contentOptions' => [
                    'class' => 'show-detail-column'
                ]
            ],
            'zakaznik' => [
                'attribute' => 'zakaznik',
                'label' => 'Zákazník',
                'value' => function ($data) {
                    return Html::a($data['zakaznik'], array('/objednavky/zakaznici/detail', 'id' => $data['zakaznik_pk']));
                },
                'format' => 'raw',
                'contentOptions' => [
                    'class' => 'show-detail-column'
                ]
            ],
            'datum_vystaveni' => [
                'attribute' => 'datum_vystaveni',
                'label' => $this->getAttributeLabel('datum_vystaveni'),
                'format' => 'date'
            ],
            'datum_splatnosti' => [
                'attribute' => 'datum_splatnosti',
                'label' => $this->getAttributeLabel('datum_splatnosti'),
                'format' => 'date'
            ],
            'cena_celkem' => [
                'class' => '\app\components\columns\PriceColumn',
                'attribute' => 'cena_celkem',
                'label' => 'Cena',
            ],
            'stav' => [
                'attribute' => 'stav',
                'label' => $this->getAttributeLabel('stav'),
                'value' => function ($data) {
                    return self::itemAlias('stav', $data['stav']);
                }
            ],
            'objednavka' => [
                'attribute' => 'objednavka',
                'label' => 'Objednávka',
                'value' => function ($data) {
                    return Html::a($data['objednavka'], array('/objednavky/default/detail', 'id' => $data['objednavka']));
                },
                'format' => 'raw',
                'contentOptions' => [
                    'class' => 'show-detail-column'
                ]
            ],
            'vystavil' => [
                'attribute' => 'vystavitel',
                'label' => $this->getAttributeLabel('vystavil'),
                'value' => function ($data) {
                    if ($data['vystavitel'] != null) {
                        // todo pridat overeni na opravneni do uzivatelskeho modulu
                        return Html::a($data['vystavitel'], array('/uzivatel/admin/detail', 'id' => $data['vystavil']));
                    } else {
                        return $data['vystavitel'];
                    }
                },
                'format' => 'raw',
                'contentOptions' => [
                    'class' => 'show-detail-column'
                ]
            ],
            'akce' => [
                'class' => 'app\components\columns\ActionColumn',
                'header' => 'Akce',
                'urlCreator' => function ($action, $model, $key, $index) {
                    switch ($action) {
                        case 'view':
                            $url = array('/faktury/default/detail', 'id' => $model['faktura_pk']);
                            break;
                        case 'update':
                            $url = array('/faktury/default/upravit', 'id' => $model['faktura_pk']);
                            break;
                        case 'delete':
                            $url = array('/faktury/default/smazat', 'id' => $model['faktura_pk']);
                            break;
                        case 'paid':
                            $url = array('/faktury/default/zaplaceno', 'id' => $model['faktura_pk']);
                            break;
                        default:
                            throw new \Exception("Undefined action '{$action}' for model");
                    }
                    return $url;
                },
                'template' => '{view} {update} {paid} {delete}',
                'buttons' => [
                    'view'   => function($url, $model, $key) {
                        return Html::a(
                            Html::tag('span', '', [
                                'class' => 'glyphicon glyphicon-eye-open'
                            ]),
                            $url,
                            [
                                'data-pjax' => 0,
                                'title' => 'Náhled'
                            ]
                        );
                    },
                    'update' => function($url, $model, $key) {
                        if ($model['stav'] == Faktura::STAV_ZALOZENA) {
                            return Html::a(
                                Html::tag('span', '', [
                                    'class' => 'glyphicon glyphicon-pencil'
                                ]),
                                $url,
                                [
                                    'data-pjax' => 0,
                                    'title' => 'Upravit'
                                ]
                            );
                        } else {
                            return null;
                        }
                    },
                    'paid' => function($url, $model, $key) {
                        if ($model['stav'] == Faktura::STAV_VYSTAVENA) {
                            return Html::a(
                                Html::icon('piggy-bank'),
                                $url,
                                [
                                    'data-pjax' => 0,
                                    'title' => 'Zaplaceno',
                                    'data-confirm' => "Potvrzením budou faktura č. {$model['cislo']} a objednávka č. {$model['objednavka']} označeny jako zaplacené.",
                                    'data-method' => "post",
                                ]
                            );
                        }

                        return null;
                    },
                    'delete' => function($url, $model, $key) {
                        if ($model['stav'] >= Faktura::STAV_ZALOZENA) {
                            return Html::a(
                                Html::tag('span', '', [
                                    'class' => 'glyphicon glyphicon-trash'
                                ]),
                                $url,
                                [
                                    'data-pjax' => 0,
                                    'data-confirm' => "Opravdu chcete smazat objednávku č. {$model['cislo']}?",
                                    'data-method' => "post",
                                    'title' => 'Smazat'
                                ]
                            );
                        } else {
                            return null;
                        }
                    },
                ]
            ],
        ];
    }

    /**
     * @return array
     */
    public static function itemAliasData()
    {
        return [
            'stav' => [
                self::STAV_ZALOZENA => 'Založená',
                self::STAV_VYSTAVENA => 'Vystavená',
                self::STAV_ZAPLACENA => 'Zaplacená',
                self::STAV_STORNO => 'Stornovaná'
            ]
        ];
    }

    /**
     * @param array $where
     * @param array $params
     * @param int $pagination
     * @return SqlDataProvider
     */
    protected function _vratDataProvider($where = [], $params = [], $pagination = 10)
    {
        $sql = "
            SELECT * FROM (
                SELECT
                      f.*
                    , sum(fp.cena_celkem_dph) AS cena_celkem
                    , o.cislo AS objednavka
                    , u.prijmeni || ' ' || u.jmeno AS vystavitel
                    , coalesce(z.prijmeni || ' ' || z.jmeno, '---') as zakaznik
                    , coalesce(o.zakaznik_pk::TEXT, '-') as zakaznik_pk
                FROM faktura f
                    LEFT JOIN uzivatel u ON f.vystavil = u.uzivatel_pk
                    LEFT JOIN objednavka o ON f.objednavka_pk = o.objednavka_pk
                    LEFT JOIN zakaznik z ON o.zakaznik_pk = z.zakaznik_pk
                    LEFT JOIN faktura_fakturacni_udaje ffu ON f.faktura_pk = ffu.faktura_pk
                    LEFT JOIN fakturacni_udaje fu ON ffu.fakturacni_udaje_pk = fu.fakturacni_udaje_pk
                    -- LEFT JOIN adresa a ON fu.adresa_pk = a.adresa_pk
                    JOIN faktura_polozka fp ON fp.faktura_pk = f.faktura_pk
                WHERE f.stav > -2 -- nechci smazane faktury
            GROUP BY f.faktura_pk, o.cislo, u.prijmeni, u.jmeno, z.prijmeni, z.jmeno, o.zakaznik_pk
            ) as t
        ";

        $aWhere = $where;
        if ($this->cislo != null) {
            $params['cislo'] = $this->cislo;
            $aWhere[] = "t.cislo = :cislo";
        }

        if ($this->vystavil != null) {
            $params['vystavil'] = $this->vystavil;
            $aWhere[] = "t.vystavil = :vystavil";
        }

        if ($this->stav != null) {
            if (!is_array($this->stav)) $this->stav = [$this->stav];

            $params[':stav'] = DbUtils::implodePgArray($this->stav);
            $aWhere[] = ' t.stav = ANY (:stav) ';
        }

        if ($this->datum_vystaveni != null) {
            if (preg_match('/^(\d{1,2}[.]\d{1,2}[.]\d{4}) až (\d{1,2}[.]\d{1,2}[.]\d{4})$/', $this->datum_vystaveni, $matches)) {
                $params[':vystaveno_od'] = $matches[1];
                $params[':vystaveno_do'] = $matches[2];

                $aWhere[] = ' t.datum_vystaveni between :vystaveno_od::date and :vystaveno_do::date ';
            }
        }

        if ($this->datum_splatnosti != null) {
            if (preg_match('/^(\d{1,2}[.]\d{1,2}[.]\d{4}) až (\d{1,2}[.]\d{1,2}[.]\d{4})$/', $this->datum_splatnosti, $matches)) {
                $params[':splatnost_od'] = $matches[1];
                $params[':splatnost_do'] = $matches[2];

                $aWhere[] = ' t.datum_splatnosti between :splatnost_od::date and :splatnost_do::date ';
            }
        }

        if (!empty($aWhere)) {
            $sql .= " where " . implode(" and ", $aWhere);
        }

        return new SqlDataProvider([
            'sql' => $sql,
            'params' => $params,
            'pagination' => $pagination === false ? false : [
                'pageSize' => $pagination
            ],
            'sort' => [
                'defaultOrder' => ['faktura_pk' => SORT_DESC],
                'attributes' => ['faktura_pk', 'cislo', 'cena_celkem', 'stav', 'datum_vystaveni', 'datum_splatnosti', 'vystavitel']
            ]
        ]);
    }

    /**
     * @param $polozka FakturaPolozka
     * @return bool|null|string
     */
    public function pridejPolozku($polozka)
    {
        // todo to proto, ze jsem udelal na zacatku delete, upravit jeste?..
        $polozka->faktura_polozka_pk = null;

        // todo pocty lepsi nad db, ze
        $polozka->cena_ks_dph       = (float) str_replace(',', '.', $polozka->cena_ks_dph);
        $polozka->cena_ks           = $polozka->cena_ks_dph / (1 + (float) $polozka->dph);
        $polozka->cena_celkem       = $polozka->cena_ks * $polozka->kusu;
        $polozka->cena_celkem_dph   = $polozka->cena_ks_dph * $polozka->kusu;

        if ($polozka->faktura_pk == null) {
            $polozka->faktura_pk = $this->faktura_pk;
        }

        return $polozka->uloz();
    }

    /**
     * @param integer $faktura_pk
     * @param array $spojit
     * @param bool $stejny_pocet
     * @return bool|null|string
     */
    public function spojPolozky($faktura_pk, $spojit, $stejny_pocet = false)
    {
        $sql = "select faktura_spoj_polozky(:faktura, :polozky, :stejny)";
        $params = [
            ':faktura' => $faktura_pk,
            ':polozky' => DbUtils::implodePgArray($spojit),
            ':stejny' => $stejny_pocet
        ];

        return \Yii::$app->db->createCommand($sql)->bindValues($params)->queryScalar();
    }

    /**
     * Nastavi datum zaplaceni na ted, prepne fakturu do spravneho stavu.
     * Mozna by mohlo poslat nejaky mail s notifikaci pripadne neco takoveho.
     */
    public function oznacJakoZaplacena()
    {
        $sql = "select * from faktura_zaplacena(:pk)";
        $params = [
            ':pk' => $this->faktura_pk
        ];

        $result = \Yii::$app->db->createCommand($sql)->bindValues($params)->queryScalar();

        if ($result < 0) {
            \Yii::error("Nepodarilo se nastavit fakturu na zaplacenou, vysledek: {$result}");
            return false;
        }

        return true;
    }

    /**
     * V podstatě fakturu nemaže, pouze jí stornuje
     */
    public function smaz()
    {
        $sql = "select * from faktura_smaz(:pk)";
        $params = [
            ':pk' => $this->faktura_pk
        ];

        $result = \Yii::$app->db->createCommand($sql, $params)->queryOne();

        if ($result['faktura_smaz'] === 1) {
            return true;
        } else {
            \Yii::error("Chyba pri mazani faktury, vysledek: {$result['faktura_smaz']}a");
            return false;
        }
    }
}