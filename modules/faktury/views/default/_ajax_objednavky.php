<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 22.08.2015
 * Time: 22:54
 *
 * @var $mObjednavka Objednavka
 */

use app\modules\objednavky\models\Objednavka;

\yii\widgets\Pjax::begin(['timeout' => false, 'enablePushState' => false]);

echo \kartik\grid\GridView::widget(array(
    'id' => 'faktura-objednavka-grid',
    'dataProvider' => $mObjednavka->search(),
    'columns' => $mObjednavka->vratSloupce(),
    'resizableColumns' => false,
    'export' => false,
));

\yii\widgets\Pjax::end();