<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 7. 4. 2015
 * Time: 21:43
 *
 * @var $this View
 * @var $mFaktura Faktura
 */

use app\components\Html;
use app\modules\faktury\models\Faktura;
use yii\web\View;

$this->title = Yii::$app->name . ' - Seznam faktur';

$this->params['breadcrumbs'] = array(
    'Faktury'
);
?>

<div class="h2-buttons">
    <h2>Faktury</h2>
    <?php
    echo Html::a(
        'Přidat',
        ['/faktury/default/pridat'],
        ['class' => 'btn btn-success']
    );
    ?>
    <div class="clearfix"></div>
</div>

<?php
echo $this->render('_search', ['mFaktura' => $mFaktura]);
?>

<?php
echo \kartik\grid\GridView::widget(array(
    'dataProvider' => $mFaktura->search(),
    'columns' => $mFaktura->vratSloupce(),
    'resizableColumns' => false,
    'export' => false
));