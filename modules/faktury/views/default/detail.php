<?php
use app\components\Html;
use app\modules\faktury\models\Faktura;
use app\modules\faktury\models\FakturaPolozka;
use app\modules\objednavky\models\FakturacniUdaje;
use app\modules\objednavky\models\Objednavka;
use app\modules\objednavky\models\Zakaznik;
use app\modules\uzivatel\models\Uzivatel;
use kartik\widgets\DatePicker;
use kartik\widgets\DateTimePicker;
use kartik\form\ActiveForm;
use yii\bootstrap\Modal;
use yii\web\View;

/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 13. 4. 2015
 * Time: 22:31
 *
 * @var $this View
 * @var $mFaktura Faktura
 * @var $mFakturaPolozka FakturaPolozka
 * @var $mZakaznik Zakaznik
 * @var $mDodavatelUdaje FakturacniUdaje
 */
$cena = 0;
$cena_dph = 0;

$this->title = Yii::$app->name . ' - Faktura ' . $mFaktura->cislo;

$this->params['breadcrumbs'] = array(
    array('label' => 'Faktury', 'url' => array('/faktury/default/index')),
    $mFaktura->cislo
);

if ($mFaktura->vystavil != null) {
    /** @var Uzivatel $mUzivatel */
    $mUzivatel = Uzivatel::findOne($mFaktura->vystavil);
    $vystavil = "{$mUzivatel->jmeno} {$mUzivatel->prijmeni}";
} else {
    $vystavil = null;
}

if ($mFaktura->objednavka->zpusob_platby != null) {
    $doprava = Objednavka::itemAlias('typ_platby', $mFaktura->objednavka->zpusob_platby);
} else {
    $doprava = "Osobně Smržovka";
}
?>
<div class="row">
    <div class="col-sm-12">
        <div class="h2-buttons">
            <div class="pull-left">
                <?php
                if ($mFaktura->stav == Faktura::STAV_ZALOZENA) {
//                if ($mFaktura->objednavka->vlozil == Yii::$app->user->id) {
                    echo Html::a(
                        'Upravit',
                        ['/faktury/default/upravit', 'id' => $mFaktura->faktura_pk],
                        array(
                            'class' => 'btn btn-warning',
                        )
                    );
//                }

                    echo Html::a(
                        'Vystavit',
                        '#',
//                    ['/faktury/default/vystavit', 'id' => $mFaktura->faktura_pk],
                        [
                            'class' => 'btn btn-info',
                            'data-toggle' => 'modal',
                            'data-target' => '#faktura-vystavit-modal'
                        ]
                    );
                }

                if ($mFaktura->stav == Faktura::STAV_VYSTAVENA) {
                    echo Html::a(
                        'Zaplaceno',
                        ['/faktury/default/zaplaceno', 'id' => $mFaktura->faktura_pk],
                        array(
                            'class' => 'btn btn-success',
                        )
                    );
                }
                ?>
            </div>

            <div class="pull-right">
                <?php
                echo Html::a(
                    Html::icon('print'), // . ' Tisk',
                    '#',
                    array(
                        'class' => 'btn btn-info',
                        'data-toggle' => 'modal',
                        'data-target' => '#objednavka-stav-modal',
                        'title' => 'Vytisknout'
                    )
                );
                echo Html::a(
                    Html::icon('save-file'), // . ' PDF',
                    ['/faktury/default/stahni-pdf', 'id' => $mFaktura->faktura_pk],
                    [
                        'class' => 'btn btn-info',
                        'target' => '_tab',
                        'title' => 'Uložit PDF'
                    ]
                );
                ?>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<div id="faktura-detail" style="width: 100%; max-width: 800px; margin: 0px auto;">
    <div id="faktura-header" class="row">
        <div class="col-sm-12" style="text-align: center;">
            <h2>Faktura <?= $mFaktura->cislo ?></h2>
        </div>
    </div>

    <div id="faktura-adresy" class="row">
        <div id="dodavatel" class="col-sm-6">
            <b>Dodavatel</b>
            <ul>
                <li><?= $mDodavatelUdaje->obchodni_jmeno ?></li>
                <li><?= $mDodavatelUdaje->jmeno ?> <?= $mDodavatelUdaje->prijmeni ?></li>
                <li><?= $mDodavatelUdaje->ulice ?></li>
                <li><?= $mDodavatelUdaje->psc ?> <?= $mDodavatelUdaje->mesto ?></li>
                <li>IČ: <?= $mDodavatelUdaje->ic ?>, DIČ: <?= $mDodavatelUdaje->dic ?></li>
                <li>Tel: +420737503884</li>
                <li>Email: <a href="mailto:vojtech.svab@voplasto.cz">vojtech.svab@voplasto.cz</a></li>
            </ul>
            <i>Nejsme plátci DPH</i>
        </div>
        <div id="odberatel" class="col-sm-6">
            <b>Odběratel</b>
            <ul>
                <li><?= $mFaktura->fakturacni_udaje->jmeno ?> <?= $mFaktura->fakturacni_udaje->prijmeni ?></li>
                <?php if ($mFaktura->fakturacni_udaje->ic != null) { ?>
                <li><?= $mFaktura->fakturacni_udaje->obchodni_jmeno ?></li>
                <li>IČ: <?= $mFaktura->fakturacni_udaje->ic ?: '---' ?>, DIČ: <?= $mFaktura->fakturacni_udaje->dic ?: '---' ?></li>
                <?php } ?>
                <li><?= $mFaktura->fakturacni_udaje->ulice ?></li>
                <li><?= $mFaktura->fakturacni_udaje->psc ?> <?= $mFaktura->fakturacni_udaje->mesto ?></li>

                <li>Tel.: <?= $mZakaznik->telefon ?></li>
                <?php
                if ($mZakaznik->email != null) {
                    echo '<li>Email: <a href="mailto:' . $mZakaznik->email . '">' . $mZakaznik->email . '</a></li>';
                }
                ?>
            </ul>
        </div>
    </div>

    <div class="row">
        <div id="faktura-udaje" class="col-sm-6">
            <div>
                <table>
                    <tbody>
                    <tr>
                        <th>Datum založení:</th>
                        <td><?= Yii::$app->formatter->asDate($mFaktura->datum_zalozeni) ?></td>
                    </tr>
                    <tr>
                        <th>Datum vystavení:</th>
                        <td><?= Yii::$app->formatter->asDate($mFaktura->datum_vystaveni) ?></td>
                    </tr>
                    <tr>
                        <th>Datum splatnosti:</th>
                        <td><?= Yii::$app->formatter->asDate($mFaktura->datum_splatnosti) ?></td>
                    </tr>
                    <tr>
                        <th>Způsob dopravy:</th>
                        <td><?= Yii::$app->formatter->asText($doprava) ?></td>
                    </tr>
                    <tr>
                        <th>Vystavil:</th>
                        <td><?= Yii::$app->formatter->asText($vystavil) ?></td>
                    </tr>
                    <tr>
                        <th>Objednávka:</th>
                        <td><?= Yii::$app->formatter->asText($mFaktura->objednavka->cislo) ?> z <?= Yii::$app->formatter->asDate($mFaktura->objednavka->datum_zalozeni) ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-12">
                    <h3>Položky faktury</h3>
                </div>
            </div>

            <div id="polozky-wrapper" class="grid-view">
                <div id="polozky-faktury" class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Kód</th>
                            <th>Produkt</th>
                            <th>Kusů</th>
                            <th>Cena ks</th>
                            <th>Celkem</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($mFaktura->polozky as $mFakturaPolozka) {
                            $cena += $mFakturaPolozka->cena_celkem;
                            $cena_dph += $mFakturaPolozka->cena_celkem_dph;

                            echo $this->render('_detail_polozka', ['mFakturaPolozka' => $mFakturaPolozka]);
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div id="faktura-ceny" class="col-sm-12">
            <span id="cena_dph">
                Cena celkem: <?= number_format($cena_dph, 2, ',', '&nbsp;') ?>&nbsp;Kč
            </span>
        </div>
    </div>

    <div id="faktura-vystavil" class="row">
        <div class="col-sm-6 text-left">
            Vystavil: ....................................
        </div>
        <div class="col-sm-6 text-right">
            Převzal: ....................................
        </div>
    </div>
</div>

<?php
/* --- modální okno s gridem - položky --- */
Modal::begin([
    'header' => '<h3>Vystavení faktury</h3>',
    'id' => 'faktura-vystavit-modal'
]);
?>
<p>
    Pokud je faktura v pořádku, vyberte datum splatnosti a potvrzením ji vystavíte.
</p>
<?php
// todo tohle je obloz, poresit
if ($mFaktura->datum_splatnosti != null) {
    $mFaktura->datum_splatnosti = Yii::$app->formatter->asDate($mFaktura->datum_splatnosti);
} else {
    $mFaktura->datum_splatnosti = Yii::$app->formatter->asDate(strtotime('+ 14 days'));
}

$form = ActiveForm::begin(array(
    'id' => 'faktura-vystavit-form',
    'method' => 'POST',
    'action' => array('/faktury/default/vystavit', 'id' => $mFaktura->faktura_pk),
    'type' => 'horizontal',
    'formConfig' => array(
        'labelSpan' => 3,
        'deviceSize' => ActiveForm::SIZE_SMALL
    ),
    'options' => array(
    )
));
?>
    <div class="form-fields">
        <div class="row">
            <div class="col-sm-12">
                <?= Html::activeHiddenInput($mFaktura, 'faktura_pk'); ?>
                <?= $form->field($mFaktura, 'datum_splatnosti')->widget(DateTimePicker::className(), [
                    'removeButton' => false,
                    'convertFormat' => true,
                    'pluginOptions' => [
//                        'initialDate' => Yii::$app->formatter->asDate($mFaktura->datum_splatnosti == null ? strtotime('+ 14 days') : $mFaktura->datum_splatnosti),
                        'autoclose'=> true,
                        'format' => 'php:j.n.Y',
                        'calendarWeeks' => true,
                        'startView' => 'month',
                        'minView' => 'month',
                        'startDate' => Yii::$app->formatter->asDate(strtotime('+ 14 days')),
                    ]
                ]); ?>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <?php
        echo Html::submitButton('Vystavit', array(
            'class' => 'btn btn-success'
        ));
        echo Html::a('Zrušit', '#', array(
            'class' => 'btn btn-danger form-cancel'
        ));
        ?>
    </div>
<?php
ActiveForm::end();

Modal::end();
/* --- ------------------------------- --- */

$this->registerJs("
$('a.form-cancel', '#faktura-vystavit-form .form-actions').on('click', function() {
    var form = $(this).closest('form');
    var modal = $('#faktura-vystavit-modal');

    form[0].reset();
    modal.modal('hide');
});
");