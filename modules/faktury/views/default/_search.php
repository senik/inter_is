<?php
use app\components\Html;
use app\modules\faktury\models\Faktura;
use app\modules\uzivatel\models\Uzivatel;
use kartik\daterange\DateRangePicker;
use kartik\widgets\ActiveForm;
use yii\web\View;
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 13. 4. 2015
 * Time: 21:37
 *
 * @var $this View
 * @var $mFaktura Faktura
 */
$form = ActiveForm::begin(array(
    'method' => 'get',
    'action' => array('/faktury/default/index'),
    'type' => 'horizontal',
    'options' => array(
        'class' => 'well'
    ),
    'formConfig' => ['labelSpan' => 3, 'spanSize' => ActiveForm::SIZE_SMALL],
    'enableClientValidation' => false
));

$vystavili = array('' => 'Všichni') + Uzivatel::vratVystaviteleProDropdown();
?>
    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($mFaktura, 'cislo') ?>
        </div>

        <div class="col-sm-4">
            <?= $form->field($mFaktura, 'vystavil')->dropDownList($vystavili, [
                'title' => 'Všichni / vyberte',
                'data-live-search' => "true",
                'class' => 'selectpicker'
            ]) ?>
        </div>

        <div class="col-sm-4">
            <?= $form->field($mFaktura, 'stav')->dropDownList(Faktura::itemAlias('stav'), [
                'title' => 'Všechny / vyberte',
                'multiple' => 'multiple',
                'class' => 'selectpicker'
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($mFaktura, 'datum_vystaveni', array(
                'addon' => ['append' => ['content' => '<i class="glyphicon glyphicon-calendar"></i>']],
            ))->widget(DateRangePicker::className(), [
                'useWithAddon' => true,
                'convertFormat' => true,
                'pluginOptions' => array(
                    'format' => 'j.n.Y',
                    'separator' => ' až ',
                    'ranges' => array(
                        "Dnes" => ["moment()", "moment()"],
                        "Včera" => ["moment().subtract('days', 1)", "moment().subtract('days', 1)"],
                        "Posledních 7 dní" => ["moment().subtract('days', 6)", "moment()"],
                        "Posledních 30 dní" => ["moment().subtract('days', 29)", "moment()"],
                        "Tento měsíc" => ["moment().startOf('month')", "moment().endOf('month')"],
                        "Předchozí měsíc" => ["moment().subtract('month', 1).startOf('month')", "moment().subtract('month', 1).endOf('month')"],
                    )
                )
            ])->label('Vystavení') ?>
        </div>

        <div class="col-sm-4">
            <?= $form->field($mFaktura, 'datum_splatnosti', array(
                'addon' => ['append' => ['content' => '<i class="glyphicon glyphicon-calendar"></i>']],
            ))->widget(DateRangePicker::className(), [
                'useWithAddon' => true,
                'convertFormat' => true,
                'pluginOptions' => array(
                    'format' => 'j.n.Y',
                    'separator' => ' až ',
                    'ranges' => array(
                        "Dnes" => ["moment().startOf('day')", "moment().endOf('day')"],
                        "Včera" => ["moment().subtract('days', 1)", "moment().subtract('days', 1)"],
                        "Posledních 7 dní" => ["moment().subtract('days', 6)", "moment()"],
                        "Posledních 30 dní" => ["moment().subtract('days', 29)", "moment()"],
                        "Tento měsíc" => ["moment().startOf('month')", "moment().endOf('month')"],
                        "Předchozí měsíc" => ["moment().subtract('month', 1).startOf('month')", "moment().subtract('month', 1).endOf('month')"],
                        // "Po splatnosti" => ["moment('20000101', 'YYYYMMDD')", "moment().subtract('days', 1)"]
                    )
                )
            ])->label('Splatnost') ?>
        </div>
    </div>

    <div class="form-actions row">
        <div class="col-sm-6">
            <div class="pull-left">
                <?php
                echo Html::submitButton('Filtrovat', array('class' => 'btn btn-success'));
                echo Html::submitButton('Vymazat filtr', array('class' => 'btn btn-warning filter-reset'));
                ?>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="pull-right filter-preset">
                <b>Předvolby filtru:</b>
                <?= Html::a('Po splatnosti', ['/faktury/default/index'], array('class' => 'btn btn-info', 'id' => 'btn-faktury-po-splatnosti')); ?>
            </div>
        </div>
    </div>

<?php
ActiveForm::end();

$this->registerJs("
$('#btn-faktury-po-splatnosti').on('click', function() {
    $('#faktura-datum_splatnosti').val(moment('20000101', 'YYYYMMDD').format('D.M.YYYY')+' až '+moment().subtract('days', 1).format('D.M.YYYY'));
    $('#faktura-stav').val(2).selectpicker('refresh');

    return false;
});
");