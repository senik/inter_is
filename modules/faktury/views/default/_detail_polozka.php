<?php
use app\modules\faktury\models\FakturaPolozka;
use yii\web\View;
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 13. 4. 2015
 * Time: 23:09
 *
 * @var $this View
 * @var $mFakturaPolozka FakturaPolozka
 */
?>

<tr class="polozka-faktury">
    <td>
        <?= $mFakturaPolozka->kod ?>
    </td>

    <td>
        <?= $mFakturaPolozka->nazev ?>
    </td>

    <td>
        <?= $mFakturaPolozka->kusu ?>
    </td>

    <td class="price-column">
        <?= number_format($mFakturaPolozka->cena_ks * (1 + $mFakturaPolozka->dph), 2, ',', '&nbsp;') ?>&nbsp;Kč
    </td>

    <td class="price-column">
        <?= number_format($mFakturaPolozka->cena_celkem_dph, 2, ',', '&nbsp;') ?>&nbsp;Kč
    </td>
</tr>