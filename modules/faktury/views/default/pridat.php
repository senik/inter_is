<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 8. 8. 2015
 * Time: 17:22
 *
 * @var $this View
 * @var $mFaktura Faktura
 * @var $mFakturacniUdaje FakturacniUdaje
 * @var $mDodaciUdaje DodaciUdaje
 * @var $mZakaznik Zakaznik
 * @var $mObjednavkaSearch Objednavka
 */

use app\modules\faktury\models\Faktura;
use app\modules\objednavky\models\DodaciUdaje;
use app\modules\objednavky\models\FakturacniUdaje;
use app\modules\objednavky\models\Objednavka;
use app\modules\objednavky\models\Zakaznik;
use yii\helpers\Html;
use yii\web\View;

$this->title = Yii::$app->name . ' - Nová faktura';

$this->params['breadcrumbs'] = array(
    array('label' => 'Faktury', 'url' => array('/faktury/default/index')),
    'Nová'
);
?>

<div class="h2-buttons">
    <h2>Nová faktura</h2>
    <?= Html::dropDownList('typ', $mFaktura->typ, ['objednavka' => 'Z objednávky', 'prazdna' => 'Prázdná'], ['id' => 'faktura-typ-toggle', 'class' => 'selectpicker']) ?>
    <div class="clearfix"></div>
</div>

<?= $this->render('_form', array(
    'mFaktura' => $mFaktura,
    'mFakturacniUdaje' => $mFakturacniUdaje,
    'mDodaciUdaje' => $mDodaciUdaje,
    'mZakaznik' => $mZakaznik,
    'mObjednavkaSearch' => $mObjednavkaSearch
)) ?>