<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 01.09.2015
 * Time: 20:44
 *
 * @var $mFaktura Faktura
 * @var $mFakturacniUdaje FakturacniUdaje
 * @var $mZakaznik Zakaznik
 * @var $this View
 */

use app\modules\faktury\models\Faktura;
use app\modules\objednavky\models\FakturacniUdaje;
use app\modules\objednavky\models\Zakaznik;
use kartik\helpers\Html;
use yii\web\View;

if ($mFakturacniUdaje != null) {
    echo '<ul class="fakturacni-udaje">';
    {
        echo Html::activeHiddenInput($mFakturacniUdaje, 'fakturacni_udaje_pk');
        echo Html::activeHiddenInput($mFakturacniUdaje, 'obchodni_jmeno', ['id' => 'hidden-fu-obchodni_jmeno']);
        echo Html::activeHiddenInput($mFakturacniUdaje, 'jmeno', ['id' => 'hidden-fu-jmeno']);
        echo Html::activeHiddenInput($mFakturacniUdaje, 'prijmeni', ['id' => 'hidden-fu-prijmeni']);
        echo Html::activeHiddenInput($mFakturacniUdaje, 'ulice', ['id' => 'hidden-fu-ulice']);
        echo Html::activeHiddenInput($mFakturacniUdaje, 'mesto', ['id' => 'hidden-fu-mesto']);
        echo Html::activeHiddenInput($mFakturacniUdaje, 'psc', ['id' => 'hidden-fu-psc']);
        echo Html::activeHiddenInput($mFakturacniUdaje, 'ic', ['id' => 'hidden-fu-ic']);
        echo Html::activeHiddenInput($mFakturacniUdaje, 'dic', ['id' => 'hidden-fu-dic']);
        echo Html::activeHiddenInput($mFakturacniUdaje, 'typ', ['id' => 'hidden-fu-typ']);
        if ($mFakturacniUdaje->ic != null) {
            echo "<li id=\"fu-obchodni-jmeno\">{$mFakturacniUdaje->obchodni_jmeno}</li>";
            echo "<li id=\"fu-ic-dic\">IČ: ". ($mFakturacniUdaje->ic ?: '---') . ", DIČ: " . ($mFakturacniUdaje->dic ?: '---') . "</li>";
        }

        echo "<li id=\"fu-zakaznik-jmeno\">{$mFakturacniUdaje->jmeno} {$mFakturacniUdaje->prijmeni}</li>";
        echo "<li id=\"fu-ulice\">{$mFakturacniUdaje->ulice}</li>";
        echo "<li id=\"fu-adresa\">{$mFakturacniUdaje->psc}, {$mFakturacniUdaje->mesto}</li>";
    }
    echo '</ul>';
}