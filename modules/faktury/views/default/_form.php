<?php
use app\components\Html;
use app\modules\faktury\models\Faktura;
use app\modules\objednavky\models\DodaciUdaje;
use app\modules\objednavky\models\FakturacniUdaje;
use app\modules\objednavky\models\Objednavka;
use app\modules\objednavky\models\Zakaznik;
use kartik\widgets\ActiveForm;
use yii\bootstrap\Modal;
use yii\web\View;
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 20. 4. 2015
 * Time: 22:21
 *
 * @var $this View
 * @var $mFaktura Faktura
 * @var $mFakturacniUdaje FakturacniUdaje
 * @var $mDodaciUdaje DodaciUdaje
 * @var $mZakaznik Zakaznik
 * @var $mObjednavkaSearch Objednavka
 */

$form = ActiveForm::begin(array(
    'id' => 'faktura-form',
    'type' => 'horizontal',
    'formConfig' => array(
        'labelSpan' => 3,
        'deviceSize' => ActiveForm::SIZE_SMALL
    )
));

?>
<div class="form-fields">
    <div class="row">
        <!-- Levá část (formulářové prvky) -->
        <div class="col-sm-6">
            <!-- číslo faktury -->
            <?= $form->field($mFaktura, 'cislo')->textInput(['disabled' => 'disabled'])->hint('Číslo faktury se generuje při vystavení') ?>
            <?= Html::activeHiddenInput($mFaktura, 'typ') ?>

            <!-- typ objednávka, collapse -->
            <div class="collapse <?= $mFaktura->typ == Faktura::TYP_OBJEDNAVKA ? 'in' : '' ?>" id="fa-objednavka">
                <div class="form-group field-faktura-objednavka-popis <?= $mFaktura->hasErrors('objednavka_pk') ? 'has-error' : '' ?>">
                    <label class="control-label col-sm-3" for="faktura-objednavka-popis">Objednávka</label>
                    <div class="col-sm-9">
                        <div class="input-group">
                            <input type="text" id="faktura-objednavka-popis" class="form-control" disabled="disabled" value="<?= $mFaktura->objednavka->popis ?>">
                            <div class="input-group-btn">
                                <button type="button" id="objednavka-modal-detail" class="btn btn-primary xtooltip" data-original-title="Náhled objednávky" title="Náhled objednávky" <?= $mFaktura->objednavka_pk == null ? 'disabled' :'' ?>>
                                    <span class="glyphicon glyphicon-search"></span>
                                </button>
                                <button type="button" id="objednavka-modal-open" class="btn btn-primary xtooltip" data-original-title="Vyhledat objednávku" title="Vyhledat objednávku" <?= $mFaktura->faktura_pk != null ? 'disabled' :'' ?>>
                                    <span class="glyphicon glyphicon-th-list"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-offset-2 col-sm-10"></div>
                    <div class="col-sm-offset-2 col-sm-10"><div class="help-block"><?php echo $mFaktura->getFirstError('objednavka_pk') ?></div></div>
                </div>
                <?= Html::activeHiddenInput($mFaktura, 'objednavka_pk') ?>
            </div>

            <!-- typ faktura, collapse -->
            <div class="collapse <?= $mFaktura->typ == Faktura::TYP_PRAZDNA ? 'in' : '' ?>" id="fa-prazdna">
                <?= $form->field($mZakaznik, 'zakaznik_pk')
                    ->dropDownList((new Zakaznik())->vratProDropdown(true), ['class' => 'selectpicker'])
                    ->label('Zákazník')
                    ->hint('<a href="#" data-toggle="modal" data-target="#faktura-zakaznik-modal">Nebo přidejte nového.</a>')
                ?>
            </div>

            <!-- datum splatnosti -->
            <?= $form->field($mFaktura, 'datum_splatnosti')->widget(\kartik\widgets\DateTimePicker::className(), [
                'language' => 'cs',
                'pluginOptions' => [
                    'autoclose'=> true,
                    'format' => 'd.m.yyyy',
                    'minView' => 'month'
                ],
            ])->label('Splatnost')->hint("Nebude-li vyplněno, nastaví se při vystavení na datum + 14 dní") ?>
        </div>

        <!-- Odsazeni -->
        <div class="col-sm-1"></div>

        <!-- Fakturační údaje -->
        <div class="col-sm-4 well">
            <div class="h4-buttons">
                <h4>Fakturační údaje</h4>
                <?php
                echo Html::a(
                    Html::tag('span', '', ['class' => 'glyphicon glyphicon-pencil']),
                    '#',
                    array(
                        'title' => 'Změnit',
                        'data-target' => '#fakturacni-udaje-modal'
                    )
                );
                ?>
                <div class="clearfix"></div>
            </div>
            <div id="faktura-fakturacni-udaje">
                <?php
                echo $this->render('_form_fakturacni_udaje', [
                    'mZakaznik' => $mZakaznik,
                    'mFakturacniUdaje' => $mFaktura->fakturacni_udaje,
                    //'mFakturacniUdaje' => $mFaktura->fakturacni_udaje,
                    'form' => $form
                ]);
                ?>
            </div>
        </div>

        <!-- Odsazeni -->
        <div class="col-sm-1"></div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="h3-buttons">
                <h3>Položky</h3>
                <?php
                echo Html::a(
                    'Přidat',
                    '#',
                    array(
                        'class' => 'btn btn-sm btn-success',
                        'id' => 'faktura-polozka-pridat'
                    )
                );
                ?>
                <div class="clearfix"></div>
            </div>
            <div id="faktura-polozky">
                <?php
                $hlavicka = count($mFaktura->polozky) > 0;
                foreach ($mFaktura->polozky as $klic => $mFakturaPolozka) {
                    echo $this->render('_form_polozka', [
                        'klic' => $klic,
                        'mFakturaPolozka' => $mFakturaPolozka,
                        'hlavicka' => $hlavicka
                    ]);

                    $hlavicka = false;
                }
                ?>
            </div>
        </div>
    </div>
</div>

<div class="form-actions well">
    <?php
//    echo '<div class="pull-left">';
//    {
//        echo Html::submitButton('Spojit', array(
//            'class' => 'btn btn-info hide',
//            'id' => 'faktura-spojit-btn',
//            'name' => 'spojit'
//        ));
//    }
//    echo '</div>';

    echo '<div class="pull-right">';
    {
        echo Html::submitButton('Uložit', array(
            'class' => 'btn btn-success',
            'name' => 'ulozit'
        ));
        echo Html::a('Zrušit', ['/faktury/default/index'], [
            'class' => 'btn btn-danger'
        ]);
    }
    echo '</div>';
    ?>
    <div class="clearfix"></div>
</div>

<?php
ActiveForm::end();

/* ---- ---- */
Modal::begin([
    'header' => '<h3>Nový zákazník</h3>',
    'id' => 'faktura-zakaznik-modal'
]);
?>

    Hello man!

<?php
Modal::end();
/* ---- ---- */


/* ---- ---- */
Modal::begin([
    'header' => '<h3>Úprava fakturačních údajů</h3>',
    'id' => 'fakturacni-udaje-modal'
]);
$form = ActiveForm::begin([
    'id' => 'fakturacni-udaje-form',
    'type' => 'horizontal',
    'action' => array('/ajax/uloz-fakturacni'),
    'method' => 'POST',
    'formConfig' => array(
        'labelSpan' => 3,
        'deviceSize' => ActiveForm::SIZE_SMALL
    ),
    'options' => []
]);
?>
    <div class="form-fields">
        <?= $form->field($mFakturacniUdaje, 'typ')->dropDownList(FakturacniUdaje::itemAlias('typ'), ['class' => 'selectpicker']) ?>
        <?php $display = $mFakturacniUdaje->typ == 'FO' ? 'display: none;' : '' ?>
        <div class="udaje-firma" style="<?= $display ?>">
            <?= $form->field($mFakturacniUdaje, 'ic', array(
                'addon' => array(
                    'append' => array(
                        'content' => Html::button(
                            '<i class="glyphicon glyphicon-search"></i> Vyhledat v ARES',
                            array('class' => 'btn btn-info', 'id' => 'ares-search')
                        ),
                        'asButton' => true
                    )
                )
            )) ?>
            <?= $form->field($mFakturacniUdaje, 'dic') ?>
            <?= $form->field($mFakturacniUdaje, 'obchodni_jmeno') ?>
        </div>
        <div class="spolecne">
            <?= $form->field($mFakturacniUdaje, 'jmeno') ?>
            <?= $form->field($mFakturacniUdaje, 'prijmeni') ?>
            <?= $form->field($mFakturacniUdaje, 'ulice') ?>
            <?= $form->field($mFakturacniUdaje, 'mesto') ?>
            <?= $form->field($mFakturacniUdaje, 'psc') ?>
        </div>
    </div>

    <div class="form-actions">
        <?php
        echo Html::submitButton('Uložit', array(
            'class' => 'btn btn-success',
            'name' => 'udaje'
        ));
        echo Html::a('Zrušit', '#', array(
            'class' => 'btn btn-danger modal-close'
        ));
        ?>
    </div>
<?php
ActiveForm::end();

Modal::end();
/* ---- ---- */


/* ---- ---- */
Modal::begin([
    'header' => '<h3>Vyhledat objednávku</h3>',
    'id' => 'faktura-objednavka-modal'
]);

$form = ActiveForm::begin([
    'id' => 'faktura-objednavka-search-form',
    'type' => 'horizontal',
    'formConfig' => array(
        'labelSpan' => 3,
        'deviceSize' => ActiveForm::SIZE_SMALL
    ),
    'options' => array(
        'class' => 'well'
    )
]);
?>
<div class="form-fields">
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($mObjednavkaSearch, 'zakaznik_pk') ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($mObjednavkaSearch, 'datum_zalozeni', array(
                'addon'=>['append'=>['content'=>'<i class="glyphicon glyphicon-calendar"></i>']],
            ))->widget(\kartik\daterange\DateRangePicker::className(), array(
                'useWithAddon' => true,
                'convertFormat' => true,
                'pluginOptions' => array(
                    'format' => 'j.n.Y',
                    'separator' => ' až ',
                    'ranges' => array(
                        "Dnes" => ["moment()", "moment()"],
                        "Včera" => ["moment().subtract('days', 1)", "moment().subtract('days', 1)"],
                        "Posledních 7 dní" => ["moment().subtract('days', 6)", "moment()"],
                        "Posledních 30 dní" => ["moment().subtract('days', 29)", "moment()"],
                        "Tento měsíc" => ["moment().startOf('month')", "moment().endOf('month')"],
                        "Předchozí měsíc" => ["moment().subtract('month', 1).startOf('month')", "moment().subtract('month', 1).endOf('month')"],
                    ),
                    'opens' => 'left'
                )
            )) ?>
        </div>
    </div>
</div>

<div class="form-actions">
    <?php
    echo Html::submitButton('Filtrovat', array(
        'class' => 'btn btn-success'
    ));
    ?>
</div>

<?php
ActiveForm::end();
?>

<div id="faktura-objednavka-modal-objednavky">
    Vole, nejsou tu žádný objednávky... !!!
</div>

<?php
Modal::end();
/* ---- ---- */


/* ---- ---- */
Modal::begin([
    'header' => '',
    'id' => 'objednavka-detail-modal'
]);
?>

    Hello man!

<?php
Modal::end();
/* ---- ---- */


$polozkaPridatUrl = Yii::$app->urlManager->createAbsoluteUrl(['faktury/faktury/ajax-pridat-polozku']);
$objednavkyHledatUrl = Yii::$app->urlManager->createAbsoluteUrl(['faktury/faktury/ajax-vyhledat-objednavky']);
$fakturacniUdajeUrl = Yii::$app->urlManager->createAbsoluteUrl(['faktury/faktury/ajax-fakturacni-udaje']);
$fakturaObjednavkaUrl = Yii::$app->urlManager->createAbsoluteUrl(['objednavky/default/ajax-detail-modal']);
$this->registerJs("
    Senovo.hideSpojitButton();
", View::POS_END);

$this->registerJs("
(function(Senovo, $, undefined) {
    Senovo.fakturaPridejObjednavku = function (pk, polozky, text) {
        $('#faktura-objednavka-popis').val(text);
        $('#faktura-objednavka_pk').val(pk);
        $('#objednavka-modal-detail').removeAttr('disabled');

        $('#faktura-objednavka-modal').modal('hide');

        $('#faktura-form').addClass('loading');

        var html = '';
        $('#faktura-polozky').html(html);
        footerMax();

        // vykreslim fakturacni udaje
        Senovo.fakturaVykresliFakturacniUdaje(pk);
        // a zacnu vykreslovat vsechny polozky
        Senovo.fakturaVykresliPolozky(polozky);
    };

    Senovo.fakturaVykresliPolozky = function (polozky) {
        var key = 0;
        var v = polozky[0];

        var hl = $('.faktura-polozky-header').length;

        if (polozky.length == 0) {
            $('#faktura-form').removeClass('loading');

            if ($('#faktura-objednavka_pk').val() != '') {
                $('.help-block', '#fa-objednavka').html('');
                $('.field-faktura-objednavka-popis').removeClass('has-error');
            }

            return false;
        } else {
            var request = $.ajax({
                url: '{$polozkaPridatUrl}',
                data: {klic: v, pk: v, hlavicka: hl},
                type: 'POST'
            });

            request.done(function(response) {
                $('#faktura-polozky').append(response);
                $('select').selectpicker();

                footerMax();

                polozky.splice(key, 1);
                Senovo.fakturaVykresliPolozky(polozky);
            });
        }
    };

    Senovo.fakturaVykresliFakturacniUdaje = function(pk) {
        var request = $.ajax({
            url: '{$fakturacniUdajeUrl}',
            data: {objednavka_pk: pk},
            type: 'POST'
        });

        request.done(function(response) {
            var data = $.parseJSON(response);

            if (data.error != undefined) {
                $('#faktura-fakturacni-udaje').html('<div class=\"error\">'+data.error+'</div>');
            } else {
                $('#faktura-fakturacni-udaje').html(data.html);
            }

            footerMax();
        });
    };

    Senovo.objednavkaModalNactiDetail = function(pk, ev) {
        var request = $.ajax({
            url: '{$fakturaObjednavkaUrl}',
            data: {id: pk},
            type: 'POST'
        });

        request.done(function(response) {
            var data = $.parseJSON(response);

            if (data.error != undefined) {
                bootbox.alert('Chyba při načítání detailu objednávky: ' + data.error, function() {});
            } else {
                var closeButton = '<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>';
                $('#objednavka-detail-modal .modal-header').html(closeButton + data.header);
                $('#objednavka-detail-modal .modal-body').html(data.body);

                $('#objednavka-detail-modal').modal('show');
            }
        });

        request.fail(function(jqXHR, textStatus) {
            bootbox.alert('Chyba při načítání detailu objednávky: ' + textStatus, function() {});
        });
    };
}(window.Senovo = window.Senovo || {}, jQuery));

$('a[data-target=\"#fakturacni-udaje-modal\"]').on('click', function(ev) {
    jHulice = $('#hidden-fu-ulice');
    jHmesto = $('#hidden-fu-mesto');
    jHpsc = $('#hidden-fu-psc');

    if ((jHulice.length == 0 || jHulice.val() == '') && (jHmesto.length == 0 || jHmesto.val() == '') && (jHpsc.length == 0 || jHpsc.val() == '')) {

        bootbox.alert('Nemám data pro editaci fakturačních údajů!', function() {});

    } else {
        //$('#fakturacniudaje-fakturacni_udaje_pk').val(data.udaje.fakturacni_udaje_pk);
        $('#fakturacniudaje-typ').val($('#hidden-fu-typ').val());
        $('#fakturacniudaje-obchodni_jmeno').val($('#hidden-fu-obchodni_jmeno').val());
        $('#fakturacniudaje-ulice').val($('#hidden-fu-ulice').val());
        $('#fakturacniudaje-mesto').val($('#hidden-fu-mesto').val());
        $('#fakturacniudaje-psc').val($('#hidden-fu-psc').val());
        $('#fakturacniudaje-ic').val($('#hidden-fu-ic').val());
        $('#fakturacniudaje-dic').val($('#hidden-fu-dic').val());

        if ($('#hidden-fu-typ').val() == 'PO') {
            $('.udaje-firma').css('display', 'block');
        } else {
            $('.udaje-firma').css('display', 'none');
        }

        $('#fakturacni-udaje-modal').modal('show');
    }
});

$('#objednavka-modal-open').on('click', function(e) {
    e.preventDefault();

    var form = $('#faktura-objednavka-search-form');

    $('#faktura-form').addClass('loading');

    var request = $.ajax({
        url: '{$objednavkyHledatUrl}',
        type: 'POST',
        data: form.serialize()
    });

    request.done(function(response) {
        $('#faktura-objednavka-modal-objednavky').html(response);

        $('#faktura-form').removeClass('loading');
        $('#faktura-objednavka-modal').modal('show');
    });

});

$('#faktura-polozka-pridat').on('click', function() {
    $('#faktura-form').addClass('loading');

    var klicPolozky = $('.faktura-polozka').length;
    var polozkyHeader = $('.faktura-polozky-header');

    while ($('.faktura-polozka-'+klicPolozky).length != 0) {
        klicPolozky++;
    }

    var request = $.ajax({
        url: '{$polozkaPridatUrl}',
        data: {klic: klicPolozky, hlavicka: polozkyHeader.length},
        type: 'POST'
    });

    request.done(function(response) {
        $('#faktura-polozky').append(response);
        $('select').selectpicker();

        $('#faktura-form').removeClass('loading');
        footerMax();
    });

});

$('#objednavka-modal-detail').on('click', function(ev) {
    jObjednavkaPk = $('#faktura-objednavka_pk');

    if (jObjednavkaPk.length == 0 || jObjednavkaPk.val() == '') {
        bootbox.alert('Nemám data pro načtení detailu objednávky!', function() {});
    } else {
        Senovo.objednavkaModalNactiDetail(jObjednavkaPk.val(), ev);
    }
});

$(document).on('click', '.polozka-faktury-smazat', function() {
    $(this).closest('.faktura-polozka').remove();

    if ($('.faktura-polozka').length == 0) {
        $('.faktura-polozky-header').remove();
    }

    footerMax();
});

$('#faktura-typ-toggle').on('change', function() {
    var typ = $(this).val();

    $('#fa-objednavka').collapse('toggle');
    $('#fa-prazdna').collapse('toggle');
    $('#faktura-typ').val(typ);

    // vymazu vsechny polozky na fakture
    $('#faktura-polozky').html('');

    // vymazu fakturacni udaje
    $('#faktura-fakturacni-udaje').html('');

    if (typ == 'prazdna') {
        $('#faktura-objednavka-popis').val('');
        $('#faktura-objednavka_pk').val('');
    }

    footerMax();

    $('#fa-objednavka').on('hidden.bs.collapse', function() {
        footerMax();
    });

    $('#fa-objednavka').on('shown.bs.collapse', function() {
        footerMax();
    });

    $('#fa-prazdna').on('hidden.bs.collapse', function() {
        footerMax();
    });

    $('#fa-prazdna').on('shown.bs.collapse', function() {
        footerMax();
    });
});

$('#fakturacni-udaje-form').on('submit', function(ev) {
    ev.preventDefault();

    $('#hidden-fu-typ').val($('#fakturacniudaje-typ').val());
    $('#hidden-fu-obchodni_jmeno').val($('#fakturacniudaje-obchodni_jmeno').val());
    $('#hidden-fu-ulice').val($('#fakturacniudaje-ulice').val());
    $('#hidden-fu-jmeno').val($('#fakturacniudaje-jmeno').val());
    $('#hidden-fu-prijmeni').val($('#fakturacniudaje-prijmeni').val());
    $('#hidden-fu-mesto').val($('#fakturacniudaje-mesto').val());
    $('#hidden-fu-psc').val($('#fakturacniudaje-psc').val());
    $('#hidden-fu-ic').val($('#fakturacniudaje-ic').val());
    $('#hidden-fu-dic').val($('#fakturacniudaje-dic').val());

    if ($('#hidden-fu-ic').val() != '') {
        if ($('#fu-obchodni-jmeno').length == 0) {
            $('ul.fakturacni-udaje')
                .prepend('<li id=\"fu-ic-dic\"></li>')
                .prepend('<li id=\"fu-obchodni-jmeno\"></li>');
        }
    } else {
        $('li#fu-obchodni-jmeno').remove();
    }

    $('#fu-obchodni-jmeno').text($('#hidden-fu-obchodni_jmeno').val());
    $('#fu-zakaznik-jmeno').text($('#fakturacniudaje-jmeno').val() + ' ' + $('#fakturacniudaje-prijmeni').val());
    $('#fu-ulice').text($('#fakturacniudaje-ulice').val());
    $('#fu-adresa').text($('#fakturacniudaje-psc').val() + ', ' + $('#fakturacniudaje-mesto').val());
    $('#fu-ic-dic').text('IČ: ' + ($('#fakturacniudaje-ic').val() || '---') + ', DIČ: ' + ($('#fakturacniudaje-dic').val() || '---'));

    $('#fakturacni-udaje-modal').modal('hide');
});
");