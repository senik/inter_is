<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 23. 4. 2015
 * Time: 20:04
 *
 * @var $form ActiveForm
 * @var $mFakturaPolozka FakturaPolozka
 * @var $klic integer
 * @var $hlavicka boolean
 */
use app\components\Html;
use app\modules\faktury\models\FakturaPolozka;
use kartik\widgets\ActiveForm;

?>
<?php if ($hlavicka) { ?>
    <div class="faktura-polozky-header row">
        <div class="col-sm-1 polozka-spojit clearfix">
            <label>Spojit</label>
        </div>

        <div class="col-sm-2 clearfix">
            <label>Kód</label>
        </div>

        <div class="col-sm-4 clearfix">
            <label>Popis</label>
        </div>

        <div class="col-sm-1 clearfix">
            <label>Kusů</label>
        </div>

        <div class="col-sm-2 clearfix">
            <label>Cena/kus s DPH</label>
        </div>

        <div class="col-sm-2 clearfix">
            <label>DPH</label>
        </div>
    </div>

<?php } ?>

<div class="faktura-polozka faktura-polozka-<?= $klic ?> row">
    <div class="col-sm-1 polozka-spojit clearfix">
        <label>Spojit</label>
        <?= Html::checkbox("Faktura[polozky][{$klic}][FakturaPolozka][spojit]") ?>
    </div>

    <div class="col-sm-2 clearfix <?= $mFakturaPolozka->hasErrors('kod') ? 'error' : '' ?>">
        <label>Kód</label>
        <?= Html::activeTextInput($mFakturaPolozka, 'kod', [
            'name' => "Faktura[polozky][{$klic}][FakturaPolozka][kod]",
            'class' => 'form-control',
            'disabled' => 'disabled'
        ]) ?>
        <?= Html::error($mFakturaPolozka, 'kod') ?>
        <?= Html::hiddenInput("Faktura[polozky][{$klic}][FakturaPolozka][faktura_polozka_pk]", $mFakturaPolozka->faktura_polozka_pk) ?>
    </div>

    <div class="col-sm-4 clearfix <?= $mFakturaPolozka->hasErrors('nazev') ? 'error' : '' ?>">
        <label>Popis</label>
        <?= Html::activeTextInput($mFakturaPolozka, 'nazev', [
            'name' => "Faktura[polozky][{$klic}][FakturaPolozka][nazev]",
            'class' => 'form-control'
        ]) ?>
        <?= Html::error($mFakturaPolozka, 'nazev') ?>
    </div>

    <div class="col-sm-1 clearfix <?= $mFakturaPolozka->hasErrors('kusu') ? 'error' : '' ?>">
        <label>Kusů</label>
        <?= Html::activeTextInput($mFakturaPolozka, 'kusu', [
            'name' => "Faktura[polozky][{$klic}][FakturaPolozka][kusu]",
            'class' => 'form-control'
        ]) ?>
        <?= Html::error($mFakturaPolozka, 'kusu') ?>
    </div>

    <div class="col-sm-2 clearfix <?= $mFakturaPolozka->hasErrors('cena_ks_dph') ? 'error' : '' ?>">
        <label>Cena kus s DPH</label>
        <?= Html::activeTextInput($mFakturaPolozka, 'cena_ks_dph', [
            'name' => "Faktura[polozky][{$klic}][FakturaPolozka][cena_ks_dph]",
            'class' => 'form-control',
            'value' => number_format((float) str_replace(',', '.', $mFakturaPolozka->cena_ks_dph), 2, ',', '')
        ]) ?>
        <?= Html::error($mFakturaPolozka, 'cena_ks') ?>
    </div>

    <div class="col-sm-1 clearfix <?= $mFakturaPolozka->hasErrors('dph') ? 'error' : '' ?>">
        <label>DPH</label>
        <?= Html::dropDownList("Faktura[polozky][{$klic}][FakturaPolozka][dph]", $mFakturaPolozka->dph, [
            '0' => '0 %',
            '0.10' => '10 %',
            '0.15' => '15 %',
            '0.21' => '21 %',
        ], ['class' => 'selectpicker form-control']) ?>
        <?= Html::error($mFakturaPolozka, 'dph') ?>
    </div>

    <div class="col-sm-1 clearfix polozka-akce">
        <?php
        echo Html::a('<span class="glyphicon glyphicon-remove polozka-smazat" aria-hidden="true"></span>', '#', array(
            'class' => 'btn btn-primary btn-sm polozka-faktury-smazat',
            'title' => 'Smazat'
        ));
        ?>
<!--        <a href="#" class="polozka-smazat"><span class="glyphicon glyphicon-remove-sign"></span></a>-->
    </div>
</div>