<?php
use app\modules\faktury\models\Faktura;
use app\modules\objednavky\models\FakturacniUdaje;
use app\modules\objednavky\models\Objednavka;
use app\modules\objednavky\models\Zakaznik;
use yii\web\View;
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 20. 4. 2015
 * Time: 22:20
 *
 * @var $this View
 * @var $mFaktura Faktura
 * @var $mZakaznik Zakaznik
 * @var $mObjednavkaSearch Objednavka
 * @var $mFakturacniUdaje FakturacniUdaje
 */

$this->title = Yii::$app->name . ' - Editace faktury ' . $mFaktura->cislo;

$this->params['breadcrumbs'] = array(
    array('label' => 'Faktury', 'url' => array('/faktury/default/index')),
    'Upravit ' . $mFaktura->cislo
);
?>

<div class="h2-buttons">
    <h2>Upravit fakturu</h2>
    <div class="clearfix"></div>
</div>

<?= $this->render('_form', array(
    'mFaktura' => $mFaktura,
    'mZakaznik' => $mZakaznik,
    'mObjednavkaSearch' => $mObjednavkaSearch,
    'mFakturacniUdaje' => $mFakturacniUdaje
)) ?>