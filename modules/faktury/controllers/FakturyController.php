<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 20. 4. 2015
 * Time: 20:03
 */

namespace app\modules\faktury\controllers;

use app\components\Application;
use app\components\Controller;
use app\modules\faktury\models\Faktura;
use app\modules\faktury\models\FakturaPolozka;
use app\modules\objednavky\models\DodaciUdaje;
use app\modules\objednavky\models\FakturacniUdaje;
use app\modules\objednavky\models\Objednavka;
use app\modules\objednavky\models\Zakaznik;
use yii\helpers\Json;
use yii\web\Response;

/**
 * Class FakturyController
 * @package app\modules\faktury\controllers
 */
class FakturyController extends Controller
{
    /**
     * @param $id
     * @return Faktura
     */
    protected function nactiFakturu($id)
    {
        $mFaktura = (new Faktura())->nactiPodlePk($id);

        if ($mFaktura == null) {
            Application::setFlashError("Faktura nenalezena");
            return $this->redirect(['/faktury/default/index']);
        }

        return $mFaktura;
    }

    /**
     *
     */
    public function actionAjaxPridatPolozku()
    {
        $klic = \Yii::$app->request->post('klic');
        $hlavicka = \Yii::$app->request->post('hlavicka');
        $pk = \Yii::$app->request->post('pk');

        if ($pk != null) {
            $mPolozkaFaktury = (new FakturaPolozka())->nactiPolozkuObjednavky($pk);
        } else {
            $mPolozkaFaktury = new FakturaPolozka();
        }

        $polozka = $this->renderPartial('@app/modules/faktury/views/default/_form_polozka.php', [
            'klic' => $klic,
            'hlavicka' => $hlavicka == 0,
            'mFakturaPolozka' => $mPolozkaFaktury
        ]);

        return $polozka;
    }

    /**
     * @return string vykresleny grid s objednavkami
     */
    public function actionAjaxVyhledatObjednavky()
    {
        $mObjednavka = new Objednavka(['scenario' => Objednavka::SCENARIO_HLEDAT_AJAX]);
        $post = \Yii::$app->request->post();

        if (!empty($post)) {
            $mObjednavka->load($post);
        }

        $mObjednavka->stav = Objednavka::$sparovatelne_stavy;

        $grid = $this->renderAjax('@app/modules/faktury/views/default/_ajax_objednavky.php', [
            'mObjednavka' => $mObjednavka
        ]);

        return $grid;
    }

    public function actionAjaxFakturacniUdaje()
    {
        $post = \Yii::$app->request->post();
        $response = [];

        try {
            if (!isset($post['objednavka_pk'])) {
                throw new \Exception('Chyba v datech!');
            }

            $mObjednavka = (new Objednavka())->nactiPodlePk($post['objednavka_pk']);
            if ($mObjednavka == null) {
                return 'Objednávka neexistuje';
            } else {
                $mFakturacniUdaje = new FakturacniUdaje();
                $mFakturacniUdaje->obchodni_jmeno   = $mObjednavka->f_obchodni_jmeno;
                $mFakturacniUdaje->ulice            = $mObjednavka->f_ulice;
                $mFakturacniUdaje->mesto            = $mObjednavka->f_mesto;
                $mFakturacniUdaje->psc              = $mObjednavka->f_psc;
                $mFakturacniUdaje->ic               = $mObjednavka->f_ic;
                $mFakturacniUdaje->dic              = $mObjednavka->f_dic;
                $mFakturacniUdaje->typ              = $mObjednavka->f_ic == null ? FakturacniUdaje::TYP_FYZICKA_OSOBA : FakturacniUdaje::TYP_PRAVNICKA_OSOBA;

                $mZakaznik = (new Zakaznik())->nactiPodlePk($mObjednavka->zakaznik_pk);

                $response['udaje'] = $mFakturacniUdaje->attributes;
                $response['html'] = $this->renderPartial('@app/modules/faktury/views/default/_form_fakturacni_udaje.php', [
                    'mZakaznik' => $mZakaznik,
                    'mFakturacniUdaje' => $mFakturacniUdaje
                ]);
            }
        } catch (\Exception $e) {
            $response['error'] = $e->getMessage();
        }

        return Json::encode($response);
    }
}