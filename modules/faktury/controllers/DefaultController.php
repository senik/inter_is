<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 15. 3. 2015
 * Time: 11:55
 */

namespace app\modules\faktury\controllers;


use app\components\Application;
use app\modules\faktury\models\Faktura;
use app\modules\faktury\models\FakturaPolozka;
use app\modules\objednavky\models\DodaciUdaje;
use app\modules\objednavky\models\FakturacniUdaje;
use app\modules\objednavky\models\Objednavka;
use app\modules\objednavky\models\Zakaznik;
use kartik\mpdf\Pdf;
use yii\base\Exception;

/**
 * Class DefaultController
 * @package app\modules\faktury\controllers
 */
class DefaultController extends FakturyController
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $mFaktura = new Faktura(['scenario' => Faktura::SCENARIO_HLEDAT]);

        $get = \Yii::$app->request->get();
        if (!empty($get) && $mFaktura->load($get)) {
            $mFaktura->validate();
        }

        return $this->render('index', [
            'mFaktura' => $mFaktura
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionDetail($id)
    {
        $mFaktura = (new Faktura())->nactiPodlePk($id);

        if ($mFaktura == null) {
            Application::setFlashError("Faktura nenalezena");
            return $this->redirect(['/faktury/default/index']);
        }

        $mZakaznik = (new Zakaznik())->nactiPodlePk($mFaktura->objednavka->zakaznik_pk);
        $mDodavatelUdaje = (new FakturacniUdaje())->nactiSystemoveProDatum($mFaktura->datum_zalozeni);

        return $this->render('detail', [
            'mFaktura' => $mFaktura,
            'mZakaznik' => $mZakaznik,
            'mDodavatelUdaje' => $mDodavatelUdaje
        ]);
    }

    /**
     * @return string
     */
    public function actionPridat()
    {
        $mFaktura = new Faktura(['cislo' => 'XXXXX']);
        $mFakturacniUdaje = new FakturacniUdaje();
        $mDodaciUdaje = new DodaciUdaje();
        $mZakaznik = new Zakaznik();
        $mObjednavkaSearch = new Objednavka(['scenario' => Objednavka::SCENARIO_HLEDAT_AJAX]);
        $post = \Yii::$app->request->post();

        // ?? really ??
        $mFaktura->objednavka = new Objednavka(['scenario' => Objednavka::SCENARIO_HLEDAT]);

        if (!empty($post)) {
            $polozky = [];
            $mFaktura->load($post);
            $valid = $mFaktura->validate();

            if (isset($post['Faktura']['polozky']) && is_array($post['Faktura']['polozky'])) {
                foreach ($post['Faktura']['polozky'] as $polozka) {
                    $mFakturaPolozka = new FakturaPolozka();
                    $mFakturaPolozka->load($polozka);
                    $mFakturaPolozka->faktura_pk = $mFaktura->faktura_pk;

                    $polozky[] = $mFakturaPolozka;

                    $valid = $valid && $mFakturaPolozka->validate();
                }
            }

            if (empty($polozky)) {
                $valid = false;
                Application::setFlashError("Nelze uložit fakturu bez položek.");
            } else {
                $mFaktura->polozky = $polozky;
            }

            if ($valid) {
                try {
                    $mFaktura->uloz();
                    Application::setFlashSuccess("Faktura byla upravena.");

                    return $this->redirect(['/faktury/default/detail', 'id' => $mFaktura->faktura_pk]);
                } catch (\Exception $e) {
                    Application::setFlashError("Chyba při ukládání faktury, opakujte akci později.");
                    \Yii::error("[defaultController] chyba pri ukladani faktury: {$e->getMessage()}");
                }
            }
        }

        return $this->render('pridat', [
            'mFaktura' => $mFaktura,
            'mFakturacniUdaje' => $mFakturacniUdaje,
            'mDodaciUdaje' => $mDodaciUdaje,
            'mZakaznik' => $mZakaznik,
            'mObjednavkaSearch' => $mObjednavkaSearch
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionVystavit($id)
    {
        $mFaktura = $this->nactiFakturu($id);
        $post = \Yii::$app->request->post();

        if ($mFaktura->load($post) && $mFaktura->validate()) {
            if ($mFaktura->vystavit()) {
                Application::setFlashSuccess("Faktura byla vystavena.");
            } else {
                Application::setFlashError("Při vystavování faktury došlo k chybě, opakujte akci později.");
            }
        } else {
            \Yii::error('[defaultController] nepodarilo se nacista data pro vystaveni faktury: ' . var_export($post, true));
            Application::setFlashError("Při vystavování faktury došlo k chybě, opakujte akci později.");
        }

        return $this->redirect(['/faktury/default/detail', 'id' => $id]);
    }

    /**
     * @param $id
     * @return string
     * @throws \Exception
     */
    public function actionUpravit($id)
    {
        $post = \Yii::$app->request->post();

        $mFaktura = $this->nactiFakturu($id);
        $mObjednavkaSearch = new Objednavka(['scenario' => Objednavka::SCENARIO_HLEDAT_AJAX]);

        if ($mFaktura->stav != Faktura::STAV_ZALOZENA) {
            Application::setFlashWarning('Fakturu nelze upravit, protože je ' . lcfirst(Faktura::itemAlias('stav', $mFaktura->stav)) . '!');
            return $this->redirect(['/faktury/default/detail', 'id' => $mFaktura->faktura_pk]);
        }

        if (!empty($post)) {

            // to se odesle i pri stisku enteru
            if (isset($post['spojit'])) {

            }
            // bez komentare
            elseif (isset($post['ulozit'])) {

            }
            // musim to odeslat jednim z tlacitek
            else {
                throw new \Exception('k odeslani formulare faktury nebylo pouzito zadne tlacitko');
            }

            $polozky = [];
            $mFaktura->load($post);
            $mFaktura->fakturacni_udaje->load($post);

            $valid = $mFaktura->validate() && $mFaktura->fakturacni_udaje->validate();

            if (isset($post['Faktura']['polozky']) && is_array($post['Faktura']['polozky'])) {
                foreach ($post['Faktura']['polozky'] as $polozka) {
                    $mFakturaPolozka = new FakturaPolozka();
                    $mFakturaPolozka->load($polozka);
                    $mFakturaPolozka->faktura_pk = $mFaktura->faktura_pk;

                    $polozky[] = $mFakturaPolozka;

                    $valid = $mFakturaPolozka->validate();
                }
            }

            if (empty($polozky)) {
                $valid = false;
                Application::setFlashError("Nelze uložit fakturu bez položek.");
            } else {
                $mFaktura->polozky = $polozky;
            }

            if ($valid) {
                try {
                    $mFaktura->uloz();
                    Application::setFlashSuccess("Faktura byla upravena.");

                    return $this->redirect(['/faktury/default/detail', 'id' => $mFaktura->faktura_pk]);
                } catch (\Exception $e) {
                    Application::setFlashError("Chyba při ukládání faktury, opakujte akci později.");
                    \Yii::error("[defaultController] chyba pri ukladani faktury: {$e->getMessage()}");
                }
            }
        }

        // upravim data pro zobrazeni
        if ($mFaktura->datum_splatnosti != null) {
            $mFaktura->datum_splatnosti = date('j.n.Y', strtotime($mFaktura->datum_splatnosti));
        }

        return $this->render('upravit', [
            'mFaktura' => $mFaktura,
            'mZakaznik' => $mFaktura->zakaznik,
            'mObjednavkaSearch' => $mObjednavkaSearch,
            'mFakturacniUdaje' => $mFaktura->fakturacni_udaje
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionSmazat($id)
    {
        $mFaktura = $this->nactiFakturu($id);

        if ($mFaktura->smaz()) {
            Application::setFlashSuccess("Faktura {$mFaktura->cislo} byla smazána.");
        } else {
            Application::setFlashSuccess("Fakturu {$mFaktura->cislo} se nepodařilo smazat, opakujte akci později.");
        }

        return $this->redirect(['/faktury/default/index']);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionZaplaceno($id)
    {
        $mFaktura = $this->nactiFakturu($id);

        $indexRoute = ['/faktury/default/index'];
        $detailRoute = ['/faktury/default/detail'];
        $dashboard = ['/dashboard'];
        $referrer = \Yii::$app->request->referrer;

        if (strpos($referrer, $detailRoute[0]) !== false) {
            $redirTo = $detailRoute;
        } else if (strpos($referrer, $dashboard[0]) !== false) {
            $redirTo = $dashboard;
        } else {
            $redirTo = $indexRoute;
        }

        if ($mFaktura->oznacJakoZaplacena()) {
            Application::setFlashSuccess("Faktura {$mFaktura->cislo} byla označena jako zaplacená.");
        } else {
            Application::setFlashSuccess("Fakturu {$mFaktura->cislo} se nepodařilo označit za zaplacenou, opakujte akci později.");
        }

        return $this->redirect($redirTo);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function actionStahniPdf($id)
    {
        $mFaktura = $this->nactiFakturu($id);
        $mZakaznik = (new Zakaznik())->nactiPodlePk($mFaktura->objednavka->zakaznik_pk);
        $mDodavatelUdaje = (new FakturacniUdaje())->nactiSystemoveProDatum($mFaktura->datum_zalozeni);

        $obsah = $this->renderPartial('detail', [
            'mFaktura' => $mFaktura,
            'mZakaznik' => $mZakaznik,
            'mDodavatelUdaje' => $mDodavatelUdaje
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'content' => $obsah,
            'destination' => Pdf::DEST_DOWNLOAD,
            'filename' => "Faktura {$mFaktura->cislo}.pdf",
            'cssFile' => '@app/modules/faktury/assets/faktura.css',
            'methods' => [
                'SetHeader' => ["Faktura {$mFaktura->cislo}"],
                'SetFooter' => ["Vygenerováno systémem ISS Voplasto"]
            ]
        ]);

        return $pdf->render();
    }

    /**
     * @param $id
     */
    public function actionTisk($id)
    {
        $mFaktura = $this->nactiFakturu($id);
        $mZakaznik = (new Zakaznik())->nactiPodlePk($mFaktura->objednavka->zakaznik_pk);

        $obsah = $this->renderPartial('detail', [
            'mFaktura' => $mFaktura,
            'mZakaznik' => $mZakaznik
        ]);
    }
}