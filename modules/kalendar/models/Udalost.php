<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 29.10.14
 * Time: 20:46
 */

namespace app\modules\kalendar\models;


use app\components\helpers\DbUtils;
use app\components\Model;
use app\components\SqlDataProvider;
use yii\base\Exception;
use yii\db\Connection;
use Yii;

/**
 * Class Udalost
 * @package app\modules\kalendar
 */
class Udalost extends Model
{
    // todo vymyslet dalsi
    const TYP_JEDNANI = 'JEDNANI';
    const TYP_UPOZORNENI = 'UPOZORNENI';
    const TYP_SCHUZKA = 'SCHUZKA';

    /**
     * @var string
     */
    public $udalost_pk = null;

    /**
     * @var string
     */
    public $typ;

    /**
     * @var string
     */
    public $nazev;

    /**
     * @var
     */
    public $meta;

    /**
     * @var string
     */
    public $popis;

    /**
     * @var array
     */
    public $ucastnici = array();

    /**
     * @var string
     */
    public $datum_od;

    /**
     * @var string
     */
    public $datum_do;

    /**
     * @var bool
     */
    public $opakovani = false;

    /**
     * @var int
     */
    public $uzivatel_pk;

    /**
     * @var string
     */
    public $barva = '#d06b64';

    /**
     * @var string
     */
    public $upozorneni;

    /**
     * Metoda vraci novou instanci tridy
     * @return Udalost
     */
    public static function model()
    {
        return new self;
    }

    /**
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    public function rules()
    {
        return array(
            array(array('typ', 'nazev', 'popis', 'datum_od', 'datum_do', 'uzivatel_pk', 'barva'), 'required'),
            array(array('ucastnici', 'opakovani', 'upozorneni'), 'safe')
        );
    }

    public function attributeLabels()
    {
        return array(
            'typ' => 'Druh události',
            'nazev' => 'Název',
            'popis' => 'Popis',
            'uzivatel_pk' => 'Organizátor',
            'datum_od' => 'Konání od',
            'datum_do' => 'Konání do',
            'opakovani' => 'Opakování',
            'ucastnici' => 'Účastníci',
            'barva' => 'Barevné označení',
            'upozorneni' => 'Upozornění',
        );
    }

    /**
     * @return bool
     * @throws \yii\base\Exception
     */
    public function ulozUdalost()
    {
        /** @var Connection $db */
        $db = \Yii::$app->db;

        $transakce = $db->beginTransaction();

        $sql = "select udalost_uloz(:udalost_pk, :uzivatel_pk, :typ, :nazev, :popis, :konani_od, :konani_do, :barva, :upozorneni, :opakovani)";

        try {
            if ($this->uzivatel_pk == null) {
                $this->uzivatel_pk = \Yii::$app->user->id;
            }

            $command = $db->createCommand($sql);
            $command->bindParam('udalost_pk', $this->udalost_pk);
            $command->bindParam('uzivatel_pk', $this->uzivatel_pk);
            $command->bindParam('typ', $this->typ);
            $command->bindParam('nazev', $this->nazev);
            $command->bindParam('popis', $this->popis);
            $command->bindParam('konani_od', $this->datum_od);
            $command->bindParam('konani_do', $this->datum_do);
            $command->bindParam('barva', $this->barva);
            $command->bindParam('upozorneni', $this->upozorneni);
            $command->bindParam('opakovani', $this->opakovani);

            $udalost_pk = $command->queryScalar();
            // odchyceni chyby
            if ($udalost_pk < 0) {
                throw new Exception("navratova hodnota procedury udalost_uloz() = $udalost_pk");
            } else {
                if (is_array($this->ucastnici) && !empty($this->ucastnici)) {
                    $sql = "select udalost_nastav_ucastniky(:udalost, :uzivatele)";
                    $ucastnici = DbUtils::implodePgArray($this->ucastnici);

                    $command = $db->createCommand($sql);
                    $command->bindParam('uzivatele', $ucastnici);
                    $command->bindParam('udalost', $udalost_pk);

                    $result = $command->queryScalar();

                    // a dalsi odchyceni chyby
                    if ($result < 0) {
                        throw new Exception("navratova hodnota procedury udalost_nastav_ucastniky() = $result");
                    } else {
                        // rozeslu maily
                    }
                }

                $transakce->commit();

                return true;
            }
        } catch (Exception $e) {
            $transakce->rollBack();
            throw $e;
        }
    }

    /**
     * @param string $id meta id
     * @throws \yii\base\Exception
     * @return bool
     */
    public function nactiUdalostPodleMeta($id)
    {
        $command = Yii::$app->db->createCommand("
            select
                  u.*
                , array_agg(uu.uzivatel_pk) as ucastnici
            from udalost u
                left join ucastnik_udalosti uu using (udalost_pk)
            where u.meta = :meta
            group by u.udalost_pk
        ");
        $command->bindParam('meta', $id);

        $result = $command->queryOne();

        if ($result == false) {
            throw new Exception("nenalezena udalost s meta: $id");
        } else {
            return $this->_nacti($result);
        }
    }

    /**
     * @param int $id udalost_pk
     * @throws \yii\base\Exception
     * @return bool
     */
    public function nactiUdalostPodlePk($id)
    {
        $command = Yii::$app->db->createCommand("
            select
                  u.*
                , array_agg(uu.uzivatel_pk) as ucastnici
            from udalost u
                left join ucastnik_udalosti uu using (udalost_pk)
            where u.udalost_pk = :pk
            group by u.udalost_pk
        ");
        $command->bindParam('pk', $id);

        $result = $command->queryOne();

        if ($result == false) {
            throw new Exception("nenalezena udalost s pk: $id");
        } else {
            return $this->_nacti($result);
        }
    }

    /**
     * Metoda vezme predana data a ulozi je do atributu tridy.
     * Pokud nenalezne atribut, vyhazuje vyjimku.
     * @param $data
     * @return $this
     * @throws \yii\base\Exception
     */
    protected function _nacti($data)
    {
        foreach ($data as $atribut => $hodnota) {
            if (!property_exists($this, $atribut)) {
                throw new Exception("trida " . __CLASS__ . " nema definovanou vlastnost {$atribut}!");
            } else {
                switch ($atribut) {
                    case 'ucastnici':
                        $this->$atribut = DbUtils::explodePgArray($hodnota);
                        break;
                    case 'datum_od':
                    case 'datum_do':
                        $this->$atribut = date('j.n.Y H:i', strtotime($hodnota));
                        break;
                    default:
                        $this->$atribut = $hodnota;
                }
            }
        }

        return $this;
    }

    /**
     * metoda primárně pro kalendář, počítám ale, že se bude dát použít i jinde...
     * @return array
     */
    public function vratUdalostiPodleDatumu()
    {
        $sql = "
            select
                  u.*
                , to_char(u.datum_od, 'DD') as den_konani_od
                , to_char(u.datum_do, 'DD') as den_konani_do
            from udalost u
            where (u.datum_od between :dat_od and :dat_do or u.datum_do between :dat_od and :dat_do)
              and (u.uzivatel_pk = :uzivatel or exists(select 1 from ucastnik_udalosti uu where uu.uzivatel_pk = :uzivatel and uu.udalost_pk = u.udalost_pk))
            order by u.datum_od asc, u.datum_do asc
        ";

        $params['dat_od'] = $this->datum_od . ' 00:00:00';
        $params['dat_do'] = $this->datum_do . ' 23:59:59';
        $params['uzivatel'] = Yii::$app->user->id;

        $command = Yii::$app->db->createCommand($sql);
        $command->bindParam('dat_od', $params['dat_od']);
        $command->bindParam('dat_do', $params['dat_do']);
        $command->bindParam('uzivatel', $params['uzivatel']);
        $result = $command->queryAll();

        $mesic = date('n', strtotime($this->datum_od));

        $akceMesic = array();
        foreach ($result as $akce) {
            $mesicOd = date('m', strtotime($akce['datum_od']));
            $mesicDo = date('m', strtotime($akce['datum_do']));

            if ($mesicOd < $mesic) {
                $denOd = 1;
                $denDo = (int)date('j', strtotime($akce['datum_do']));
            } else if ($mesicDo > $mesic) {
                $denOd = (int)date('j', strtotime($akce['datum_od']));
                $denDo = (int)date('j', strtotime($params['dat_do']));
            } else {
                $denOd = (int)date('j', strtotime($akce['datum_od']));
                $denDo = (int)date('j', strtotime($akce['datum_do']));
            }

            for ($i = $denOd; $i <= $denDo; $i++) {
                $akceMesic[$mesic][$i][] = $akce;
            }
        }

        return $akceMesic;
    }

    /**
     * @param int $pocet
     * @return SqlDataProvider
     */
    public function vratPrvniNadchazejici($pocet = 5)
    {
        $sql = "
            select u.* from udalost u
            where u.datum_od >= now() or (u.datum_od < now() and u.datum_do >= now())
                and (u.uzivatel_pk = :uzivatel or exists(select 1 from ucastnik_udalosti uu where uu.uzivatel_pk = :uzivatel and uu.udalost_pk = u.udalost_pk))
            group by u.udalost_pk order by u.datum_od asc
        ";

        if ($pocet != null) {
            $sql .= " limit $pocet";
        }

        return new SqlDataProvider([
            'sql' => $sql,
            'params' => [
                'uzivatel' => Yii::$app->user->id
            ],
            'pagination' => false
        ]);
    }

    /**
     * @param int $pocet
     * @return SqlDataProvider
     */
    public function vratPosledniProbehle($pocet = 5)
    {
        $sql = "
            select u.* from udalost u
            where u.datum_do < now()
                and (u.uzivatel_pk = :uzivatel or exists(select 1 from ucastnik_udalosti uu where uu.uzivatel_pk = :uzivatel and uu.udalost_pk = u.udalost_pk))
            group by u.udalost_pk order by u.datum_od desc
        ";

        if ($pocet != null) {
            $sql .= " limit $pocet";
        }

        return new SqlDataProvider([
            'sql' => $sql,
            'params' => [
                'uzivatel' => Yii::$app->user->id
            ],
            'pagination' => false
        ]);
    }

    public static function itemAliasData()
    {
        return array(
            'typy' => array(
                self::TYP_JEDNANI => 'Jednání',
                self::TYP_UPOZORNENI => 'Upozornění',
                self::TYP_SCHUZKA => 'Schůzka'
            )
        );
    }

} 