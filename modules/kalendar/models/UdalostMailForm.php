<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 1.11.14
 * Time: 18:13
 */

namespace app\modules\kalendar\models;


use app\components\Model;
use app\modules\uzivatel\models\Uzivatel;
use yii\base\Exception;

/**
 * trida pro odesilani sdeleni ucastnikum akce
 *
 * Class UdalostMailForm
 * @package app\modules\kalendar\models
 */
class UdalostMailForm extends Model
{
    /**
     * @var
     */
    public $udalost_pk;

    /**
     * @var
     */
    public $predmet;

    /**
     * @var
     */
    public $telo;

    /**
     * @var array
     */
    public $prijemci = array();

    public function rules() {
        return array(
            array(array('predmet', 'prijemci'), 'safe'),
            array(array('udalost_pk', 'telo'), 'required')
        );
    }

    public function attributeLabels() {
        return array(
            'predmet'   => 'Předmět',
            'telo'      => 'Obsah sdělení',
            'prijemci'  => 'Příjemci'
        );
    }

    /**
     * @return bool
     */
    public function odesliMaily() {
        try {
            if ($this->udalost_pk != null) {
                /** @var Udalost $mUdalost */
                $mUdalost = Udalost::model()->nactiUdalostPodlePk($this->udalost_pk);

                $this->prijemci = $mUdalost->ucastnici;
                $this->predmet = $mUdalost->nazev;
            }

            if (empty($this->prijemci)) {
                throw new Exception('nemam zadne prijemce k odeslani');
            }

            foreach ($this->prijemci as $uzivatel_pk) {
                $mUzivatel = Uzivatel::findOne($uzivatel_pk);
                if (false == $mUzivatel) {
                    throw new Exception("udalostMail: uzivatel ($uzivatel_pk) nenalezen!");
                }

                // todo odeslu email, zatim fake
                $this-> _odesliMail(
                    $mUzivatel->email,
                    $this->predmet,
                    $this->telo
                );
            }

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param $prijemce
     * @param $predmet
     * @param $telo
     * @return bool
     */
    protected function _odesliMail($prijemce, $predmet, $telo) {
        return true;
    }

}