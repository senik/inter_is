<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 17.10.14
 * Time: 19:43
 */

namespace app\modules\kalendar;


use app\components\ModuleInterface;
use yii\base\Module;

class KalendarModule extends Module implements ModuleInterface
{
    /**
     * @var string
     */
    public $controllerNamespace = 'app\modules\kalendar\controllers';

    /**
     * @var
     */
    public $name = "Kalendář";

    /**
     * @var array
     */
    public $baseUrl = array('/kalendar/default/index');

    public function vratMenuModulu()
    {
        return array(
            array('label' => 'Kalendář', 'url' => array('/kalendar/default/index')),
            array('label' => 'Přidat událost', 'url' => array('/kalendar/udalost/pridat')),
        );
    }
}