<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 27.10.14
 * Time: 11:43
 *
 * @var $mUdalost Udalost
 */

use app\modules\kalendar\models\Udalost;
use app\modules\uzivatel\models\Uzivatel;

$this->title = Yii::$app->name . ' - Nová událost';

$this->params['breadcrumbs'] = array(
    array('label' => 'Kalendář', 'url' => array('/kalendar/default/index')),
    'Nová událost'
);
?>

<div class="h2-buttons">
    <h2>Nová událost do kalendáře</h2>
    <div class="clearfix"></div>
</div>

<?= $this->render('_form', array('mUdalost' => $mUdalost));