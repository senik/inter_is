<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 29.10.14
 * Time: 21:12
 *
 * @var $mUdalost Udalost
 * @var $uzivatele array
 * @var $this View
 */

use app\modules\kalendar\models\Udalost;
use app\modules\uzivatel\models\Uzivatel;
use kartik\widgets\DateTimePicker;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

$uzivatele = Uzivatel::vratProDropdown(array(Yii::$app->user->id));

$form = ActiveForm::begin(array(
    'id' => 'udalost-pridat-form',
    'layout' => 'horizontal',
    'options' => array(
        'class' => 'horizontal'
    )
));
// mam dva stejne datepickery, tak jim udelam konfiguraci jen jednou
$dateTimePickerOptions = array(
    'convertFormat' => true,
    'pluginOptions' => array(
        'autoclose'=>true,
        'format' => 'php:j.n.Y H:i'
    )
);
?>

<div class="form-fields">
    <?= $form->field($mUdalost, 'typ')->dropDownList(Udalost::itemAlias('typy'), ['class' => 'selectpicker']) ?>
    <?= $form->field($mUdalost, 'nazev') ?>
    <?= $form->field($mUdalost, 'datum_od')->widget(DateTimePicker::className(), $dateTimePickerOptions) ?>
    <?= $form->field($mUdalost, 'datum_do')->widget(DateTimePicker::className(), $dateTimePickerOptions) ?>
    <?= $form->field($mUdalost, 'popis')->textarea() ?>
    <!-- todo tady z toho hnusu bych taky udělal nějakej color-picker - asi jako vykreslený labely + hidden input, nebude to tak těžký napsat -->
    <?= $form->field($mUdalost, 'barva')->dropDownList(array(
        '#d06b64' => 'defaultní',
        '#5484ed' => 'modrá',
        '#a4bdfc' => 'světle modrá',
        '#46d6db' => 'tyrkysová',
        '#7ae7bf' => 'světle tyrkysová',
        '#51b749' => 'zelená',
        '#fbd75b' => 'žlutá',
        '#ffb878' => 'oranžová',
        '#ff887c' => 'lososová',
        '#dc2127' => 'červená',
        '#dbadff' => 'fialová',
        '#e1e1e1' => 'šedá',
    ), array(
        'class' => 'selectpicker',
        'options' => array(
            '#d06b64' => array('data-content' => '<span class="label" style="background-color: #d06b64;">defaultní</span>'),
            '#5484ed' => array('data-content' => '<span class="label" style="background-color: #5484ed;">modrá</span>'),
            '#a4bdfc' => array('data-content' => '<span class="label" style="background-color: #a4bdfc;">světle modrá</span>'),
            '#46d6db' => array('data-content' => '<span class="label" style="background-color: #46d6db;">tyrkysová</span>'),
            '#7ae7bf' => array('data-content' => '<span class="label" style="background-color: #7ae7bf;">světle tyrkysová</span>'),
            '#51b749' => array('data-content' => '<span class="label" style="background-color: #51b749;">zelená</span>'),
            '#fbd75b' => array('data-content' => '<span class="label" style="background-color: #fbd75b;">žlutá</span>'),
            '#ffb878' => array('data-content' => '<span class="label" style="background-color: #ffb878;">oranžová</span>'),
            '#ff887c' => array('data-content' => '<span class="label" style="background-color: #ff887c;">lososová</span>'),
            '#dc2127' => array('data-content' => '<span class="label" style="background-color: #dc2127;">červená</span>'),
            '#dbadff' => array('data-content' => '<span class="label" style="background-color: #dbadff;">fialová</span>'),
            '#e1e1e1' => array('data-content' => '<span class="label" style="background-color: #e1e1e1;">šedá</span>')
        )
    )) ?>
    <?= $form->field($mUdalost, 'ucastnici')->dropDownList($uzivatele, array('multiple' => 'multiple', 'title' => 'Žádný další účastník', 'class' => 'selectpicker')) ?>
</div>
<div class="form-actions well">
    <?php
    if (Yii::$app->controller->action->id == 'upravit') {
        $resetText = 'Původní hodnoty';
        $returnUrl = array('/kalendar/udalost/detail', 'id' => $mUdalost->meta);
    } else {
        $resetText = 'Vymazat formulář';
        $returnUrl = array('/kalendar/default/index');
    }

    echo Html::submitButton('Uložit', array(
        'class' => 'btn btn-success'
    ));
    echo Html::resetButton($resetText, array(
        'class' => 'btn btn-primary',
        'onclick' => 'resetForm();'
    ));
    echo Html::a('Zrušit', $returnUrl, array(
        'class' => 'btn btn-danger'
    ));
    ?>
</div>

<?php
ActiveForm::end();