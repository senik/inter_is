<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 31.10.14
 * Time: 21:43
 *
 * @var $mUdalost Udalost
 */

use app\modules\kalendar\models\Udalost;
use app\modules\uzivatel\models\Uzivatel;

$this->title = Yii::$app->name . ' - Upravit událost';

$this->params['breadcrumbs'] = array(
    array('label' => 'Kalendář', 'url' => array('/kalendar/default/index')),
    'Upravit událost'//$mUdalost->nazev
);
?>

<div class="h2-buttons">
    <h2>Upravit událost <?= $mUdalost->nazev ?></h2>
    <div class="clearfix"></div>
</div>

<?= $this->render('_form', array('mUdalost' => $mUdalost));