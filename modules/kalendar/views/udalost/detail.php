<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 31.10.14
 * Time: 20:54
 *
 * @var $mUdalost Udalost
 * @var $mUdalostMail UdalostMailForm
 */

use app\modules\kalendar\models\Udalost;
use app\modules\kalendar\models\UdalostMailForm;
use app\modules\uzivatel\models\Uzivatel;
use kartik\detail\DetailView;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = Yii::$app->name . ' - ' . $mUdalost->nazev;

$this->params['breadcrumbs'] = array(
    array('label' => 'Kalendář', 'url' => array('/kalendar/default/index')),
    $mUdalost->nazev
);
?>

<div class="h2-buttons">
    <h2>Detail události <?= $mUdalost->nazev ?></h2>
    <?php
    if ($mUdalost->uzivatel_pk == Yii::$app->user->id) {
        echo Html::a(
            'Upravit',
            array('/kalendar/udalost/upravit', 'id' => $mUdalost->udalost_pk),
            array(
                'class' => 'btn btn-warning'
            )
        );
    }
    ?>
    <div class="clearfix"></div>
</div>

<?php
echo DetailView::widget(array(
    'model' => $mUdalost,
    'attributes' => array(
        'typ' => array(
            'attribute' => 'typ',
            'label' => $mUdalost->getAttributeLabel('typ'),
            'value' => Udalost::itemAlias('typy', $mUdalost->typ)
        ),
        'nazev',
        'datum_od',
        'datum_do',
        'upozorneni',
        'barva',
        'popis',
        'uzivatel_pk' => array(
            'attribute' => 'uzivatel_pk',
            'label' => $mUdalost->getAttributeLabel('uzivatel_pk'),
            'value' => Uzivatel::findOne($mUdalost->uzivatel_pk)->vratCeleJmeno()
        )
    ),
    'options' => array(
        'id' => 'udalost-detailview',
    )
));

if (!empty($mUdalost->ucastnici) && in_array($mUdalost->typ, array(Udalost::TYP_JEDNANI))) {
    echo Html::beginTag('div', array('class' => 'col-sm-4'));
    {
        echo Html::tag('h3', 'Další přihlášení');

        echo Html::beginTag('ul');
        foreach ($mUdalost->ucastnici as $uzivatel_pk) {
            $mUzivatel = Uzivatel::findOne($uzivatel_pk);
            echo Html::tag('li', $mUzivatel->vratCeleJmeno());
        }
        echo Html::endTag('ul');
    }
    echo Html::endTag('div');

    echo Html::beginTag('div', array('class' => 'col-sm-8'));
    {
        echo Html::tag('h3', 'Odeslat přihlášeným email');

        $form = ActiveForm::begin(array(
            'id' => 'udalost-email-form',
            'layout' => 'horizontal',
            'options' => array(
                'class' => 'horizontal'
            )
        ));

        echo Html::beginTag('div', array('class' => 'form-fields'));
        {
            echo Html::activeHiddenInput($mUdalostMail, 'udalost_pk', array('value' => $mUdalost->udalost_pk));
            echo $form->field($mUdalostMail, 'telo')->textarea();
        }
        echo Html::endTag('div');

        echo Html::beginTag('div', array('class' => 'form-actions'));
        {
            echo Html::submitButton('Odeslat', array(
                'class' => 'btn btn-success'
            ));
        }
        echo Html::endTag('div');

        ActiveForm::end();
    }
    echo Html::endTag('div');
}