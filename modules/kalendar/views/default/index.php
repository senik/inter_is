<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 23.10.14
 * Time: 20:12
 *
 * @var $this View
 * @var $udalosti array
 */

use yii\helpers\Html;
use yii\web\View;

$this->title = Yii::$app->name . ' - Kalendář';

$this->params['breadcrumbs'] = array(
    array('label' => 'Kalendář')
);

$this->registerJs("$('span.den-cislo', 'table.kalendar tbody td').fitText(1.2);");

$monthNames = array("leden", "únor", "březen", "duben", "květen", "červen", "červenec", "srpen", "září", "říjen", "listopad", "prosinec");

$getParams = Yii::$app->request->get();
$cMonth = isset($getParams["mesic"]) ? $getParams["mesic"] : date("n");
$cYear = isset($getParams["rok"]) ? $getParams["rok"] : date("Y");
?>

<div class="h2-buttons">
    <h2>Kalendář - <?php echo $monthNames[$cMonth-1].' '.$cYear; ?></h2>
    <?php
    echo Html::a(
        'Nová událost',
        array('/kalendar/udalost/pridat'),
        array(
            'class' => 'btn btn-success'
        )
    );
    ?>
    <div class="clearfix"></div>
</div>

<?php
$prev_year = $cYear;
$next_year = $cYear;
$prev_month = $cMonth - 1;
$next_month = $cMonth + 1;

if ($prev_month == 0 ) {
    $prev_month = 12;
    $prev_year = $cYear - 1;
}
if ($next_month == 13 ) {
    $next_month = 1;
    $next_year = $cYear + 1;
}
?>

<table class="kalendar">
    <thead>
    <tr>
        <th colspan="2">
            <a href="<?= Yii::$app->urlManager->createAbsoluteUrl(array('/kalendar/default/index', 'mesic' => $prev_month, 'rok' => $prev_year)) ?>">
                &lt;&lt; <?= $monthNames[$prev_month-1] ?>
            </a>
        </th>
        <th colspan="3" class="mesic">
            <?php if ($cMonth != date('n') || $cYear != date('Y')) { ?>
            <a href="<?= Yii::$app->urlManager->createAbsoluteUrl(array('/kalendar/default/index')) ?>">
                zpět na aktuální měsíc
            </a>
            <?php } ?>
        </th>
        <th colspan="2">
            <a href="<?= Yii::$app->urlManager->createAbsoluteUrl(array('/kalendar/default/index', 'mesic' => $next_month, 'rok' => $next_year)) ?>">
                <?= $monthNames[$next_month-1] ?> &gt;&gt;
            </a>
        </th>
    </tr>
    <tr>
        <td class="den"><strong>Po</strong></td>
        <td class="den"><strong>Út</strong></td>
        <td class="den"><strong>St</strong></td>
        <td class="den"><strong>Čt</strong></td>
        <td class="den"><strong>Pá</strong></td>
        <td class="den"><strong>So</strong></td>
        <td class="den"><strong>Ne</strong></td>
    </tr>
    </thead>
    <tbody>
    <?php
    /** @var int $timestamp aktualni cas */
    $timestamp = mktime(0,0,0,$cMonth,1,$cYear);
    /** @var int $maxday posledniho v mesici */
    $maxday = date("t",$timestamp);
    /** @var int $thismonth aktualni mesic */
    $thismonth = getdate($timestamp);
    /** @var int $startday kolikaty den v tydnu je prvniho */
    $startday = $thismonth['wday'] == 0 ? 6 : $thismonth['wday'] - 1;
    /** @var int $zbytekPoDeleni kolikaty den v tydnu je prvniho */
    $zbytekPoDeleni = (7 - (($maxday + $startday) % 7));
    /** @var int $doKonceMesice kolik zbyva do konce mesice */
    $doKonceMesice = $zbytekPoDeleni == 7 ? 0 : $zbytekPoDeleni;

    // cela ta magie s vykreslovanim
    for ($i = 0; $i < ($maxday + $startday + $doKonceMesice); $i++) {

        if (($i % 7) == 0 ) {
            echo "<tr>";
        }

        if ($i < $startday) {
            echo "<td></td>";
        } else if ($i >= ($maxday+$startday)) {
            echo "<td></td>";
        } else {
            $den = ($i - $startday + 1);
            $class = array();
            $akceDen = '';

            if (isset($udalosti[$cMonth][$den])) {
                $class[] = 'akce';
                $akceDen = '<div class="seznam-akci-'.$den.'">';
                $akceDen .= '<ul>';
                foreach ($udalosti[$cMonth][$den] as $jedna) {
                    // to nastaveni pozadi by chtelo poresit jinak, libi se mi CSS3 attr(), ale zatim to jde jen na content
                    $akceDen .= '<li style="background-color: ' . $jedna['barva'] . ';"><a href="' . Yii::$app->urlManager->createAbsoluteUrl(array('/kalendar/udalost/detail', 'id' => $jedna['meta'])) . '">'.$jedna['nazev'].'</a></li>';
                }
                $akceDen .= '</ul></div>';
            }
            if ($den == date('j')) {
                $class[] = 'dnes';
            }

            echo "<td><span class='den-cislo'>{$den}</span><div><span class='".implode(' ', $class)."'></span>{$akceDen}<div></td>";
        }

        if (($i % 7) == 6 ) {
            echo "</tr>";
        }
    }
    ?>
    </tbody>
</table>