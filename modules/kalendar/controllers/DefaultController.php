<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 23.10.14
 * Time: 20:11
 */

namespace app\modules\kalendar\controllers;

use app\components\Application;
use app\components\Controller;
use app\modules\kalendar\models\Udalost;

/**
 * Class DefaultController
 * @package app\modules\kalendar\controllers
 */
class DefaultController extends Controller
{

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $get = \Yii::$app->request->get();

        $mUdalost = new Udalost();

        // tenhle try/catch je tu hlavně proto, aby mi nekdo neposlal v getu nejakou blbost
        try {
            if (isset($get['rok'])) {
                if ($get['rok'] < 0) {
                    throw new \Exception("[kalendar] neplatna hodnota roku v getu: {$get['rok']}", 2101);
                } else {
                    $rok = $get['rok'];
                }
            } else {
                $rok = date('Y');
            }

            if (isset($get['mesic'])) {
                if ($get['mesic'] < 1 || $get['mesic'] > 12) {
                    throw new \Exception("[kalendar] neplatna hodnota mesice v getu: {$get['mesic']}", 2102);
                } else {
                    $mesic = $get['mesic'];
                }
            } else {
                $mesic = date('n');
            }

            $mUdalost->datum_od = sprintf('%s-%s-1', $rok, $mesic);
            $mUdalost->datum_do = sprintf('%s-%s-%s', $rok, $mesic, date('t', strtotime("$rok-$mesic")));

            $udalosti = $mUdalost->vratUdalostiPodleDatumu();

            return $this->render('index', array(
                'udalosti' => $udalosti
            ));
        } catch (\Exception $e) {
            Application::setFlashWarning("Vyskytla se chyba, zobrazuji aktuální měsíc.");

            if (in_array($e->getCode(), [2101, 2102])) {
                \Yii::warning($e->getMessage());
            } else {
                \Yii::error($e->getMessage());
            }

            return $this->redirect(['/kalendar/default/index']);
        }
    }
}