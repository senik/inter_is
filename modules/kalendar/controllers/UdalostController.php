<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 27.10.14
 * Time: 11:38
 */

namespace app\modules\kalendar\controllers;


use app\components\Application;
use app\components\Controller;
use app\modules\kalendar\models\Udalost;
use app\modules\kalendar\models\UdalostMailForm;
use yii\base\Exception;
use Yii;

/**
 * Class UdalostController
 * @package app\modules\kalendar\controllers
 */
class UdalostController extends Controller
{

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionPridat()
    {
        $mUdalost = new Udalost();
        $mUdalost->uzivatel_pk = Yii::$app->user->id;

        try {
            $post = \Yii::$app->request->post();

            if ($mUdalost->load($post) && $mUdalost->validate()) {
                if ($mUdalost->ulozUdalost()) {
                    Application::setFlashSuccess("Vytvořena nová událost: {$mUdalost->nazev}");
                }

                return $this->redirect(array('/kalendar/default/index'));
            }
        } catch (Exception $e) {
            Application::setFlashError("Chyba při zakládání události: {$e->getMessage()}");
        }

        return $this->render('pridat', array(
            'mUdalost' => $mUdalost
        ));
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionUpravit($id)
    {
        /** @var Udalost $mUdalost */
        $mUdalost = Udalost::model()->nactiUdalostPodlePk($id);

        if ($mUdalost->uzivatel_pk != \Yii::$app->user->id) {
            Application::setFlashError('Nemáte oprávnění upravovat tuto událost!');
            return $this->redirect(array('/kalendar/default/index'));
        }

        try {
            $post = \Yii::$app->request->post();

            if ($mUdalost->load($post) && $mUdalost->validate()) {
                if ($mUdalost->ulozUdalost()) {
                    Application::setFlashSuccess("Upravena událost: {$mUdalost->nazev}");
                }

                return $this->redirect(array('/kalendar/default/index'));
            }
        } catch (Exception $e) {
            Application::setFlashError("Chyba při úpravě události: {$e->getMessage()}");
        }

        return $this->render('upravit', array(
            'mUdalost' => $mUdalost
        ));
    }

    /**
     * @param $id
     * @return string
     */
    public function actionDetail($id)
    {
        /** @var Udalost $mUdalost */
        $mUdalost = Udalost::model()->nactiUdalostPodleMeta($id);
        $mUdalostMail = new UdalostMailForm();

        $post = Yii::$app->request->post();

        if ($mUdalostMail->load($post) && $mUdalostMail->validate()) {
            if ($mUdalostMail->odesliMaily()) {
                Application::setFlashSuccess('Mail(y) úspěšně odeslán(y).');
            } else {
                Application::setFlashError('Mail(y) se nepodařilo odeslat.');
            }

            return $this->redirect(array('/kalendar/udalost/detail', 'id' => $id));
        }

        return $this->render('detail', array(
            'mUdalost' => $mUdalost,
            'mUdalostMail' => $mUdalostMail
        ));
    }

} 