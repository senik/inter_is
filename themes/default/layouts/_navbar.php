<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 24.10.14
 * Time: 19:54
 *
 * slouzi k vykresleni navbaru, at je to prehledne
 */

use app\components\Nastaveni;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

NavBar::begin([
    'brandLabel' => 'ISS VoPlasTo',
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar-inverse navbar-fixed-top',
    ],
]);

// vytahnu si z modulu jejich menu
$items = array();
foreach (Nastaveni::instance()->vratInstalovaneModuly() as $id) {
    if (in_array($id, array('gii', 'debug'))) continue;
    /** @var \app\components\ModuleInterface $module */
    $module = Yii::$app->getModule($id);

    $moduleMenu = $module->vratMenuModulu();

    $items[] = '<li class="dropdown-header">' . $id . '</li>';
    foreach ($moduleMenu as $menu2) {
        $items[] = $menu2;
    }
}

$menuItems = array(
    ['label' => 'Dashboard', 'url' => ['/site/index']],
    ['label' => 'Nastavení', 'url' => ['/site/nastaveni'], 'visible' => Yii::$app->user->identity->isAdmin],
    ['label' => 'Moduly', 'items' => $items],
);

/*
$moduleId = Yii::$app->controller->module->id;
if ($moduleId != 'basic') {
    $moduleItems = array(
        ['label' => $moduleId, 'items' => Yii::$app->getModule($moduleId)->vratMenuModulu()],
    );

    $menuItems = array_merge($menuItems, $moduleItems);
}
*/

$loginItems = array(
    Yii::$app->user->isGuest ?
        ['label' => 'Přihlásit', 'url' => ['/uzivatel/default/prihlaseni']] :
        ['label' => 'Odhlásit (' . Yii::$app->user->identity->login . ')',
            'url' => ['/uzivatel/default/odhlaseni'],
            'linkOptions' => ['data-method' => 'post']],
);

$menuItems = array_merge($menuItems, $loginItems);

// a sestavim cele menu do navbaru
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'items' => $menuItems,
]);
NavBar::end();