<?php
use app\components\MenuWidget;
use app\components\Nastaveni;
use yii\bootstrap\BootstrapPluginAsset;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

defined('DS') or define('DS', DIRECTORY_SEPARATOR);

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
BootstrapPluginAsset::register($this);

$this->registerJs("
    $('select').on('change', footerMax);
    $('.selectpicker', 'form').selectpicker();
    $('.fancybox').fancybox();
");
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>

<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?= Yii::$app->request->baseUrl ?>/favicon.ico" rel="icon" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="light">
<noscript>
    <div style="position: fixed; top: 0px; left: 0px; z-index: 3000;
                height: 100%; width: 100%; background-color: #FFFFFF">
        <p style="margin-left: 10px">ISS VoPlasTo vyžaduje ke svému běhu JavaScript. Zapněte jej, prosím.</p>
    </div>
</noscript>
<?php $this->beginBody() ?>
    <div class="wrap">
        <!-- leve mobilni menu -->
        <div class="wrapperLeft">
            <?= MenuWidget::renderLeftMobileMenu() ?>
        </div>

        <!-- hlavni, stredni cast -->
        <div class="wrapperMiddle">
            <!-- header -->
            <div class="wrapperHead">
                <div class="top-navbar">
                    <?php
                    echo MenuWidget::renderMobileNavbar();
                    echo MenuWidget::renderModuleMenu();
                    echo MenuWidget::renderSystemMenu();
                    ?>
                    <div class="clearfix"></div>
                </div>
            </div>

            <!-- vlastni vykreslovany content -->
            <div class="wrapperContent">
                <?php
                echo Html::beginTag('div', array('class' => 'breadcrumbs'));
                {
                    echo Breadcrumbs::widget([
                        'homeLink' => array(
                            'label' => Yii::$app->name,
                            'url' => array('/dashboard')
                        ),
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        'options' => [
                            'class' => 'breadcrumb stable'
                        ]
                    ]);
                    ?>
                    <ul class="breadcrumb hint">
                        <li><a href="<?= Yii::$app->urlManager->createAbsoluteUrl('/') ?>">ISS VoPlasTo</a></li>
                        <li class="active">Objednávky</li>
                    </ul>
                    <?php
                    echo Html::beginTag('span', array('class' => 'logged-as'));
                    {
                        echo Yii::$app->user->isGuest ? 'Nepřihlášen' : Html::tag('span', '', ['class' => 'glyphicon glyphicon-user']) . '&nbsp;' . Html::a(
                            "Přihlášen jako " . Yii::$app->user->identity->login,
                            ['/uzivatel/default/muj-profil']
                        );
                    }
                    echo Html::endTag('span');

                    echo '<div class="clearfix"></div>';
                }
                echo Html::endTag('div');

                $flashes = Yii::$app->session->getAllFlashes(true);
                foreach ($flashes as $key => $flash) {
                    echo \yii\bootstrap\Alert::widget(array(
                        'options' => [
                            'class' => "alert-{$key}",
                        ],
                        'body' => is_array($flash) ? implode(', ', $flash) : $flash,
                    ));
                }
                ?>
                <div class="content">
                    <?= $content ?>
                    <div class="clearfix"></div>
                </div>

                <div class="wrapperFooterFixedMax"></div>
            </div>

            <!-- footer -->
            <div class="wrapperFooter">
                <?= $this->render('_footer') ?>
                <div class="clearfix"></div>
            </div>
        </div>

        <!-- prave mobilni menu -->
        <div class="wrapperRight">
            <?= MenuWidget::renderRightMobileMenu() ?>
        </div>
    </div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>