<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 23.10.14
 * Time: 21:18
 */
?>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <a href="http://senovo.cz" target="_blank">Senovo.cz</a> <?= date('Y') ?></p>
        <p class="pull-right"><?php if (!Yii::$app->user->isGuest) echo 'Technická podpora: <a href="mailto:lukas.senohrabek@email.cz">lukas.senohrabek@email.cz</a>'; ?></p>
    </div>
</footer>