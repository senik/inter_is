<?php
use yii\helpers\Html;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>

<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?= Yii::$app->request->baseUrl ?>/favicon.ico" rel="icon" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="login-page">

<?php $this->beginBody() ?>
    <div class="wrap">
        <div class="login-wrapper">
            <div class="container">
                <?php
                $flashes = Yii::$app->session->getAllFlashes(true);
                foreach ($flashes as $key => $flash) {
                    echo \yii\bootstrap\Alert::widget(array(
                        'options' => [
                            'class' => "alert-{$key}",
                        ],
                        'body' => is_array($flash) ? implode(', ', $flash) : $flash,
                    ));
                }
                ?>
                <?= $content ?>
            </div>
            <?= $this->render('_footer') ?>
        </div>
    </div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>