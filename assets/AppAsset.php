<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'bootstrap-select/css/bootstrap-select.min.css',
        'fancybox/jquery.fancybox.css?v=2.1.5',
    ];
    public $js = [
        'js/bootbox.min.js',
        'js/js.cookie.js',
        'js/application.js?v=1.1.1',
        'bootstrap-select/js/bootstrap-select.min.js',
        'fancybox/jquery.fancybox.pack.js?v=2.1.5',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
