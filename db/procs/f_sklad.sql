-- CREATE OR REPLACE FUNCTION material_zmena(
--     a_material_pk material.material_pk%TYPE
--   , a_nazev       material_druh.nazev%TYPE
-- ) RETURNS INTEGER AS $func$
-- /**
--  * metoda zalozi nebo upravi material na sklade
--  */
-- DECLARE
-- BEGIN
-- END;
-- $func$ LANGUAGE plpgsql;
---

---
CREATE OR REPLACE FUNCTION material_druh_zmena(
    a_druh_id   material_druh.material_druh_id%TYPE
  , a_nazev     material_druh.nazev%TYPE
  , a_hustota   material_druh.hustota%TYPE
) RETURNS TEXT AS $func$
/**
 * metoda zalozi nebo upravi material na sklade
 */
DECLARE
    l_druh_id   material_barva.material_barva_id%TYPE;
    l_meta_id   TEXT;
    l_pocet     INTEGER;
    l_suffix    TEXT;
BEGIN
    IF a_druh_id IS NULL THEN
        l_meta_id := generuj_meta_id(a_nazev);

        -- kontrola na existenci // nebo ma vyletet exception, ze uz takovy druh existuje?
        l_druh_id := l_meta_id;
        WHILE EXISTS(SELECT 1 FROM material_druh WHERE material_druh_id = l_druh_id) LOOP
            IF (coalesce(l_pocet, 0) = 0) THEN l_suffix := ''; ELSE l_suffix := '-' || coalesce(l_pocet, 0); END IF;

            l_druh_id := l_meta_id || l_suffix;
            l_pocet := l_pocet + 1;
        END LOOP;

        INSERT INTO material_druh (
            material_druh_id
            , nazev
            , hustota
        ) VALUES (
            l_druh_id
            , a_nazev
            , a_hustota
        );

    ELSE
        UPDATE material_druh SET
            nazev = a_nazev
            , hustota = a_hustota
        WHERE material_druh_id = a_druh_id
        RETURNING material_druh_id INTO l_druh_id;

    END IF;

    RETURN l_druh_id;
END;
$func$ LANGUAGE plpgsql;
---

---
CREATE OR REPLACE FUNCTION material_barva_zmena(
    a_barva_id  material_barva.material_barva_id%TYPE
  , a_nazev     material_barva.barva%TYPE
) RETURNS TEXT AS $func$
/**
 * metoda zalozi nebo upravi material na sklade
 */
DECLARE
    l_barva_id  material_barva.material_barva_id%TYPE;
    l_meta_id   TEXT;
    l_pocet     INTEGER;
    l_suffix    TEXT;
BEGIN
    IF a_barva_id IS NULL THEN
    l_meta_id := generuj_meta_id(a_nazev);

    -- kontrola na existenci
    l_barva_id := l_meta_id;
    WHILE EXISTS(SELECT 1 FROM material_barva WHERE material_barva_id = l_barva_id) LOOP
        IF (coalesce(l_pocet, 0) = 0) THEN l_suffix := ''; ELSE l_suffix := '-' || coalesce(l_pocet, 0); END IF;

        l_barva_id := l_meta_id || l_suffix;
        l_pocet := l_pocet + 1;
    END LOOP;

    INSERT INTO material_barva (
        material_barva_id
      , barva
    ) VALUES (
        l_barva_id
      , a_nazev
    );

  ELSE
    UPDATE material_barva SET
      barva = a_nazev
    WHERE material_barva_id = a_barva_id
    RETURNING material_barva_id INTO l_barva_id;

  END IF;

  RETURN l_barva_id;
END;
$func$ LANGUAGE plpgsql;
---

---
CREATE OR REPLACE FUNCTION material_dopln_na_sklad(
    a_druh_id     material.material_druh_id%TYPE
  , a_barva_id    material.material_barva_id%TYPE
  , a_mnozstvi    material.skladem_celkem%TYPE
) RETURNS INTEGER AS $func$
/**
 * procedura doplni na sklad predane mnozstvi materialu podle barvy a druhu
 * pokud material neexistuje, tak by ho procedura mohla zalozit (ale k tomu zase nemam hustotu)
 */
DECLARE
  lr_material material%ROWTYPE;
  l_material_pk material.material_pk%TYPE;
BEGIN
  SELECT * INTO lr_material FROM material WHERE material_barva_id = a_barva_id AND material_druh_id = a_druh_id;

  IF FOUND THEN
    UPDATE material SET
      skladem_celkem = skladem_celkem + a_mnozstvi
    WHERE material.material_pk = lr_material.material_pk
    RETURNING material_pk INTO l_material_pk;

  ELSE
    INSERT INTO material (
        skladem_celkem
      , skladem_blokace
      , material_druh_id
      , material_barva_id
    ) VALUES (
        a_mnozstvi
      , 0
      , a_druh_id
      , a_barva_id
    ) RETURNING material_pk INTO l_material_pk;

  END IF;

  RETURN l_material_pk;
END;
$func$ LANGUAGE plpgsql;
---

---
create or replace function vrat_prumernou_cenu_materialu(
    a_material_pk material.material_pk%type
  , a_mesic date
) returns NUMERIC(4,2) as $func$
DECLARE
  l_pocet      integer := 0;
  l_cena       numeric := 0.00;
  l_datum      date;
  l_prvniho    date;
  l_posledniho date;
  lr_material  material_cena%ROWTYPE;
BEGIN
  l_prvniho := to_char(a_mesic, '01.MM.YYYY') :: date;
  l_posledniho := l_prvniho + interval '1 month' - interval '1 day';

  for l_datum in (
    SELECT to_char(generate_series :: date, 'DD.MM.YYYY') :: date as datum FROM generate_series(l_prvniho, l_posledniho, '1 day')
  ) LOOP

    for lr_material in (
      SELECT *
      FROM material_cena mc
      WHERE mc.material_pk = a_material_pk AND
            l_datum BETWEEN mc.platne_od AND coalesce(mc.platne_do, l_posledniho)
      ) LOOP

        l_cena := l_cena + lr_material.cena;
        l_pocet := l_pocet + 1;

    end loop;

  end loop;

  if (l_pocet = 0) THEN
    return 0.00;
  else
    RETURN round(l_cena / l_pocet, 2);
  end if;
end;
$func$ LANGUAGE plpgsql;
---

---
create or replace function material_zmen_cenu(
    a_material_pk material.material_pk%TYPE
  , a_cena material_cena.cena%TYPE
  , a_platne_od material_cena.platne_od%TYPE
  , a_platne_do material_cena.platne_do%TYPE
) returns integer as $func$
DECLARE
  lr_material_cena material_cena%ROWTYPE;
  l_return integer;
BEGIN
  --select * into lr_material_cena from material_cena where material_pk = a_material_pk;
  IF NOT EXISTS(SELECT 1 FROM material WHERE material_pk = a_material_pk) THEN
    l_return := -1;
  ELSE

    if (a_platne_od is null) THEN
      a_platne_od := now();
    end if;

    -- pokud novy zaznam nekonci
    if a_platne_do is null then
      -- najdu vsechy, ktere zacinaji s nebo po zacatku nove ceny
      select * into lr_material_cena from material_cena where material_pk = a_material_pk and platne_od >= a_platne_od order by platne_od asc limit 1;
      if found THEN
        delete from material_cena where material_pk = a_material_pk and platne_od >= a_platne_od;
      end if;

      -- najdu takove, ktere zacinaji pred zacatkem noveho a konci po zacatku noveho nebo nekonci
      select * into lr_material_cena from material_cena where material_pk = a_material_pk and platne_od < a_platne_od and (platne_do >= a_platne_do or platne_do is null) order by platne_od asc limit 1;
      if found THEN
        update material_cena set platne_do = a_platne_od where material_cena.material_cena_pk = lr_material_cena.material_cena_pk;
      end if;
    -- pokud novy zaznam konci
    else
      -- 1. varianta - hledam ty, ktere zacinaji a konci behem noveho
      select * into lr_material_cena from material_cena where material_pk = a_material_pk and platne_od >= a_platne_od and platne_do <= a_platne_do order by platne_od asc limit 1;
      if found THEN
        -- mazu vsechny, ktere jsou uvnitr noveho
        delete from material_cena where material_pk = a_material_pk and platne_od >= a_platne_od and platne_do <= a_platne_do;
      end if;

      -- 2. varianta - hledam takovou, ktera konci behem nove (a zacina pred novou)
      select * into lr_material_cena from material_cena where material_pk = a_material_pk and platne_od < a_platne_od and platne_do <= a_platne_do order by platne_od asc limit 1;
      if found THEN
        -- nastavim konec pred zacatkem nove ceny
        update material_cena set platne_do = a_platne_od where material_pk = a_material_pk;
      end if;

      -- 3. varianta - hledam takovou, ktera zacina uvnitr nove (a konci po konci nove)
      select * into lr_material_cena from material_cena where material_pk = a_material_pk and platne_od >= a_platne_od and platne_od <= a_platne_do and (platne_do >= a_platne_do or platne_do is null) order by platne_od asc limit 1;
      if found then
        update material_cena set platne_od = a_platne_do where material_cena.material_cena_pk = lr_material_cena.material_cena_pk;
      end if;

      -- 4. varianta - hledam takovou cenu, do ktere novou vkladam
      select * into lr_material_cena from material_cena where material_pk = a_material_pk and platne_od < a_platne_od and (platne_do >= a_platne_do or platne_do is null) order by platne_od asc limit 1;
      if found THEN
        update material_cena set platne_do = a_platne_od where material_cena.material_cena_pk = lr_material_cena.material_cena_pk;

        insert into material_cena (platne_od, platne_do, cena, material_pk) VALUES
          (a_platne_do, lr_material_cena.platne_do, lr_material_cena.cena, a_material_pk);
      end if;
    end if;

    insert into material_cena (platne_od, platne_do, cena, material_pk) VALUES
      (a_platne_od, a_platne_do, a_cena, a_material_pk) RETURNING material_cena_pk into l_return;

  END IF;

  RETURN l_return;
end;
$func$ LANGUAGE 'plpgsql';
---

---
create or REPLACE FUNCTION polozka_vrat_material_na_sklad(
    a_objednavka_pk objednavka_polozka.objednavka_pk%type
  , a_model_pk      objednavka_polozka.model_pk%TYPE
  , a_material_pk   objednavka_polozka.material_pk%type
) returns void as $func$
DECLARE
  lr_polozka objednavka_polozka%ROWTYPE;
  l_mnozstvi integer;
BEGIN

  SELECT
      sum(round(m3d.objem * md.hustota, 0) * op.pocet) into l_mnozstvi
  FROM objednavka_polozka op
    JOIN material m ON op.material_pk = m.material_pk
    JOIN material_druh md ON m.material_druh_id = md.material_druh_id
    JOIN model3d m3d ON op.model_pk = m3d.model_pk
  WHERE op.objednavka_pk = a_objednavka_pk
    and op.model_pk = a_model_pk
    and op.material_pk = a_material_pk
  ;

  if not found THEN
    raise EXCEPTION 'polozka objednavky % modelu % z materialu % nenalezena', a_objednavka_pk, a_model_pk, a_material_pk;
  end if;

  update material SET
      skladem_blokace = skladem_blokace - l_mnozstvi
    , skladem_celkem = skladem_celkem
  where material_pk = lr_polozka.material_pk;

end;
$func$ language 'plpgsql';