create or replace function udalost_uloz(
    a_udalost_pk  udalost.udalost_pk%type
  , a_uzivatel_pk udalost.uzivatel_pk%type
  , a_typ         udalost.typ%type
  , a_nazev       udalost.nazev%type
  , a_popis       udalost.popis%type
  , a_konani_od   udalost.datum_od%type
  , a_konani_do   udalost.datum_do%type
  , a_barva       udalost.barva%type
  , a_upozorneni  udalost.upozorneni%type
  , a_opakovani   udalost.opakovani%type
) returns integer as $func$
/**
 * pokud je parametr a_udalost_pk je null, tak zalozi novou udalost, jinak udalost updatuje
 *
 * navratove hodnoty:
 * >0 v poradku, vracim udalost_pk
 * -1 neplatny typ udalosti
 * -2 udalost nenalezena
 * -3 udalost nepatri uzivateli
 */
declare
  l_pk udalost.udalost_pk%type;
  l_row udalost%rowtype;
  l_pocet integer;
  l_meta_id text;
begin

  if a_typ not in ('JEDNANI', 'UPOZORNENI', 'SCHUZKA') then
    return -1;
  end if;

  create extension if not exists unaccent;

  l_pocet := 0;
  l_meta_id := trim(both ' ' from lower(unaccent(a_nazev)));
  l_meta_id := trim(both '-' from regexp_replace(l_meta_id, '[^a-z0-9]', '-', 'g'));

  while exists(select 1 from udalost where meta = l_meta_id || (case when l_pocet = 0 then '' else '-' || l_pocet end)) loop
    l_pocet := l_pocet + 1;
  end loop;

  l_meta_id := l_meta_id || (case when l_pocet = 0 then '' else '-' || l_pocet end);

  -- pokud je udalost_pk, tak insertuju
  if (a_udalost_pk is null) then
    l_pk := nextval('udalost_udalost_pk_seq'::regclass);

    insert into udalost (
        udalost_pk
      , typ
      , nazev
      , meta
      , popis
      , datum_od
      , datum_do
      , opakovani
      , barva
      , upozorneni
      , uzivatel_pk
    ) values (
        l_pk
      , a_typ
      , a_nazev
      , l_meta_id
      , a_popis
      , a_konani_od
      , a_konani_do
      , a_opakovani
      , a_barva
      , a_upozorneni
      , a_uzivatel_pk
    );

  -- pokud neni, updatuju
  else
    select * into l_row from udalost where udalost_pk = a_udalost_pk;

    if not found then
      return -2;
    end if;

    if l_row.uzivatel_pk <> a_uzivatel_pk then
      return -3;
    end if;

    l_pk := a_udalost_pk;

    update udalost set
        typ = a_typ
      , nazev = a_nazev
      , meta = l_meta_id
      , popis = a_popis
      , datum_od = a_konani_od
      , datum_do = a_konani_do
      , opakovani = a_opakovani
      , barva = a_barva
      , upozorneni = a_upozorneni
    where udalost_pk = a_udalost_pk;

  end if;

  return l_pk;

end;
$func$ language 'plpgsql';


create or replace function udalost_nastav_ucastniky(
    a_udalost_pk  ucastnik_udalosti.udalost_pk%type
  , a_uzivatele integer[]
) returns integer as $func$
/**
 * nastavi udalosti vsechny ucastniky
 *
 * navratove hodnoty
 * 0  v poradku
 * -1 udalost nenalezena
 */
declare
  l_pk udalost.udalost_pk%type;
begin

  if not exists(select 1 from udalost where udalost_pk = a_udalost_pk) then
    return -1;
  end if;

  delete from ucastnik_udalosti where udalost_pk = a_udalost_pk;

  for u_pk in 1 .. array_upper(a_uzivatele, 1) loop
    insert into ucastnik_udalosti (uzivatel_pk, udalost_pk) values (a_uzivatele[u_pk], a_udalost_pk);
  end loop;

  return 0;

end;
$func$ language 'plpgsql';