CREATE OR REPLACE FUNCTION fakturacni_udaje_zmena(
      a_fakturacni_udaje    fakturacni_udaje.fakturacni_udaje_pk%TYPE
    , a_ic                  fakturacni_udaje.ic%TYPE
    , a_dic                 fakturacni_udaje.dic%TYPE
    , a_obchodni_jmeno      fakturacni_udaje.obchodni_jmeno%TYPE
    , a_jmeno               fakturacni_udaje.jmeno%TYPE
    , a_prijmeni            fakturacni_udaje.prijmeni%TYPE
    , a_ulice               fakturacni_udaje.ulice%TYPE
    , a_mesto               fakturacni_udaje.mesto%TYPE
    , a_psc                 fakturacni_udaje.psc%TYPE
    , a_zeme_id             fakturacni_udaje.zeme_id%TYPE
) RETURNS INTEGER AS $func$
DECLARE
    l_pk fakturacni_udaje.fakturacni_udaje_pk%TYPE;
BEGIN
    IF a_fakturacni_udaje IS NULL THEN
        l_pk := nextval('fakturacni_udaje_fakturacni_udaje_pk_seq'::REGCLASS);

        INSERT INTO fakturacni_udaje (
              fakturacni_udaje_pk
            , typ
            , ic
            , dic
            , obchodni_jmeno
            , jmeno
            , prijmeni
            , ulice
            , mesto
            , psc
            , zeme_id
        ) VALUES (
              l_pk
            , CASE WHEN nullif(a_ic, '') IS NOT NULL THEN 'PO' ELSE 'FO' END
            , a_ic
            , a_dic
            , a_obchodni_jmeno
            , a_jmeno
            , a_prijmeni
            , a_ulice
            , a_mesto
            , a_psc
            , a_zeme_id
        );
    ELSE
        l_pk := a_fakturacni_udaje;

        UPDATE fakturacni_udaje SET
              typ = CASE WHEN nullif(a_ic, '') IS NOT NULL THEN 'PO' ELSE 'FO' END
            , ic = a_ic
            , dic = a_dic
            , obchodni_jmeno = a_obchodni_jmeno
            , jmeno = a_jmeno
            , prijmeni = a_prijmeni
            , ulice = a_ulice
            , mesto = a_mesto
            , psc = a_psc
            , zeme_id = a_zeme_id
        WHERE fakturacni_udaje_pk = l_pk;

        IF NOT FOUND THEN
            RETURN -1;
        END IF;
    END IF;

    RETURN l_pk;
END;
$func$ LANGUAGE 'plpgsql';
---

---
CREATE OR REPLACE FUNCTION dodaci_udaje_zmena(
      a_dodaci_udaje    dodaci_udaje.dodaci_udaje_pk%TYPE
    , a_firma           dodaci_udaje.firma%TYPE
    , a_jmeno           dodaci_udaje.jmeno%TYPE
    , a_prijmeni        dodaci_udaje.prijmeni%TYPE
    , a_ulice           dodaci_udaje.ulice%TYPE
    , a_mesto           dodaci_udaje.mesto%TYPE
    , a_psc             dodaci_udaje.psc%TYPE
    , a_zeme_id         dodaci_udaje.zeme_id%TYPE
) RETURNS INTEGER AS $func$
DECLARE
    l_pk dodaci_udaje.dodaci_udaje_pk%TYPE;
BEGIN
    IF a_dodaci_udaje IS NULL THEN
        l_pk := nextval('dodaci_udaje_dodaci_udaje_pk_seq'::REGCLASS);

        INSERT INTO dodaci_udaje (
              dodaci_udaje_pk
            , firma
            , jmeno
            , prijmeni
            , ulice
            , mesto
            , psc
            , zeme_id
        ) VALUES (
              l_pk
            , a_firma
            , a_jmeno
            , a_prijmeni
            , a_ulice
            , a_mesto
            , a_psc
            , a_zeme_id
        );
    ELSE
        l_pk := a_dodaci_udaje;

        UPDATE dodaci_udaje SET
              firma = a_firma
            , jmeno = a_jmeno
            , prijmeni = a_prijmeni
            , ulice = a_ulice
            , mesto = a_mesto
            , psc = a_psc
            , zeme_id = a_zeme_id
        WHERE dodaci_udaje_pk = l_pk;

        IF NOT FOUND THEN
            RETURN -1;
        END IF;
    END IF;

    RETURN l_pk;
END;
$func$ LANGUAGE 'plpgsql';