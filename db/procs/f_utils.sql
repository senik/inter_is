create or replace function generuj_meta_id(
  a_text text
) returns text as $func$
declare
  l_meta_id text;
begin

  create extension if not exists unaccent;

  l_meta_id := trim(both ' ' from lower(unaccent(a_text)));
  l_meta_id := trim(both '-' from regexp_replace(l_meta_id, '[^a-z0-9]', '-', 'g'));

  return l_meta_id;

end;
$func$ language 'plpgsql';


create or replace function model_generuj_meta(
  a_text text
) returns text as $func$
declare
  l_meta_id text;
  l_pocet integer := 0;
  l_add text;
begin

  l_meta_id := generuj_meta_id(a_text);

  while exists(select 1 from model3d where model_id = l_meta_id) loop
    l_pocet := l_pocet + 1;
    if (l_pocet = 0) then l_add = ''; else l_add = '-' || l_pocet; end if;

    l_meta_id := l_meta_id || l_add;
  end loop;

  return l_meta_id;
end;
$func$ language 'plpgsql';


create or replace function new_pkey(a_table text, a_primary_key text) returns integer as $func$
begin
    return nextval(format('%s_%s_seq', a_table, a_primary_key));
end;
$func$ language 'plpgsql';