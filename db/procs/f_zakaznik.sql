create or replace function zakaznik_uloz(
    a_zakaznik_pk zakaznik.zakaznik_pk%type
  , a_jmeno       zakaznik.jmeno%type
  , a_prijmeni    zakaznik.prijmeni%type
  , a_email       zakaznik.email%type
  , a_telefon     zakaznik.telefon%type
) returns integer as $func$
/**
 * metoda ulozi noveho/upraveneho zakaznika
 * >0 v poradku, vracim pk zaznamu
 * -1 zakladam zakaznika s emailem, ktery uz v DB je
 * -2 updatuju neexistujiciho zakaznika
 */
declare
  l_pk    zakaznik.zakaznik_pk%type;
  l_email zakaznik.email%type;
  l_row   zakaznik%rowtype;
begin

  l_email := trim(both ' ' from lower(a_email));

  if (a_zakaznik_pk is null) then

    l_pk := nextval('zakaznik_zakaznik_pk_seq' :: regclass);

    select into l_row * from zakaznik where trim(both ' ' from lower(email)) = l_email;

    if found then
      return -1;
    end if;

    insert into zakaznik (
        zakaznik_pk
      , jmeno
      , prijmeni
      , email
      , telefon
    ) values (
        l_pk
      , a_jmeno
      , a_prijmeni
      , l_email
      , a_telefon
    );
  else

    select * into l_row from zakaznik where zakaznik_pk = a_zakaznik_pk;

    if not found then
      return -2;
    end if;

    l_pk := l_row.zakaznik_pk;

    update zakaznik set
        jmeno = a_jmeno
      , prijmeni = a_prijmeni
      , email = l_email
      , telefon = a_telefon
    where zakaznik_pk = l_pk;

  end if;

  return l_pk;

end;
$func$ language 'plpgsql';
---

---
create or replace function zakaznik_uloz_dodaci_udaje(
      a_dodaci_udaje_pk dodaci_udaje.dodaci_udaje_pk%type
    , a_firma           dodaci_udaje.firma%type
    , a_jmeno           dodaci_udaje.jmeno%type
    , a_prijmeni        dodaci_udaje.prijmeni%type
    , a_ulice           dodaci_udaje.ulice%type
    , a_mesto           dodaci_udaje.mesto%type
    , a_psc             dodaci_udaje.psc%type
    , a_zeme_id         dodaci_udaje.zeme_id%type
    , a_zakaznik_pk     zakaznik.zakaznik_pk%type
) returns integer as $func$
declare
  l_row zakaznik_dodaci_udaje%rowtype;
  l_pk  dodaci_udaje.dodaci_udaje_pk%type;
  l_udaje_row  dodaci_udaje%rowtype;
    lr_fakturacni fakturacni_udaje%ROWTYPE;
begin
    if not exists(select 1 from zakaznik where zakaznik_pk = a_zakaznik_pk) then
        return -1;
    end if;

    -- pokud jsou dodaci_udaje_pk null, tak insert
    if (a_dodaci_udaje_pk is null or a_dodaci_udaje_pk > 0) THEN
        l_pk := dodaci_udaje_zmena(
              a_dodaci_udaje_pk
            , a_firma
            , a_jmeno
            , a_prijmeni
            , a_ulice
            , a_mesto
            , a_psc
            , a_zeme_id
        );
    else
        -- kontrola, ze nekdo neodeslal jina data
        select * into lr_fakturacni from zakaznik_fakturacni_udaje zfu
            JOIN fakturacni_udaje fu ON zfu.fakturacni_udaje_pk = fu.fakturacni_udaje_pk
        where zfu.zakaznik_pk = a_zakaznik_pk
            AND fu.jmeno    IS NOT DISTINCT FROM a_jmeno
            AND fu.prijmeni IS NOT DISTINCT FROM a_prijmeni
            AND fu.ulice    IS NOT DISTINCT FROM a_ulice
            AND fu.mesto    IS NOT DISTINCT FROM a_mesto
            AND fu.psc      IS NOT DISTINCT FROM a_psc
            AND fu.zeme_id  IS NOT DISTINCT FROM a_zeme_id
        ORDER BY fu.fakturacni_udaje_pk LIMIT 1;

        IF NOT FOUND THEN
            RETURN -2;
        END IF;

        l_pk := dodaci_udaje_zmena(
              NULL -- dodaci_udaje_pk
            , NULL -- firma
            , lr_fakturacni.jmeno
            , lr_fakturacni.prijmeni
            , lr_fakturacni.ulice
            , lr_fakturacni.mesto
            , lr_fakturacni.psc
            , lr_fakturacni.zeme_id
        );
    end if;

    -- pokud nenalezeno, tak propojim
    if not exists(select 1 from zakaznik_dodaci_udaje where zakaznik_pk = a_zakaznik_pk and dodaci_udaje_pk = l_pk) THEN
        INSERT INTO zakaznik_dodaci_udaje (zakaznik_pk, dodaci_udaje_pk) VALUES (a_zakaznik_pk, l_pk);
    end if;

    return l_pk;
end;
$func$ language 'plpgsql';
---

---
create or replace function zakaznik_uloz_fakturacni_udaje(
      a_fakturacni_udaje_pk fakturacni_udaje.fakturacni_udaje_pk%type
    , a_typ                 fakturacni_udaje.typ%type
    , a_ic                  fakturacni_udaje.ic%type
    , a_dic                 fakturacni_udaje.dic%type
    , a_obchodni_jmeno      fakturacni_udaje.obchodni_jmeno%type
    , a_jmeno               fakturacni_udaje.jmeno%type
    , a_prijmeni            fakturacni_udaje.prijmeni%type
    , a_ulice               fakturacni_udaje.ulice%type
    , a_mesto               fakturacni_udaje.mesto%type
    , a_psc                 fakturacni_udaje.psc%type
    , a_zeme_id             fakturacni_udaje.zeme_id%type
    , a_zakaznik_pk         zakaznik.zakaznik_pk%type
) returns integer as $func$
declare
    l_row zakaznik_fakturacni_udaje%rowtype;
    l_pk  fakturacni_udaje.fakturacni_udaje_pk%type;

    lr_zakaznik zakaznik%ROWTYPE;
begin

    SELECT * INTO lr_zakaznik FROM zakaznik WHERE zakaznik_pk = a_zakaznik_pk;
    IF NOT FOUND THEN
        RETURN -1;
    END IF;

    l_pk := fakturacni_udaje_zmena(
          a_fakturacni_udaje_pk
        , a_ic
        , a_dic
        , a_obchodni_jmeno
        , a_jmeno
        , a_prijmeni
        , a_ulice
        , a_mesto
        , a_psc
        , a_zeme_id
    );

    -- pokud nenalezeno, tak propojim
    IF NOT EXISTS(SELECT 1 FROM zakaznik_fakturacni_udaje WHERE fakturacni_udaje_pk = l_pk AND zakaznik_pk = a_zakaznik_pk) THEN
        INSERT INTO zakaznik_fakturacni_udaje (
              zakaznik_pk
            , fakturacni_udaje_pk
        ) VALUES (
              a_zakaznik_pk
            , l_pk
        );
    END IF;

    RETURN l_pk;
end;
$func$ language 'plpgsql';