create or replace function objednavka_generuj_cislo() returns text as $func$
/**
 *
 */
declare
  l_pocet integer;
  l_rok   integer;
  l_cislo integer;
begin
  l_pocet := (select count(*) from objednavka where datum_zalozeni between date_trunc('year', now()) AND CURRENT_TIMESTAMP);
  l_rok := (select extract(year from now()));

  l_cislo := l_rok || lpad((l_pocet + 1)::text, 5, '0');

  -- snad se to nezacykli
  if exists(select 1 from objednavka where cislo = l_cislo) then
    l_cislo := objednavka_generuj_cislo();
  end if;

  return l_cislo;
end;
$func$ language 'plpgsql';
---

---
create or replace function objednavka_zmena(
    a_objednavka_pk objednavka.objednavka_pk%type
  , a_cislo objednavka.cislo%type
  , a_zakaznik_pk zakaznik.zakaznik_pk%type
  , a_vlozil_pk uzivatel.uzivatel_pk%type
  , a_termin objednavka.termin%type
  , a_fa_udaje_pk fakturacni_udaje.fakturacni_udaje_pk%type
  , a_do_udaje_pk dodaci_udaje.dodaci_udaje_pk%type
  , a_polozky text[][]
) returns integer as $func$
/**
 * zalozi objednavku
 * todo jeste predelat na polozku kvuli eshopu
 */
declare
  lr_objednavka objednavka%rowtype;
  lr_fa_udaje   record;
  lr_do_udaje   record;
  lr_model      model3d%rowtype;
  lr_material   record;
  lr_polozka    objednavka_polozka%rowtype;
  l_cislo       integer;
  l_error       text;
  l_pk          integer;
begin

  if a_fa_udaje_pk is not null and a_fa_udaje_pk > 0 then
    select * into lr_fa_udaje from fakturacni_udaje fa where fa.fakturacni_udaje_pk = a_fa_udaje_pk;
    if not found then return -1; end if;
  end if;

  if a_do_udaje_pk is not null and a_do_udaje_pk > 0 then
    select * into lr_do_udaje from dodaci_udaje du where du.dodaci_udaje_pk = a_do_udaje_pk;
    if not found then return -2; end if;
  end if;

  if (a_objednavka_pk is null) then
    if exists(select 1 from objednavka where cislo = a_cislo) then
      l_cislo := objednavka_generuj_cislo();
    else
      l_cislo := a_cislo;
    end if;

    l_pk := nextval('objednavka_objednavka_pk_seq'::regclass);

    insert into objednavka (
        objednavka_pk
      , datum_zalozeni
      , stav
      , cislo
      , zakaznik_pk
      , vlozil
      , termin
    ) values (
        l_pk
      , now()
      , 1
      , l_cislo
      , a_zakaznik_pk
      , a_vlozil_pk
      , a_termin
    );

  else
    -- tady toho asi moc updatovat nebudu
    UPDATE objednavka SET
      termin = a_termin
    where objednavka_pk = a_objednavka_pk RETURNING objednavka_pk into l_pk;

    -- protoze menim polozky, tak udelam delete vsech, co tam uz mam
    -- ale nejdriv musim vratit material na sklad
    for lr_polozka in select * from objednavka_polozka where objednavka_pk = a_objednavka_pk LOOP
      perform polozka_vrat_material_na_sklad(lr_polozka.objednavka_pk, lr_polozka.model_pk, lr_polozka.material_pk);

    end loop;
    delete from objednavka_polozka where objednavka_polozka.objednavka_pk = a_objednavka_pk;

  end if;

    IF a_do_udaje_pk IS NOT NULL THEN
        DELETE FROM objednavka_dodaci_udaje WHERE objednavka_pk = l_pk;
        INSERT INTO objednavka_dodaci_udaje(objednavka_pk, dodaci_udaje_pk) VALUES (l_pk, a_do_udaje_pk);
    END IF;

    IF a_fa_udaje_pk IS NOT NULL THEN
        DELETE FROM objednavka_fakturacni_udaje WHERE objednavka_pk = l_pk;
        INSERT INTO objednavka_fakturacni_udaje(objednavka_pk, fakturacni_udaje_pk) VALUES (l_pk, a_fa_udaje_pk);
    END IF;

  select * into lr_objednavka from objednavka where objednavka_pk = l_pk;

  for i in 1..array_upper(a_polozky, 1) loop

    select * into lr_model from model3d where model_pk = a_polozky[i][1]::int;
    if not found THEN
      raise EXCEPTION 'objednavka_zmena - model s pk = % neexistuje!', a_polozky[i][1];
    end if;

    select m.material_pk, md.hustota into lr_material from material m join material_druh md on m.material_druh_id = md.material_druh_id where m.material_pk = a_polozky[i][2]::int;
    if not found then
      raise exception 'objednavka_zmena - material s pk = % neexistuje!', a_polozky[i][2]::int;
    end if;

    insert into objednavka_polozka (objednavka_pk, model_pk, material_pk, pocet)
    values (lr_objednavka.objednavka_pk, a_polozky[i][1]::int, a_polozky[i][2]::int, a_polozky[i][3]::int);

    update material set
      skladem_blokace = skladem_blokace + (round(lr_model.objem * lr_material.hustota, 0) * a_polozky[i][3]::int)
    where material_pk = a_polozky[i][2]::int;

  end loop;

  return lr_objednavka.objednavka_pk;

end;
$func$ language 'plpgsql';
---

---
create or replace function objednavka_zmen_dodaci(
    a_objednavka_pk objednavka.objednavka_pk%type
  , a_dodaci_udaje_pk dodaci_udaje.dodaci_udaje_pk%type
) returns integer as $func$
/**
 * navratove kody:
 * -1   nenalezena objednavka podle pk
 * -2   nenalezeny dodaci udaje (at jiz chybejici udaje nebo adresa)
 * >0   v poradku, vracim pk objednavky
 */
declare
  lr_dodaci record;
begin
  if not exists(select 1 from objednavka where objednavka_pk = a_objednavka_pk) THEN
    return -1;
  end if;

    if not exists(select 1 from dodaci_udaje du where du.dodaci_udaje_pk = a_dodaci_udaje_pk) then
        return -2;
    end if;

    -- nejdriv smazu vsechny navazane
    for lr_dodaci in select * from objednavka_dodaci_udaje where objednavka_pk = a_objednavka_pk and dodaci_udaje_pk <> a_dodaci_udaje_pk loop
        delete from dodaci_udaje where dodaci_udaje_pk = lr_dodaci.dodaci_udaje_pk;
        delete from objednavka_dodaci_udaje where objednavka_pk = lr_dodaci.objednavka_pk and dodaci_udaje_pk = lr_dodaci.dodaci_udaje_pk;
    end loop;

    -- a insertuju nove
    insert into objednavka_dodaci_udaje (objednavka_pk, dodaci_udaje_pk) VALUES
        (a_objednavka_pk, a_dodaci_udaje_pk);

  return a_objednavka_pk;
end;
$func$ language 'plpgsql';
---

---
create or replace function objednavka_zmen_fakturacni(
    a_objednavka_pk objednavka.objednavka_pk%type
  , a_fakturacni_pk fakturacni_udaje.fakturacni_udaje_pk%type
) returns integer as $func$
/**
 *
 */
declare
    lr_fakturacni record;
begin
    if not exists(select 1 from objednavka where objednavka_pk = a_objednavka_pk) THEN
        return -1;
    end if;

    if not exists(select 1 from fakturacni_udaje fu where fu.fakturacni_udaje_pk = a_fakturacni_pk) THEN
        return -2;
    end if;

    -- nejdriv smazu vsechny navazane
    for lr_fakturacni in select * from objednavka_fakturacni_udaje where objednavka_pk = a_objednavka_pk and fakturacni_udaje_pk <> a_fakturacni_pk loop
        delete from fakturacni_udaje where fakturacni_udaje_pk = lr_fakturacni.fakturacni_udaje_pk;
        delete from objednavka_fakturacni_udaje where objednavka_pk = lr_fakturacni.objednavka_pk and fakturacni_udaje_pk = lr_fakturacni.fakturacni_udaje_pk;
    end loop;

    -- a zalozim nove
    insert into objednavka_fakturacni_udaje (objednavka_pk, fakturacni_udaje_pk) VALUES
        (a_objednavka_pk, a_fakturacni_pk);

  return a_objednavka_pk;
end;
$func$ language 'plpgsql';
---

---
CREATE OR REPLACE FUNCTION objednavka_zmen_stav(
    a_objednavka_pk objednavka.objednavka_pk%TYPE
  , a_stav          objednavka.stav%TYPE
) RETURNS INTEGER AS $func$
/**
 * zmeni stav objednavky (po filtraci stavu v PHP, takze se o to nemusim starat)
 * soucasne upravi skladovou dostupnost materialu pri zmene
 * taky bude potreba resit fakturu, pokud bude objednavka vyfakturovana a ja objednavku vystornuju
 *
 * navratove hodnoty:
 */
DECLARE
  lr_objednavka         objednavka%ROWTYPE;
  lr_objednavka_polozka objednavka_polozka%ROWTYPE;
  lr_row                record;
  l_material_blokace    material.skladem_blokace%TYPE;
  l_material            material.material_pk%TYPE;
  l_polozka_cena        objednavka_polozka.cena_kus%TYPE;
  l_faktura_pk          faktura.faktura_pk%TYPE;
BEGIN
--   BEGIN
    SELECT *
    INTO lr_objednavka
    FROM objednavka
    WHERE objednavka_pk = a_objednavka_pk;

    IF NOT found THEN
      RETURN -1;
    END IF;

  UPDATE objednavka SET
      stav        = a_stav
    , datum_zmeny = now()
  WHERE objednavka_pk = a_objednavka_pk;

    -- pokud je objednavka hotovo
    IF lr_objednavka.stav = 3 AND a_stav = 4 THEN
      l_faktura_pk := faktura_zaloz_z_objednavky(a_objednavka_pk);

      if l_faktura_pk < 0 THEN
        raise exception 'nepodarilo se zalozit fakturu (%)', l_faktura_pk;
      end if;
    -- ze stavu zalozena do stavu potvrzena,
    ELSEIF lr_objednavka.stav = 1 AND a_stav = 2 THEN
      -- odecitam material
      -- jestli udelam historii materialu, tak budu joinovat to... tyhle vypocty jsou akorat zdroj chyb
      FOR l_material_blokace, l_material IN
        SELECT
            sum(round(m3d.objem * md.hustota, 0) * op.pocet)
          , m.material_pk
        FROM objednavka_polozka op
          JOIN material m ON op.material_pk = m.material_pk
          JOIN material_druh md ON m.material_druh_id = md.material_druh_id
          JOIN model3d m3d ON op.model_pk = m3d.model_pk
        WHERE op.objednavka_pk = a_objednavka_pk
        GROUP BY op.objednavka_pk, m.material_pk
      LOOP
        UPDATE material SET
            skladem_blokace   = skladem_blokace - l_material_blokace
          , skladem_celkem    = skladem_celkem - l_material_blokace
        WHERE material_pk = l_material;
      END LOOP;

      -- no a taky ulozim cenu do polozky, jinak se z toho zblaznim...
      FOR lr_row IN
        SELECT
            round((msv.hustota * msv.cena * m.objem), 2) as cena_kus
          , op.objednavka_pk
          , op.model_pk
          , op.material_pk
        FROM objednavka_polozka op
          JOIN material_skladem_view msv on op.material_pk = msv.material_pk
          JOIN model3d m on m.model_pk = op.model_pk
        WHERE op.objednavka_pk = a_objednavka_pk
      LOOP
        UPDATE objednavka_polozka SET
          cena_kus = lr_row.cena_kus
        WHERE objednavka_polozka.objednavka_pk    = lr_row.objednavka_pk
              AND objednavka_polozka.material_pk  = lr_row.material_pk
              AND objednavka_polozka.model_pk     = lr_row.model_pk;

      END LOOP;
    ELSEIF a_stav in (-1, -2) AND lr_objednavka.stav IN (2, 3) THEN
      -- stornuju objednavku potvrzenou nebo zpracovavanou objednavku, takze musim vratit blokovany material
      FOR l_material_blokace, l_material IN
        SELECT
            sum(round(m3d.objem * md.hustota, 0) * op.pocet)
          , m.material_pk
        FROM objednavka_polozka op
          JOIN material m ON op.material_pk = m.material_pk
          JOIN material_druh md ON m.material_druh_id = md.material_druh_id
          JOIN model3d m3d ON op.model_pk = m3d.model_pk
        WHERE op.objednavka_pk = a_objednavka_pk
        GROUP BY op.objednavka_pk, m.material_pk
      LOOP
        UPDATE material SET
            skladem_blokace   = CASE WHEN lr_objednavka.stav = 2 THEN skladem_blokace - l_material_blokace ELSE skladem_blokace END
          , skladem_celkem    = CASE WHEN lr_objednavka.stav = 2 THEN skladem_celkem ELSE skladem_celkem + l_material_blokace END
        WHERE material_pk = l_material;
      END LOOP;
    END IF;

    RETURN a_objednavka_pk;

--   EXCEPTION
--     WHEN OTHERS THEN
--       RETURN -2;
--   END;

END;
$func$ LANGUAGE 'plpgsql';
---

---
create or replace function polozka_vytisteno(
    a_objednavka_pk   objednavka.objednavka_pk%TYPE
  , a_model_pk        model3d.model_pk%TYPE
  , a_material_pk     material.material_pk%TYPE
  , a_pocet           integer
) returns integer as $func$
DECLARE
  lr_objednavka objednavka%ROWTYPE;
  lr_model model3d%ROWTYPE;
  lr_material material%ROWTYPE;
  lr_polozka objednavka_polozka%ROWTYPE;

  l_hotovo integer;
  l_polozek integer;
  l_stav integer;
BEGIN
  select * into lr_objednavka from objednavka where objednavka_pk = a_objednavka_pk;
  -- tohle by nemelo nastat diky cizimu klici na polozce, ale jistota je jistota
  if not found THEN
    return -1;
  end if;

  -- menit pocet vytistenych polozek muzu jen u objednavek, ktere jsou potvrzene nebo se zpracovavaji
  if lr_objednavka.stav NOT IN (2,3) THEN
    return -2;
  end if;

  select * into lr_polozka from objednavka_polozka op where op.objednavka_pk = a_objednavka_pk and op.model_pk = a_model_pk and op.material_pk = a_material_pk;
  -- pokud jsem nenasel polozku objednavky, je to blbe
  if not found THEN
    return -3;
  end if;

  -- pokud je pocet hotovych vetsi nez celkovy pocet, je to blbe, ale na tohle bude validace v PHP
  if (a_pocet + lr_polozka.pocet_hotovo > lr_polozka.pocet) THEN
    return -4;
  end if;

  -- pokud je objednavka ve stavu Potvrzena a nemam jeste nic vytisteno, prepinam ji do stavu Zpracovava se
  select sum(pocet_hotovo) into l_hotovo from objednavka_polozka where objednavka_pk = a_objednavka_pk group by objednavka_pk;
  if (l_hotovo = 0 and lr_objednavka.stav = 2) THEN
    l_stav := objednavka_zmen_stav(lr_objednavka.objednavka_pk, 3);

    if l_stav <> a_objednavka_pk THEN
      raise exception 'nepodarilo se zmenit stav objednavky (%), stary stav (%), novy stav (%), navrat (%)!', a_objednavka_pk, lr_objednavka.stav, 3, l_stav;
    end if;
  end if;

  -- provedu samotny update
  update objednavka_polozka op set pocet_hotovo = op.pocet_hotovo + a_pocet where op.objednavka_pk = a_objednavka_pk and op.model_pk = a_model_pk and op.material_pk = a_material_pk;

  -- pokud je objednavka ve stavu Potvrzena/Zpracovava se a mam vse vytisteno, prepinam ji do stavu Hotovo
  select sum(pocet_hotovo), sum(pocet) into l_hotovo, l_polozek from objednavka_polozka where objednavka_pk = a_objednavka_pk group by objednavka_pk;
  if (l_hotovo = l_polozek and lr_objednavka.stav IN (2, 3)) THEN
    l_stav := objednavka_zmen_stav(lr_objednavka.objednavka_pk, 4);

    if l_stav <> a_objednavka_pk THEN
      raise exception 'nepodarilo se zmenit stav objednavky (%), stary stav (%), novy stav (%), navrat (%)!', a_objednavka_pk, lr_objednavka.stav, 4, l_stav;
    end if;
  end if;

  return 0;

end;
$func$ LANGUAGE 'plpgsql';