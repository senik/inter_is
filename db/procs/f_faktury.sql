create or replace function faktura_generuj_cislo() returns text as $func$
/**
 *
 */
declare
  l_pocet integer;
  l_rok   integer;
  l_cislo integer;
begin
  l_pocet := (select count(*) from faktura where (datum_zalozeni between date_trunc('year', now()) AND CURRENT_TIMESTAMP) AND cislo <> 'XXXXX');
  l_rok := (select extract(year from now()));

  l_cislo := l_rok || lpad((l_pocet + 1)::text, 5, '0');

  -- snad se to nezacykli
  if exists(select 1 from faktura where cislo = 'FA' || l_cislo) then
    l_cislo := faktura_generuj_cislo();
  end if;

  return 'FA' || l_cislo;
end;
$func$ language 'plpgsql';
---

---
create or replace FUNCTION faktura_zaloz_z_objednavky(
  a_objednavka_pk faktura.objednavka_pk%type
) returns integer as $func$
DECLARE
  OBJ_HOTOVO CONSTANT int := 4;
  FA_CISLO_X CONSTANT text := 'XXXXX'; -- docasne, pri vystaveni se vygeneruje cislo FA
  lr_objednavka     objednavka%rowtype;
  lr_obj_polozka    RECORD;
  l_faktura_pk      faktura.faktura_pk%type;
  lr_fakturacni_udaje fakturacni_udaje%ROWTYPE;
  l_model_nazev     model3d.nazev%type;
  l_material_nazev  material_view.nazev_dlouhy%type;
BEGIN

  select * into lr_objednavka from objednavka where objednavka_pk = a_objednavka_pk;
  if not found THEN
    return -1;
  end if;

  if (lr_objednavka.stav < OBJ_HOTOVO) THEN
    return -2;
  end if;

    select faktura_pk into l_faktura_pk from faktura where objednavka_pk = a_objednavka_pk and stav > -1;
    if found then
        return l_faktura_pk;
    end if;

    l_faktura_pk := faktura_zmena(null, FA_CISLO_X, null, a_objednavka_pk);

    -- todo doladit ten view...
    for lr_obj_polozka in
        select
            op.*
            , mv.nazev_dlouhy AS material_nazev_dlouhy
            , mdl.nazev AS model_nazev
        from objednavka_polozka op
            JOIN material_view mv ON mv.material_pk = op.material_pk
            JOIN model3d mdl ON mdl.model_pk = op.model_pk
        where objednavka_pk = a_objednavka_pk
    LOOP

      select nazev_dlouhy into l_material_nazev from material_skladem_view where material_pk = lr_obj_polozka.material_pk;
      if not found THEN raise EXCEPTION 'nenalezen material pk = %', lr_obj_polozka.material_pk;
      end if;

      select nazev into l_model_nazev from model3d where model_pk = lr_obj_polozka.model_pk;
      if not found THEN raise EXCEPTION 'nenalezen model pk = %', lr_obj_polozka.model_pk;
      end if;

      insert into faktura_polozka (faktura_pk, nazev, kusu, cena_ks, cena_celkem, cena_ks_dph, cena_celkem_dph) values (
          l_faktura_pk
        , lr_obj_polozka.material_nazev_dlouhy || ', ' || lr_obj_polozka.model_nazev
        , lr_obj_polozka.pocet
        , lr_obj_polozka.cena_kus / (1 + dph_datum_vrat(now()::date))
        , (lr_obj_polozka.cena_kus * lr_obj_polozka.pocet) / (1 + dph_datum_vrat(now()::date))
        , lr_obj_polozka.cena_kus
        , lr_obj_polozka.cena_kus * lr_obj_polozka.pocet
      );

    end loop;

    select fu.* into lr_fakturacni_udaje from objednavka_fakturacni_udaje ofu
        join fakturacni_udaje fu on ofu.fakturacni_udaje_pk = fu.fakturacni_udaje_pk
    where ofu.objednavka_pk = a_objednavka_pk;

    if not found THEN
        return -3;
    end if;

    PERFORM faktura_uloz_fakturacni_udaje(
          l_faktura_pk
        , CASE WHEN lr_fakturacni_udaje.ic IS NULL THEN 'FO' ELSE 'PO' END
        , lr_fakturacni_udaje.ic
        , lr_fakturacni_udaje.dic
        , lr_fakturacni_udaje.obchodni_jmeno
        , lr_fakturacni_udaje.jmeno
        , lr_fakturacni_udaje.prijmeni
        , lr_fakturacni_udaje.ulice
        , lr_fakturacni_udaje.mesto
        , lr_fakturacni_udaje.psc
        , lr_fakturacni_udaje.zeme_id
    );

  RETURN l_faktura_pk;

END;
$func$ LANGUAGE 'plpgsql';
---

---
CREATE OR REPLACE FUNCTION faktura_vystav(
    a_faktura_pk        faktura.faktura_pk%type
  , a_uzivatel_pk       faktura.vystavil%type
  , a_datum_splatnosti  faktura.datum_splatnosti%type
) RETURNS INTEGER AS $func$
DECLARE
  lr_faktura              faktura%rowtype;
  lr_objednavka           objednavka%ROWTYPE;

  l_objednavka_stav       objednavka.stav%TYPE;

  STAV_ZALOZENA CONSTANT  INT := 1;
  STAV_VYSTAVENA CONSTANT INT := 2;
BEGIN
  select * into lr_faktura from faktura where faktura.faktura_pk = a_faktura_pk;
  if not found THEN
    return -1;
  end if;

  if lr_faktura.stav <> STAV_ZALOZENA THEN
    return -2;
  end if;

-- zmenim stav objednavky
  IF lr_faktura.objednavka_pk is not null THEN
    l_objednavka_stav := objednavka_zmen_stav(lr_faktura.objednavka_pk, 5);

    IF l_objednavka_stav < 0 THEN
      RETURN -3;
    END IF;
  END IF;

  IF a_datum_splatnosti IS NULL THEN
    a_datum_splatnosti := (now() + INTERVAL '14 days');
  END IF;

  UPDATE faktura SET
      vystavil         = a_uzivatel_pk
    , datum_vystaveni  = now()
    , datum_splatnosti = a_datum_splatnosti :: date
    , cislo            = faktura_generuj_cislo()
    , stav             = STAV_VYSTAVENA
  WHERE faktura_pk = a_faktura_pk;

  RETURN 1;

END;
$func$ LANGUAGE 'plpgsql';
---

---
CREATE OR REPLACE FUNCTION faktura_zmena(
    a_faktura_pk    faktura.faktura_pk%type
  , a_cislo         faktura.cislo%TYPE
  , a_splatnost     faktura.datum_splatnosti%TYPE
  , a_objednavka_pk faktura.objednavka_pk%TYPE
) RETURNS INTEGER AS $func$
DECLARE
  l_pk faktura.faktura_pk%TYPE;
BEGIN

  IF a_objednavka_pk IS NOT NULL AND NOT EXISTS(SELECT 1 FROM objednavka WHERE objednavka_pk = a_objednavka_pk) THEN
    RETURN -2;
  END IF;

  IF a_faktura_pk IS NULL THEN

    INSERT INTO faktura (
        cislo
      , datum_zalozeni
      , datum_splatnosti
      , objednavka_pk
      , stav
    ) VALUES (
        a_cislo
      , now()
      , a_splatnost
      , a_objednavka_pk
      , 1 -- stav zalozena
    ) RETURNING faktura_pk INTO l_pk;

  ELSE

    IF NOT EXISTS(SELECT 1 FROM faktura WHERE faktura_pk = a_faktura_pk) THEN
      RETURN -1;
    END IF;

    UPDATE faktura SET
        cislo = a_cislo
      , datum_splatnosti = a_splatnost
      , objednavka_pk = a_objednavka_pk
    WHERE faktura_pk = a_faktura_pk RETURNING faktura_pk INTO l_pk;

  END IF;

  RETURN l_pk;

END;
$func$ LANGUAGE 'plpgsql';
---

---
-- CREATE OR REPLACE FUNCTION faktura_pridej_polozku(
--     a_faktura_pk  faktura.faktura_pk%TYPE
--   , a_kod         faktura_polozka.kod%TYPE
--   , a_nazev       faktura_polozka.nazev%TYPE
--   , a_kusu        faktura_polozka.kusu%TYPE
--   , a_cena_ks     faktura_polozka.cena_ks%TYPE
--   , a_dph         faktura_polozka.dph%TYPE
-- ) RETURNS INTEGER AS $func$
-- DECLARE
--   lr_faktura      faktura%ROWTYPE;
--   l_cena_bez_dph  faktura_polozka.cena_ks%TYPE;
-- BEGIN
--
--   SELECT * INTO lr_faktura FROM faktura WHERE faktura_pk = a_faktura_pk;
--
--   IF NOT FOUND THEN
--     RETURN -1;
--   END IF;
--
--   RETURN faktura_polozka_zmena();
--
-- END;
-- $func$ LANGUAGE 'plpgsql';
---

---
CREATE OR REPLACE FUNCTION faktura_zmen_stav(
    a_faktura_pk  faktura.faktura_pk%TYPE
  , a_stav        faktura.stav%TYPE
) RETURNS INTEGER AS $func$
DECLARE

BEGIN

END;
$func$ LANGUAGE 'plpgsql';
---

---
CREATE OR REPLACE FUNCTION faktura_smaz(
  a_faktura_pk faktura.faktura_pk%TYPE
) RETURNS INTEGER AS $func$
DECLARE
    l_return INTEGER;
    lr_faktura faktura%ROWTYPE;
BEGIN

    SELECT * INTO lr_faktura FROM faktura WHERE faktura_pk = a_faktura_pk;
    IF NOT FOUND THEN
        RETURN -1;
    END IF;

    IF lr_faktura.stav = -2 THEN
        RETURN -2;
    END IF;

    IF NOT EXISTS(SELECT 1 FROM objednavka WHERE objednavka_pk = lr_faktura.objednavka_pk) THEN
        RETURN -3;
    END IF;

    WITH t AS (
        UPDATE faktura SET stav = -2 WHERE faktura_pk = a_faktura_pk RETURNING *
    ) SELECT count(*) INTO l_return FROM t;

    RETURN l_return;

END;
$func$ LANGUAGE 'plpgsql';
---

---
CREATE OR REPLACE FUNCTION faktura_smaz_polozky(
    a_faktura_pk faktura.faktura_pk%TYPE
) RETURNS INTEGER AS $func$
DECLARE

BEGIN

  IF NOT exists(SELECT 1 FROM faktura WHERE faktura_pk = a_faktura_pk) THEN
    RETURN -1;
  END IF;

  DELETE FROM faktura_polozka WHERE faktura_pk = a_faktura_pk;

  RETURN 0;

END;
$func$ LANGUAGE 'plpgsql';
---

---
CREATE OR REPLACE FUNCTION faktura_polozka_zmena(
    a_pk              faktura_polozka.faktura_polozka_pk%TYPE
  , a_faktura         faktura_polozka.faktura_pk%TYPE
  , a_nazev           faktura_polozka.nazev%TYPE
  , a_kusu            faktura_polozka.kusu%TYPE
  , a_cena_ks         faktura_polozka.cena_ks%TYPE
  , a_cena_celkem     faktura_polozka.cena_celkem%TYPE
  , a_cena_ks_dph     faktura_polozka.cena_ks_dph%TYPE
  , a_cena_celkem_dph faktura_polozka.cena_celkem_dph%TYPE
  , a_dph             faktura_polozka.dph%TYPE
) RETURNS INTEGER AS $func$
DECLARE
  l_pk faktura_polozka.faktura_polozka_pk%TYPE;
BEGIN

  IF NOT EXISTS(SELECT 1 FROM faktura WHERE faktura_pk = a_faktura) THEN
    RETURN -1;
  END IF;

  IF a_pk IS NULL THEN

    INSERT INTO faktura_polozka (
        nazev
      , kusu
      , cena_ks
      , cena_celkem
      , cena_ks_dph
      , cena_celkem_dph
      , dph
      , faktura_pk
    ) VALUES (
        a_nazev
      , a_kusu
      , a_cena_ks
      , a_cena_celkem
      , a_cena_ks_dph
      , a_cena_celkem_dph
      , a_dph
      , a_faktura
    ) RETURNING faktura_polozka_pk INTO l_pk;

  ELSE

    UPDATE faktura_polozka SET
        nazev = a_nazev
      , kusu = a_kusu
      , cena_ks = a_cena_ks
      , cena_celkem = a_cena_celkem
      , cena_ks_dph = a_cena_ks_dph
      , cena_celkem_dph = a_cena_celkem_dph
      , dph = a_dph
      , faktura_pk = a_faktura
    WHERE faktura_polozka_pk = a_pk RETURNING faktura_polozka_pk INTO l_pk;

  END IF;

  RETURN l_pk;

END;
$func$ LANGUAGE 'plpgsql';
---

---
CREATE OR REPLACE FUNCTION faktura_spoj_polozky(
    a_faktura_pk faktura.faktura_pk%TYPE
  , a_spojit INT[]
  , a_stejny_pocet boolean
) RETURNS INTEGER AS $func$
DECLARE
  lr_polozka faktura_polozka%ROWTYPE;
  l_nazev    TEXT := '';
  l_kusu     INTEGER := 0;
  l_cena     NUMERIC(10, 2) := 0.00;
  l_pk       faktura_polozka.faktura_polozka_pk%TYPE;
BEGIN

  FOR i IN 1..array_upper(a_spojit, 1) LOOP

    SELECT * INTO lr_polozka FROM faktura_polozka WHERE faktura_polozka_pk = a_spojit[i];

    IF NOT FOUND THEN
      RAISE EXCEPTION 'polozka faktury s pk = % nenalezena', a_spojit[i];
    END IF;

    IF a_stejny_pocet THEN
      l_nazev := coalesce(l_nazev, '') || lr_polozka.nazev || (CASE WHEN i < array_upper(a_spojit, 1) THEN '; ' ELSE '' END);
      l_cena := coalesce(l_cena, 0.00) + lr_polozka.cena_ks_dph;
    ELSE
      l_nazev := coalesce(l_nazev, '') || lr_polozka.nazev || ' (' || lr_polozka.kusu || ' ks)' || (CASE WHEN i < array_upper(a_spojit, 1) THEN '; ' ELSE '' END);
      l_cena := coalesce(l_cena, 0.00) + lr_polozka.cena_celkem_dph;
    END IF;

  END LOOP;

  IF a_stejny_pocet THEN
    l_kusu := lr_polozka.kusu;
  ELSE
    l_kusu := 1;
  END IF;

  INSERT INTO faktura_polozka (nazev, kusu, cena_ks, cena_celkem, cena_ks_dph, cena_celkem_dph, dph, faktura_pk) VALUES
    (l_nazev, l_kusu, (l_cena / (1 + coalesce(lr_polozka.dph, 0.21))), ((l_cena * l_kusu) / (1 + coalesce(lr_polozka.dph, 0.21))), l_cena, (l_cena * l_kusu), coalesce(lr_polozka.dph, 0.21), a_faktura_pk)
  RETURNING faktura_polozka_pk INTO l_pk;

  DELETE FROM faktura_polozka WHERE faktura_polozka_pk = ANY(a_spojit::INT[]);

  RETURN l_pk;

END;
$func$ LANGUAGE 'plpgsql';
---

---
create or REPLACE function dph_datum_vrat(
  a_datum date
) returns numeric as $func$
DECLARE

BEGIN
  RETURN 0.21;
END;
$func$ LANGUAGE 'plpgsql';
---

---
CREATE OR REPLACE FUNCTION faktura_uloz_fakturacni_udaje(
      a_faktura_pk          faktura.faktura_pk%TYPE
    , a_typ                 fakturacni_udaje.typ%TYPE
    , a_ic                  fakturacni_udaje.ic%TYPE
    , a_dic                 fakturacni_udaje.dic%TYPE
    , a_obchodni_jmeno      fakturacni_udaje.obchodni_jmeno%TYPE
    , a_jmeno               fakturacni_udaje.jmeno%TYPE
    , a_prijmeni            fakturacni_udaje.prijmeni%TYPE
    , a_ulice               fakturacni_udaje.ulice%TYPE
    , a_mesto               fakturacni_udaje.mesto%TYPE
    , a_psc                 fakturacni_udaje.psc%TYPE
    , a_zeme                zeme.zeme_id%TYPE
) RETURNS INTEGER AS $func$
DECLARE
    l_row zakaznik_fakturacni_udaje%ROWTYPE;
    l_pk  fakturacni_udaje.fakturacni_udaje_pk%TYPE;
    lr_fakturacni_udaje RECORD;
    lr_faktura faktura%ROWTYPE;

    lb_propojit BOOLEAN;
BEGIN

    IF NOT EXISTS(SELECT 1 FROM faktura WHERE faktura_pk = a_faktura_pk) THEN
        RETURN -3;
    END IF;

    -- dohledam uz ulozene fakturacni udaje
    SELECT fakturacni_udaje_pk INTO l_pk FROM faktura_fakturacni_udaje WHERE faktura_pk = a_faktura_pk;
    lb_propojit := NOT FOUND;

    -- uprava fakturacnich udaju
    l_pk := fakturacni_udaje_zmena(
          l_pk
        , a_ic
        , a_dic
        , a_obchodni_jmeno
        , a_jmeno
        , a_prijmeni
        , a_ulice
        , a_mesto
        , a_psc
        , a_zeme
    );

    IF lb_propojit THEN
        INSERT INTO faktura_fakturacni_udaje (faktura_pk, fakturacni_udaje_pk) VALUES
            (a_faktura_pk, l_pk);
    END IF;

    RETURN l_pk;

END;
$func$ LANGUAGE 'plpgsql';
---

---
CREATE OR REPLACE FUNCTION faktura_zaplacena(
    a_faktura_pk faktura.faktura_pk%TYPE
) RETURNS INTEGER AS $func$
DECLARE
    l_return INTEGER;
    lr_faktura faktura%ROWTYPE;
    lr_objednavka objednavka%ROWTYPE;

    FA_STAV_ZAPLACENA CONSTANT INT := 10;
    OB_STAV_ZAPLACENA CONSTANT INT := 10;
    OB_STAV_VYFAKTURO CONSTANT INT := 5;
    OB_STAV_ODESLANO  CONSTANT INT := 9;
BEGIN

    SELECT * INTO lr_faktura FROM faktura WHERE faktura_pk = a_faktura_pk;
    IF NOT FOUND THEN
        RETURN -1;
    END IF;

    UPDATE faktura SET
        stav = FA_STAV_ZAPLACENA
        , datum_zaplaceni = now()
    WHERE faktura_pk = a_faktura_pk;

    -- pokud je faktura zaplacena, je zaplacena i objednavka
    SELECT * INTO lr_objednavka FROM objednavka WHERE objednavka_pk = lr_faktura.objednavka_pk
                                                      AND stav = ANY(ARRAY[OB_STAV_VYFAKTURO, OB_STAV_ODESLANO]::INT[]);
    IF FOUND THEN
        PERFORM objednavka_zmen_stav(lr_objednavka.objednavka_pk, OB_STAV_ZAPLACENA);
    END IF;

    RETURN lr_faktura.faktura_pk;

END;
$func$ LANGUAGE 'plpgsql';