﻿/*
Created: 05.10.2014
Modified: 02.07.2017
Model: inter_is
Database: PostgreSQL 9.2
*/


-- Create tables section -------------------------------------------------

-- Table uzivatel

CREATE TABLE "uzivatel"(
 "uzivatel_pk" Serial NOT NULL,
 "email" Text NOT NULL,
 "heslo" Text NOT NULL,
 "stav" Text DEFAULT 'AKTIVNI' NOT NULL
        CONSTRAINT "check_stav" CHECK (stav in ('AKTIVNI', 'NEAKTIVNI')),
 "cas_registrace" Timestamp with time zone NOT NULL,
 "cas_prihlaseni" Timestamp with time zone,
 "jmeno" Text NOT NULL,
 "prijmeni" Text NOT NULL,
 "validacni_klic" Text NOT NULL,
 "role" Text NOT NULL
)
;

-- Add keys for table uzivatel

ALTER TABLE "uzivatel" ADD CONSTRAINT "Key1" PRIMARY KEY ("uzivatel_pk")
;

ALTER TABLE "uzivatel" ADD CONSTRAINT "email" UNIQUE ("email")
;

ALTER TABLE "uzivatel" ADD CONSTRAINT "validacni_klic" UNIQUE ("validacni_klic")
;

-- Table udalost

CREATE TABLE "udalost"(
 "udalost_pk" Serial NOT NULL,
 "typ" Text NOT NULL,
 "nazev" Text NOT NULL,
 "meta" Text NOT NULL,
 "popis" Text,
 "datum_od" Timestamp with time zone NOT NULL,
 "datum_do" Timestamp with time zone NOT NULL,
 "opakovani" Text,
 "barva" Text NOT NULL,
 "upozorneni" Text,
 "uzivatel_pk" Integer NOT NULL
)
;

-- Create indexes for table udalost

CREATE INDEX "ix_zakladatel_udalosti" ON "udalost" ("uzivatel_pk")
;

-- Add keys for table udalost

ALTER TABLE "udalost" ADD CONSTRAINT "Key2" PRIMARY KEY ("udalost_pk")
;

ALTER TABLE "udalost" ADD CONSTRAINT "meta" UNIQUE ("meta")
;

-- Table ucastnik_udalosti

CREATE TABLE "ucastnik_udalosti"(
 "uzivatel_pk" Integer NOT NULL,
 "udalost_pk" Integer NOT NULL
)
;

-- Create indexes for table ucastnik_udalosti

CREATE INDEX "ix_ucastnik_udalosti_uzivatel" ON "ucastnik_udalosti" ("uzivatel_pk")
;

CREATE INDEX "ix_ucastnik_udalosti_udalost" ON "ucastnik_udalosti" ("udalost_pk")
;

-- Add keys for table ucastnik_udalosti

ALTER TABLE "ucastnik_udalosti" ADD CONSTRAINT "Key3" PRIMARY KEY ("uzivatel_pk","udalost_pk")
;

-- Table zakaznik

CREATE TABLE "zakaznik"(
 "zakaznik_pk" Serial NOT NULL,
 "jmeno" Text NOT NULL,
 "prijmeni" Text NOT NULL,
 "email" Text NOT NULL,
 "telefon" Text
)
;

-- Add keys for table zakaznik

ALTER TABLE "zakaznik" ADD CONSTRAINT "Key4" PRIMARY KEY ("zakaznik_pk")
;

-- Table fakturacni_udaje

CREATE TABLE "fakturacni_udaje"(
 "fakturacni_udaje_pk" Serial NOT NULL,
 "typ" Text DEFAULT 'FO' NOT NULL
        CONSTRAINT "check_stav" CHECK (typ in ('FO', 'PO')),
 "ic" Text,
 "dic" Text,
 "obchodni_jmeno" Text,
 "jmeno" Text,
 "prijmeni" Text,
 "ulice" Text NOT NULL,
 "mesto" Text NOT NULL,
 "psc" Integer NOT NULL,
 "zeme_id" Text
)
;

-- Create indexes for table fakturacni_udaje

CREATE INDEX "ix_fakturacni_udaje_zeme_id" ON "fakturacni_udaje" ("zeme_id")
;

-- Add keys for table fakturacni_udaje

ALTER TABLE "fakturacni_udaje" ADD CONSTRAINT "Key5" PRIMARY KEY ("fakturacni_udaje_pk")
;

-- Table mail

CREATE TABLE "mail"(
 "mail_pk" Serial NOT NULL,
 "mail_id" Text,
 "trida" Text NOT NULL,
 "adresat" Text NOT NULL,
 "predmet" Text NOT NULL,
 "data" Text,
 "cas_vlozeni" Timestamp with time zone DEFAULT now() NOT NULL,
 "cas_naplanovani" Timestamp with time zone NOT NULL,
 "cas_odeslani" Timestamp with time zone,
 "stav" Text NOT NULL,
 "chyba" Text
)
;

-- Create indexes for table mail

CREATE INDEX "ix_mail_adresat" ON "mail" ("adresat")
;

CREATE INDEX "ix_mail_cas_vlozeni" ON "mail" ("cas_vlozeni")
;

CREATE INDEX "ix_mail_stav" ON "mail" ("stav")
;

-- Add keys for table mail

ALTER TABLE "mail" ADD CONSTRAINT "pkey_mail_pk" PRIMARY KEY ("mail_pk")
;

-- Table dodaci_udaje

CREATE TABLE "dodaci_udaje"(
 "dodaci_udaje_pk" Serial NOT NULL,
 "firma" Text,
 "jmeno" Text,
 "prijmeni" Text,
 "ulice" Text NOT NULL,
 "mesto" Text NOT NULL,
 "psc" Integer NOT NULL,
 "zeme_id" Text
)
;

-- Create indexes for table dodaci_udaje

CREATE INDEX "ix_dodaci_udaje_zeme_id" ON "dodaci_udaje" ("zeme_id")
;

-- Add keys for table dodaci_udaje

ALTER TABLE "dodaci_udaje" ADD CONSTRAINT "Key6" PRIMARY KEY ("dodaci_udaje_pk")
;

-- Table zakaznik_fakturacni_udaje

CREATE TABLE "zakaznik_fakturacni_udaje"(
 "zakaznik_pk" Integer NOT NULL,
 "fakturacni_udaje_pk" Integer NOT NULL
)
;

-- Create indexes for table zakaznik_fakturacni_udaje

CREATE INDEX "IX_Relationship6" ON "zakaznik_fakturacni_udaje" ("zakaznik_pk")
;

CREATE INDEX "IX_Relationship7" ON "zakaznik_fakturacni_udaje" ("fakturacni_udaje_pk")
;

-- Add keys for table zakaznik_fakturacni_udaje

ALTER TABLE "zakaznik_fakturacni_udaje" ADD CONSTRAINT "Key8" PRIMARY KEY ("zakaznik_pk","fakturacni_udaje_pk")
;

-- Table zakaznik_dodaci_udaje

CREATE TABLE "zakaznik_dodaci_udaje"(
 "zakaznik_pk" Integer NOT NULL,
 "dodaci_udaje_pk" Integer NOT NULL
)
;

-- Create indexes for table zakaznik_dodaci_udaje

CREATE INDEX "IX_Relationship8" ON "zakaznik_dodaci_udaje" ("zakaznik_pk")
;

CREATE INDEX "IX_Relationship9" ON "zakaznik_dodaci_udaje" ("dodaci_udaje_pk")
;

-- Add keys for table zakaznik_dodaci_udaje

ALTER TABLE "zakaznik_dodaci_udaje" ADD CONSTRAINT "Key9" PRIMARY KEY ("dodaci_udaje_pk","zakaznik_pk")
;

-- Table objednavka

CREATE TABLE "objednavka"(
 "objednavka_pk" Serial NOT NULL,
 "datum_zalozeni" Timestamp with time zone NOT NULL,
 "datum_zmeny" Timestamp with time zone,
 "stav" Integer NOT NULL,
 "zakaznik_pk" Integer,
 "cislo" Integer NOT NULL,
 "f_obchodni_jmeno" Text,
 "f_ic" Text,
 "f_dic" Text,
 "f_ulice" Text,
 "f_mesto" Text,
 "f_psc" Integer,
 "d_ulice" Text,
 "d_mesto" Text,
 "d_psc" Integer,
 "termin" Date NOT NULL,
 "vlozil" Integer NOT NULL
)
;
COMMENT ON COLUMN "objednavka"."stav" IS '-2 .. zruseno systemem
-1 .. zruseno uzivatelem
0 .. zbozi v kosiku
1 a vice .. prubeh zpracovani'
;

-- Create indexes for table objednavka

CREATE INDEX "ix_objednavka_zakaznik_pk" ON "objednavka" ("zakaznik_pk")
;

CREATE INDEX "ix_objednavka_vlozil_pk" ON "objednavka" ("vlozil")
;

-- Add keys for table objednavka

ALTER TABLE "objednavka" ADD CONSTRAINT "Key10" PRIMARY KEY ("objednavka_pk")
;

ALTER TABLE "objednavka" ADD CONSTRAINT "cislo" UNIQUE ("cislo")
;

-- Table model3d

CREATE TABLE "model3d"(
 "model_pk" Serial NOT NULL,
 "model_id" Text NOT NULL,
 "nazev" Text NOT NULL,
 "soubor" Text NOT NULL,
 "objem" Numeric NOT NULL,
 "poznamka" Text,
 "typ" Text DEFAULT 'stl' NOT NULL
        CONSTRAINT "check_typ" CHECK (typ in ('stl', 'obj'))
)
;

-- Add keys for table model3d

ALTER TABLE "model3d" ADD CONSTRAINT "Key11" PRIMARY KEY ("model_pk")
;

ALTER TABLE "model3d" ADD CONSTRAINT "model_id" UNIQUE ("model_id")
;

-- Table objednavka_polozka

CREATE TABLE "objednavka_polozka"(
 "objednavka_polozka_pk" Serial NOT NULL,
 "objednavka_pk" Integer NOT NULL,
 "model_pk" Integer NOT NULL,
 "material_pk" Integer NOT NULL,
 "pocet" Smallint NOT NULL,
 "cena_kus" Numeric(20,2),
 "pocet_hotovo" Smallint DEFAULT 0 NOT NULL
        CONSTRAINT "CheckPocetHotovych" CHECK ((("pocet_hotovo" <= pocet) and ("pocet_hotovo" >= 0)))
)
;

-- Create indexes for table objednavka_polozka

CREATE INDEX "ix_objednavka_polozka_objednavka_pk" ON "objednavka_polozka" ("objednavka_pk")
;

CREATE INDEX "ix_objednavka_polozka_model_pk" ON "objednavka_polozka" ("model_pk")
;

CREATE INDEX "ix_objednavka_polozka_material_pk" ON "objednavka_polozka" ("material_pk")
;

-- Add keys for table objednavka_polozka

ALTER TABLE "objednavka_polozka" ADD CONSTRAINT "objednavka_polozka_key" PRIMARY KEY ("objednavka_pk","model_pk","material_pk")
;

-- Table _log

CREATE TABLE "_log"(
 "log_pk" Serial NOT NULL,
 "datum" Timestamp with time zone NOT NULL,
 "zprava" Text NOT NULL,
 "level" Text NOT NULL
)
;

-- Add keys for table _log

ALTER TABLE "_log" ADD CONSTRAINT "Key13" PRIMARY KEY ("log_pk")
;

-- Table material

CREATE TABLE "material"(
 "material_pk" Serial NOT NULL,
 "skladem_celkem" Integer NOT NULL,
 "skladem_blokace" Integer DEFAULT 0 NOT NULL,
 "material_druh_id" Text,
 "material_barva_id" Text,
  CONSTRAINT "skladem_blokace_nenulove" CHECK ((skladem_celkem - skladem_blokace) >= 0)
)
;

-- Create indexes for table material

CREATE INDEX "ix_material_material_druh_id" ON "material" ("material_druh_id")
;

CREATE INDEX "ix_material_material_barva_id" ON "material" ("material_barva_id")
;

-- Add keys for table material

ALTER TABLE "material" ADD CONSTRAINT "Key14" PRIMARY KEY ("material_pk")
;

-- Table material_barva

CREATE TABLE "material_barva"(
 "material_barva_pk" Serial NOT NULL,
 "material_barva_id" Text NOT NULL,
 "barva" Text NOT NULL
)
;

-- Add keys for table material_barva

ALTER TABLE "material_barva" ADD CONSTRAINT "Key15" PRIMARY KEY ("material_barva_pk")
;

ALTER TABLE "material_barva" ADD CONSTRAINT "material_barva_id" UNIQUE ("material_barva_id")
;

-- Table material_cena

CREATE TABLE "material_cena"(
 "material_cena_pk" Serial NOT NULL,
 "platne_od" Timestamp with time zone NOT NULL,
 "platne_do" Timestamp with time zone,
 "cena" Numeric(10,2) NOT NULL,
 "material_pk" Integer
)
;

-- Create indexes for table material_cena

CREATE INDEX "ix_material_cena_material_pk" ON "material_cena" ("material_pk")
;

-- Add keys for table material_cena

ALTER TABLE "material_cena" ADD CONSTRAINT "Key16" PRIMARY KEY ("material_cena_pk")
;

-- Table material_druh

CREATE TABLE "material_druh"(
 "material_druh_pk" Serial NOT NULL,
 "material_druh_id" Text NOT NULL,
 "hustota" Numeric(4,3) NOT NULL,
 "nazev" Text NOT NULL
)
;

-- Add keys for table material_druh

ALTER TABLE "material_druh" ADD CONSTRAINT "Key17" PRIMARY KEY ("material_druh_pk")
;

ALTER TABLE "material_druh" ADD CONSTRAINT "material_druh_id" UNIQUE ("material_druh_id")
;

-- Table faktura

CREATE TABLE "faktura"(
 "faktura_pk" Serial NOT NULL,
 "cislo" Text NOT NULL,
 "datum_zalozeni" Timestamp with time zone NOT NULL,
 "datum_vystaveni" Timestamp with time zone,
 "datum_splatnosti" Timestamp with time zone,
 "datum_zaplaceni" Timestamp with time zone,
 "stav" Integer NOT NULL,
 "objednavka_pk" Integer NOT NULL,
 "vystavil" Integer
)
;

-- Create indexes for table faktura

CREATE INDEX "ix_faktura_objednavka" ON "faktura" ("objednavka_pk")
;

CREATE INDEX "ix_faktura_vystavil" ON "faktura" ("vystavil")
;

CREATE UNIQUE INDEX "ix_cislo_unique" ON "faktura" ("cislo")
WHERE cislo <> 'XXXXX'
;

-- Add keys for table faktura

ALTER TABLE "faktura" ADD CONSTRAINT "Key18" PRIMARY KEY ("faktura_pk")
;

-- Table faktura_polozka

CREATE TABLE "faktura_polozka"(
 "faktura_polozka_pk" Serial NOT NULL,
 "nazev" Text NOT NULL,
 "kusu" Smallint NOT NULL,
 "cena_ks" Numeric(20,2) NOT NULL,
 "cena_celkem" Numeric(20,2) NOT NULL,
 "cena_ks_dph" Numeric(20,2) NOT NULL,
 "cena_celkem_dph" Numeric(20,2) NOT NULL,
 "dph" Numeric(3,2),
 "faktura_pk" Integer NOT NULL
)
;

-- Create indexes for table faktura_polozka

CREATE INDEX "ix_faktura_polozka_faktury" ON "faktura_polozka" ("faktura_pk")
;

-- Add keys for table faktura_polozka

ALTER TABLE "faktura_polozka" ADD CONSTRAINT "Key19" PRIMARY KEY ("faktura_polozka_pk")
;

-- Table faktura_fakturacni_udaje

CREATE TABLE "faktura_fakturacni_udaje"(
 "faktura_pk" Integer NOT NULL,
 "fakturacni_udaje_pk" Integer NOT NULL
)
;

-- Create indexes for table faktura_fakturacni_udaje

CREATE INDEX "ix_faktura_udaje_faktura_udaje" ON "faktura_fakturacni_udaje" ("faktura_pk")
;

CREATE INDEX "ix_faktura_faktura_udaje" ON "faktura_fakturacni_udaje" ("fakturacni_udaje_pk")
;

-- Add keys for table faktura_fakturacni_udaje

ALTER TABLE "faktura_fakturacni_udaje" ADD CONSTRAINT "Key20" PRIMARY KEY ("faktura_pk","fakturacni_udaje_pk")
;

-- Table system_fakturacni_udaje

CREATE TABLE "system_fakturacni_udaje"(
 "fakturacni_udaje_pk" Integer NOT NULL,
 "platne_od" Timestamp with time zone NOT NULL,
 "platne_do" Timestamp with time zone
)
;

-- Create indexes for table system_fakturacni_udaje

CREATE INDEX "ix_system_fakturacni_udaje" ON "system_fakturacni_udaje" ("fakturacni_udaje_pk")
;

-- Add keys for table system_fakturacni_udaje

ALTER TABLE "system_fakturacni_udaje" ADD CONSTRAINT "Key21" PRIMARY KEY ("fakturacni_udaje_pk")
;

-- Table zeme

CREATE TABLE "zeme"(
 "zeme_pk" Serial NOT NULL,
 "zeme_id" Text NOT NULL,
 "nazev" Text NOT NULL,
 "poradi" Smallint
)
;

-- Add keys for table zeme

ALTER TABLE "zeme" ADD CONSTRAINT "pkey_zeme_pk" PRIMARY KEY ("zeme_pk")
;

ALTER TABLE "zeme" ADD CONSTRAINT "uniq_zeme_id" UNIQUE ("zeme_id")
;

-- Table objednavka_dodaci_udaje

CREATE TABLE "objednavka_dodaci_udaje"(
 "objednavka_pk" Integer NOT NULL,
 "dodaci_udaje_pk" Integer NOT NULL
)
;

-- Create indexes for table objednavka_dodaci_udaje

CREATE INDEX "ix_objednavka_dodaci_udaje_objednavka_pk" ON "objednavka_dodaci_udaje" ("objednavka_pk")
;

CREATE INDEX "ix_objednavka_dodaci_udaje_dodaci_udaje_pk" ON "objednavka_dodaci_udaje" ("dodaci_udaje_pk")
;

-- Add keys for table objednavka_dodaci_udaje

ALTER TABLE "objednavka_dodaci_udaje" ADD CONSTRAINT "pkey_objednavka_dodaci_udaje" PRIMARY KEY ("objednavka_pk","dodaci_udaje_pk")
;

-- Table objednavka_fakturacni_udaje

CREATE TABLE "objednavka_fakturacni_udaje"(
 "objednavka_pk" Integer NOT NULL,
 "fakturacni_udaje_pk" Integer NOT NULL
)
;

-- Create indexes for table objednavka_fakturacni_udaje

CREATE INDEX "ix_objednavka_fakturacni_udaje_objednavka_pk" ON "objednavka_fakturacni_udaje" ("objednavka_pk")
;

CREATE INDEX "ix_objednavka_fakturacni_udaje_fakturacni_udaje_pk" ON "objednavka_fakturacni_udaje" ("fakturacni_udaje_pk")
;

-- Add keys for table objednavka_fakturacni_udaje

ALTER TABLE "objednavka_fakturacni_udaje" ADD CONSTRAINT "pkey_objednavka_fakturacni_udaje" PRIMARY KEY ("objednavka_pk","fakturacni_udaje_pk")
;

-- Create views section -------------------------------------------------

/******************************************************************************
   NAME: material_view      
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        18. 2. 2015    Lukáš       1. Created this view.

   NOTES:

******************************************************************************/

CREATE OR REPLACE VIEW material_view
AS select
    m.material_pk,
    m.skladem_celkem,
    m.skladem_blokace,
    md.nazev,
    md.nazev || ' - ' || mb.barva as nazev_dlouhy,
    md.hustota,
    mb.barva,
    coalesce(mc.cena, 0.00) as cena,
    mc.platne_od,
    mc.platne_do,
    mb.material_barva_id as barva_id,
    md.material_druh_id as druh_id
  from material m
    join material_barva mb on m.material_barva_id = mb.material_barva_id
    join material_druh md on m.material_druh_id = md.material_druh_id
    left join material_cena mc on m.material_pk = mc.material_pk
  where mc.platne_od <= now()
    and (mc.platne_do > now() or mc.platne_do is null)
;

/******************************************************************************
   NAME: material_skladem_view      
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        18. 2. 2015    Lukáš       1. Created this view.

   NOTES:

******************************************************************************/

CREATE OR REPLACE VIEW material_skladem_view
AS select
    m.material_pk,
    m.skladem_celkem,
    m.skladem_blokace,
    md.nazev,
    md.nazev || ' - ' || mb.barva as nazev_dlouhy,
    md.hustota,
    mb.barva,
    mc.cena,
    mc.platne_od,
    mc.platne_do,
    mb.material_barva_id as barva_id,
    md.material_druh_id as druh_id
  from material m
    join material_barva mb on m.material_barva_id = mb.material_barva_id
    join material_druh md on m.material_druh_id = md.material_druh_id
    join material_cena mc on m.material_pk = mc.material_pk
  where (m.skladem_celkem - coalesce(m.skladem_blokace, 0)) > 0
    and mc.platne_od <= now()
    and (mc.platne_do > now() or mc.platne_do is null) 
;

CREATE VIEW adresa AS
    SELECT jmeno, prijmeni, ulice, mesto, psc FROM (
        SELECT z.jmeno, z.prijmeni, fu.ulice, fu.mesto, fu.psc FROM fakturacni_udaje fu
            JOIN zakaznik_fakturacni_udaje zfu ON fu.fakturacni_udaje_pk = zfu.fakturacni_udaje_pk
            JOIN zakaznik z ON zfu.zakaznik_pk = z.zakaznik_pk

        UNION ALL

        SELECT z.jmeno, z.prijmeni, du.ulice, du.mesto, du.psc FROM dodaci_udaje du
            JOIN zakaznik_dodaci_udaje zdu ON du.dodaci_udaje_pk = zdu.dodaci_udaje_pk
            JOIN zakaznik z ON zdu.zakaznik_pk = z.zakaznik_pk
    ) AS t
    GROUP BY 1, 2, 3, 4, 5;
;

-- Create relationships section ------------------------------------------------- 

ALTER TABLE "udalost" ADD CONSTRAINT "zakladatel_udalosti" FOREIGN KEY ("uzivatel_pk") REFERENCES "uzivatel" ("uzivatel_pk") ON DELETE RESTRICT ON UPDATE RESTRICT
;

ALTER TABLE "ucastnik_udalosti" ADD CONSTRAINT "Relationship2" FOREIGN KEY ("uzivatel_pk") REFERENCES "uzivatel" ("uzivatel_pk") ON DELETE RESTRICT ON UPDATE RESTRICT
;

ALTER TABLE "ucastnik_udalosti" ADD CONSTRAINT "Relationship3" FOREIGN KEY ("udalost_pk") REFERENCES "udalost" ("udalost_pk") ON DELETE RESTRICT ON UPDATE RESTRICT
;

ALTER TABLE "zakaznik_fakturacni_udaje" ADD CONSTRAINT "Relationship6" FOREIGN KEY ("zakaznik_pk") REFERENCES "zakaznik" ("zakaznik_pk") ON DELETE RESTRICT ON UPDATE RESTRICT
;

ALTER TABLE "zakaznik_fakturacni_udaje" ADD CONSTRAINT "Relationship7" FOREIGN KEY ("fakturacni_udaje_pk") REFERENCES "fakturacni_udaje" ("fakturacni_udaje_pk") ON DELETE RESTRICT ON UPDATE RESTRICT
;

ALTER TABLE "zakaznik_dodaci_udaje" ADD CONSTRAINT "Relationship8" FOREIGN KEY ("zakaznik_pk") REFERENCES "zakaznik" ("zakaznik_pk") ON DELETE RESTRICT ON UPDATE RESTRICT
;

ALTER TABLE "zakaznik_dodaci_udaje" ADD CONSTRAINT "Relationship9" FOREIGN KEY ("dodaci_udaje_pk") REFERENCES "dodaci_udaje" ("dodaci_udaje_pk") ON DELETE RESTRICT ON UPDATE RESTRICT
;

ALTER TABLE "objednavka" ADD CONSTRAINT "objednavka_zakaznika" FOREIGN KEY ("zakaznik_pk") REFERENCES "zakaznik" ("zakaznik_pk") ON DELETE RESTRICT ON UPDATE RESTRICT
;

ALTER TABLE "objednavka_polozka" ADD CONSTRAINT "objednavka_polozka_objednavky" FOREIGN KEY ("objednavka_pk") REFERENCES "objednavka" ("objednavka_pk") ON DELETE RESTRICT ON UPDATE RESTRICT
;

ALTER TABLE "objednavka_polozka" ADD CONSTRAINT "model3d_polozka_objednavky" FOREIGN KEY ("model_pk") REFERENCES "model3d" ("model_pk") ON DELETE RESTRICT ON UPDATE RESTRICT
;

ALTER TABLE "material" ADD CONSTRAINT "material_material_druh" FOREIGN KEY ("material_druh_id") REFERENCES "material_druh" ("material_druh_id") ON DELETE RESTRICT ON UPDATE RESTRICT
;

ALTER TABLE "material" ADD CONSTRAINT "material_material_barva" FOREIGN KEY ("material_barva_id") REFERENCES "material_barva" ("material_barva_id") ON DELETE RESTRICT ON UPDATE RESTRICT
;

ALTER TABLE "material_cena" ADD CONSTRAINT "material_material_cena" FOREIGN KEY ("material_pk") REFERENCES "material" ("material_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "objednavka_polozka" ADD CONSTRAINT "objednavka_polozka_material" FOREIGN KEY ("material_pk") REFERENCES "material" ("material_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "objednavka" ADD CONSTRAINT "objednavku_vlozil" FOREIGN KEY ("vlozil") REFERENCES "uzivatel" ("uzivatel_pk") ON DELETE RESTRICT ON UPDATE RESTRICT
;

ALTER TABLE "faktura_polozka" ADD CONSTRAINT "faktura_polozka_faktury" FOREIGN KEY ("faktura_pk") REFERENCES "faktura" ("faktura_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "faktura" ADD CONSTRAINT "faktura_objednavka" FOREIGN KEY ("objednavka_pk") REFERENCES "objednavka" ("objednavka_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "faktura" ADD CONSTRAINT "faktura_vystavil" FOREIGN KEY ("vystavil") REFERENCES "uzivatel" ("uzivatel_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "faktura_fakturacni_udaje" ADD CONSTRAINT "faktura_fakturacni_udaje" FOREIGN KEY ("faktura_pk") REFERENCES "faktura" ("faktura_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "faktura_fakturacni_udaje" ADD CONSTRAINT "faktura_fakturacni_udaje_2" FOREIGN KEY ("fakturacni_udaje_pk") REFERENCES "fakturacni_udaje" ("fakturacni_udaje_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "system_fakturacni_udaje" ADD CONSTRAINT "Relationship10" FOREIGN KEY ("fakturacni_udaje_pk") REFERENCES "fakturacni_udaje" ("fakturacni_udaje_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "dodaci_udaje" ADD CONSTRAINT "dodaci_udaje_zeme" FOREIGN KEY ("zeme_id") REFERENCES "zeme" ("zeme_id") ON DELETE RESTRICT ON UPDATE RESTRICT
;

ALTER TABLE "fakturacni_udaje" ADD CONSTRAINT "fakturacni_udaje_zeme" FOREIGN KEY ("zeme_id") REFERENCES "zeme" ("zeme_id") ON DELETE RESTRICT ON UPDATE RESTRICT
;

ALTER TABLE "objednavka_dodaci_udaje" ADD CONSTRAINT "Relationship13" FOREIGN KEY ("objednavka_pk") REFERENCES "objednavka" ("objednavka_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "objednavka_fakturacni_udaje" ADD CONSTRAINT "Relationship14" FOREIGN KEY ("objednavka_pk") REFERENCES "objednavka" ("objednavka_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "objednavka_dodaci_udaje" ADD CONSTRAINT "objednavka_dodaci_udaje" FOREIGN KEY ("dodaci_udaje_pk") REFERENCES "dodaci_udaje" ("dodaci_udaje_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "objednavka_fakturacni_udaje" ADD CONSTRAINT "objednavka_fakturacni_udaje" FOREIGN KEY ("fakturacni_udaje_pk") REFERENCES "fakturacni_udaje" ("fakturacni_udaje_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;





