
-- uzivatele
insert into uzivatel (email, heslo, role, stav, validacni_klic, cas_registrace, cas_prihlaseni, jmeno, prijmeni) values
    ('superadmin', '$2y$13$neBcuaWa1QhboDs5qEglCu39tUtAUIWaUF4iYnDmG8KxLLQWxKQ92', 'superadmin', 'AKTIVNI', md5('superadmin'), now(), null, 'super', 'admin')
  , ('test@senovo.cz', '$2y$13$neBcuaWa1QhboDs5qEglCu39tUtAUIWaUF4iYnDmG8KxLLQWxKQ92', 'admin', 'AKTIVNI', md5('test@senovo.cz'), now(), null, 'test', 'test')
  , ('vojtech.svab@voplasto.cz', '$2y$13$neBcuaWa1QhboDs5qEglCu39tUtAUIWaUF4iYnDmG8KxLLQWxKQ92', 'user', 'AKTIVNI', md5('vojtech.svab@voplasto.cz'), now(), null, 'Vojtěch', 'Šváb')
;

-- materialy - pripravim si tmp tabulku
drop TABLE if EXISTS tmp_material;
create temp table tmp_material (
  nazev text,
  barva text,
  cena numeric,
  hustota numeric
);

insert into tmp_material (nazev, barva, cena, hustota) values
  ('ABS', 'hnědá', 6, 1.030),
  ('ABS', 'tmavě modrá', 6, 1.030),
  ('ABS', 'světle modrá', 6, 1.030),
  ('ABS', 'červená', 6, 1.030),
  ('ABS', 'žlutá', 6, 1.030),
  ('ABS', 'černá', 6, 1.030),
  ('ABS', 'bílá', 6, 1.030),
  ('ABS', 'bílá natural', 6, 1.030),
  ('ABS', 'zelená ', 6, 1.030),
  ('ABS', 'šedá', 6, 1.030),
  ('ABS', 'transparent', 6, 1.030),
  ('PLA', 'bílá', 6, 1.250),
  ('PLA', 'zelená ', 6, 1.250),
  ('Dřevo', 'světlé', 7, 1.250),
  ('Dřevo', 'tmavé', 7, 1.250),
  ('Guma', 'černá', 7.5, 0.947),
  ('Guma', 'červená', 7.5, 0.947),
  ('Guma', 'modrá', 7.5, 0.947)
;

create EXTENSION IF NOT EXISTS unaccent;

---- a naladuju to tam procedurou, protoze to je vic tabulek
do $func$
declare
  l_row record;
  l_row2 record;
  l_pk integer;
begin
  for l_row in select nazev, hustota from tmp_material group by nazev, hustota loop
    l_pk := nextval('material_druh_material_druh_pk_seq'::regclass);
    insert into material_druh (
        material_druh_pk
      , material_druh_id
      , hustota
      , nazev
    ) values (
        l_pk
      , unaccent(lower(l_row.nazev))
      , l_row.hustota
      , l_row.nazev
    );
  end loop;

  for l_row in select barva from tmp_material group by barva LOOP
    insert into material_barva (
        material_barva_id
      , barva
    ) values (
        unaccent(lower(l_row.barva))
      , l_row.barva
    );
  end loop;

  for l_row in
    select
        d.*
      , b.*
      , t.cena
    from tmp_material t
      join material_druh d on t.nazev = d.nazev
      join material_barva b on b.barva = t.barva
  loop
      l_pk := nextval('material_material_pk_seq'::regclass);
    insert into material (
        material_pk
      , skladem_celkem
      , skladem_blokace
      , material_druh_id
      , material_barva_id
    ) values (
        l_pk
      , 5000
      , 0
      , l_row.material_druh_id
      , l_row.material_barva_id
    );

    insert into material_cena (
        material_pk
      , platne_od
      , platne_do
      , cena
    ) values (
        l_pk
      , now()
      , null
      , l_row.cena
    );
  end loop;
end;
$func$;