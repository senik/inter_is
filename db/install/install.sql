begin;

-- db schema
\i ./create/create.sql

-- naliju zakladni data (uzivatele...)
\i ./data/init-data.sql

-- db procedury
\i ./procs/f_objednavka.sql
\i ./procs/f_udalost.sql
\i ./procs/f_utils.sql
\i ./procs/f_zakaznik.sql
\i ./procs/f_sklad.sql
\i ./procs/f_faktury.sql