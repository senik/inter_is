BEGIN;

ALTER TABLE faktura ALTER COLUMN stav TYPE INTEGER USING stav::INTEGER;

\i ../procs/f_sklad.sql
\i ../procs/f_faktury.sql