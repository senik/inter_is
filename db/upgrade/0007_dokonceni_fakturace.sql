BEGIN;

ALTER TABLE faktura ALTER COLUMN objednavka_pk DROP NOT NULL;
ALTER TABLE objednavka_polozka ADD COLUMN objednavka_polozka_pk SERIAL;
ALTER TABLE faktura_polozka DROP COLUMN dph;
ALTER TABLE faktura_polozka ADD COLUMN dph NUMERIC(3,2);

CREATE TABLE faktura_fakturacni_udaje (
  faktura_pk integer not NULL,
  fakturacni_udaje_pk integer NOT NULL,
  PRIMARY KEY (faktura_pk, fakturacni_udaje_pk)
) WITHOUT OIDS ;

ALTER TABLE faktura_fakturacni_udaje ADD FOREIGN KEY (faktura_pk) REFERENCES faktura (faktura_pk) on UPDATE restrict on delete restrict;
ALTER TABLE faktura_fakturacni_udaje ADD FOREIGN KEY (fakturacni_udaje_pk) REFERENCES fakturacni_udaje (fakturacni_udaje_pk) on UPDATE restrict on delete restrict;

DROP FUNCTION IF EXISTS faktura_spoj_polozky ( integer, text[] );

-- naleju nove verze procedur
\i ../procs/f_objednavka.sql
\i ../procs/f_faktury.sql