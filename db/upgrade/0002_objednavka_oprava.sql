-- uprava procedur objednavky
-- uprava tabilky uzivatel - pro alespon nejake (staticke) omezeni pristupu

begin;

\i ../procs/f_objednavka.sql

alter table uzivatel add column role text;

update uzivatel set role = 'user' where email = 'test@senovo.cz';
update uzivatel set role = 'admin' where email = 'vojtech.svab@voplasto.cz';
update uzivatel set role = 'superadmin' where email = 'superadmin';

alter table uzivatel alter column role set not null;