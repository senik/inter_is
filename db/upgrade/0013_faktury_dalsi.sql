SET CHECK_FUNCTION_BODIES TO TRUE;

BEGIN;

ALTER TABLE faktura ADD COLUMN datum_zaplaceni TIMESTAMPTZ;

\i ../procs/f_faktury.sql