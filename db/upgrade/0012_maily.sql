BEGIN;

-- Table mail

CREATE TABLE "mail"(
    "mail_pk" Serial NOT NULL,
    "mail_id" Text,
    "trida" Text NOT NULL,
    "adresat" Text NOT NULL,
    "predmet" Text NOT NULL,
    "data" Text,
    "cas_vlozeni" Timestamp with time zone DEFAULT now() NOT NULL,
    "cas_naplanovani" Timestamp with time zone NOT NULL,
    "cas_odeslani" Timestamp with time zone,
    "stav" Text NOT NULL,
    "chyba" Text
)
;

-- Create indexes for table mail

CREATE INDEX "ix_mail_adresat" ON "mail" ("adresat")
;

CREATE INDEX "ix_mail_cas_vlozeni" ON "mail" ("cas_vlozeni")
;

CREATE INDEX "ix_mail_stav" ON "mail" ("stav")
;

-- Add keys for table mail

ALTER TABLE "mail" ADD CONSTRAINT "pkey_mail_pk" PRIMARY KEY ("mail_pk")
;