begin;

alter table objednavka add COLUMN termin date;
update objednavka set termin = '31.5.2015'::date;
alter table objednavka alter COLUMN termin set not null;

alter table objednavka_polozka add COLUMN pocet_hotovo smallint DEFAULT 0 not null;
alter table objednavka_polozka add CONSTRAINT "CheckPocetHotovych" CHECK ((("pocet_hotovo" <= pocet) and ("pocet_hotovo" >= 0)));

-- posunu stav (protoze 4 = HOTOVO)
update objednavka set stav = 5 where stav = 4;

alter table objednavka add COLUMN vlozil INT;
update objednavka set vlozil = (select uzivatel_pk from uzivatel where email = 'superadmin');
alter table objednavka alter COLUMN vlozil set NOT NULL;

drop function if EXISTS objednavka_zaloz ( integer, integer, integer, integer, text[]);
drop function if EXISTS objednavka_zaloz ( integer, integer, integer, integer, integer, text[]);
drop function if EXISTS objednavka_zaloz ( integer, integer, integer, text, integer, integer, text[]);

DROP view if EXISTS material_view;
CREATE OR REPLACE VIEW material_view
AS select
     m.material_pk,
     m.skladem_celkem,
     m.skladem_blokace,
     md.nazev,
     md.nazev || ' - ' || mb.barva as nazev_dlouhy,
     md.hustota,
     mb.barva,
     coalesce(mc.cena, 0.00) as cena,
     mc.platne_od,
     mc.platne_do,
     mb.material_barva_id as barva_id,
     md.material_druh_id as druh_id
   from material m
     join material_barva mb on m.material_barva_id = mb.material_barva_id
     join material_druh md on m.material_druh_id = md.material_druh_id
     left join material_cena mc on m.material_pk = mc.material_pk
   where mc.platne_od <= now()
         and (mc.platne_do > now() or mc.platne_do is null)
;

CREATE TABLE "faktura"(
  "faktura_pk" Serial NOT NULL,
  "cislo" Text NOT NULL,
  "datum_zalozeni" Timestamp with time zone NOT NULL,
  "datum_vystaveni" Timestamp with time zone,
  "datum_splatnosti" Timestamp with time zone,
  "stav" Text NOT NULL,
  "objednavka_pk" Integer NOT NULL,
  "vystavil" Integer
) WITH (OIDS=FALSE);

CREATE INDEX "ix_faktura_objednavka" ON "faktura" ("objednavka_pk");
ALTER TABLE "faktura" ADD CONSTRAINT "Key18" PRIMARY KEY ("faktura_pk");
ALTER TABLE "faktura" ADD CONSTRAINT "cislo1" UNIQUE ("cislo");

CREATE TABLE "faktura_polozka"(
  "faktura_polozka_pk" Serial NOT NULL,
  "nazev" Text NOT NULL,
  "kusu" Smallint NOT NULL,
  "cena_ks" Numeric(20,2) NOT NULL,
  "cena_celkem" Numeric(20,2) NOT NULL,
  "cena_ks_dph" Numeric(20,2) NOT NULL,
  "cena_celkem_dph" Numeric(20,2) NOT NULL,
  "dph" smallint null,
  "faktura_pk" Integer NOT NULL
) WITH (OIDS=FALSE);

CREATE INDEX "ix_faktura_polozka_faktury" ON "faktura_polozka" ("faktura_pk");
ALTER TABLE "faktura_polozka" ADD CONSTRAINT "Key19" PRIMARY KEY ("faktura_polozka_pk");
ALTER TABLE "faktura_polozka" ADD CONSTRAINT "faktura_polozka_faktury" FOREIGN KEY ("faktura_pk") REFERENCES "faktura" ("faktura_pk") ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE "faktura" ADD CONSTRAINT "faktura_objednavka" FOREIGN KEY ("objednavka_pk") REFERENCES "objednavka" ("objednavka_pk") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "faktura" ADD CONSTRAINT "faktura_vystavil" FOREIGN KEY ("vystavil") REFERENCES "uzivatel" ("uzivatel_pk") ON DELETE NO ACTION ON UPDATE NO ACTION;

\i ../procs/f_objednavka.sql
\i ../procs/f_sklad.sql
\i ../procs/f_faktury.sql