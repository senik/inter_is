BEGIN;

-- Table system_fakturacni_udaje

CREATE TABLE "system_fakturacni_udaje"(
    "fakturacni_udaje_pk" Integer NOT NULL,
    "platne_od" Timestamp with time zone NOT NULL,
    "platne_do" Timestamp with time zone
)
;

-- Create indexes for table system_fakturacni_udaje

CREATE INDEX "ix_system_fakturacni_udaje" ON "system_fakturacni_udaje" ("fakturacni_udaje_pk")
;

-- Add keys for table system_fakturacni_udaje

ALTER TABLE "system_fakturacni_udaje"
    ADD CONSTRAINT "Key21"
    PRIMARY KEY ("fakturacni_udaje_pk")
;

ALTER TABLE "system_fakturacni_udaje"
    ADD CONSTRAINT "Relationship10"
    FOREIGN KEY ("fakturacni_udaje_pk")
    REFERENCES "fakturacni_udaje" ("fakturacni_udaje_pk")
    ON DELETE NO ACTION ON UPDATE NO ACTION
;


-- Table zeme

CREATE TABLE "zeme"(
    "zeme_pk" Serial NOT NULL,
    "zeme_id" Text NOT NULL,
    "nazev" Text NOT NULL,
    "poradi" Smallint
)
;

-- Add keys for table zeme

ALTER TABLE "zeme" ADD CONSTRAINT "pkey_zeme_pk" PRIMARY KEY ("zeme_pk")
;

ALTER TABLE "zeme" ADD CONSTRAINT "uniq_zeme_id" UNIQUE ("zeme_id")
;


ALTER TABLE fakturacni_udaje ADD COLUMN jmeno TEXT;
ALTER TABLE fakturacni_udaje ADD COLUMN prijmeni TEXT;
ALTER TABLE fakturacni_udaje ADD COLUMN ulice TEXT;
ALTER TABLE fakturacni_udaje ADD COLUMN mesto TEXT;
ALTER TABLE fakturacni_udaje ADD COLUMN psc TEXT;
ALTER TABLE fakturacni_udaje ADD COLUMN zeme_id TEXT;

ALTER TABLE dodaci_udaje ADD COLUMN firma TEXT;
ALTER TABLE dodaci_udaje ADD COLUMN jmeno TEXT;
ALTER TABLE dodaci_udaje ADD COLUMN prijmeni TEXT;
ALTER TABLE dodaci_udaje ADD COLUMN ulice TEXT;
ALTER TABLE dodaci_udaje ADD COLUMN mesto TEXT;
ALTER TABLE dodaci_udaje ADD COLUMN psc TEXT;
ALTER TABLE dodaci_udaje ADD COLUMN zeme_id TEXT;


CREATE TABLE "objednavka_dodaci_udaje"(
    "objednavka_pk" Integer NOT NULL,
    "dodaci_udaje_pk" Integer NOT NULL
);


CREATE TABLE "objednavka_fakturacni_udaje"(
    "objednavka_pk" Integer NOT NULL,
    "fakturacni_udaje_pk" Integer NOT NULL
);


ALTER TABLE "objednavka_fakturacni_udaje" ADD CONSTRAINT "pkey_objednavka_fakturacni_udaje" PRIMARY KEY ("objednavka_pk","fakturacni_udaje_pk");
ALTER TABLE "dodaci_udaje" ADD CONSTRAINT "dodaci_udaje_zeme" FOREIGN KEY ("zeme_id") REFERENCES "zeme" ("zeme_id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "fakturacni_udaje" ADD CONSTRAINT "fakturacni_udaje_zeme" FOREIGN KEY ("zeme_id") REFERENCES "zeme" ("zeme_id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "objednavka_dodaci_udaje" ADD CONSTRAINT "pkey_objednavka_dodaci_udaje" PRIMARY KEY ("objednavka_pk","dodaci_udaje_pk");

CREATE INDEX "ix_dodaci_udaje_zeme_id" ON "dodaci_udaje" ("zeme_id");
CREATE INDEX "ix_fakturacni_udaje_zeme_id" ON "fakturacni_udaje" ("zeme_id");
CREATE INDEX "ix_objednavka_dodaci_udaje_objednavka_pk" ON "objednavka_dodaci_udaje" ("objednavka_pk");
CREATE INDEX "ix_objednavka_dodaci_udaje_dodaci_udaje_pk" ON "objednavka_dodaci_udaje" ("dodaci_udaje_pk");
CREATE INDEX "ix_objednavka_fakturacni_udaje_objednavka_pk" ON "objednavka_fakturacni_udaje" ("objednavka_pk");
CREATE INDEX "ix_objednavka_fakturacni_udaje_fakturacni_udaje_pk" ON "objednavka_fakturacni_udaje" ("fakturacni_udaje_pk");


INSERT INTO zeme (zeme_id, nazev, poradi) VALUES
    ('CZ', 'Česká republika', 1),
    ('SK', 'Slovenská republika', 2)
;


-- nahraju nove verze procedur
\i ../procs/f_objednavka.sql
\i ../procs/f_udaje.sql
\i ../procs/f_faktury.sql
\i ../procs/f_zakaznik.sql
\i ../procs/f_utils.sql


-- nahraju systemove fakturacni udaje
DO $$
DECLARE
    l_row RECORD;

    li_adresa_pk adresa.adresa_pk%TYPE;
    li_udaje_pk fakturacni_udaje.fakturacni_udaje_pk%TYPE;
    ld_predel DATE := '2016-07-01';
BEGIN

    -- nejprve updatovat fakturacni udaje vsech zakazniku
    FOR l_row IN SELECT zfu.*, z.* FROM fakturacni_udaje fu
        JOIN zakaznik_fakturacni_udaje zfu ON fu.fakturacni_udaje_pk = zfu.fakturacni_udaje_pk
        JOIN zakaznik z ON zfu.zakaznik_pk = z.zakaznik_pk
    LOOP

        RAISE NOTICE 'pro udaje s pk = % nastavuji jmeno = % a prijmeni = %', l_row.fakturacni_udaje_pk, l_row.jmeno, l_row.prijmeni;

        UPDATE fakturacni_udaje SET
              jmeno     = l_row.jmeno
            , prijmeni  = l_row.prijmeni
        WHERE fakturacni_udaje_pk = l_row.fakturacni_udaje_pk;

    END LOOP;

    -- a zalozim si systemove
    -- nejprve ty stare
    INSERT INTO adresa (ulice, mesto, psc) VALUES
        ('Na Kopečku 891', 'Smržovka', 46851)
    RETURNING adresa_pk INTO li_adresa_pk;


    INSERT INTO fakturacni_udaje (typ, ic, dic, obchodni_jmeno, jmeno, prijmeni, adresa_pk) VALUES
        ('PO', '03464539', 'CZ8609085210', 'VoPlasTo', 'Vojtěch', 'Šváb', li_adresa_pk)
    RETURNING fakturacni_udaje_pk INTO li_udaje_pk;


    INSERT INTO system_fakturacni_udaje (fakturacni_udaje_pk, platne_od, platne_do) VALUES
        (li_udaje_pk, '2001-01-01'::TIMESTAMPTZ, ld_predel::TIMESTAMPTZ);

    -- a pak ty nove
    INSERT INTO adresa (ulice, mesto, psc) VALUES
        ('Sportovní 1295', 'Smržovka', 46851)
    RETURNING adresa_pk INTO li_adresa_pk;


    INSERT INTO fakturacni_udaje (typ, ic, dic, obchodni_jmeno, jmeno, prijmeni, adresa_pk) VALUES
        ('PO', '03464539', 'CZ8609085210', 'VoPlasTo', 'Vojtěch', 'Šváb', li_adresa_pk)
    RETURNING fakturacni_udaje_pk INTO li_udaje_pk;


    INSERT INTO system_fakturacni_udaje (fakturacni_udaje_pk, platne_od, platne_do) VALUES
        (li_udaje_pk, ld_predel::TIMESTAMPTZ, NULL);

END;
$$;

SELECT DISTINCT z.*, du.*, a.* FROM zakaznik_dodaci_udaje zdu
    JOIN dodaci_udaje du ON zdu.dodaci_udaje_pk = du.dodaci_udaje_pk
    JOIN adresa a ON du.adresa_pk = a.adresa_pk
    JOIN zakaznik z ON zdu.zakaznik_pk = z.zakaznik_pk;


-- preliju vsechny data
DO $$
DECLARE
    lr_dodaci_udaje RECORD;
    lr_fakturacni_udaje RECORD;
    lr_objednavka RECORD;
    lr_faktura RECORD;
    lr_system RECORD;

    l_pkey INTEGER;
BEGIN

    -- zakaznik
    FOR lr_dodaci_udaje IN SELECT DISTINCT z.*, du.dodaci_udaje_pk, a.* FROM zakaznik_dodaci_udaje zdu
        JOIN dodaci_udaje du ON zdu.dodaci_udaje_pk = du.dodaci_udaje_pk
        JOIN adresa a ON du.adresa_pk = a.adresa_pk
        JOIN zakaznik z ON zdu.zakaznik_pk = z.zakaznik_pk
    LOOP
        UPDATE dodaci_udaje SET
              firma = NULL
            , jmeno = lr_dodaci_udaje.jmeno
            , prijmeni = lr_dodaci_udaje.prijmeni
            , ulice = lr_dodaci_udaje.ulice
            , mesto = lr_dodaci_udaje.mesto
            , psc = lr_dodaci_udaje.psc
            , zeme_id = 'CZ'
        WHERE dodaci_udaje_pk = lr_dodaci_udaje.dodaci_udaje_pk;
    END LOOP;

    FOR lr_fakturacni_udaje IN SELECT DISTINCT z.*, fu.fakturacni_udaje_pk, a.* FROM zakaznik_fakturacni_udaje zfu
        JOIN fakturacni_udaje fu ON zfu.fakturacni_udaje_pk = fu.fakturacni_udaje_pk
        JOIN adresa a ON fu.adresa_pk = a.adresa_pk
        JOIN zakaznik z ON zfu.zakaznik_pk = z.zakaznik_pk
    LOOP
        UPDATE fakturacni_udaje SET
              ulice = lr_fakturacni_udaje.ulice
            , mesto = lr_fakturacni_udaje.mesto
            , psc = lr_fakturacni_udaje.psc
            , zeme_id = 'CZ'
        WHERE fakturacni_udaje_pk = lr_fakturacni_udaje.fakturacni_udaje_pk;
    END LOOP;

    -- objednavka
    FOR lr_objednavka IN SELECT DISTINCT * FROM objednavka o
        JOIN zakaznik z ON o.zakaznik_pk = z.zakaznik_pk
    LOOP
        INSERT INTO dodaci_udaje (firma, jmeno, prijmeni, ulice, mesto, psc, zeme_id)
            VALUES (NULL, lr_objednavka.jmeno, lr_objednavka.prijmeni, lr_objednavka.d_ulice, lr_objednavka.d_mesto, lr_objednavka.d_psc, 'CZ')
        RETURNING dodaci_udaje_pk INTO l_pkey;

        INSERT INTO objednavka_dodaci_udaje (objednavka_pk, dodaci_udaje_pk)
            VALUES (lr_objednavka.objednavka_pk, l_pkey);

        INSERT INTO fakturacni_udaje (typ, obchodni_jmeno, ic, dic, jmeno, prijmeni, ulice, mesto, psc, zeme_id)
            VALUES (CASE WHEN lr_objednavka.f_ic IS NOT NULL AND lr_objednavka.f_ic <> '' THEN 'PO' ELSE 'FO' END,
                lr_objednavka.f_obchodni_jmeno, lr_objednavka.f_ic, lr_objednavka.f_dic, lr_objednavka.jmeno, lr_objednavka.prijmeni, lr_objednavka.f_ulice, lr_objednavka.f_mesto, lr_objednavka.f_psc, 'CZ')
        RETURNING fakturacni_udaje_pk INTO l_pkey;

        INSERT INTO objednavka_fakturacni_udaje (objednavka_pk, fakturacni_udaje_pk)
            VALUES (lr_objednavka.objednavka_pk, l_pkey);
    END LOOP;

    -- faktura
    FOR lr_faktura IN SELECT DISTINCT f.faktura_pk, fu.fakturacni_udaje_pk, fu.ic, a.*, z.* FROM faktura f
        JOIN faktura_fakturacni_udaje ffu ON f.faktura_pk = ffu.faktura_pk
        JOIN fakturacni_udaje fu ON ffu.fakturacni_udaje_pk = fu.fakturacni_udaje_pk
        JOIN objednavka o ON f.objednavka_pk = o.objednavka_pk
        JOIN zakaznik z ON o.zakaznik_pk = z.zakaznik_pk
        JOIN adresa a ON fu.adresa_pk = a.adresa_pk
    LOOP
        UPDATE fakturacni_udaje SET
              ulice = lr_faktura.ulice
            , mesto = lr_faktura.mesto
            , psc = lr_faktura.psc
            , zeme_id = 'CZ'
            , jmeno = lr_faktura.jmeno
            , prijmeni = lr_faktura.prijmeni
            , typ = CASE WHEN lr_faktura.ic IS NOT NULL AND lr_faktura.ic <> '' THEN 'PO' ELSE 'FO' END
        WHERE fakturacni_udaje_pk = lr_faktura.fakturacni_udaje_pk;
    END LOOP;

    -- systemove
    FOR lr_system IN SELECT DISTINCT sfu.fakturacni_udaje_pk, fu.ic, fu.jmeno, fu.prijmeni, a.* FROM system_fakturacni_udaje sfu
        JOIN fakturacni_udaje fu ON sfu.fakturacni_udaje_pk = fu.fakturacni_udaje_pk
        JOIN adresa a ON fu.adresa_pk = a.adresa_pk
    LOOP
        UPDATE fakturacni_udaje SET
              ulice = lr_system.ulice
            , mesto = lr_system.mesto
            , psc = lr_system.psc
            , zeme_id = 'CZ'
            , jmeno = lr_system.jmeno
            , prijmeni = lr_system.prijmeni
            , typ = CASE WHEN lr_system.ic IS NOT NULL AND lr_system.ic <> '' THEN 'PO' ELSE 'FO' END
        WHERE fakturacni_udaje_pk = lr_system.fakturacni_udaje_pk;
    END LOOP;
END;
$$;


SELECT * FROM zakaznik_dodaci_udaje zdu JOIN dodaci_udaje du ON zdu.dodaci_udaje_pk = du.dodaci_udaje_pk;


ALTER TABLE dodaci_udaje DROP COLUMN adresa_pk;
ALTER TABLE fakturacni_udaje DROP COLUMN adresa_pk;
DROP TABLE adresa;

ALTER TABLE objednavka DROP COLUMN f_ic;
ALTER TABLE objednavka DROP COLUMN f_dic;
ALTER TABLE objednavka DROP COLUMN f_obchodni_jmeno;
ALTER TABLE objednavka DROP COLUMN f_ulice;
ALTER TABLE objednavka DROP COLUMN f_mesto;
ALTER TABLE objednavka DROP COLUMN f_psc;

ALTER TABLE objednavka DROP COLUMN d_ulice;
ALTER TABLE objednavka DROP COLUMN d_mesto;
ALTER TABLE objednavka DROP COLUMN d_psc;


CREATE VIEW adresa AS
  SELECT jmeno, prijmeni, ulice, mesto, psc FROM (
                                                   SELECT z.jmeno, z.prijmeni, fu.ulice, fu.mesto, fu.psc FROM fakturacni_udaje fu
                                                     JOIN zakaznik_fakturacni_udaje zfu ON fu.fakturacni_udaje_pk = zfu.fakturacni_udaje_pk
                                                     JOIN zakaznik z ON zfu.zakaznik_pk = z.zakaznik_pk

                                                   UNION ALL

                                                   SELECT z.jmeno, z.prijmeni, du.ulice, du.mesto, du.psc FROM dodaci_udaje du
                                                     JOIN zakaznik_dodaci_udaje zdu ON du.dodaci_udaje_pk = zdu.dodaci_udaje_pk
                                                     JOIN zakaznik z ON zdu.zakaznik_pk = z.zakaznik_pk
                                                 ) AS t
  GROUP BY 1, 2, 3, 4, 5;