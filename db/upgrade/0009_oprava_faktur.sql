BEGIN;

\i ../procs/f_faktury.sql

DO $$
DECLARE
    l_row RECORD;
BEGIN

    FOR l_row IN SELECT
                     o.*
                     , f.faktura_pk
                 FROM objednavka o
                     JOIN faktura f ON o.objednavka_pk = f.objednavka_pk
                 WHERE o.stav > 4
    LOOP

        PERFORM faktura_uloz_fakturacni_udaje(
            l_row.faktura_pk,
            CASE WHEN nullif(l_row.f_ic, '') IS NULL THEN 'FO' ELSE 'PO' END,
            nullif(l_row.f_ic, ''),
            l_row.f_dic,
            l_row.f_obchodni_jmeno,
            l_row.f_ulice,
            l_row.f_mesto,
            l_row.f_psc
        );

    END LOOP ;

END;
$$;