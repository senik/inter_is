BEGIN;

ALTER TABLE uzivatel DROP CONSTRAINT check_stav;
ALTER TABLE uzivatel ADD CONSTRAINT check_stav CHECK (stav = ANY(ARRAY['AKTIVNI'::text, 'NEAKTIVNI'::text, 'SMAZANO'::text]))