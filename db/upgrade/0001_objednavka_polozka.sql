BEGIN;

ALTER TABLE objednavka_polozka ADD COLUMN material_pk INTEGER;

UPDATE objednavka_polozka SET material_pk = 1;

ALTER TABLE objednavka_polozka ALTER COLUMN material_pk SET NOT NULL;

CREATE INDEX "ix_objednavka_polozka_material_pk" ON "objednavka_polozka" ("material_pk");

ALTER table objednavka_polozka drop CONSTRAINT objednavka_polozka_key;
ALTER TABLE "objednavka_polozka" ADD CONSTRAINT "objednavka_polozka_key" PRIMARY KEY ("objednavka_pk", "model_pk", "material_pk");

\i ../procs/f_objednavka.sql