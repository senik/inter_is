<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 15.06.2017
 * Time: 20:41
 */

namespace app\mail\models;


use app\mail\Email;
use yii\console\Exception;

/**
 * Class ZapomenuteHesloMail
 * @package app\mail\models
 */
class ZapomenuteHesloMail extends Email
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->predmet = "ISS VoPlasTo - Zapomenuté heslo";
    }

    /**
     * metoda vezme zaznam z databaze, naplni sablonu daty a vrati vykreslene telo emailu jako string
     *
     * @throws \Exception
     * @return string
     */
    public function vykresliEmail()
    {
        if (!isset($this->data['klic']) || $this->data['klic'] == NULL) {
            throw new Exception("Email s pk = {$this->mail_pk} nema v datech klic!");
        }

        return $this->_mailer->view->render('@app/mail/views/zapomenute-heslo', ['klic' => $this->data['klic']]);
    }
}