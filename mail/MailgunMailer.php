<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 21.01.2016
 * Time: 19:56
 */

namespace app\mail;


use Mailgun\Mailgun;
use yii\mail\BaseMailer;
use yii\mail\MessageInterface;

/**
 * Class MailgunMailer
 * @package app\mail
 */
class MailgunMailer extends BaseMailer
{
    /**
     * @var array
     */
    protected $_errorInfo = [];

    /**
     * @var string apikey mailgunu
     */
    protected $_apiKey;

    /**
     * @var string domena mailu
     */
    protected $_domain;

    /**
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $config = \Yii::$app->params['mailer'];

        if (!isset($config['api-key']) || NULL == $config['api-key']) {
            throw new MailgunConfigException("Neni nastaveny parametr \"api-key\" v konfiguracnim poli maileru!");
        } else {
            $this->_apiKey = $config['api-key'];
        }

        if (!isset($config['domain']) || NULL == $config['domain']) {
            throw new MailgunConfigException("Neni nastaveny parametr \"domain\" v konfiguracnim poli maileru!");
        } else {
            $this->_domain = $config['domain'];
        }
    }

    /**
     * @param MessageInterface $message
     * @return bool
     */
    public function beforeSend($message)
    {
        $beforeSend = parent::beforeSend($message);

        // todo osetrit development

        return $beforeSend;
    }

    /**
     * Sends the specified message.
     * This method should be implemented by child classes with the actual email sending logic.
     * @param MessageInterface $message the message to be sent
     * @return boolean whether the message is sent successfully
     */
    protected function sendMessage($message)
    {
        $mg = new Mailgun(
            $this->_apiKey
        );

        $messageStruct = $this->prepareMessageStruct($message);

        try {
            // ukazka response
            /*
            stdClass (2) (
                public http_response_body -> stdClass (2) (
                    public id -> string (38) "<20160121202721.14195.29237@senovo.cz>"
                    public message -> string (18) "Queued. Thank you."
                )
                public http_response_code -> integer 200
            )
            */
            $result = $mg->sendMessage(
                $this->_domain,
                $messageStruct
            );

            if ($result->http_response_code == 200) {
                $class = new MailerResult(
                    $result->http_response_body->id,
                    $result->http_response_body->message
                );

                return $class;
            } else {
                throw new \Exception('neocekavany navrat z Mailgunu: ' . var_export($result, true));
            }
        } catch (\Exception $e) {
            $this->_errorInfo[] = $e->getMessage();
            return false;
        }
    }

    /**
     * todo doladit
     * @param MessageInterface $message the message to be sent
     * @return array
     */
    protected function prepareMessageStruct($message)
    {
        $messageStruct = [
            'from' => $this->getMessageSender($message),
            'to' => $this->getMessageTo($message),
            'cc' => $this->getMessageCc($message),
            'bcc' => $this->getMessageBcc($message),
            'subject' => $message->getSubject(),
            'text' => $message->textBody,
            'html' => $message->isHtml() ? $message->htmlBody : NULL,
            'recipient-variables' => json_encode([])
        ];

        foreach ($messageStruct as $key => $value) {
            if ($value == '' || $value == NULL) {
                unset($messageStruct[$key]);
            }
        }

        return $messageStruct;
    }

    /**
     * @param $message MessageInterface $message the message to be sent
     * @return string
     */
    protected function getMessageSender($message)
    {
        $sender = $message->getFrom();

        return $this->formatEmailName(
            $sender['email'],
            isset($sender['name']) ? $sender['name'] : null
        );
    }

    /**
     * @param $message MessageInterface $message the message to be sent
     * @return string
     */
    protected function getMessageTo($message)
    {
        $to = $message->getTo();

        $toOut = [];
        foreach ($to as $recipient) {
            $toOut[] = $this->formatEmailName(
                $recipient['email'],
                isset($recipient['name']) ? $recipient['name'] : NULL
            );
        }

        return implode(', ', $toOut);
    }

    /**
     * @param $message MessageInterface $message the message to be sent
     * @return string
     */
    protected function getMessageBcc($message)
    {
        $bcc = $message->getBcc();

        $bccOut = [];
        foreach ($bcc as $recipient) {
            $bccOut[] = $this->formatEmailName(
                $recipient['email'],
                isset($recipient['name']) ? $recipient['name'] : NULL
            );
        }

        return implode(', ', $bccOut);
    }

    /**
     * @param $message MessageInterface $message the message to be sent
     * @return string
     */
    protected function getMessageCc($message)
    {
        $cc = $message->getCc();

        $ccOut = [];
        foreach ($cc as $recipient) {
            $ccOut[] = $this->formatEmailName(
                $recipient['email'],
                isset($recipient['name']) ? $recipient['name'] : NULL
            );
        }

        return implode(', ', $ccOut);
    }

    /**
     * @param string $email
     * @param NULL|string $name
     * @return string
     */
    protected function formatEmailName($email, $name = NULL)
    {
        if (NULL == $name) {
            $formatted = $email;
        } else {
            $formatted = sprintf(
                "'%s' <%s>",
                $name,
                $email
            );
        }

        return $formatted;
    }

    /**
     * Vrati chybove hlasky maileru
     * @return array
     */
    public function getErrorInfo()
    {
        return $this->_errorInfo;
    }

    /**
     * Vrati prvni chybovou hlasku maileru
     * @return string
     */
    public function getFirstError()
    {
        if (is_array($this->_errorInfo)) {
            reset($this->_errorInfo);
            $key = key($this->_errorInfo);

            return $this->_errorInfo[$key];
        } else {
            return $this->_errorInfo;
        }
    }
}

/**
 * Class MailgunConfigException
 * @package app\mail
 */
class MailgunConfigException extends \Exception {}