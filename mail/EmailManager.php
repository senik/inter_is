<?php
/**
 * Created by PhpStorm.
 * User: senohrabekl
 * Date: 8.1.15
 * Time: 13:53
 */

namespace app\mail;


use Exception;
use yii\base\Model;

/**
 * Class EmailManagerObsluha
 * @package comgate\email\templates
 */
class EmailManager extends Model
{
    /**
     * @var bool
     */
    public $test = false;

    /**
     * @param $class
     * @return EmailManager
     * @throws \Exception
     */
    public static function factory($class) {
        if (empty($class)) {
            throw new \Exception("Nebyla zadána obslužná třída");
        }
        return new $class;
    }

    /**
     * @return Email[]
     */
    public function vratDavku()
    {
        return Email::findBySql(
            "update mail set cas_odeslani = now(), stav = :stav_zpracovava WHERE stav = :stav_novy and cas_naplanovani <= now() RETURNING *",
            [
                'stav_zpracovava'   => Email::STAV_ZPRACOVAVA_SE,
                'stav_novy'         => Email::STAV_NOVY
            ]
        )->all();
    }

    /**
     * @param $pk
     * @return Email
     * @throws Exception pokud neni uloha nalezena
     */
    public function najdiUlohu($pk)
    {
        $email = Email::findBySql("select * from mail where mail_pk = :pk", ['pk' => $pk])->one();
        if (null == $email) {
            throw new Exception("email manager: nepodarilo se nalezt email s mail_pk = {$pk}");
        } else {
            return $email;
        }
    }
}