<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 15.06.2017
 * Time: 20:44
 *
 * @var $klic string
 */

$url = Yii::$app->urlManager->createAbsoluteUrl(['/uzivatel/default/zapomenute-heslo-zmena', 'klic' => $klic]);
?>

<h2>Zapomenuté heslo do ISS VoPlasTo</h2>

<p>
    Své heslo si můžete vyresetovat pomocí odkazu níže.
</p>

<p class="well text-center">
    <a href="<?= $url ?>"><?= $url ?></a>
</p>

<p>
    Pokud jste si reset hesla nevyžádal(a), na odkaz neklikejte a skutečnost nahlašte technické podpoře.
</p>