<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 22.01.2016
 * Time: 20:24
 */

namespace app\mail;

/**
 * Class MailerResult
 * @package app\mail
 */
class MailerResult
{
    /**
     * @var string ID mailu v externi sluzbe
     */
    protected $id;

    /**
     * @var string Textovy popis navratu
     */
    protected $text;

    /**
     * @param $id string
     * @param $text string
     */
    public function __construct($id, $text)
    {
        $this->id = $id;
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }
}