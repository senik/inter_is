/* aplikacni js */

/* namespace */
(function(Senovo, $, undefined) {
    Senovo.hideSpojitButton = function() {
        window.console.log('aa');
        if ($('input[type=checkbox]:checked', 'div.polozka-spojit').length > 1) {
            $('button[type="submit"]', 'form#faktura-form').html('Spojit a uložit');
        } else {
            $('button[type="submit"]', 'form#faktura-form').html('Uložit');
        }
    };

    /**
     * schovava nebo zobrazuje dodatecna pole pro fakturacni udaje, jako callback ma footerMax
     */
    Senovo.zmenTypFakturacniUdaje = function() {
        var val = $('#fakturacniudaje-typ').val();
        if (val == 'PO') {
            $('div.udaje-firma').slideDown({
                duration: 500,
                step: function(now, fx) {
                    footerMax();
                }
            });
        } else {
            $('div.udaje-firma').slideUp({
                duration: 500,
                step: function(now, fx) {
                    footerMax();
                }
            });
        }
    };

    /**
     *
     * @param event
     */
    Senovo.handleMaterialForm = function(event) {
        var target = event.target.getAttribute('id'),
            value = $('#'+target).val();

        if (target == 'material-druh_id') {
            // aktualizovat dostupné barvy pro materiál

            // a prepnout pole pro novy nazev
            if (value == '') {
                $('.field-material-nazev').show();
                $('#material-nazev').prop('disabled', false);
            } else {
                $('.field-material-nazev').hide();
                $('#material-nazev').prop('disabled', true);
            }
        } else if (target == 'material-barva_id') {
            if (value == '') {
                $('.field-material-barva').show();
                $('#material-barva').prop('disabled', false);
            } else {
                $('.field-material-barva').hide();
                $('#material-barva').prop('disabled', true);
            }
        }

        if ($('#material-barva_id').val() == '' || $('#material-druh_id').val() == '') {
            $('.field-material-hustota').show();
            $('#material-hustota').prop('disabled', false);
        } else {
            $('.field-material-hustota').hide();
            $('#material-hustota').prop('disabled', true);
        }

        // tohle je vopruz...
        footerMax();
    };

    /**
     *
     * @param cname
     * @param cvalue
     * @param exdays
     */
    Senovo.setCookie = function(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    };

    Senovo.getCookie = function(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }

        return "";
    };
}(window.Senovo = window.Senovo || {}, jQuery));

/**
 * Prevede prvni pismeno stringu na uppercase
 * @returns {string}
 */
String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

yii.allowAction = function ($e) {
    var message = $e.data('confirm');
    return message === undefined || yii.confirm(message, $e);
};

yii.confirm = function (message, ok, cancel) {

    bootbox.confirm(
        {
            message: message,
            buttons: {
                confirm: {
                    label: "OK"
                },
                cancel: {
                    label: "Zrušit"
                }
            },
            callback: function (confirmed) {
                if (confirmed) {
                    !ok || ok();
                } else {
                    !cancel || cancel();
                }
            }
        }
    );
    // confirm will always return false on the first call
    // to cancel click handler
    return false;
};

/**
 * Pozicuje patičku vždy "dolu"
 */
function footerMax() {

    var body = jQuery(window).height();
    var head = $('.wrapperHead').height();
    var midd = $('.wrapperContent').outerHeight();
    var foot = $('.wrapperFooter').outerHeight();

    // footer 92 + 20
    var res = body - head - midd - foot; // 7px je korekce

    // pokud mohu zvetsit
    if (res > 0) {
        $('.wrapperFooterFixedMax').css('margin-top', res);
    } else {
        $('.wrapperFooterFixedMax').css('margin-top', 0);
    }
}

/**
 * resetovani formulare kvuli selectpickeru
 */
function resetForm() {
    event.target.form.reset();
    $('select').selectpicker('refresh');
}

/**
 *
 * @returns {boolean}
 */
function presunIkonyModulu() {
    var windowWidth = $(window).width();
    var moved = ($('.module-navbar').children('a').length == 0);
//        window.console.log(moved);
    if (windowWidth < 768 && !moved) {
        $('.module-navbar a').appendTo('#left-mobile-menu');
    } else if (windowWidth >= 768 && moved) {
        $('a', '#left-mobile-menu').appendTo('.module-navbar');
    }

    window.console.log('presun');

    return true;
}

//fixing column height problem using Prototype
//Event.observe(window,"load",function(){
//    if(parseInt($('leftform').getStyle('height')) > parseInt($('rightform').getStyle('height')))
//        $('rightform').setStyle({'height' : parseInt($('leftform').getStyle('height'))+'px'});
//    else
//        $('leftform').setStyle({'height' : parseInt($('rightform').getStyle('height'))+'px'});
//});//observe

/**
 *
 * @param position
 */
function resetMobileMenu(position)
{
    var $body = $('body');
    var $menu = $('#' + position + '-mobile-menu');
    var $wrap = $('div.wrap');
    var $wrapperSide = $('div.wrapper' + position.capitalize());
    var $wrapperMiddle = $('div.wrapperMiddle');

//        var $wrapperHead = $('div.wrapperHead');
    var $wrapperContent = $('div.wrapperContent');
    var $wrapperFooter = $('div.wrapperFooter');

    var menuClass = position + '-open';
    if ($body.hasClass(menuClass)) {
        //var height = $wrap.height();
        //
        //$wrapperSide.css({
        //    "height": height + 'px'
        //});
        $body.removeClass(menuClass);
    }


    $wrap.removeAttr('style');
    $wrapperSide.removeAttr('style');
    $wrapperMiddle.removeAttr('style');
    $wrapperContent.removeAttr('style');
    $wrapperFooter.removeAttr('style');

    //$menu.removeClass('open');
}

/**
 * todo tohle bude potreba udelat jako funkci
 * predevsim kvuli resizovani okna a prekresleni
 * @param position
 * @param el
 */
function toggleSideMobileMenu(position, el)
{
    var $body = $('body');
    var $wrap = $('div.wrap');
    var $wrapperSide = $('div.wrapper'+position.capitalize());

    if (!$body.hasClass('left-open')) {
        var height = $wrap.height();

        $wrapperSide.css({
            "height": height + 'px'
        });
    }

    $body.toggleClass('left-open');

    el.blur();
}

/**
 * Metoda prida polozku do objednavky (volano z modalu)
 * @param pk
 */
function objednavkaPridejPolozku(pk) {
    var wrapper = $('div#objednavka-polozky');
    var $material = $('select[name="material_'+pk+'"]');
    var material = $material.val();

    //window.console.log(window.location);

    if ($('#Objednavka_polozky_'+pk+'_'+material).length == 0) {
        var input = '';
        var path = window.location.pathname;

        //window.console.log(path.indexOf('upravit'));

        if (path.indexOf('upravit') > -1) {
            path = path.replace('upravit', 'pridat');
        }

        path = path + '-polozku';
        var request = $.ajax({
            url: path,
            type: 'POST',
            data: {material_pk: material, model_pk: pk}
        });

        request.done(function(response) {
            var data = $.parseJSON(response);
            if (data.success == 1) {
                wrapper.append(data.polozka);
                footerMax();

                updateCenaObjednavky();
            }
        });

        request.fail(function(jqXHR, textStatus) {
            // todo tohle chce resit nejakym alertem nebo tak neco
            window.console.log(jqXHR, textStatus);
        });
    } else {
        var kusu_input = $('#Objednavka_polozky_'+pk+'_'+material+'_kusu');
        var kusu = parseInt(kusu_input.val());

        kusu_input.val(kusu + 1);

        updateCenaObjednavky();
    }

    $('.xtooltip').tooltip();
    // ??? nevim, asi bych ho nezaviral
    //$("#objednavka-polozka-modal").modal("hide");
}

/**
 *
 */
function updateCenaObjednavky()
{
    var cena = 0;

    $('input[type="text"][id$="cena"]').each(function() {
        var cena_p = $(this).val().toString().replace(',', '.');
        var kusu_p = $(this).parent().find('input[type="text"][id$="kusu"]').val();

        cena += kusu_p * cena_p;
    });

    $('span[id="cena"]').text((Math.round(cena * 100 )/100).toFixed(2).toString().replace('.', ','));
}

/**
 * Odebere polozku z objednavky
 * @param event
 */
function objednavkaOdeberPolozku(event) {
    yii.confirm("Opravdu odebrat položku?", function() {
        //$('#Objednavka_polozky_'+pk).remove();
        $(event.target).closest('.objednavka-polozka').remove();
        footerMax();
        updateCenaObjednavky();
    });

    return false;
}

// ready block
//$(document).ready(function () {
(function ($) {

    /**
     * Zmena velikosti fontu v okynkach kalendare
     * Musel jsem si ho drobne upravit kvuli velkosti redku
     * @href https://github.com/davatron5000/FitText.js
     *
     * @param kompressor
     * @param options
     * @returns {*}
     */
    $.fn.fitText = function(kompressor, options) {

        // Setup options
        var compressor = kompressor || 1,
            settings = $.extend({
                'minFontSize' : Number.NEGATIVE_INFINITY,
                'maxFontSize' : Number.POSITIVE_INFINITY
            }, options);

        return this.each(function(){

            // Store the object
            var $this = $(this);

            // Resizer() resizes items based on the object width divided by the compressor * 10
            var resizer = function () {
                var size = Math.max(Math.min($this.width() / (compressor), parseFloat(settings.maxFontSize)), parseFloat(settings.minFontSize));
                $this.css('font-size', size + 'px');
                $this.css('line-height', size + 'px');
            };

            // Call once to set.
            resizer();

            // Call on resize. Opera debounces their resize by default.
            $(window).on('resize.fittext orientationchange.fittext', resizer);

        });

    };

    /** vsechny mozny eventy, na ktery je potreba navazat footerMax() **/
    $.event.add(window, "load", footerMax);
    $.event.add(window, "resize", footerMax);

    $('.alert').on('closed.bs.alert', function () {
        footerMax();
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        //e.target // newly activated tab
        //e.relatedTarget // previous active tab
        footerMax();
    });

    $('form').on('afterValidateAttribute', function (event, attribute, messages) {
        setTimeout(function() {
            footerMax();
        }, 1);
    });
    /** ************************************************************* **/

    var toolTip = $('.xtooltip');
    if (toolTip.length != 0) {
        toolTip.tooltip();
    }

    $('.modal-close').on('click', function() {
        $(this).closest('div[id$="-modal"]').modal('hide');
    });

    $('button[data-target="#left-mobile-menu"]', '.mobile-navbar').on('click', function() {
        resetMobileMenu('right');
        toggleSideMobileMenu('left', $(this));
    });

    $('button[data-target="#right-mobile-menu"]', '.mobile-navbar').on('click', function() {
        resetMobileMenu('left');
        toggleSideMobileMenu('right', $(this));
    });

    $(window).on('load', function() {
        var windowWidth = $(window).width();
        var moved = ($('.module-navbar').children('a').length == 0);

        if (windowWidth < 768 && !moved) {
//            $('.module-navbar a').appendTo('#left-mobile-menu');
        } else if (windowWidth >= 768 && moved) {
//            $('#left-mobile-menu a').appendTo('.module-navbar');
            resetMobileMenu('left');
            resetMobileMenu('right');
        }

        // handle material form on load
        if ($('#material-pridat-form').length > 0) {
            var barva_id = $('#material-barva_id').val(),
                druh_id  = $('#material-druh_id').val();

            if (druh_id == '') {
                $('.field-material-nazev').show();
                $('#material-nazev').prop('disabled', false);
            } else {
                $('.field-material-nazev').hide();
                $('#material-nazev').prop('disabled', true);
            }

            if (barva_id == '') {
                $('.field-material-barva').show();
                $('#material-barva').prop('disabled', false);
            } else {
                $('.field-material-barva').hide();
                $('#material-barva').prop('disabled', true);
            }

            if (barva_id == '' || druh_id == '') {
                $('.field-material-hustota').show();
                $('#material-hustota').prop('disabled', false);
            } else {
                $('.field-material-hustota').hide();
                $('#material-hustota').prop('disabled', true);
            }
        }
    });

    $(window).on('resize', function() {
        var windowWidth = $(window).width();
        var moved = ($('div[id$="mobile-menu"]').hasClass('open'));

        if (windowWidth < 768 && !moved) {
//            $('.module-navbar a').appendTo('#left-mobile-menu');
        } else if (windowWidth >= 768 && moved) {
//            $('#left-mobile-menu a').appendTo('.module-navbar');
            resetMobileMenu('left');
            resetMobileMenu('right');
        }
    });

    $('.module-navbar a').hover(function() {
        $('ul.breadcrumb.stable').hide();
        $('ul.breadcrumb.hint li.active').text($(this).attr('data-module-name')).show();
        $('ul.breadcrumb.hint').show();
    }, function () {
        $('ul.breadcrumb.hint').hide();
        $('ul.breadcrumb.hint li.active').text('');
        $('ul.breadcrumb.stable').show();
    });

    // klikani na polozky v objednavce
    $(document).on('click', 'div.objednavka-polozka a.polozka-odebrat', function(event) {
        event.preventDefault();
        var pk = $(this).attr('data-polozka');
        objednavkaOdeberPolozku(event);

        updateCenaObjednavky();
    });

    $(document).on('click', 'div.objednavka-polozka a.polozka-plus', function(event) {
        event.preventDefault();
        var model = $(this).attr('data-polozka');
        var material = $(this).attr('data-material');
        var kusu_input = $('#Objednavka_polozky_'+model+'_'+material+'_kusu');
        var kusu = parseInt(kusu_input.val());

        kusu_input.val((isNaN(kusu) ? 0 : kusu) + 1);

        updateCenaObjednavky();
    });

    $(document).on('click', 'div.objednavka-polozka a.polozka-minus', function(event) {
        event.preventDefault();
        var model = $(this).attr('data-polozka');
        var material = $(this).attr('data-material');
        var kusu_input = $('#Objednavka_polozky_'+model+'_'+material+'_kusu');
        var kusu = parseInt(kusu_input.val());

        if (kusu == 1) {
            // tahle fce update vola, proto neni potreba tady
            objednavkaOdeberPolozku(event);
        } else {
            kusu_input.val((isNaN(kusu) ? 1 : kusu - 1));
            updateCenaObjednavky();
        }
    });

    $(document).on('keyup', 'input[type="text"][id^="Objednavka_polozky"][id$="kusu"]', function(event) {
        var val = $(this).val();
        if ($.isNumeric(val)) {

        } else {
            $(this).val(1);
        }

        updateCenaObjednavky();
    });
    // -------------------------------

    $(document).on('click', 'div.polozka-spojit input[type=checkbox]', Senovo.hideSpojitButton);
    $(document).on('change', '#fakturacniudaje-typ', Senovo.zmenTypFakturacniUdaje);

    $(document).on('change', '#material-pridat-form select', Senovo.handleMaterialForm);

    $('.prijmeni-copy, .jmeno-copy').on('click', function(ev) {
        ev.preventDefault();
        var input_id = $(this).closest('div.input-group').find('input')[0].id;

        if (input_id.length > 0) {
            var copy_co = input_id.substring(input_id.indexOf('-') + 1, input_id.length),
                copy_from = 'zakaznik-'+copy_co;

            $('#'+input_id).val($('#'+copy_from).val());
        }
    });

    // chtelo by to jeste poladit kvuli defaultnim hodnotam...
    // ale to by bylo potreba plnit uz v modelu
    $('.filter-reset').on('click', function(ev) {
        ev.preventDefault();

        var form = $(this).closest('form'),
            inputs = form.find('div.form-group input[id]'),
            selects = form.find('div.form-group select[id]'),
            defaultValue = '',
            value;

        $.each(inputs, function(k, v) {
            value = defaultValue;
            $(v).val(value);
        });

        $.each(selects, function(k, v) {
            value = defaultValue;
            $(v).val(value).selectpicker('refresh');
        });
    });
})(jQuery);