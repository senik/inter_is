<?php

use app\components\Instance;

require(__DIR__ . '/../components/Instance.php');

// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');

$config = Instance::vratKonfig();

(new yii\web\Application($config))->run();
