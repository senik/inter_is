<?php

namespace app\controllers;

use app\components\Application;
use app\models\NastaveniModel;
use app\modules\faktury\models\Faktura;
use app\modules\kalendar\models\Udalost;
use app\modules\objednavky\models\Objednavka;
use Yii;
use yii\filters\AccessControl;
use app\components\Controller;
use yii\filters\VerbFilter;


/**
 * Class SiteController
 *
 * <pre>
 * zakladni controller pro nemodulove zalezitosti
 * - dashboard
 * - nastaveni
 * - about
 * </pre>
 *
 * @package app\controllers
 */
class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $mFaktura = new Faktura();
        $mObjednavka = new Objednavka(['scenario' => Objednavka::SCENARIO_HLEDAT]);
        $mUdalost = new Udalost();

        return $this->render('index', [
            'mFaktura' => $mFaktura,
            'mObjednavka' => $mObjednavka,
            'mUdalost' => $mUdalost
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionNastaveni()
    {
        $mNastaveni = NastaveniModel::nahraj();

        $post = Yii::$app->request->post();

        if (isset($post['NastaveniModel'])) {
            // todo tohle by melo jit udelat pres $model->load()
            $mNastaveni->_attributes = $post['NastaveniModel'];

            // todo a tohle taky refactorovat
            if ($mNastaveni->validate()) {
                $return = $mNastaveni->uloz();

                if ($return > 0) {
                    Application::setFlashSuccess('Nova konfigurace byla ulozena!');
                    return $this->redirect(array('/dashboard'));
                } else {
                    Application::setFlashError('Nepodarilo se ulozit novou konfiguraci!');
                }
            }
        }

        return $this->render('nastaveni', [
            'model' => $mNastaveni,
        ]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
}
