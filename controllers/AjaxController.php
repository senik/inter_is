<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 4.11.14
 * Time: 22:57
 */

namespace app\controllers;


use app\components\Ares;
use app\components\Controller;
use app\modules\modely\models\Model3D;
use app\modules\objednavky\models\DodaciUdaje;
use app\modules\objednavky\models\FakturacniUdaje;
use app\modules\objednavky\models\Zakaznik;
use yii\helpers\Json;
use Yii;
use yii\web\HttpException;

/**
 * Class AjaxController
 * @package app\controllers
 *
 * tohle je centralni controller pro vsechny AJAX akce, bude to pekne prehledne na jednom miste
 * a budou se dobre resit opravneni a pristupy
 */
class AjaxController extends Controller
{

    public function beforeAction($action)
    {
        $ret = parent::beforeAction($action);

        if (!\Yii::$app->request->isAjax) {
            throw new HttpException(500, 'Toto není AJAXový request!');
        }

        return $ret;
    }

    public function actionIndex()
    {
        die('SEM SE NESMIS DOSTAT!!!');
    }

    public function actionNactiDodaciAdresu($dodaci_udaje_pk)
    {
        Yii::$app->response->format = 'json';
        return array(
            'dodaci_udaje_pk' => $dodaci_udaje_pk
        );
    }

    /**
     * todo ucesat, hlavne fakturacni udaje (udelat jako dodaci...)
     * @return string
     */
    public function actionNactiAdresyZakaznika()
    {
        $post = Yii::$app->request->post();
        $response = array();

        if (!isset($post['zakaznik'])) {
            $response['error'] = 'Neplatný request!';
        } else {
            try {
                $do_udaje = (new DodaciUdaje())->nactiProDropdown('zakaznik', $post['zakaznik']);
                /** @var FakturacniUdaje $fa_udaje */
                $fa_udaje = (new FakturacniUdaje())->nactiPodleZakaznika($post['zakaznik']);

                if (empty($do_udaje)) {
                    $response['do_udaje'][] = array(
                        'pk' => '',
                        'udaje' => 'Žádné údaje'
                    );
                } else {
                    foreach ($do_udaje as $udaje) {
                        $response['do_udaje'][] = array(
                            'pk' => $udaje['dodaci_udaje_pk'],
                            'udaje' => $udaje['adresa']
                        );
                    }
                }

                if ($fa_udaje == null) {
                    $response['fa_udaje'][] = array(
                        'pk' => '',
                        'udaje' => 'Žádné údaje'
                    );
                } else {
                    $response['fa_udaje'][] = array(
                        'pk' => $fa_udaje->fakturacni_udaje_pk,
                        'udaje' => sprintf('%s, %s %s', $fa_udaje->ulice, $fa_udaje->psc, $fa_udaje->mesto)
                    );
                }

                $response['success'] = 1;
            } catch (\Exception $e) {
                $response['error'] = $e->getMessage();
            }
        }

        return Json::encode($response);
    }

    /**
     *
     */
    public function actionZalozZakaznikaRychle()
    {
        $post = Yii::$app->request->post();
        $response = array();

        $mZakaznik = new Zakaznik(['scenario' => Zakaznik::SCENARIO_RYCHLE]);
        $mZakaznik->load($post);

        if ($mZakaznik->validate()) {
            if ($mZakaznik->uloz()) {
                $zakaznik_pk = $mZakaznik->zakaznik_pk;

                $mZakaznik->unsetAttrubutes();

                $response = array(
                    'success' => 1,
                    'pk' => $zakaznik_pk,
                    'zakaznici' => $mZakaznik->search()->getModels()
                );
            } else {
                $response['error'] = $mZakaznik->getLastError();
            }
        } else {
            $response = array(
                'novalidate' => 1,
                'errors' => $mZakaznik->getErrors()
            );
        }

        return Json::encode($response);
    }

    /**
     * @return string
     */
    public function actionObjednavkaFiltrujModely()
    {
        $post = Yii::$app->request->post();

        $mModel = new Model3D(array('scenario' => Model3D::SCENARIO_OBJEDN));

        if (!empty($post)) {
            $mModel->load($post);
        }

        return $this->renderPartial('objednavkaModelyGrid', array(
            'mModel' => $mModel
        ));
    }

    /**
     * AJAX pro ulozeni dodacich udaju
     * @throws \Exception
     * @return string
     */
    public function actionZakaznikUlozDodaci()
    {
        $post = Yii::$app->request->post();

        $response = array();
        try {
            if (!isset($post['DodaciUdaje']['zakaznik_pk'])) {
                $response['error'] = 'Neplatný request!';
            }

            $zakaznik_pk = $post['DodaciUdaje']['zakaznik_pk'];

            $mZakaznik = (new Zakaznik())->nacti($zakaznik_pk);

            /** @var DodaciUdaje $mDoUdaje */
            if ($post['DodaciUdaje']['dodaci_udaje_pk'] != null) {
                $mDoUdaje = (new DodaciUdaje())->nactiPodlePk($post['DodaciUdaje']['dodaci_udaje_pk']);
            } else {
                $mDoUdaje = new DodaciUdaje();
            }

            $mDoUdaje->load($post);

            if ($mDoUdaje->validate()) {
                $pk = $mDoUdaje->uloz();

                $do_udaje = (new DodaciUdaje())->nactiProDropdown('zakaznik', $zakaznik_pk);

                foreach ($do_udaje as $udaje) {
                    $response['do_udaje'][] = array(
                        'pk' => $udaje['dodaci_udaje_pk'],
                        'udaje' => $udaje['adresa']
                    );
                }

                $response['success'] = $pk;
            } else {
                throw new \Exception(implode("; ", $mDoUdaje->getErrors()));
            }
        } catch (\Exception $e) {
            $response['error'] = $e->getMessage();
        }

        return Json::encode($response);
    }

    /**
     * AJAX pro ulozeni fakturacnich udaju
     * @throws \Exception
     * @return string
     */
    public function actionZakaznikUlozFakturacni()
    {
        $post = Yii::$app->request->post();

        $response = array();
        try {
            if (!isset($post['FakturacniUdaje']['zakaznik_pk'])) {
                $response['error'] = 'Neplatný request!';
            }

            $zakaznik_pk = $post['FakturacniUdaje']['zakaznik_pk'];

            //var_dump($post);

            $mZakaznik = (new Zakaznik())->nacti($zakaznik_pk);
            /** @var FakturacniUdaje $mFaUdaje */
            if ($post['FakturacniUdaje']['fakturacni_udaje_pk'] != null) {
                $mFaUdaje = (new FakturacniUdaje())->nactiPodlePk($post['FakturacniUdaje']['fakturacni_udaje_pk']);
            } else {
                $mFaUdaje = new FakturacniUdaje();
            }

            $mFaUdaje->load($post);

            if ($mFaUdaje->validate()) {
                $pk = $mFaUdaje->uloz();

                $fa_udaje = $mFaUdaje->nactiProDropdown('zakaznik', $zakaznik_pk);

                foreach ($fa_udaje as $udaje) {
                    $response['fa_udaje'][] = array(
                        'pk' => $udaje['fakturacni_udaje_pk'],
                        'udaje' => $udaje['adresa']
                    );
                }
            } else {
                throw new \Exception(implode("; ", $mFaUdaje->getErrors()));
            }

            $response['success'] = $pk;
        } catch (\Exception $e) {
            $response['error'] = $e->getMessage();
        }

        return Json::encode($response);
    }

    /**
     *
     */
    public function actionUlozFakturacni()
    {
        $post = Yii::$app->request->post();
        $response = [];

        try {
            $response = $post;
        } catch (\Exception $e) {

        }

        return Json::encode($response);
    }

    /**
     * Refactor
     * @return string
     * @throws \Exception
     */
    public function actionUlozScreen()
    {
        $post = Yii::$app->request->post();

        $response = array();
        try {
            $data = $post['img'];
            $pk = $post['model'];

            $mModel = Model3D::findOne($pk);
            if ($mModel == null) {
                throw new \Exception("Model s pk = {$pk} nenalezen!");
            }

            $suffix = '.png';
            $path = Yii::$app->basePath .
                DIRECTORY_SEPARATOR . 'web' .
                DIRECTORY_SEPARATOR . 'images' .
                DIRECTORY_SEPARATOR . 'modely'
            ;

            // nejprve overim slozku modely
            if (!file_exists($path)) {
                mkdir($path);
            }

            // a pak jeste slozku konkretniho modelu
            $path .= DIRECTORY_SEPARATOR . $pk;
            if (!file_exists($path)) {
                mkdir($path);
            }

            if (!is_writable($path)) {
                chmod($path, 0777);
            }

            $path .= DIRECTORY_SEPARATOR;

            $soubor = 'pic';

            $index = 0;
            $filename = $path.$soubor.$suffix;
            while(file_exists($filename)) {
                $index++;
                $filename = $path.$soubor.$index.$suffix;
            }

            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
            $data = base64_decode($data);

            file_put_contents($filename, $data);

            $response['success'] = 1;
            $response['data'] = array(
                'soubor' => $soubor.($index == 0 ? '' : $index).$suffix,
                'pk' => $pk
            );
        } catch (\Exception $e) {
            $response['error'] = $e->getMessage();
        }

        return Json::encode($response);
    }

    /**
     *
     */
    public function actionAres()
    {
        $post = Yii::$app->request->post();

        $response = array();
        try {
            $ares = new Ares($post['ico']);
            $data = $ares->nactiPodleIc();

            $response['success'] = 1;
            $response['firma'] = $data;
        } catch (\Exception $e) {
            $response['error'] = $e->getMessage();
        }

        return Json::encode($response);
    }

}