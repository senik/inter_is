<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 24.10.14
 * Time: 21:13
 */

namespace app\models;

use app\components\Nastaveni;
use yii\base\Exception;
use yii\base\Model;
use Yii;

/**
 * Class NastaveniModel
 * @package app\models
 */
class NastaveniModel extends Model
{

    public $_attributes = array();

    public function __construct($config = [])
    {
        parent::__construct($config);

        $params = Yii::$app->params['polozky-nastaveni'];
        foreach ($params as $param) {
            $this->_attributes[$param] = null;
        }
    }

    public function rules()
    {
        return array(
            array(array_keys($this->_attributes), 'required'),
            array('instalovaneModuly', 'modulyValidace')
        );
    }

    public function uloz()
    {
        $params = Yii::$app->params['polozky-nastaveni'];
        $values = array();
        foreach ($params as $param) {
            $val = $this->__get($param);
            switch ($param) {
                case 'admini':
                    $values[$param] = explode(PHP_EOL, $val);
                    break;
                case 'instalovaneModuly':
                    $values[$param] = $val;
                    break;
                case Nastaveni::KLIC_KONFIGUROVANO:
                    $values[$param] = date('Y-m-d H:i:s', time());
                    break;
                default:
                    $values[$param] = $val;
            }
        }

        if (!empty($values)) {
            return Nastaveni::instance()->zapisKonfigDoSouboru($values);
        } else {
            throw new Exception("Zadna konfiguracni data k zapsani!");
        }
    }

    /**
     * @return NastaveniModel
     * @throws \yii\base\Exception
     */
    public static function nahraj()
    {
        $konfig = Nastaveni::instance()->vratKonfig();
        $model = new self;

        $attributes = array_keys($model->_attributes);
        foreach ($konfig as $key => $value) {
            if (in_array($key, $attributes)) {
                switch ($key) {
                    case 'admini':
                        $model->__set($key, implode(PHP_EOL, $value));
                        break;
                    case 'instalovaneModuly':
                        $model->__set($key, $value);
                        break;
                    case Nastaveni::KLIC_KONFIGUROVANO:
                        $model->__set($key, $value == null ? date('Y-m-d H:i:s', time()) : $value);
                        break;
                    default:
                        $model->__set($key, $value);
                }
            } else {
                throw new Exception("Nedefinovany atribut: {$key}");
            }
        }

        return $model;
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function modulyValidace($attribute, $params)
    {
        if (
            !is_array($this->$attribute)
            || !in_array('uzivatel', $this->$attribute)
        ) {
            $this->addError($attribute, 'Není vybrán uživatelský modul nutný pro běh aplikace!');
        }
    }

    /**
     * pretizeni standardniho setteru
     * @param string $attribute
     * @param mixed $value
     */
    public function __set($attribute, $value)
    {
        if (in_array($attribute, array_keys($this->_attributes))) {
            $this->_attributes[$attribute] = $value;
        } else {
            parent::__set($attribute, $value);
        }
    }

    /**
     * pretizeni standardniho getteru
     * @param string $attribute
     * @return mixed
     */
    public function __get($attribute)
    {
        if (in_array($attribute, array_keys($this->_attributes))) {
            return $this->_attributes[$attribute];
        } else {
            return parent::__get($attribute);
        }
    }
}