<?php

return [
    'adminEmail' => 'lukas.senohrabek@email.cz',
    'supportEmail' => 'voplasto@senovo.cz',
    'login-duration' => 7200,
    'polozky-nastaveni' => [
        'konfigurovano', 'admini', 'instalovaneModuly'
    ],

    'mailer' => [
        'api-key' => 'key-73ddad5314ebe6b14c7bbd8cf883b5a0',
        'domain' => 'senovo.cz',
    ]
];
