<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 12. 12. 2015
 * Time: 17:25
 */

return [
    'class' => 'app\mail\MailgunMailer',
    'messageClass' => 'app\mail\Message',
    // send all mails to a file by default. You have to set
    // 'useFileTransport' to false and configure a transport
    // for the mailer to send real emails.
    'useFileTransport' => false,
    /*'transport' => [
        'class' => 'Swift_SmtpTransport',
        'host' => 'smtp.gmail.com',
        'username' => 'lukas.senohrabek@gmail.com',
        'password' => 'heslo',
        'port' => '465',
        'encryption' => 'ssl',
    ],*/
];