<?php

$params = require(__DIR__ . '/params.php');
$mailer = require(__DIR__ . '/_mailer.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'language' => 'cs-CZ',
    'bootstrap' => ['log'],
    'homeUrl' => array('/dashboard'),
    'name' => 'ISS VoPlasTo',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'JlZrlXMNR0xNTv7GDD-wuWLvl810jZwB',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'class' => 'app\components\WebUser',
            'identityClass' => 'app\modules\uzivatel\components\UzivatelIdentity',
            'enableAutoLogin' => true,
            'loginUrl' => array('/uzivatel/default/prihlaseni')
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => $mailer,
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error'],
                    'logFile' => '@runtime/logs/app-error.log'
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['warning'],
                    'logFile' => '@runtime/logs/app-warning.log'
                ],
                /*[
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['trace'],
                    'logFile' => '@runtime/logs/app-trace.log'
                ],*/
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info'],
                    'logFile' => '@runtime/logs/app-info.log'
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            //'urlFormat' => 'path',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => require(__DIR__ . '/_url_rules.php'),
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@app/views' => '@app/themes/default',
                ],
                'baseUrl' => '@web/themes/default',
            ],
        ],
        'formatter' => array(
            'class' => 'yii\i18n\Formatter',
            'thousandSeparator' => '&nbsp;',
            'decimalSeparator' => ',',
            'dateFormat' => 'php:j.n.Y',
            'datetimeFormat' => 'php:j.n.Y H:i:s',
            'currencyCode' => 'Kč'
        )
    ],
    'modules' => [
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
            // enter optional module parameters below - only if you need to
            // use your own export download action or custom translation
            // message source
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => []
        ],
        'uzivatel' => [
            'class' => 'app\modules\uzivatel\UzivatelModule',
            'name' => 'Uživatelé'
        ],
        'objednavky' => [
            'class' => 'app\modules\objednavky\ObjednavkyModule',
            'name' => 'Objednávky',
        ],
        'faktury' => [
            'class' => 'app\modules\faktury\FakturyModule',
            'name' => 'Faktury',
        ],
        'sklad' => [
            'class' => 'app\modules\sklad\SkladModule',
            'name' => 'Skladové zásoby',
        ],
        'kalendar' => [
            'class' => 'app\modules\kalendar\KalendarModule',
            'name' => 'Kalendář',
        ],
        'modely' => [
            'class' => 'app\modules\modely\ModelyModule',
            'name' => '3D Modely',
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
