<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 17.06.2017
 * Time: 15:06
 */

return [
    '/dashboard' => '/site/index',
    '/prihlaseni' => '/uzivatel/default/prihlaseni',
    '/zapomenute-heslo' => '/uzivatel/default/zapomenute-heslo',
    '/zapomenute-heslo-zmena' => '/uzivatel/default/zapomenute-heslo-zmena',
];