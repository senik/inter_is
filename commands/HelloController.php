<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

/**
 * Pretizeni controlleru, pro dlouhotrvajici akce na odchytavani signalu
 */
class HelloController extends ConsoleController
{
    /**
     * @return mixed
     */
    protected function doTask()
    {
        echo 'hello world' . PHP_EOL;
    }
}
