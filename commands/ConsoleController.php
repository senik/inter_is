<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 26.06.2017
 * Time: 21:26
 */

namespace app\commands;


use yii\console\Controller;

/**
 * Class ConsoleController
 * @package app\commands
 */
abstract class ConsoleController extends Controller
{
    /**
     * @var bool
     */
    protected $_isTerminated = false;

    /**
     * @var int
     */
    protected $_sleepTime = 5;

    public $loop = 'once';

    /**
     * @var array
     */
    protected $signals = [
        'abort'     => SIGABRT,
        'alarm'     => SIGALRM,
        'child'     => SIGCHLD,
        'continue'  => SIGCONT,
        'hangup'    => SIGHUP,
        'interrupt' => SIGINT,
        'kill'      => SIGKILL,
        'pipe'      => SIGPIPE,
        'quit'      => SIGQUIT,
        'stop'      => SIGSTOP,
        'suspend'   => SIGTSTP,
        'terminate' => SIGTERM,
    ];

    /**
     * tady bezi hlavni logika aplikace
     * @return mixed
     */
    abstract protected function doTask();

    /**
     * registrace handleru
     */
    public function init()
    {
        parent::init();

        // appka pobezi (doufejme) dele nez je nastaveny limit
        set_time_limit(0);

        // definuju callbacky
        $stop = array($this, 'stop');
        $ignore = SIG_IGN;

        // These define the signal handling
        pcntl_signal(SIGHUP,    $stop);
        pcntl_signal(SIGALRM,   $stop);
        pcntl_signal(SIGINT,    $stop);
        pcntl_signal(SIGTERM,   $stop);
        pcntl_signal(SIGQUIT,   $stop);
        pcntl_signal(SIGUSR1,   $ignore);
        pcntl_signal(SIGUSR2,   $ignore);
    }

    /**
     * @inheritdoc
     */
    public function options()
    {
        return ['loop'];
    }

    /**
     * @return bool
     */
    public function isTerminated()
    {
        // how often to check for signals
        // declare(ticks = 1);
        pcntl_signal_dispatch();

        return $this->_isTerminated;
    }

    /**
     * na jak dlouho se aplikace uspi pred dalsim spustenim
     * bylo by zahodno sem doplnit i cekani mezi loopy... i kdyz to si spis resi doTask()
     */
    public function sleep()
    {
        if ($this->_sleepTime) {
            sleep($this->_sleepTime);
        }
    }

    /**
     *
     */
    public function destroy()
    {

    }

    /**
     * @param $sig integer
     */
    public function stop($sig)
    {
        $key = array_search($sig, $this->signals);
        $this->stdout("catched signal: $sig = $key" . PHP_EOL);

        $this->_isTerminated = TRUE;
    }

    /**
     * tady probiha vlastni logika aplikace
     */
    public function actionIndex()
    {
        do {
            $this->doTask();

            if ($this->loop === 'once') { // cron? spustit jednou
                break;
            }

            $this->sleep();
        } while (!$this->isTerminated());
    }
}