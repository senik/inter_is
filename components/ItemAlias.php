<?php

namespace app\components;

use yii\base\Component;
use yii\base\Exception;

/**
 * ItemAlias
 *
 * Slouzi k servirovani seznamu ciselniku, prekladu konstatnt trid atd.
 * Vola ji hned nekolik typu trid pocinaje AR pres dalsi komponenty a modely
 * aby pro ne tuto sluzbu vykonala
 *
 */
class ItemAlias extends Component
{

    /**
     * metoda zpracovava obecne vsechny pozadavky na metodu itemAlias()
     * ktera slouzi v modelech na ziskani ciselniku a popisu konstant.
     * metody jsou presmerovany sem a aby byl shodny kod metody jen jednou a to zde,
     * zmena teto metody tedy aktualne ovlivni i chovani itemalias u ECgFormModel a ECgActoveRecord
     * @param $aItems
     * @param $typ
     * @param null $kod
     * @throws Exception
     * @return bool|array
     */
    public static function zpracuj($aItems, $typ, $kod = NULL)
    {
        // na zvazenou je pouzit vyjimku...
        if (!is_array($aItems)) Throw New Exception('Trida potomka musi spravne implementovat statickou metodu itemAliasData() vracejici pole!');

        // pokud je kod pole, cekame ze v nem bude seznam vice pozadovanych klicu
        if (is_array($kod)) {

            $aVysledek = array();

            // nasel jsem typ v poli aliasu
            if (isset($aItems[$typ])) {
                $aTypy = $aItems[$typ];

                // snazim se dotahat pozadovane klice a k nim prelozene hodnoty
                foreach ($kod as $jedenKod) {
                    // pokud klic co chci jeho hodnotu v aliasech je, zaradiim ho
                    if (isset($aTypy[$jedenKod])) {
                        $aVysledek[$jedenKod] = $aTypy[$jedenKod];
                    }
                }
            }
            // vracim pole aliasu daneho typu pro dany seznam kodu v poli
            return $aVysledek;
        }

        // je zadan kod, tedy mam odeslat jednu konkretni polozku
        if (isset($kod)) {
            // pokud je polozka existujici, odeslu jeji nazev
            if (isset($aItems[$typ][$kod])) {
                return $aItems[$typ][$kod];
            // jinak vracim false
            } else {
                return false;
            }
        // neni zadan klic konkretni polozky takze je pozadovan cely seznam
        } else {
            // pokud existuje typ odeslu cely seznam polozek jako pole
            if (isset($aItems[$typ])) {
                return $aItems[$typ];
            } else {
                // jinak vracim false
                return false;
            }
        }
    }
}
