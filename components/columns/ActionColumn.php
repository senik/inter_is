<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 9.1.15
 * Time: 21:49
 */

namespace app\components\columns;

/**
 * Class ActionColumn
 * @package app\components\columns
 */
class ActionColumn extends \yii\grid\ActionColumn
{
    public $contentOptions = ['class' => 'action-column'];
} 