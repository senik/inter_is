<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 9.1.15
 * Time: 21:49
 */

namespace app\components\columns;
use yii\grid\DataColumn;
use Closure;
use app\components\Html;
use app\modules\sklad\models\Material;

/**
 * Class ActionColumn
 * @package app\components\columns
 */
class MaterialColumn extends DataColumn
{
    /**
     * @inheritdoc
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        return Html::tag(
            'div',
            Html::dropDownList('material_' . $model['model_pk'], null, (new Material())->vratMaterialySklademProDropdown()) . Html::tag('span', '', ['class' => 'caret']),
            ['class' => 'select-wrapper']
        );
    }

    /**
     * @inheritdoc
     */
    public function renderDataCell($model, $key, $index)
    {
        if (!($this->contentOptions instanceof Closure)) {
            $this->contentOptions = array_merge($this->contentOptions, array('class' => 'material-column'));
        }
        return parent::renderDataCell($model, $key, $index);
    }
} 