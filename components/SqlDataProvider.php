<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 10.11.14
 * Time: 18:49
 */

namespace app\components;


use yii\db\Connection;

class SqlDataProvider extends \yii\data\SqlDataProvider
{
    /**
     * @return bool|null|string
     */
    protected function prepareTotalCount()
    {
        $yii = \Yii::$app;

        /** @var $connection Connection */
        $connection = (($this->db !== null) ? $this->db : $yii->db);

        $command = $connection->createCommand("SELECT count(*) FROM ({$this->sql}) AS subselect");
        $command->bindValues($this->params);
        $res = $command->queryScalar();

        return (int) $res;
    }
} 