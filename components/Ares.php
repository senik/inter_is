<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 11. 3. 2015
 * Time: 22:38
 */

namespace app\components;


class Ares
{
    public $ic;

    protected $_url = 'http://wwwinfo.mfcr.cz/cgi-bin/ares/darv_rzp.cgi';

    public function __construct($ic)
    {
        $this->ic = $ic;
    }

    /**
     *
     */
    public function nactiPodleIc()
    {
        if (!$this->_validate()) {
            throw new \Exception("[ares] neplatna hodnota pro ico: {$this->ic}!");
        }

        $params = ['ico' => $this->ic];

        $url = $this->_sestavUrl($this->_url, $params);

        $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curl);
        curl_close($curl);

        return $this->_parseResponse($response, $params);
    }

    /**
     * @param $response
     * @param $firma_data
     * @return mixed
     */
    protected function _parseResponse($response, $firma_data)
    {
        try {
            $xml = simplexml_load_string($response);

            if ($xml) {

                $ns = $xml->getDocNamespaces();
                $data = $xml->children($ns['are']);

                if (isset($ns['d'])) {
                    $used_ns = $ns['d'];

                    $oFirme = $data->children($used_ns)->vypis_rzp->zau;
                    $adresy = $data->children($used_ns)->vypis_rzp->adresy;
                    $adresa = $adresy[0]->a;

                    $obchodni_jmeno = (string)$oFirme->of;

                    $mesto = (string)$adresa->n;
                    if (isset($adresa->NCO)) $mesto .= " - " . (string)$adresa->nco;

                    $ulice = (string)$adresa->nu;

                    $cislo_popisne = (string)$adresa->cd;
                    if (isset($adresa->ca)) $cislo_popisne .= "/".(string)$adresa->ca;

                    $psc = (string)$adresa->psc;
                } else if (isset($ns['D'])) {
                    $used_ns = $ns['D'];

                    $oFirme = $data->children($used_ns)->Vypis_RZP->ZAU;
                    $adresy = $data->children($used_ns)->Vypis_RZP->Adresy;
                    $adresa = $adresy[0]->A;

                    $obchodni_jmeno = (string)$oFirme->OF;

                    $mesto = (string)$adresa->N;
                    if (isset($adresa->NCO) && $adresa->NCO != $mesto) $mesto .= " - " . (string)$adresa->NCO;

                    $ulice = (string)$adresa->NU;

                    $cislo_popisne = (string)$adresa->CD;
                    if (isset($adresa->CA)) $cislo_popisne .= "/".(string)$adresa->CA;

                    $psc = (string)$adresa->PSC;
                } else {
                    throw new \Exception("neznamy namespace, data: " . print_r($ns));
                }

                $firma_data['obchodni_jmeno'] = $obchodni_jmeno;
                $firma_data['mesto'] = $mesto;
                $firma_data['ulice'] = "$ulice $cislo_popisne";
                $firma_data['psc'] = $psc;
            } else {
                throw new \Exception("nepodarilo se rozparsovat odpoved z ares: $response");
            }
        } catch (\Exception $e) {
            \Yii::error("[ares] chyba pri parsovani: {$e->getMessage()}");
        }

        // v parametru uz dostavam ico
        return $firma_data;
    }

    /**
     * @param $url
     * @param $parametry
     * @return string
     */
    protected function _sestavUrl($url, $parametry)
    {
        $queryArr = array();
        foreach ($parametry as $param => $value) {
            $queryArr[] = $param . '=' . htmlspecialchars($value);
        }

        $queryString = implode('&', $queryArr);

        return $url . '?' . $queryString;
    }

    /**
     * validace hodnoty ico
     * @return bool
     */
    protected function _validate()
    {
        $ic = str_replace(' ', '', $this->ic);
        $ic = str_pad($ic, 8, '0', STR_PAD_LEFT);

        if ($ic == null) {
            return false;
        }

        if (preg_match('/^\d{8}$/', $this->ic) !== 1) {
            return false;
        }

        $this->ic = $ic;
        return true;
    }
}