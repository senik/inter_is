<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 8.11.14
 * Time: 7:09
 */

namespace app\components;
use yii\web\View;


/**
 * Class Html
 * @package app\components
 */
class Html extends \yii\helpers\Html
{
    /**
     * @param $class
     * @return string
     */
    public static function icon($class)
    {
        return Html::tag(
            'i',
            '',
            ['class' => "glyphicon glyphicon-{$class}"]
        );
    }

    /**
     * @param int $n
     * @param string $elem
     * @return string
     */
    public static function urovenDlePoctu($n = 0, $elem = 'label')
    {
        $level = 'success';

        if ($n > 0 && $n <= 3) {
            $level = 'info';
        } else if ($n > 3 && $n <= 7) {
            $level = 'warning';
        } else if ($n > 7) {
            $level = 'danger';
        }

        return $elem . '-' . $level;
    }

    /**
     * Vykresluje button pro ovladani pridruzenecho collapse pomoci predaneho ID.
     * Resi si i JavaScript a ukladani stavu do cookie.
     * @param string $id
     * @return string
     */
    public static function collapseButton($id)
    {
        $s = isset($_COOKIE[$id]) ? $_COOKIE[$id] : null;

        if ($s == null || $s == 'show') {
            $chevron = 'up';
            $s = 'show';
            $title = 'Sbalit';
        }
        // budu predpokladat, ze tam mam jen null, open, close
        else {
            $s = 'hide';
            $chevron = 'down';
            $title = 'Rozbalit';
        }

        $view = \Yii::$app->getView();
        $view->registerJs("
            Cookies.set('{$id}', '{$s}');

            $('#{$id}').on('hidden.bs.collapse', function () {
                $('button[data-target=\"#{$id}\"]').attr('title', 'Rozbalit');
                $('button[data-target=\"#{$id}\"] i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
                Cookies.set('{$id}', 'hide', { expires: 90 });
                footerMax();
            });

            $('#{$id}').on('shown.bs.collapse', function () {
                $('button[data-target=\"#{$id}]\"').attr('title', 'Sbalit');
                $('button[data-target=\"#{$id}\"] i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
                Cookies.set('{$id}', 'show', { expires: 90 });
                footerMax();
            });

            $('#{$id}').collapse('{$s}');
            footerMax();
        ", View::POS_END);

        return Html::button(
            Html::icon("chevron-$chevron"),
            ['class' => 'btn btn-sm btn-info', 'title' => $title, 'data-toggle' => 'collapse', 'data-target' => "#{$id}"]
        );
    }
}