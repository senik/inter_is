<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 24.10.14
 * Time: 20:09
 */

namespace app\components;
use yii\base\Exception;

/**
 * Class Nastaveni
 * @package app\components
 */
class Nastaveni
{

    const KLIC_KONFIGUROVANO        = 'konfigurovano';
    const KLIC_ADMINI               = 'admini';
    const KLIC_INSTALOVANE_MODULY   = 'instalovaneModuly';

    /**
     * Singleton objekt
     * @var Nastaveni|null
     */
    protected static $_instanceSingleton = null;

    /**
     * @var bool
     */
    protected $_konfigNacten = false;

    /**
     * @var array
     */
    protected $_konfig = array();

    /**
     * @var string
     */
    protected $_cestaKonfig = '../config/settings.json';

    /**
     * konstruktor
     * jsou tu zakomentovane var_dumpy kvuli sledovani casu loadu a parsovani konfiguracniho souboru
     */
    public function __construct() {
//        var_dump(time());
        if (false == $this->_konfigNacten) {
            $this->_konfig = $this->zpracujKonfig();
        }
//        var_dump(time()); die;
    }

    /**
     * vrati cely konfiguracni soubor
     * @return array
     */
    public function vratKonfig() {
        return $this->_konfig;
    }

    /**
     * vrati rucne nastavene adminy z konfigu
     * @return array
     * @throws \yii\base\Exception
     */
    public function vratAdminy() {
        if (isset($this->_konfig['admini'])) {
            return $this->_konfig['admini'];
        } else {
            throw new Exception("Polozka 'admini' neni pritomna v konfigu!");
        }
    }

    /**
     * vrati instalovane moduly
     * @return array
     * @throws \yii\base\Exception
     */
    public function vratInstalovaneModuly() {
        if (isset($this->_konfig['instalovaneModuly'])) {
            return $this->_konfig['instalovaneModuly'];
        } else {
            throw new Exception("Polozka 'instalovaneModuly' neni pritomna v konfigu!");
        }
    }

    /**
     * @throws \yii\base\Exception
     * @return array
     */
    protected function zpracujKonfig() {
        $cesta = realpath($this->_cestaKonfig);

        if (!file_exists($this->_cestaKonfig)) {
            throw new Exception("Nenalezen konfiguracni soubor: {$cesta}!");
        }

        if ($konfig = fopen($this->_cestaKonfig, 'r')) {
            $content = fread($konfig, filesize($this->_cestaKonfig));

            if ($content === false) {
                throw new Exception("Nepodarilo se precist konfiguracni soubor: {$cesta}!");
            }

            fclose($konfig);

            if (($decoded = json_decode($content, true)) !== null ) {
                $this->_konfigNacten = true;
                return $decoded;
            } else {
                throw new Exception("Nepodarilo se dekodovat konfig: " . print_r($content, true));
            }
        }

        throw new Exception("Neco se pokazilo pri loadu konfigu: {$cesta}!");
    }

    /**
     * zapise data do konfiguracniho souboru
     * @param $data array
     * @throws \yii\base\Exception
     * @return mixed
     */
    public function zapisKonfigDoSouboru($data) {
        $cesta = realpath($this->_cestaKonfig);

        if ($konfig = fopen($this->_cestaKonfig, 'w+')) {
            $zapsano = 0;
            if ($encoded = json_encode($data)) {
                $zapsano = fwrite($konfig, $encoded);
            }

            fclose($konfig);

            if (0 == $zapsano || false === $zapsano) {
                throw new Exception("Nepodarilo se zapsat data do konfigu: " . print_r($data, true));
            } else {
                return $zapsano;
            }
        }

        throw new Exception("Neco se pokazilo pri vytvareni konfigu: {$cesta}, data: " . print_r($data, true));
    }

    /**
     * @throws \yii\base\Exception
     * @return bool
     */
    public function konfigurovat()
    {
        if (!isset($this->_konfig['konfigurovano'])) {
            throw new Exception("Polozka 'konfigurovano' neni pritomna v konfigu!");
        }
        else if (isset($this->_konfig['konfigurovano']) && $this->_konfig['konfigurovano'] == '') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return Nastaveni|null
     */
    public static function instance() {
        if (null == self::$_instanceSingleton) {
            self::$_instanceSingleton = new self;
        }

        return self::$_instanceSingleton;
    }

} 