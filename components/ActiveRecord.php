<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 17.10.14
 * Time: 15:07
 */

namespace app\components;


class ActiveRecord extends \yii\db\ActiveRecord
{

    /**
     * metoda vraci preklad hodnoty konstanty ci seznam konstatnt a jejich svazanych nazvu,
     * vice viz ECgComponent::itemAliasData();
     * @param $typ
     * @param null $kod
     * @return bool|array
     */
    public static function itemAlias($typ, $kod = NULL)
    {
        // static zajisti ze oddedena trida bude volat tuto metodu sama nad sebou
        $aItems = static::itemAliasData();
        return ItemAlias::zpracuj($aItems, $typ, $kod);

    }

    /**
     * funkce k přetížení v jejích potomcích, slouží jako seznam všech potřebných seznamů konstant atd.
     * ukazujících na jejich přeloženou reprezentaci
     * pro danou třídu a též jejího datového podkladu - tabulek
     * příklad:
     *
     * $aItems = array(
     *     'stav' => array(
     *         self::STAV_CEKA_UZAMCENO => IHera::t('hera', 'STAV CEKA UZAMCENO'),
     *         self::STAV_SCHVALENO => IHera::t('hera', 'STAV SCHVALENO'),
     *         self::STAV_ZAMITNUTO => IHera::t('hera', 'STAV ZAMITNUTO'),
     *         self::STAV_KE_ZRUSENI => IHera::t('hera', 'STAV KE ZRUSENI'),
     *         self::STAV_ZRUSENO => IHera::t('hera', 'STAV ZRUSENO'),
     *     ),
     *     'zdroj' => array(
     *         self::ZDROJ_POBOX => 'P.O.Box',
     *         self::ZDROJ_WEB => 'Web'
     *     )
     * );
     * return $aItems
     *
     * @return array
     */
    protected static function itemAliasData()
    {
        return array();
    }
} 