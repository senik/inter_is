<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 17.12.14
 * Time: 19:44
 */

namespace app\components;


use app\modules\objednavky\ObjednavkyModule;
use app\modules\uzivatel\models\Uzivatel;
use Yii;

/**
 * Class MenuWidget
 * @package app\components
 *
 * Pomocná třída pro vykreslování jednotlivých menu
 */
class MenuWidget
{
    /**
     * Pripravi podklady pro vykresleni menu
     * @return array
     */
    protected static function prepareMobileMenu()
    {
        $menu = [
            [
                'url'       => Yii::$app->getUrlManager()->createAbsoluteUrl(array('/uzivatel/default/muj-profil')),
                'image'     => Yii::$app->request->baseUrl . DS . 'images' . DS . 'module-icons' . DS . 'small' . DS . 'muj-ucet.png',
                'label'     => Yii::$app->user->identity->login,
                'visible'   => true,
                'active'    => false
            ],
            [
                'url'       => Yii::$app->getUrlManager()->createAbsoluteUrl(array('/dashboard')),
                'image'     => Yii::$app->request->baseUrl . DS . 'images' . DS . 'module-icons' . DS . 'small' . DS . 'dashboard.png',
                'label'     => "Dashboard",
                'visible'   => true,
                'active'    => false
            ],
        ];

        $instalovane = Nastaveni::instance()->vratInstalovaneModuly();
        foreach ($instalovane as $module) {
            $moduleClass = Yii::$app->getModule($module);

            $menu[] = [
                'url'       => Yii::$app->getUrlManager()->createAbsoluteUrl($moduleClass->baseUrl),
                'image'     => Yii::$app->request->baseUrl . DS . 'images' . DS . 'module-icons' . DS . 'small' . DS . $module . '.png',
                'label'     => $moduleClass->name,
                'visible'   => true,
                'active'    => false
            ];

            if ($moduleClass instanceof ObjednavkyModule) {
                $menu[] = [
                    'url'       => Yii::$app->getUrlManager()->createAbsoluteUrl(['/objednavky/zakaznici/index']),
                    'image'     => Yii::$app->request->baseUrl . DS . 'images' . DS . 'module-icons' . DS . 'small' . DS . 'zakaznici' . '.png',
                    'label'     => 'Zákazníci',
                    'visible'   => true,
                    'active'    => false
                ];
            }
        }

        $rightMenu = [
            [
                'url'       => Yii::$app->getUrlManager()->createAbsoluteUrl(array('/site/nastaveni')),
                'image'     => Yii::$app->request->baseUrl . DS . 'images' . DS . 'module-icons' . DS . 'small' . DS . 'nastaveni.png',
                'label'     => "Nastavení",
                'visible'   => Yii::$app->user->identity->isAdmin,
                'active'    => false
            ],
            [
                'url'       => Yii::$app->getUrlManager()->createAbsoluteUrl(array('/uzivatel/default/odhlaseni')),
                'image'     => Yii::$app->request->baseUrl . DS . 'images' . DS . 'module-icons' . DS . 'small' . DS . 'odhlasit.png',
                'label'     => "Odhlásit se",
                'visible'   => true,
                'active'    => false
            ],
        ];

        return array_merge($menu, $rightMenu);
    }

    /**
     * @return string
     */
    public static function renderLeftMobileMenu()
    {
        if (Yii::$app->user->isGuest) {
            return null;
        }

        $setUp = self::prepareMobileMenu();

        $menu = '<div id="left-mobile-menu">';
        foreach ($setUp as $polozka) {
            if (!$polozka['visible']) {
                continue;
            }

            $menu .= Html::beginTag('a', array(
                'href' => $polozka['url'],
            ));
            {
                $menu .= Html::img(
                    $polozka['image'],
                    array()
                );

                $menu .= $polozka['label'];
            }
            $menu .= Html::endTag('a');
        }
        $menu .= '</div>';
        $menu .= '<div class="clearfix"></div>';

        return $menu;
    }

    /**
     * @return string
     */
    public static function renderRightMobileMenu()
    {
        if (Yii::$app->user->isGuest) {
            return null;
        }

        $menu = '<div id="right-mobile-menu">';
        {
            if (Yii::$app->user->identity->isAdmin) {
                $menu .= Html::beginTag('a', array(
                    'href' => Yii::$app->getUrlManager()->createAbsoluteUrl(array('/site/nastaveni')),
                ));
                {
                    $menu .= Html::img(
                        Yii::$app->request->baseUrl . DS . 'images' . DS . 'module-icons' . DS . 'small' . DS . 'nastaveni.png',
                        array(
                            "style" => "width: 32px; height: 32px;"
                        )
                    );

                    $menu .= "Nastavení";
                }
                $menu .= Html::endTag('a');
            }

            $menu .= Html::beginTag('a', array(
                'href' => Yii::$app->getUrlManager()->createAbsoluteUrl(array('/uzivatel/default/odhlaseni')),
            ));
            {
                $menu .= Html::img(
                    Yii::$app->request->baseUrl . DS . 'images' . DS . 'module-icons' . DS . 'small' . DS . 'odhlasit.png',
                    array(
                        "style" => "width: 32px; height: 32px;"
                    )
                );

                $menu .= "Odhlásit se";
            }
            $menu .= Html::endTag('a');
        }
        $menu .= '</div>';

        return $menu;
    }

    /**
     * TODO REFAKTOR JEN TO BUDE ČAS!!!
     * @return string
     */
    public static function renderModuleMenu()
    {
        if (Yii::$app->user->isGuest) {
            return null;
        }

        $menu = '<div class="module-navbar">';

        $activeClassCss = self::jePolozkaAktivni(['/site/index']) ? 'active' : '';
        $menu .= Html::beginTag('a', array(
            'href' => Yii::$app->getUrlManager()->createAbsoluteUrl(array('/dashboard')),
            'class' => "{$activeClassCss}",
            'data-module-name' => "Dashboard"
        ));
        {
            $menu .= Html::img(
                Yii::$app->request->baseUrl . DS . 'images' . DS . 'module-icons' . DS . 'small' . DS . 'dashboard.png',
                array()
            );
        }
        $menu .= Html::endTag('a');

        $instalovane = Nastaveni::instance()->vratInstalovaneModuly();
        foreach ($instalovane as $module) {
            if ($module == 'uzivatel' && Yii::$app->user->identity->getRole() == Uzivatel::ROLE_USER) {
                continue;
            }

            $moduleClass = Yii::$app->getModule($module);
            if ($moduleClass instanceof ObjednavkyModule) {
                $objednavkyActiveClass = '';
                $zakazniciActiveClass = '';
                if (strpos(Yii::$app->request->getPathInfo(), 'zakaznici') !== false) {
                    $zakazniciActiveClass = 'active';
                } elseif (strpos(Yii::$app->request->getPathInfo(), 'objednavky') !== false) {
                    $objednavkyActiveClass = 'active';
                }

                $menu .= Html::beginTag('a', array(
                    'href' => Yii::$app->getUrlManager()->createAbsoluteUrl(['/objednavky/zakaznici/index']),
                    'class' => "{$zakazniciActiveClass}",
                    'data-module-name' => 'Zákazníci'
                ));
                {
                    $menu .= Html::img(
                        Yii::$app->request->baseUrl . DS . 'images' . DS . 'module-icons' . DS . 'small' . DS . 'zakaznici' . '.png',
                        array()
                    );
                }
                $menu .= Html::endTag('a');

                $menu .= Html::beginTag('a', array(
                    'href' => Yii::$app->getUrlManager()->createAbsoluteUrl($moduleClass->baseUrl),
                    'class' => "{$objednavkyActiveClass}",
                    'data-module-name' => $moduleClass->name
                ));
                {
                    $menu .= Html::img(
                        Yii::$app->request->baseUrl . DS . 'images' . DS . 'module-icons' . DS . 'small' . DS . 'objednavky' . '.png',
                        array()
                    );
                }
                $menu .= Html::endTag('a');
            } else {
                $activeClassCss = (self::jePolozkaAktivni($module)) ? 'active' : '';
                $menu .= Html::beginTag('a', array(
                    'href' => Yii::$app->getUrlManager()->createAbsoluteUrl($moduleClass->baseUrl),
                    'class' => "{$activeClassCss}",
                    'data-module-name' => $moduleClass->name
                ));
                {
                    $menu .= Html::img(
                        Yii::$app->request->baseUrl . DS . 'images' . DS . 'module-icons' . DS . 'small' . DS . $module . '.png',
                        array()
                    );
                }
                $menu .= Html::endTag('a');
            }
        }

        $menu .= '</div>';

        return $menu;
    }

    /**
     * @return string
     */
    public static function renderSystemMenu()
    {
        if (Yii::$app->user->isGuest) {
            return null;
        }

        $menu = '<div class="system-navbar">';
        {
            if (Yii::$app->user->identity->isAdmin) {
                $menu .= Html::beginTag('a', array(
                    'href' => Yii::$app->getUrlManager()->createAbsoluteUrl(array('/site/nastaveni')),
                ));
                {
                    $menu .= Html::img(
                        Yii::$app->request->baseUrl . DS . 'images' . DS . 'module-icons' . DS . 'small' . DS . 'nastaveni.png',
                        array()
                    );
                }
                $menu .= Html::endTag('a');
            }

            $menu .= Html::beginTag('a', array(
                'href' => Yii::$app->getUrlManager()->createAbsoluteUrl(array('/uzivatel/default/odhlaseni')),
            ));
            {
                $menu .= Html::img(
                    Yii::$app->request->baseUrl . DS . 'images' . DS . 'module-icons' . DS . 'small' . DS . 'odhlasit.png',
                    array()
                );
            }
            $menu .= Html::endTag('a');
        }
        $menu .= '</div>';

        return $menu;
    }

    /**
     * @return string
     */
    public static function renderMobileNavbar()
    {
        if (Yii::$app->user->isGuest) {
            return null;
        }

        $menu = <<<HTML
<div class="mobile-navbar">
    <button type="button" class="navbar-toggle left" data-target="#left-mobile-menu"> <!-- data-toggle="collapse"  -->
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>

    <div class="clearfix"></div>
</div>
HTML;
        return $menu;
    }

    /**
     * @param array $routes
     * @return bool
     */
    public static function jePolozkaAktivni($routes = array())
    {
        if (is_array($routes)) {
            $path = '';
            $app = Yii::$app;

            if (!is_array($routes))
                $routes = array($routes);

            if ($app->controller->module != null && $app->controller->module->id != 'basic')
                $path .= '/' . $app->controller->module->id;

            if ($app->controller != null)
                $path .= '/' . $app->controller->id;

            if ($app->controller->action != null)
                $path .= '/' . $app->controller->action->id;

            return in_array($path, $routes);
        } else {
            $path = Yii::$app->request->getPathInfo();
            $parts = explode('/', $path);

            foreach ($parts as $part) {
                if ($part == $routes) return true;
            }
        }

        return false;
    }
} 