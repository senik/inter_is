<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 25.10.14
 * Time: 11:08
 */

namespace app\components;


use app\modules\uzivatel\components\UzivatelIdentity;
use yii\web\User;

/**
 * Class WebUser
 * @package app\components
 */
class WebUser extends User
{

    /** @var UzivatelIdentity */
    private $_identity;

    /**
     * @var bool
     */
    public $isAdmin;

    /**
     *
     */
    public function init() {
        $this->isAdmin = $this->getIsAdmin();

        parent::init();
    }

    /**
     * @return bool
     */
    public function getIsAdmin()
    {
        if (!is_null($this->_identity)) {
            return $this->_identity->getIsAdmin();
        } else {
            return false;
        }
    }

    /**
     * @param  $value bool
     * @return void
     */
    public function setIsAdmin($value)
    {
        $this->_identity->setIsAdmin($value);
    }

} 