<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 26.12.14
 * Time: 13:48
 */

namespace app\components;


use Yii;

Class Log
{
    protected $category;

    public function __construct($class = __CLASS__) {
        $this->category = $class;
    }

    public function info($message)
    {
        Yii::info($message, $this->category);
    }
} 