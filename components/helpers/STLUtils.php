<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 21.11.14
 * Time: 18:35
 */

namespace app\components\helpers;


use yii\base\Exception;

/**
 * Class STLUtils
 *
 * inspired with:
 * STL-Volume-Model-Calculator by mcanet
 * http://github.com/mcanet/STL-Volume-Model-Calculator/blob/master/mesure_volume.py
 *
 * PHP + STL Files by MD Schmidt
 * http://schmidtcds.com/2011/07/php-stl-files/
 *
 * @package app\components\helpers
 * @property float weight
 * @property float volume
 */
class STLUtils
{
    /**
     * @var string
     */
    protected $_file;

    /**
     * @var float
     */
    protected $_density;

    /**
     * @var array
     */
    protected $_triangles = array();

    /**
     * @var null
     */
    protected $_volume = null;

    /**
     * @var STLUtils singleton
     */
    protected static $_instance;

    /**
     * @param string $file path to stl file
     * @param float $density material desity
     * @throws \yii\base\Exception
     */
    public function __construct($file, $density = 1.04)
    {
        if (!file_exists($file)) {
            throw new Exception("File not exists: $file!");
        } elseif (!is_readable($file)) {
            throw new Exception("File is not readable: $file!");
        } else {
            $this->_file = $file;
            $this->_density = $density;

            $this->_load();
        }
    }

    /**
     * @param $file
     * @param float $density
     * @return STLUtils
     */
    public static function getInstance($file, $density = 1.04)
    {
        if (null == self::$_instance) {
            self::$_instance = new self($file, $density);
        }

        return self::$_instance;
    }

    /**
     *
     */
    protected function _load()
    {
        if ($this->_file == null) {
            throw new Exception("No file to load!");
        }

        try {
            $fp = fopen($this->_file, "rb");
            $header = file_get_contents($this->_file, null, null, 0, 79);

            fseek($fp, 80);
            $data = fread($fp, 4);
            $total = unpack("I", $data)[1];

            for ($i = 0; $i < $total; $i++) {
                $this->_triangles[] = array(
                    'N'  => unpack("f3", fread($fp, 12)),
                    'V1' => unpack("f3", fread($fp, 12)),
                    'V2' => unpack("f3", fread($fp, 12)),
                    'V3' => unpack("f3", fread($fp, 12)),
                    'B'  => unpack("S",  fread($fp, 2))[1]
                );
            }
        } catch (\Exception $e) {
            throw new STLException($e->getMessage());
        }
    }

    /**
     * @return null
     */
    protected function _calculateVolume()
    {
        $volume = null;
        foreach ($this->_triangles as $triangle) {
            $volume += $this->_volumeOfTriangle($triangle['V1'], $triangle['V2'], $triangle['V3']);
        }

        return $volume;
    }

    /**
     * Calculates volume of tetrahedron
     *
     * @param $v1
     * @param $v2
     * @param $v3
     * @return float
     */
    protected function _volumeOfTriangle($v1, $v2, $v3)
    {
        $v321 = $v3[1] * $v2[2] * $v1[3];
        $v231 = $v2[1] * $v3[2] * $v1[3];
        $v312 = $v3[1] * $v1[2] * $v2[3];
        $v132 = $v1[1] * $v3[2] * $v2[3];
        $v213 = $v2[1] * $v1[2] * $v3[3];
        $v123 = $v1[1] * $v2[2] * $v3[3];

        return (1.0 / 6.0) * (-$v321 + $v231 + $v312 - $v132 - $v213 + $v123);
    }

    /**
     * Get total volume of model
     * @return null
     */
    public function getVolume()
    {
        if ($this->_volume == null) {
            $this->_volume = $this->_calculateVolume() / 1000;
        }

        return $this->_volume;
    }

    /**
     * Get total weight of object
     * @return float
     */
    public function getWeight()
    {
        return $this->_volume * $this->_density;
    }

    /**
     * @param $name
     * @return float|null
     * @throws \yii\base\Exception
     */
    public function __get($name)
    {
        switch ($name) {
            case 'volume':
                return $this->getVolume();
                break;
            case 'weight':
                return $this->getWeight();
                break;
            default:
                throw new Exception("Unknown property: $name");
        }
    }
}

/**
 * Class STLException
 * @package app\components\helpers
 */
class STLException extends Exception {}