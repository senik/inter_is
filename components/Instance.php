<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 26.06.2017
 * Time: 19:40
 */

namespace app\components;


/**
 * Class Instance
 * @package app\components
 */
class Instance {

    /**
     * @return array
     */
    protected static function vratProdukcniAdresy()
    {
        return ['is.voplasto.cz'];
    }

    /**
     * @return string
     */
    protected static function vratProdukcniCestu()
    {
        return '/opt/senovo/voplasto';
    }

    /**
     * @return bool
     */
    public static function jeConsole()
    {
        return (PHP_SAPI === 'cli');
    }

    /**
     * @return bool
     */
    public static function jeProdukcni()
    {
        if (PHP_SAPI === 'cli') {
            return strpos(__FILE__, self::vratProdukcniCestu()) !== false;
        } else {
            return in_array(
                $_SERVER['HTTP_HOST'],
                self::vratProdukcniAdresy()
            );
        }
    }

    /**
     * todo detekce, zda se jedna o produkcni prostredi
     * @return mixed
     */
    public static function vratKonfig()
    {
        if (PHP_SAPI === 'cli') {
            $config = require(__DIR__ . '/../config/console.php');
        } else {
            $config = require(__DIR__ . '/../config/web.php');
        }

        return $config;
    }
}