<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 17.10.14
 * Time: 14:25
 */

namespace app\components;

use yii\base\Action;
use yii\filters\AccessControl;
use yii\web\HttpException;

/**
 * Class Controller
 * přetížení běžného controlleru kvůli vlastní funkcionalitě
 *
 * @package app\components
 */
class Controller extends \yii\web\Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Ještě, než zpracuji action, tak ověřím, že je modul nainstalovaný
     *
     * @param Action $action
     * @throws \yii\web\HttpException
     * @return bool|\yii\web\Response
     */
    public function beforeAction(Action $action)
    {
        $nastaveni = Nastaveni::instance();
        if ($nastaveni->konfigurovat()) {
            if ($this->module->id == 'basic' && $action->id == 'nastaveni') {
                return parent::beforeAction($action);
            } else {
                Application::setFlashWarning('Nastavte první konfiguraci!');
                return $this->redirect(array('/site/nastaveni'));
            }
        } else {
            // povolene moduly v konfiguraci
            $instalovane = $nastaveni->vratInstalovaneModuly();
            // nutne moduly pro provoz aplikace
            $defaultni = array('basic', 'gii', 'debug', 'gridview');

            if (!in_array('uzivatel', $instalovane)) {
                throw new HttpException(500, 'Není nainstalovaný uživatelský modul. Tento modul je nutný pro běh aplikace!');
            }

            // pokud neni nainstalovany, tak vyhodim hlasku a presmeruju na dashboard
            if (!in_array($this->module->id, array_merge($instalovane, $defaultni))) {
                Application::setFlashError('Tento modul nemáte nainstalovaný, chcete-li jej používat, kontaktujte support.');
                return $this->goHome();
            }
            // jinak normálně pokračuji
            else {
                return parent::beforeAction($action);
            }
        }
    }
}